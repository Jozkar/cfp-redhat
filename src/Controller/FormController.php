<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Datasource\ConnectionManager;
/**
 * Static content controller
 *
 * This controller will render views from Template/HomePage/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class FormController extends AppController
{
    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Network\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function show(...$action)
    {

        if(!parent::getAdmin() && !parent::getSuperUser()){
            return $this->redirect("/");
        }

        $connection = ConnectionManager::get('cfp');
        $supported = array("session","workshop","meetup","booth","confirmation", "collect", "quiz", "contest", "lightning talk", "activity", "covered accommodation", "accommodation");

        if(sizeof($action) > 0 && ($action[0] == "add" || $action[0] == "edit" || $action[0] == "delete" || $action[0] == "update" || $action[0] == "order")){
            // /form/add/type/eventID/token/multiple/speakerLimit/cfpClose
            if($action[0] == "add"){
                $type = $_POST["type"];
                $eventID = $_POST["eventID"];
                $token = $_POST["token"];
                $oneResp = $_POST["multiple"];
                $sched = $_POST["schedule"];
                $additionals = $_POST["additionals"];
                $close = $_POST["close"];
                $placeholder = $_POST["placeholder"];
                $color = $_POST["color"];

                if(in_array($type, $supported) && is_numeric($eventID) && is_numeric($additionals)){
                    if($token == $_SESSION['token']){
                        if(parent::getAdminForEvent($eventID)){
                            $check = $connection->execute('SELECT 1 FROM cfp.forms WHERE event_id = ' . $eventID . ' and type like ("' . $type . '")')->rowCount();
                            if($check == 0){
                                $close = ($close!=""?"'". $close."'":"NULL");
                                $color = (isset($color)?$color:"#FFFFFF");
                                $placeholder = (isset($placeholder)?"'". $placeholder."'":"NULL");
                                $connection->execute("INSERT INTO cfp.forms (event_id, type, one_response, schedule, speaker_limit, cfp_close, placeholder, color, text_color) VALUES (" . $eventID . ", '" . $type . "', ".
                                        ($oneResp=="true"?"1":"0") . ", " . ($sched=="true"?"1":"0") . ", " . $additionals .", " . $close . ", " . $placeholder . ", '" . $color . "', '" . $this->getTextColor($color) . "')");
                                $id = $connection->execute('SELECT id FROM cfp.forms WHERE event_id = ' . $eventID . ' and type like ("' . $type . '")')->fetch('assoc');
                                $connection->execute("UPDATE cfp.forms SET form_order = " . $id['id'] . " WHERE id = " . $id['id']);
                                if($type != 'confirmation' && $type != 'collect' && $type != 'covered accommodation' && $type != 'accommodation'){
                                    $connection->execute("INSERT INTO cfp.emails (event_id, type, form_type) VALUES (" . $eventID . ", 'accept', '" . $type . "')," .
                                        "(" . $eventID . ", 'reject', '" . $type . "'), (" . $eventID . ", 'waitList', '" . $type . "'), (" . $eventID . ", 'all', '" . $type . "'), " .
                                        "(" . $eventID . ", 'unconfirmedSpeakers', '" . $type . "'), (" . $eventID . ", 'to-accept', '" . $type . "'), (" . $eventID . ", 'to-reject', '" . $type . "')");

                                    if($type == 'quiz'){
                                        $connection->execute("INSERT INTO cfp.emails (event_id, type, form_type) VALUES (" . $eventID . ", 'confirmation', '" . $type . "')");
                                    }
                                }
                                if($type == 'covered accommodation'){
                                	$connection->execute("INSERT INTO cfp.emails (event_id, type, form_type) VALUES (" . $eventID . ", 'to-hotel-paid', '" . $type . "'), (" . $eventID . ", 'hotel-paid', '" . $type . "')");
                                }
                                if($type == 'accommodation'){
                                	$connection->execute("INSERT INTO cfp.emails (event_id, type, form_type) VALUES (" . $eventID . ", 'to-hotel-unpaid', '" . $type . "'), (" . $eventID . ", 'hotel-unpaid', '" . $type . "')");
                                }
                                die(json_encode(array("result"=>"success", "action"=>"add", "href"=>"/form/edit/" . $id['id'])));    
                            } else {
                                die(json_encode(array("result"=>"error", "reason"=>"This type of form is already present. You can't create this one.")));
                            }
                        } else {
                            die(json_encode(array("result"=>"error", "reason"=>"Your are not allowed to do this opperation.")));
                        }
                    } else {
                        die(json_encode(array("result"=>"error", "reason"=>"Your session expired. Please, reload this page and try it again.")));
                    }
                } else {
                    $_SESSION['errorMessage'][] = "Unsufficient data. Your link is probably broken.";
                    return $this->redirect("/admin");
                }
                        
            }

            // /form/update/type/eventID/token/multiple/speakerLimit/cfpClose
            if($action[0] == "update"){
                $type = $_POST["type"];
                $eventID = $_POST["eventID"];
                $token = $_POST["token"];
                $oneResp = $_POST["multiple"];
                $sched = $_POST["schedule"];
                $additionals = $_POST["additionals"];
                $close = $_POST["close"];
                $placeholder = $_POST["placeholder"];
                $color = $_POST["color"];

                if(in_array($type, $supported) && is_numeric($eventID) && is_numeric($additionals)){
                    if(parent::getAdminForEvent($eventID)){
                        if($token == $_SESSION['token']){
                            $check = $connection->execute('SELECT 1 FROM cfp.forms WHERE event_id = ' . $eventID . ' and type like ("' . $type . '")')->rowCount();
                            if($check == 0){
                                die(json_encode(array("result"=>"error", "reason"=>"This type of form doesn't exist. You can't update it.")));
                            } else {
                                $close = ($close != ''?"'". $close."'":"NULL");
                                $color = (isset($color)?$color:"#FFFFFF");
                                $placeholder = (isset($placeholder)?"'". $placeholder."'":"NULL");
                                $connection->execute("UPDATE cfp.forms SET one_response = " . ($oneResp=="true"?"1":"0") . ", schedule = " . ($sched=="true"?"1":"0")
                                    . ", speaker_limit = " . $additionals .", cfp_close = " . $close . ", placeholder = " . $placeholder . ", color = '" . $color . "', text_color = '" . $this->getTextColor($color) . "' WHERE "
                                    . "event_id = " . $eventID . " and type like ('" . $type . "')");
                                die(json_encode(array("result"=>"success", "action"=>"update")));
                            }
                        } else {
                            die(json_encode(array("result"=>"error", "reason"=>"Your session expired. Please, reload this page and try it again.")));
                        }
                    } else {
                        die(json_encode(array("result"=>"error", "reason"=>"You are not allowed to do this opperation.")));
                    }
                } else {
                    die(json_encode(array("result"=>"error", "reason"=>"Unsufficient data. Your link is probably broken.")));
                }
            }

            // /form/edit/formId
            if($action[0] == "edit"){
                if(sizeof($action) >= 2){
                    if(is_numeric($action[1])){
                        $event = $connection->execute("SELECT e.id, e.name, e.year, f.type FROM cfp.events as e, cfp.forms as f WHERE e.id = f.event_id and f.id = " . $action[1])->fetch("assoc");
                    } else {
                        $_SESSION['errorMessage'][] = "Unsufficient data. Your link is probably broken.";
                        return $this->redirect("/admin");
                    }
                } else {
                    $_SESSION['errorMessage'][] = "Unsufficient data. Your link is probably broken.";
                    return $this->redirect("/admin");
                }        
            }

            // /form/delete/formId/token
            if($action[0] == "delete"){
                if(sizeof($action) >= 3){
                    if(is_numeric($action[1])){
                        $form = $connection->execute("SELECT * FROM cfp.forms WHERE id = " . $action[1])->fetch("assoc");
                        if($action[2] == $_SESSION['token']){
                            if(parent::getAdminForEvent($form['event_id'])){
                                $connection->execute("DELETE FROM cfp.forms WHERE id = " . $action[1]);
                                $connection->execute("DELETE FROM cfp.questions WHERE form_id = " . $action[1]);
                                $connection->execute("DELETE FROM cfp.question_options WHERE form_id = " . $action[1]);
                                $connection->execute("DELETE FROM cfp.answer_parts WHERE answer_id IN (SELECT id FROM answers WHERE form_id = " . $action[1] . ")");
                                $connection->execute("DELETE FROM cfp.comments WHERE answer_id IN (SELECT id FROM answers WHERE form_id = " . $action[1] . ")");
                                $connection->execute("DELETE FROM cfp.schedule WHERE answer_id IN (SELECT id FROM answers WHERE form_id = " . $action[1] . ")");
                                $connection->execute("DELETE FROM cfp.answers WHERE form_id = " . $action[1]);
                                $connection->execute("DELETE FROM cfp.emails WHERE event_id = " . $form['event_id'] . " AND form_type like ('" . $form['type'] . "')");
                                $_SESSION['successMessage'][] = "Form has been successfully removed.";
                                return $this->redirect("/forms/".$form['event_id']);
                            } else {
                                $_SESSION['errorMessage'][] = "You are not allowed to do this opperation.";
                                return $this->redirect("/admin");
                            }
                        } else {
                            $_SESSION['errorMessage'][] = "Your session expired. Please, try it again.";
                            return $this->redirect("/forms/".$form['event_id']);
                        }
                    } else {
                        $_SESSION['errorMessage'][] = "Unsufficient data. Your link is probably broken.";
                        return $this->redirect("/admin");
                    }
                } else {
                    $_SESSION['errorMessage'][] = "Unsufficient data. Your link is probably broken.";
                    return $this->redirect("/admin");
                }
            }     
            
            if($action[0] == "order"){
                $eventID = $_POST["eventID"];
                $token = $_POST["token"];
                $order = $_POST["order"];
                //  die(var_dump($type, $eventID, $token, $oneResp, $additionals, $close, $placeholder));
                if(is_numeric($eventID) && is_array($order)){
                    if(parent::getAdminForEvent($eventID)){
                        if($token == $_SESSION['token']){
                                try{
                                	foreach ($order as $i=>$q){
                                		$connection->execute("UPDATE forms SET form_order = " . $i . " WHERE id = " . $q . " AND event_id = " . $eventID);
                                	}
                                }catch(\Exception $e){
                                	die(json_encode(array("result"=>"error", "reason"=>$e->getMessage())));
                                }
                                die(json_encode(array("result"=>"success", "action"=>"order")));                            
                        } else {
                            die(json_encode(array("result"=>"error", "reason"=>"Your session expired. Please, reload this page and try it again.")));
                        }
                    } else {
                        die(json_encode(array("result"=>"error", "reason"=>"You are not allowed to do this opperation.")));
                    }
                } else {
                    die(json_encode(array("result"=>"error", "reason"=>"Unsufficient data. Your link is probably broken.")));
                }
            }   
        }else{
            $_SESSION['errorMessage'][] = "Unsufficient data. Your link is probably broken.";
            return $this->redirect("/admin");
        }
    
        $cols = array(
            array("value"=>"abstract","column"=>"Abstract (up to 65 535 characters)"),
            array("value"=>"difficulty","column"=>"Difficulty (up to 100 characters)"),
            array("value"=>"duration","column"=>"Duration (up to 150 characters)"),
            array("value"=>"keywords","column"=>"Keywords (up to 65 535 characters)"),
            array("value"=>"summary","column"=>"Summary (up to 65 535 characters)"),
            array("value"=>"title","column"=>"Title (up to 512 characters)"),
            array("value"=>"type","column"=>"Type (up to 30 characters)"),
            array("value"=>"self","column"=>"Create auto-generated storage")
        );

        if(!parent::getAdminForEvent($event['id'])){
            $_SESSION['errorMessage'][] = "You are not allowed to do this opperation.";
            return $this->redirect("/admin");
        }

        $this->set("active", "form");
        $this->set("admin", parent::getAdmin() | parent::getSuperUser());
        $this->set("reviewer", parent::getReviewer());
        $this->set("program_manager", parent::getProgramManager());
        $results = $connection->execute('SELECT * FROM cfp.users WHERE id like ("' . $_SESSION['loggedUser'] . '")')->fetchAll('assoc');
        $questions = $connection->execute('SELECT * FROM cfp.questions WHERE form_id = ' . $action[1] . ' ORDER BY question_order, id')->fetchAll('assoc');
        $options = $connection->execute('SELECT * FROM cfp.question_options WHERE form_id = ' . $action[1] . ' ORDER BY question_id, id')->fetchAll('assoc');
        $token = $this->request->getParam('_csrfToken');

        $_SESSION['token'] = $token;
        $this->set("token", $token);
        $this->set('username', $_SESSION['first-name']." ".$_SESSION['last-name']);
        $this->set('eventInfo', $event);
        $this->set('formType', $event['type']);
        $this->set('formId', $action[1]);
        $this->set('questions', $questions);
        $this->set('options', $options);
        $this->set('columns', $cols);

        try {
            $this->render('home');
        } catch (MissingTemplateException $exception) {
            if (Configure::read('debug')) {
                throw $exception;
            }
            throw new NotFoundException();
        }
    }

    public function getTextColor($color){
        list($r,$g,$b) = sscanf($color, "#%02x%02x%02x");

        if((($r*0.299) + ($g*0.587) + ($b*0.114)) > 186){
            return "#333333";
        } else {
            return "#ffffff";
        }
    }
}
