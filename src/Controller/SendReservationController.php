<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Datasource\ConnectionManager;
use Cake\Mailer\Email;

/**
 * Static content controller
 *
 * This controller will render views from Template/HomePage/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class SendReservationController extends AppController
{

    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Network\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function show(...$path)
    {
        if(empty($path) || count($path) < 1 || !is_numeric($path[0])){
            $_SESSION['errorMessage'][] = "Unsufficient data. Your link is probably broken.";
            return $this->redirect("/");
        }

        $connection = ConnectionManager::get('cfp');

        $form = $connection->execute('SELECT * FROM forms WHERE id = ' . $path[0])->fetchAll('assoc');

        if(count($form) < 1){
            $_SESSION['errorMessage'][] = "Form not found. Your link is probably broken.";
            return $this->redirect("/");
        }

        $form = $form[0];
        
        $event = $connection->execute('SELECT id, year, name, redhat_event, accommodation FROM cfp.events WHERE id = ' . $form['event_id'])->fetch('assoc');

        if(!parent::hasSMTP($form['event_id'])){
            $_SESSION['warningMessage'][] = "We aren't able to send you nor hotel itself any email due unsufficient settings for this event. Please, contact event organizers directly.";
            return $this->redirect("/booking/" . $form['event_id']);
        }

        if($event['redhat_event'] == 0){
            $_SESSION['errorMessage'][] = "This event isn't organized by Red Hat. You can't book anything.";
            return $this->redirect("/acceptproposals");
        }
        
        if($event['accommodation'] == 0){
            $_SESSION['errorMessage'][] = "This event doesn't handle accommodation booking via this service.";
            return $this->redirect("/acceptproposals");
        }

        $questions = $connection->execute('SELECT * FROM cfp.questions WHERE form_id = ' . $path[0] . ' ORDER BY question_order')->fetchAll('assoc');
        return $this->add($connection, $questions, $form);
    }

    public function add($connection, $questions, $form) {
        //check for existing response from this user based on it's type
        $exists = $connection->execute("SELECT id FROM cfp.answers WHERE user_id like '" . $_SESSION['loggedUser'] . "' AND form_id = " . $form['id'] . " AND event_id = " . $form['event_id'])->fetchAll("assoc");
        
        if( count($exists) < 1 ){
 
 			$confirmation = array();
            $answerID = -1;
            $roomID = -1;
         
         	foreach($questions as $q){

				if($q['required'] && !isset($_POST[$q['take_as']])){
                    $_SESSION['errorMessage'][] = "You didn't specified required fields. Your booking request wasn't proceeded.";
                    if($answerID > 0){
                        $connection->execute("DELETE FROM answer_parts WHERE answer_id = ". $answerID);
                        $connection->execute("DELETE FROM answers WHERE id = " . $answerID);
                    }
                    return $this->redirect("/booking/" . $form['event_id']);
                }
            
                switch($q['type']){
                    case "text":
                    case "textarea":
                    case "radio":
                        if(strpos($q['take_as'], "self") !== false){
                            $answer = "'" . str_replace(array("'", "\""), array("\'", "\\\""), $_POST[$q['take_as']]) . "'";
                            try {
                                if($answerID < 0){
                                    $connection->execute("INSERT INTO answers (event_id, user_id, form_id) VALUES (" .$form['event_id']. ", '" .$_SESSION['loggedUser']. "', " .$form['id']. ")");
                                    $answerID = $connection->execute("SELECT LAST_INSERT_ID() as id")->fetch('assoc')['id'];
                                }
                                // INSERT INTO answers
                                $connection->execute("INSERT INTO answer_parts (question_id, answer_id, text) VALUES (" . $q['id'] . ", " . $answerID . ", " . $answer . ")");
                                $confirmation [] = array("question"=>$q['question'], "answer"=>nl2br(htmlspecialchars($_POST[$q['take_as']])));
                            }catch(\Exception $e){
                                $_SESSION['errorMessage'][] = $e->getMessage();
                                $connection->execute("DELETE FROM answer_parts WHERE answer_id = ". $answerID);
                                $connection->execute("DELETE FROM answers WHERE id = " . $answerID);
                                return $this->redirect("/booking/" . $form['event_id']);
                            }
                        } else {
                        	// accommodation related question
                        	$answer = "'" . str_replace(array("'", "\""), array("\'", "\\\""), $_POST[$q['take_as']]) . "'";
                            try {
                            	$room = $connection->execute("SELECT name, full_price, covered_price, amount FROM hotel_rooms WHERE id = " . $_POST[$q['take_as']])->fetch("assoc");
                            	if($room['amount'] > 0){
	                                if($answerID < 0){
	                                    $connection->execute("INSERT INTO answers (event_id, user_id, form_id) VALUES (" .$form['event_id']. ", '" .$_SESSION['loggedUser']. "', " .$form['id']. ")");
	                                    $answerID = $connection->execute("SELECT LAST_INSERT_ID() as id")->fetch('assoc')['id'];
	                                }
	                                // INSERT INTO answers
	                                $connection->execute("INSERT INTO answer_parts (question_id, answer_id, text) VALUES (" . $q['id'] . ", " . $answerID . ", " . $answer . ")");
	                                // decrease amount of available rooms
	                                $connection->execute("UPDATE cfp.hotel_rooms SET amount = amount - 1 WHERE id = " . $_POST[$q['take_as']]);
	                                $confirmation [] = array("question"=>$q['question'], "answer"=>nl2br(htmlspecialchars($room['name'] . " (" . (strpos($form['type'], "covered") !== false?$room['covered_price']:$room['full_price']) . ")")));
	                                $roomID = $_POST[$q['take_as']];
	                            } else {
	                            	throw new Exception("Selected room is not available.");
	                            }
                            }catch(\Exception $e){
                                $_SESSION['errorMessage'][] = $e->getMessage();
                                $connection->execute("DELETE FROM answer_parts WHERE answer_id = ". $answerID);
                                $connection->execute("DELETE FROM answers WHERE id = " . $answerID);
                                return $this->redirect("/booking/" . $form['event_id']);
                            }
                        }
                        break;
                    case "checkbox":
                        if(isset($_POST[$q['take_as']]) && count($_POST[$q['take_as']]) > 0){
                            $answers = array();
                            foreach($_POST[$q['take_as']] as $t){
                                $answers[] = str_replace(array("'", "\""), array("\'", "\\\""), $t);
                            }
                            try{
                                if($answerID < 0){;
                                    $connection->execute("INSERT INTO answers (event_id, user_id, form_id) VALUES (" .$form['event_id']. ", '" .$_SESSION['loggedUser']. "', " .$form['id']. ")");
                                    $answerID = $connection->execute("SELECT LAST_INSERT_ID() as id")->fetch('assoc')['id'];
                                }
                                //INSERT INTO answers
                                $connection->execute("INSERT INTO answer_parts (question_id, answer_id, text) VALUES (" . $q['id'] . ", " . $answerID . ", '" . implode(",", $answers) . "')");
								$confirmation [] = array("question"=>$q['question'], "answer"=>nl2br(htmlspecialchars(implode("; ", $_POST[$q['take_as']]))));
                            }catch(\Exception $e){
                                $_SESSION['errorMessage'][] = $e->getMessage();
                                $connection->execute("DELETE FROM answer_parts WHERE answer_id = ". $answerID);
                                $connection->execute("DELETE FROM answers WHERE id = " . $answerID);
                                return $this->redirect("/booking/" . $form['event_id']);
                            }
                        }
                        break;
                    default: 
                        break;
                }
            }
        } else {
            $_SESSION['errorMessage'][] = "You've book a room for this event already. No other reservation is supported.";
            return $this->redirect("/acceptproposals");
        }

        try{
            $this->sendConfirm($connection, $form, $answerID, $roomID, $confirmation);
            $_SESSION['successMessage'][] = "Check your email address (" . $_SESSION['loggedUser'] . ") for additional information.";
        }catch(\Exception $e){
            $_SESSION['errorMessage'][] = $e->getMessage();
        }
        return $this->redirect("/acceptproposals");
    }
    
    public function sendConfirm($connection, $form, $aid, $roomID, $answers){

        $event = $connection->execute('SELECT name, year, id FROM cfp.events WHERE id = ' . $form['event_id'])->fetch('assoc');
        $mail = $connection->execute('SELECT * FROM cfp.emails WHERE event_id = ' . $form['event_id'] . ' and type = "' . ($form['type'] == "accommodation"?"hotel-unpaid":"hotel-paid") . '"')->fetch('assoc');
        $hotel = $connection->execute('SELECT name, email FROM cfp.hotels WHERE event_id = ' . $form['event_id'] . " AND id = (SELECT hotel_id FROM cfp.hotel_rooms WHERE id = " . $roomID . ")")->fetch("assoc");
        $smtp = $connection->execute('SELECT * FROM cfp.smtps WHERE event_id = ' . $form['event_id'])->fetch('assoc');
        $smtp['password'] = parent::decrypt($smtp['password']);
        $smtp['username'] = parent::decrypt($smtp['username']);
        Email::configTransport("custom", ['className'=>"Smtp", "timeout"=>60, "tls"=>($smtp['tls']==1?true:false), 'host'=>$smtp['host'], 'port'=>$smtp['port'], 'username'=>$smtp['username'], 
        'password'=>$smtp['password'], 'context'=>['ssl'=>['allow_self_signed'=>true, 'verify_peer'=>false, 'verify_peer_name'=>false]]]);
        $ans = "";
        foreach($answers as $a){
            if($mail['format'] == "html"){
                $ans .= "<strong>" . $a['question'] . "</strong>: " . $a['answer'] . "<br>";
            } else {
                $ans .= "Q: " . $a['question'] . "\nA: " . $a['answer'] . "\n\n";
            }
        }
        $text = str_replace(parent::getKeywords($form['type']), array($_SESSION['first-name'], $_SESSION['last-name'], $_SESSION['loggedUser'], $event['name'], $event['year'], $ans, $hotel['name'], $hotel['email']), $mail['text']);

        $email = new Email();
        $email->dropTransport("Smtp");
        $email->emailFormat($mail['format']);
        $email->setFrom([$smtp['username'] => $smtp['nick']]);
        $email->setReplyTo($_SESSION['loggedUser']);
        $email->setTo($hotel['email']);
        $email->setCc($_SESSION['loggedUser']);
        $email->addCc($smtp['username']);
        $email->setSubject($mail['subject']);
        $email->send($text);
    }
}
