<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Datasource\ConnectionManager;
/**
 * Static content controller
 *
 * This controller will render views from Template/HomePage/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class HotelRoomsController extends AppController
{

    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Network\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function show(...$action)
    {
		if(count($action) < 1 or !is_numeric($action[0])){
			$_SESSION['errorMessage'][] = "Unsufficient data. Your link is probably broken.";
			return $this->redirect("/manager");
		}
		
        $connection = ConnectionManager::get('cfp');
        
        $event = $connection->execute("SELECT name, id, year, accommodation FROM events WHERE id = (SELECT event_id FROM hotels WHERE id = " . $action[0] . ")")->fetch("assoc");
        
        if(!parent::getProgramManagerForEvent($event['id']) && !parent::getSuperUser()){
        	$_SESSION['errorMessage'][] = "You're not allowed to visit this page.";
            return $this->redirect("/");
        }
        
        parent::printFlush($this->request->here());
        
        $this->set("admin", parent::getAdmin() | parent::getSuperUser());
        $this->set("reviewer", parent::getReviewer());
        $this->set("program_manager", parent::getProgramManager());

        $formAction = "add";            

        if(!$event['accommodation']){
        	$_SESSION['errorMessage'][] = "This event can't manage accommodation via CfP.";
    	    return $this->redirect("/manager");
        }
        
        $hotel = $connection->execute("SELECT id, name FROM hotels WHERE id = " . $action[0])->fetch("assoc");
        $rooms = $connection->execute("SELECT id, name, description, amount, full_price, covered_price FROM cfp.hotel_rooms WHERE hotel_id = " . $action[0] . " ORDER BY name")->fetchAll("assoc");
        
        $this->set("event", $event);
        $this->set("hotel", $hotel);
        $this->set("rooms", $rooms);
        $this->set("action", $formAction);
        $this->set("active", "manager");
        $token = $this->request->getParam('_csrfToken');

        $_SESSION['token'] = $token;
        $this->set("token", $token);
        $this->set('username', $_SESSION['first-name']." ".$_SESSION['last-name']);

        try {
            $this->render('home');
        } catch (MissingTemplateException $exception) {
            if (Configure::read('debug')) {
                throw $exception;
            }
            throw new NotFoundException();
        }
    }
}
