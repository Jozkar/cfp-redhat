<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Datasource\ConnectionManager;

/**
 * Static content controller
 *
 * This controller will render views from Template/HomePage/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class ManageBookingController extends AppController
{

    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Network\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function show(...$action)
    {
        if(!parent::getProgramManager() && !parent::getSuperUser()){
    		return $this->redirect("/");
    	}
    	
    	if(count($action) < 1 or !is_numeric($action[0])){
    		$_SESSION['errorMessage'][] = "Unsufficient data. Your link is probably broken.";
    		return $this->redirect("/manager");
    	}

    	parent::printFlush($this->request->here());
    	$this->set("active", "manager");
    	$this->set("admin", parent::getAdmin() | parent::getSuperUser());
    	$this->set("reviewer", parent::getReviewer());
    	$this->set("program_manager", parent::getProgramManager());

    	if($this->request->session()->read("first-name")){
    		$this->set('username', $_SESSION['first-name'] . " " . $_SESSION['last-name']);
    	}
    
    	$connection = ConnectionManager::get('cfp');
    
    	$myEvent = $connection->execute('SELECT 1 FROM managers WHERE user_id like ("' . $_SESSION['loggedUser'] . '") and event_id = ' . $action[0])->fetchAll('assoc'); 

    	if(!parent::getSuperUser() && $myEvent != '' && count($myEvent) < 1){
    		$_SESSION['errorMessage'][] = "You aren't allowed to do this operation.";
    		return $this->redirect("/manager");
    	}

    	$event = $connection->execute('SELECT name, year, accommodation FROM cfp.events WHERE id  = ' . $action[0])->fetch('assoc');
   		$answers = $connection->execute('SELECT 
                                            a.id, 
                                            CONCAT(u.first_name, " ", u.last_name, " (", u.email, ")") as speaker, 
                                            u.id as user_id,
                                            a.form_id,
                                            u.country
                                         FROM forms as f, users as u, answers as a WHERE 
                                              f.id = a.form_id AND f.type like ("confirmation") and a.user_id = u.id and f.event_id = ' . $action[0]. ' 
                                              GROUP BY a.id ORDER BY a.id')->fetchAll('assoc');
    	$responses = array();

    	foreach ($answers as $a){
    		$res = $connection->execute('SELECT q.question, ap.text, ap.as_title, q.take_as FROM questions as q, answer_parts as ap WHERE ap.answer_id = ' . $a['id'] .
    			' AND ap.question_id = q.id and q.form_id = ' . $a['form_id'])->fetchAll('assoc');
    		$newRes = array("answer_id"=>$a['id'], "speaker"=>$a['speaker'], "country"=>$a['country'], "form_id"=>$a['form_id'], "user_id"=>$a['user_id']);
    		foreach($res as $r){
                if($r['as_title'] == 1){
       				$newRes['Title'] = $r['text'];
                } else if($r['take_as'] == 'topics'){
                    $curTops = $connection->execute('SELECT GROUP_CONCAT(" ", name) as name FROM topics WHERE id IN(' . $r['text'] . ')')->fetch("assoc");
               		$newRes[$r['question']] = $curTops['name'];
                } else if($r['as_duration'] == 1){
                    $newRes['Duration'] = $r['text'];
                } else {
            		$newRes[$r['question']] = $r['text'];
                }
            }
    			
    		$responses[] = $newRes;
    	}
    	$votes = $connection->execute('SELECT user_id, vote FROM cfp.hotel_booking WHERE event_id = ' . $action[0])->fetchAll('assoc');
    	
    	$token = $this->request->getParam('_csrfToken');

    	$accepted = array();
    	$rejected = array();
    	$unreviewed = array();

    	
    	foreach($responses as $r){
            $added = false;
   			foreach($votes as $v){
  				if($v['user_id'] == $r['user_id']){
  					$added = true;
   					if ($v['vote'] > 0){
   						$r['vote'] = $v['vote'];
   						$accepted[] = $r;
   					}else if($v['vote'] < 0){
   						$r['vote'] = $v['vote'];
   						$rejected[] = $r;
                    }
   				}
   			}
   			if(!$added){
   				$unreviewed[] = $r;
   			}
   		}
    
    	$this->request->session()->write("token", $token);
        $this->set("token", $token);
    	$this->set("eventInfo", $event);		
    	$this->set("event", $action[0]);
    	$this->set("granted", $accepted);
    	$this->set("rejected", $rejected);
    	$this->set("unreviewed", $unreviewed);
    	$this->set("manager", $_SESSION['loggedUser']);

    	try {
   			$this->render("booking");
        } catch (MissingTemplateException $exception) {
            if (Configure::read('debug')) {
                throw $exception;
            }
            throw new NotFoundException();
        }
    }
}
