<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Datasource\ConnectionManager;

/**
 * Static content controller
 *
 * This controller will render views from Template/HomePage/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class ProposalsController extends AppController
{

    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Network\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function show(...$action)
    {
    	
    if(count($action) < 1 or !is_numeric($action[0])){
    	$_SESSION['errorMessage'][] = "Unsufficient data. Your link is probably broken.";
    	return $this->redirect("/");
    }	
    
    parent::printFlush($this->request->here());

    $this->set("active", "home");
    $this->set("admin", parent::getAdmin() | parent::getSuperUser());
    $this->set("reviewer", parent::getReviewer());
    $this->set("program_manager", parent::getProgramManager());     
    
    if($this->request->session()->read("first-name")){
            $this->set('username', $_SESSION['first-name'] . " " . $_SESSION['last-name']);
    }

    $connection = ConnectionManager::get('cfp');
    $event = $connection->execute('SELECT id, name, year, for_redhat FROM cfp.events WHERE id  = ' . $action[0])->fetch('assoc');
    $this->set("eventInfo", $event);
    $this->set("current_program_manager", parent::getProgramManagerForEvent($action[0]));
    
    if(count($action) > 4 && $action[1] == "delete" && is_numeric($action[2]) && is_numeric($action[3]) && $action[4] == $_SESSION['token']){
        if(!parent::getSuperUser() && !parent::getProgramManagerForEvent($action[0])){
            $_SESSION['errorMessage'][] = "You don't have access to do this operation. Permission denied.";
            return $this->redirect("/proposals/" . $action[0]);
        }
        try{
            $answer = $connection->execute("SELECT answer_id FROM responses WHERE id=" . $action[3])->fetch("assoc");
            $connection->execute("DELETE FROM responses WHERE id=" . $action[3] . " AND event_id=" . $action[2]);
            $connection->execute("DELETE FROM additional_speakers WHERE response_id=" . $action[3]);
            $connection->execute("DELETE FROM comments WHERE response_id=" . $action[3]);
            $collectanswer = $connection->execute("SELECT id FROM answers WHERE collect_response_id=" . $action[3])->fetch("assoc");
            $rtt = $connection->execute("SELECT id FROM responses_to_topics WHERE response_id = " . $action[3])->fetchAll("assoc");
            foreach($rtt as $r){
                $connection->execute("DELETE FROM accepts WHERE response_to_topic_id=" . $r['id']);
                $connection->execute("DELETE FROM votes WHERE response_to_topic_id=" . $r['id']);
            }
            if(is_numeric($answer['answer_id'])){
                $connection->execute("DELETE FROM answers WHERE id=" . $answer['answer_id']);
                $connection->execute("DELETE FROM answer_parts WHERE answer_id=" . $answer['answer_id']);                
            }
            if(is_numeric($collectanswer['id'])){
                $connection->execute("DELETE FROM answers WHERE id=" . $collectanswer['id']);
                $connection->execute("DELETE FROM answer_parts WHERE answer_id=" . $collectanswer['id']);                
            }
            
            $connection->execute("DELETE FROM responses_to_topics WHERE response_id=" . $action[3]);   
        }catch(\Exception $e){
            $_SESSION['errorMessage'][] = $e->getMessage();
            return $this->redirect("/proposals/" . $action[0]);
        }

        $_SESSION['successMessage'][] = "Proposal no." . $action[3] . " has been removed.";
        return $this->redirect("/proposals/" . $action[0]);           
    }

    $token = $this->request->getParam('_csrfToken');
    $this->request->session()->write("token", $token);
	$this->set("token", $token);

    if(count($action) == 2 && is_numeric($action[1])){
    	$questions = $connection->execute('SELECT * FROM cfp.questions WHERE form_id = (SELECT id FROM cfp.forms WHERE event_id = ' . $action[0] . ' AND type like ("session")) ORDER BY question_order')->fetchAll('assoc');

    	$values = [];

    	$answerId = $connection->execute("SELECT answer_id FROM responses WHERE id = " . $action[1])->fetchAll('assoc');
    	if(count($answerId) > 0) {
                        $answerId = $answerId[0]['answer_id'];
                }else{
                        $answerId = NULL;
                }
    	foreach($questions as $q){
    		if($q['type'] == "description"){
    			$values[] = NULL;
                                continue;
                        }
                        if(strpos($q['take_as'], "self") !== false){
                                $v = $connection->execute("SELECT text FROM cfp.answer_parts WHERE question_id = " . $q['id'] . " AND answer_id = " . ($answerId != NULL?$answerId:"-1"))->fetchAll("assoc");
                                if(count($v) > 0){
                                    $values[] = $v[0]['text'];
                                } else {
                                      $values[] = NULL;
                                }
                        } else if($q['take_as'] == "topics"){
                                $v = $connection->execute("SELECT GROUP_CONCAT(t.name SEPARATOR ', ') as topics FROM responses_to_topics as rtt, topics as t WHERE".
    				" rtt.response_id = " . $action[1] .  " AND t.id = rtt.topic_id")->fetchAll('assoc');
                                $values[] = $v[0]['topics'];
                        } else {
                                $v = $connection->execute("SELECT " . $q['take_as'] . " FROM cfp.responses WHERE id = " . $action[1])->fetchAll('assoc');
                                if(count($v) > 0){
                                      $values[] = $v[0][$q['take_as']];
                                } else {
                                      $values[] = NULL;
                                }
                        }
    	}

    	$talk = $connection->execute('SELECT r.title, r.id as response_id, CONCAT(u.first_name, " ", u.last_name ) as speaker, u.id FROM cfp.users as u, cfp.responses as r WHERE'.
    		' u.id = r.user_id AND r.id = ' . $action[1])->fetchAll('assoc');
    	
    	$permission = $connection->execute('SELECT distinct 1 FROM reviewers as r, managers as m WHERE (r.event_id = ' . $action[0] . ' and r.user_id like ("' . $_SESSION["loggedUser"] . '")) or (m.event_id = ' . $action[0] . ' and m.user_id like ("' . $_SESSION['loggedUser'] . '"))')->fetchAll('assoc');

    	if(parent::getAdmin() || parent::getSuperUser() || ($permission != null && $permission[0]['1'])){
    		if($permission != null && $permission[0]['1']){
    			$userType = "reviewer";
    			if(parent::getProgramManager()){
    				$userType = "program manager";
    			}
    		} else {
    			$userType = "admin";
    		}
    		$permission = true;
    		foreach($talk as $key=>$o){
                            $talk[$key]['comments'] = $connection->execute('SELECT author, comment, time, public FROM cfp.comments WHERE response_id = ' . $action[1] . ' ORDER BY id desc')->fetchAll('assoc');
    			$additional = $connection->execute('SELECT CONCAT(u.first_name, " ", u.last_name) as additional from users u, additional_speakers a WHERE u.id = a.user_id AND a.response_id = ' . $action[1])->fetchAll('assoc');
    			if (is_array($additional) && count($additional) > 0){
    				$talk[$key]['speaker'] .= " (main speaker)";
    				foreach($additional as $k => $a) {
    					$talk[$key]['speaker'] .= ", " . $a['additional'];
    				}
    			}
    		}
    	} else {
    		if(count($talk)>0 && $talk[0]['id'] == $_SESSION['loggedUser']){
    			$permission = true;
    			$userType = "speaker";
    		} else {
    			$permission = false;
    		}
    		foreach($talk as $key=>$o){
    			$talk[$key]['comments'] = $connection->execute('SELECT author, comment, time, public FROM cfp.comments WHERE response_id = ' . $action[1] . ' AND public = 1 ORDER BY id desc')->fetchAll('assoc');
    			$additional = $connection->execute('SELECT CONCAT(u.first_name, " ", u.last_name) as additional from users u, additional_speakers a WHERE u.id = a.user_id AND a.response_id = ' . $action[1])->fetchAll('assoc');
    			if (is_array($additional) && count($additional) > 0){
    				$talk[$key]['speaker'] .= " (main speaker)";
    				foreach($additional as $k => $a) {
    					$talk[$key]['speaker'] .= ", " . $a['additional'];
    				}
    			}
                    }
    	}
    	if($event['for_redhat'] == 1 && !parent::isRedHatter()){
                $talk = [];
            }
    	if(count($talk) > 0){
    		$this->set("talk", $talk[0]);
    		$this->set("questions", $questions);
    		$this->set("values", $values);
    		$this->set("permission", $permission);
    		$this->set("userType", $userType);
    	} else {
    		$this->set("talk", null);
    		$this->set("questions", null);
    		$this->set("values", null);
    		$this->set("permission", $permission);
    	}

    	try {
    		$this->render('talk');
    	} catch (MissingTemplateException $exception) {
    		if (Configure::read('debug')) {
    			throw $exception;
    		}
    		throw new NotFoundException();
    	}
    } else {
        if(!parent::getSuperUser() && !parent::getProgramManagerForEvent($action[0]) && !parent::getAdminForEvent($action[0]) && !parent::getReviewerForEvent($action[0])){
            $_SESSION['errorMessage'][] = "You don't have access to this page. Permission denied.";
            return $this->redirect("/");
        }

    	if(parent::isRedHatter() || $event['for_redhat'] != 1){
    		$others = $connection->execute('SELECT r.id, r.title, r.type, r.duration, r.difficulty, r.abstract, r.summary, r.notes, r.notes2, r.notes3, r.keywords, ' . 
    				'r.answer_id, CONCAT(u.first_name, " ", u.last_name ) as speaker, GROUP_CONCAT(t.name SEPARATOR ", ") as topic, f.id as form_id FROM ' .
    				'cfp.topics as t, cfp.forms as f, cfp.users as u, cfp.responses as r, cfp.responses_to_topics as c WHERE ' .
    				'u.id = r.user_id AND c.event_id = ' . $action[0] .' AND r.id = c.response_id AND t.id = c.topic_id AND f.event_id = c.event_id AND f.type like ("session") ' .
    				'group by r.id ORDER BY c.id')->fetchAll('assoc');

    		foreach($others as $key=>$o){
    			$others[$key]['comments'] = $connection->execute('SELECT author, comment, time, public FROM cfp.comments WHERE response_id = ' . $o['id'] . ' AND public = 1 ORDER BY id desc')->fetchAll('assoc');
    			$additional = $connection->execute('SELECT CONCAT(u.first_name, " ", u.last_name) as additional from users u, additional_speakers a WHERE u.id = a.user_id AND a.response_id = ' 
    				. $others[$key]['id'])->fetchAll('assoc');
    				if (is_array($additional) && count($additional) > 0){
    					$others[$key]['speaker'] .= " (main speaker)";
    					foreach($additional as $k => $a) {
    					$others[$key]['speaker'] .= ", " . $a['additional'];
    				}
    			}
    			$note = $connection->execute('SELECT q.question FROM questions as q, forms as f WHERE take_as like ("notes") and form_id = f.id and f.event_id = ' . $action[0] . ' and f.type like ("session")')->fetchAll('assoc');

    			if(count($note) > 0){
    				$others[$key][$note[0]['question']] = $o['notes'];
    			}
    	                unset($others[$key]['notes']);
    			$note = $connection->execute('SELECT q.question FROM questions as q, forms as f WHERE take_as like ("notes2") and form_id = f.id and f.event_id = ' . $action[0] . ' and f.type like ("session")')->fetchAll('assoc');
    			if(count($note) > 0){
    				$others[$key][$note[0]['question']] = $o['notes2'];
    			}
    			unset($others[$key]['notes2']);
    			$note = $connection->execute('SELECT q.question FROM questions as q, forms as f WHERE take_as like ("notes3") and form_id = f.id and f.event_id = ' . $action[0] . ' and f.type like ("session")')->fetchAll('assoc');
    			if(count($note) > 0){
    				$others[$key][$note[0]['question']] = $o['notes3'];
    			}
    			unset($others[$key]['notes3']);
    			if($o['answer_id'] != ""){
    				$answers = $connection->execute('SELECT q.question, ap.text FROM questions as q, answer_parts as ap, answers as a WHERE a.id = ' . $o['answer_id'] .
    					' AND a.form_id = q.form_id and ap.question_id = q.id AND ap.answer_id = a.id')->fetchAll('assoc');
    				$others[$key]['answers'] = $answers;
    			}
    		}
    	} else {
    		$this->set("eventInfo", array());
    		$others = array();
    	}
    	$this->set("all", $others);

    	try {
                $this->render("home");
            } catch (MissingTemplateException $exception) {
                if (Configure::read('debug')) {
                    throw $exception;
                }
                throw new NotFoundException();
            }
    }
    }
}
