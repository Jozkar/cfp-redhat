<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Datasource\ConnectionManager;
/**
 * Static content controller
 *
 * This controller will render views from Template/HomePage/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class ManagersController extends AppController
{

    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Network\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function show(...$action)
    {

        if(!parent::getAdmin() && !parent::getSuperUser()){
            return $this->redirect("/");
        }

        if(count($action) < 1){
            $_SESSION['errorMessage'][] = "Unsufficient data. Your link is probably broken.";
            return $this->redirect("/admin");
        }
        $connection = ConnectionManager::get('cfp');
        
        if(!isset($action[0]) || !is_numeric($action[0])){
            $_SESSION['errorMessage'][] = "Unsufficient data. Your link is probably broken.";
            return $this->redirect("/admin");
        }

        if(!parent::getAdminForEvent($action[0])){
            $_SESSION['errorMessage'][] = "You are not allowed to do this opperation.";
            return $this->redirect("/admin");
        }

        if(isset($_POST['save'])){
            if(isset($_SESSION['token']) && isset($_POST['_Token']) && $_POST['_Token'] == $_SESSION['token']){
                if(isset($action[1])  && isset($_POST['user']) && $action[1] == "add"){
                    // get user id
                    $_POST['user'] = "'" . str_replace(array("'", "\""), array("\'", "\\\""), $_POST['user']) . "'";
                    $newAdmin = $connection->execute("SELECT id FROM users WHERE email like (" . $_POST['user'] . ")")->fetchAll("assoc");
                    // set admin column in db to 1
                    return $this->modify($connection,$newAdmin[0]["id"],$action[0],1);
                } else {
                    $_SESSION['errorMessage'][] = "You are not allowed to do this opperation";
                    return $this->redirect("/managers/" . $action[0]);
                }
            } else {
                $_SESSION['errorMessage'][] = "You are not allowed to do this opperation";
                return $this->redirect("/managers/" . $action[0]);
            }
        }

        parent::printFlush($this->request->here());
        $this->set("eventId", $action[0]);
        $this->set("admin", parent::getAdmin() | parent::getSuperUser());
        $this->set("reviewer", parent::getReviewer());
        $this->set("program_manager", parent::getProgramManager());

        $formAction = "add";            

        // check, if are passed any arguments
        if(!empty($action) && count($action) > 1){
            // supported are arguments /.../event/action/userid/token
            if( count($action) <= 3 ){
                //not enough arguments error message  + redirect
                $_SESSION['errorMessage'][] = "Unsufficient data. Your link is probably broken.";
                return $this->redirect("/managers/".$action[0]); 
            }
            //for delete action
            if ($action[1] == "delete") {
                if($_SESSION['token'] == $action[3]){
                    // set program_manager column in db to 0
                    return $this->modify($connection,$action[2],$action[0],0);
                } else {
                    $_SESSION['errorMessage'][] = "This action can't be performet - invalid token.";
                    return $this->redirect("/managers/".$action[0]);
                }
            }
        }

        $eventInfo = $connection->execute("SELECT name, year FROM events WHERE id = " . $action[0])->fetchAll("assoc"); 
        $allUsers = $connection->execute("SELECT distinct first_name, last_name, id, email, avatar FROM users LEFT OUTER JOIN managers as m ON (m.user_id = id) WHERE m.event_id != " . $action[0] . " or program_manager = 0")->fetchAll("assoc");
        $managerUsers = $connection->execute("SELECT distinct first_name, last_name, id, email, avatar FROM users, managers as m WHERE m.user_id = id and m.event_id = " . $action[0])->fetchAll("assoc");

        $this->set("eventInfo", $eventInfo[0]);
        $this->set("managers", $managerUsers);
        $this->set("users", $allUsers);
        $this->set("action", $formAction);
        $this->set("active", "admin");
        $token = $this->request->getParam('_csrfToken');

        $_SESSION['token'] = $token;
        $this->set("token", $token);
        $this->set('username', $_SESSION['first-name']." ".$_SESSION['last-name']);
    
        try {
            $this->render('list');
        } catch (MissingTemplateException $exception) {
            if (Configure::read('debug')) {
                throw $exception;
            }
            throw new NotFoundException();
        }
    }
    
    public function modify($connect, $key, $event, $val){
        $key = "'" . str_replace(array("'", "\""), array("\'", "\\\""), $key) . "'";        
        try{
            if($val){
                $connect->execute("INSERT INTO cfp.managers (event_id, user_id) VALUES (" . $event .", " . $key . ")");        
                $connect->execute("UPDATE cfp.users SET `program_manager`=" . $val . " WHERE `id` like (" . $key . ")");
                $_SESSION['successMessage'][] = "Program manager has been successfully created.";
            } else {
                $connect->execute("DELETE FROM cfp.managers WHERE event_id = " . $event . " and user_id like(" . $key . ")");        
                $test = $connect->execute("SELECT * FROM cfp.managers WHERE user_id like(" . $key . ")");        
                if(count($test) < 1) {
                    $connect->execute("UPDATE cfp.users SET `program_manager`= 0 WHERE `id` like (" . $key . ")");
                }
                $_SESSION['successMessage'][] = "Program manager has been successfully removed.";
            }
        }catch(\Exception $e){
            if($val){
                $_SESSION['errorMessage'][] = "Program manager can't be created.";
            } else {
                $_SESSION['errorMessage'][] = "Program manager can't be removed.";
            }
        }    
        $this->redirect("/managers/".$event);
    }
}
