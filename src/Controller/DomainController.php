<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Datasource\ConnectionManager;
use Cake\Filesystem\File;
/**
 * Static content controller
 *
 * This controller will render views from Template/HomePage/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class DomainController extends AppAdminController
{

    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Network\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function show(...$action)
    {

    	if(!parent::getSuperUser()){
    		return $this->redirect("/");
    	}
    	$connection = ConnectionManager::get('cfp');
    	
    	if(isset($_POST['save'])){
    		if(isset($_POST['_Token']) && $_POST['_Token'] == $_SESSION['token'] && isset($_POST['action'])){
    			if($_POST['action'] == "update"){
    				return $this->update($connection);
    			}else{
    				return $this->add($connection);
    			}
    		} else {
    			$_SESSION['errorMessage'][] = "You are not allowed to do this opperation";
    			return $this->redirect("/domains");
    		}
    	}

    	parent::printFlush($this->request->here());
    	$this->set("admin", parent::getAdmin() | parent::getSuperUser());
    	$this->set("reviewer", parent::getReviewer());
    	$this->set("program_manager", parent::getProgramManager());

    	// check, if are passed any arguments
    	if(!empty($action)){
    		// supported are arguments /.../action/domain
    		if(count($action) >= 1 && !in_array($action[0], ["add","edit","remove"])){
    			//not enough arguments error message  + redirect
    			$_SESSION['errorMessage'][] = "Unsufficient data. Your link is probably broken.";
    			return $this->redirect("/domains"); 
    		}

    		switch($action[0]){
    			case "edit":
    				if(!isset($action[1])){
    					$_SESSION['errorMessage'][] = "Unsufficient data. Your link is probably broken.";
    					return $this->redirect("/domains");
    				}
    				$results = $connection->execute('SELECT * FROM cfp.domains WHERE domain like ("' . $action[1] . '")')->fetch('assoc');
                    $this->set("domain", $results);
    				$this->set("action", "update");
    				break;
    			case "add":
    				$this->set("domain", null);
    				$this->set("action", "create");
    				break;
    			case "remove":
    				return $this->delete($connection, $action[1]);
    				break;
    			default:
    				$_SESSION['errorMessage'][] = "Unsupported operation. Your link is probably broken.";
    				return $this->redirect("/domains");
    				break;
    		}

    		$_SESSION['token'] = $this->request->getParam('_csrfToken');
    		$this->set("token", $_SESSION['token']);
    		$this->set("active","admin");
            $this->set('username', $_SESSION['first-name']." ".$_SESSION['last-name']);

    		try{
    			 $this->render("form");
    		}catch(MissingTemplateException $exception) {
    			if (Configure::read('debug')) {
    				throw $exception;
    			}
    			throw new NotFoundException();
    		}
    		
    	}else{
    		$dom = $connection->execute("SELECT domain, name, image FROM domains ORDER BY domain")->fetchAll('assoc');
    		$this->set("domains", $dom);
    		$this->set("active","admin");
    		$this->set('username', $_SESSION['first-name']." ".$_SESSION['last-name']);
    	
    		try {
    		    $this->render('home');
    		} catch (MissingTemplateException $exception) {
    		    if (Configure::read('debug')) {
    	        	throw $exception;
    		    }
    		    throw new NotFoundException();
    		}
    	}
    }
    
    public function update($connect){
        foreach($_POST as $key=>$val){
        	if(!is_numeric($val) && !is_array($_POST[$key])){
        		if($key == "client_id" || $key == "client_secret"){
        			$_POST[$key] = "'".parent::encrypt($val)."'";
        		}else{
        			$_POST[$key] = "'" . str_replace(array("'", "\""), array("\'", "\\\""), $val) . "'";
        		}
        	}
        }
        if($_FILES['logo']['name'] != ""){
            $name = "/webroot/img/logos/" . str_replace(array("'","/"),"",$_POST['name']) . "_" . $_POST['year'] . "_" . $_FILES['logo']['name'];
            move_uploaded_file($_FILES['logo']['tmp_name'], WWW_ROOT . "img/logos/" . str_replace(array("'","/"),"", $_POST['name']) . "_" . $_POST['year'] . "_" . $_FILES['logo']['name']);
        } else {
           $name = "";
        }
        try{
        	$res = $connect->execute("UPDATE cfp.signins SET `domain`=" . $_POST['domain']. ", `client_id`=" . $_POST['client_id'] . ", `client_secret`=" . $_POST['client_secret'] . ", `callback`=" . $_POST['callback'] .
                                             ", `name`=" . $_POST['name'] . " WHERE `service` = " . $_POST['service'] . " AND `domain` = " . $_POST['old_domain']);
        	$_SESSION['successMessage'][] = "Credentials has been successfully modified.";
        }catch(\Exception $e){
        	$_SESSION['errorMessage'][] = "Credentials can't be updated.";
        }
        return $this->redirect("/domains");    
    }

    public function add($connect){
        foreach($_POST as $key=>$val){
        	if(!is_numeric($val) && !is_array($_POST[$key])){
     			$_POST[$key] = "'" . str_replace(array("'", "\""), array("\'", "\\\""), $val) . "'";
        	}
        }
        if($_FILES['logo']['name'] != ""){
            $name = "/webroot/img/logos/domains/" . str_replace(array("'","/"),"",$_POST['name']) . "_" . $_FILES['logo']['name'];
            move_uploaded_file($_FILES['logo']['tmp_name'], WWW_ROOT . "img/logos/domains/" . str_replace(array("'", "/"),"", $_POST['name']) . "_" . $_FILES['logo']['name']);
        } else {
            $name = "";
        }

        try{
        	$check = $connect->execute("SELECT 1 FROM cfp.domains WHERE domain like (" . $_POST['domain'] . ")")->fetchAll('assoc');
        	if(!$check){
        		$connect->execute("INSERT INTO cfp.domains (domain, name, image) VALUES (" . $_POST['domain'] . ", " . $_POST['name'] . ", '" . $name . "')");
        	}else{
        		throw new Exception();
        	}
        }catch(\Exception $e){
        	$_SESSION['errorMessage'][] = "Domain info can't be saved.";
        }
        return $this->redirect("/domains");
    }
    
    public function delete($connection, $domain){
        $file = $connect->execute("SELECT logo FROM cfp.domains WHERE domain like (" . $domian . ")")->fetch('assoc');
        $f = new File(str_replace("webroot", $file['logo'], WWW_ROOT));
        if( $f->exists() ){
            $f->delete();
        }
        $connection->execute("DELETE FROM cfp.domains WHERE domain like ('" . $action[1] . "')");
    	$_SESSION['successMessage'][] = "Domain " . $domain . " has been successfully removed.";
        return $this->redirect("/domains");
    }
}
