<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\Mailer\Email;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class CommentController extends Controller
{

    public function initialize()
    {

        parent::initialize();

        $this->loadComponent('RequestHandler');
    }

    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Network\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function add(...$path)
    {

        $token = $this->request->getData("token");
        $oldFrom = $this->request->getData("from");
        $response_id = $this->request->getData("response");
        $answer_id = $this->request->getData("answer");
        $oldWhat = $this->request->getData("what");
        $when = $this->request->getData("when");
        $public = $this->request->getData("public");
        $oldFrom = urldecode($oldFrom);
        $from = str_replace('%', '', $oldFrom);
        $from = str_replace('\'', '\\\'', $from);
        $oldWhat = urldecode($oldWhat);
        $what = str_replace('%', '', $oldWhat);
        $what = str_replace('\'', '\\\'', $what);

        if($token == null){
            die(json_encode(array("result"=>"error", "reason"=>"No token received")));
        }

        if($token != $this->request->session()->read('token')){
            die(json_encode(array("result"=>"error", "reason"=>"Token expired. Please, refresh your page.")));
        }

        if(count($path) < 1){
            if($from == '' || ($response_id == '' && $answer_id == '') || $what == '' || $when == '' || !is_numeric($when)){
                die(json_encode(array("result"=>"error", "reason"=>"Unsufficient data. Your comment wasn't accepted.")));
            }

            $connection = ConnectionManager::get('cfp');    
            if($answer_id != ""){
                if($connection->execute('INSERT INTO cfp.comments (author, comment, answer_id, time, public) VALUES (\'' . $from . '\', \'' . $what . '\', '. $answer_id . ', "' . $when . '", ' . ($public != ""? 1:0) . ')')->errorCode() != 0){
                    die(json_encode(array("result"=>"error", "reason"=>"Database error. Your comment wasn't accepted.")));
                } else {
                    $message = "ok";
                    if($public != "" && strpos($from, "speaker") === false){
                        $message = $this->sendComment($oldFrom, $oldWhat, $answer_id, $connection, true);
                    }
                    if($message != "ok"){
                        die(json_encode(array("result"=>"success", "message"=>$message)));
                    } else {
                        die(json_encode(array("result"=>"success")));
                    }
                }
            } else {
                if($connection->execute('INSERT INTO cfp.comments (author, comment, response_id, time, public) VALUES (\'' . $from . '\', \'' . $what . '\', '. $response_id . ', "' . $when . '", ' . ($public != ""? 1:0) . ')')->errorCode() != 0){    	
                  die(json_encode(array("result"=>"error", "reason"=>"Database error. Your comment wasn't accepted.")));
              } else {
                  $message = "ok";
                  if($public != "" && strpos($from, "speaker") === false){
                      $message = $this->sendComment($oldFrom, $oldWhat, $response_id, $connection, false);
                  }
                  if($message != "ok"){
                      die(json_encode(array("result"=>"success", "message"=>$message)));
                  }else{
                      die(json_encode(array("result"=>"success")));
                  }
              }
          }
      } else {
          die(json_encode(array("result"=>"error", "reason"=>"Unsupported operation. Permission denied.")));
      }
  }

  public function sendComment($from, $what, $id, $con, $answer) {
      if($answer){
          $resp = $con->execute("SELECT a.id, e.name, e.year, u.first_name, u.email FROM answers as a, events as e, users as u WHERE u.id = a.user_id AND e.id = a.event_id AND a.id = " . $id)->fetchAll('assoc');
          $addit = $con->execute("SELECT a.id, e.name, e.year, u.first_name, u.email FROM answers as a, events as e, users as u, additional_speakers as s WHERE u.id = s.user_id AND s.answer_id = a.id AND " .
                                 "e.id = a.event_id AND a.id = " . $id)->fetchAll('assoc');
          $tit = $con->execute("SELECT text FROM answer_parts WHERE as_title = 1 and answer_id = " . $resp[0]['id'])->fetch("assoc");

          if(isset($tit) && isset($tit['text']) && $tit['text'] != ""){
              $title = $tit['text'];
          } else {
              $title = "";
          }
          $smtp = $con->execute('SELECT * FROM cfp.smtps WHERE event_id = (SELECT event_id FROM cfp.answers WHERE id = ' . $id . ")")->fetchAll('assoc')[0];
      } else {
          $resp = $con->execute("SELECT r.title, e.name, e.year, u.first_name, u.email FROM responses as r, events as e, users as u WHERE u.id = r.user_id AND e.id = r.event_id AND r.id = " . $id)->fetchAll('assoc');
          $addit = $con->execute("SELECT r.title, e.name, e.year, u.first_name, u.email FROM responses as r, events as e, users as u, additional_speakers as s " .
                                 "WHERE u.id = s.user_id AND s.response_id = r.id AND e.id = r.event_id AND r.id = " . $id)->fetchAll('assoc');
          $title = $resp[0]['title'];
          $smtp = $con->execute('SELECT * FROM cfp.smtps WHERE event_id = (SELECT event_id FROM cfp.responses WHERE id = ' . $id . ")")->fetchAll('assoc')[0];
      }
      $response = array_merge($resp, $addit);

      if($smtp['host'] == ""){
          return "No SMTP credentials available. Email has not been sent.";
      }

      $smtp['password'] = $this->decrypt($smtp['password']);
      $smtp['username'] = $this->decrypt($smtp['username']);
      Email::configTransport("custom", ['className'=>"Smtp", "timeout"=>60, "tls"=>($smtp['tls']==1?true:false), 'host'=>$smtp['host'], 'port'=>$smtp['port'], 'username'=>$smtp['username'], 
      'password'=>$smtp['password'], 'context'=>['ssl'=>['allow_self_signed'=>true, 'verify_peer'=>false, 'verify_peer_name'=>false]]]);
      if(!empty($response)) {
          foreach($response as $r){
              $email = new Email();
              $email->dropTransport("Smtp");
              $email->setTemplate('comment');
              $email->emailFormat('text');
              $email->setFrom([$smtp['username'] => $smtp['nick']]);
              if($smtp['replyto'] != ''){
                  $email->setReplyTo($smtp['replyto']);
              }
              $email->viewVars(['name'=>$r['first_name'],
                                'comment'=>$what,
                                'from'=>$from,
                                'talk'=>$title,
                                'answer'=>(in_array('id', $r)?$r['id']:""),
                                'event'=>$r['name'],
                                'link'=>$_SERVER['REQUEST_SCHEME']."://".$_SERVER['HTTP_HOST']]);
              $email->setTo($r['email']);
              $email->setSubject("New comment for your proposal for " . $response[0]["name"] ." ".$response[0]['year']);
              $email->send();
          }
          return "Speaker has been notified by email.";
      } else {
          return "Response not found. No email has been sent.";
      }
  }

  public function decrypt($what) {
      return openssl_decrypt($what, $cipher=env("CFP_ENC_METHOD","aes-256-cbc-hmac-sha256"), $cipherKey=env("CAKEPHP_SECRET_TOKEN",NULL), $options=0, $iv=env("CAKEPHP_SECURITY_SALT",NULL));
  }
}
