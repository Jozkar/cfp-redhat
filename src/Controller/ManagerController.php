<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Datasource\ConnectionManager;


/**
 * Static content controller
 *
 * This controller will render views from Template/HomePage/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class ManagerController extends AppController
{

    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Network\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function show(...$action)
    {
        if(!parent::getProgramManager() && !parent::getSuperUser()){
            return $this->redirect("/");
        }

        $connection =  ConnectionManager::get('cfp');

        if(!empty($action)){
            if(isset($_SESSION['token']) && isset($action[2]) && $action[2] == $_SESSION['token']){
                $secCheck = $connection->execute("SELECT 1 FROM managers WHERE event_id = " . $action[1] . " AND user_id like ('" . $_SESSION['loggedUser'] . "')")->rowCount();
                if($secCheck || parent::getAdmin()){
                    if(count($action) < 4 || 
                            (count($action) >= 4 && !is_numeric($action[1])) 
                      ){
                        //not enough arguments error message  + redirect
                        $_SESSION['errorMessage'][] = "Unsufficient data. Your link is probably broken.";
                        return $this->redirect("/manager");
                    }

                    switch($action[0]){                      
                        case "export":
                            return $this->export($connection, $action[1], ($action[4] == "speakers"?1:0), $action[3], ($action[4] == "confirmed"?1:0));
                            break;
                        case "stats":
                            return $this->stats($connection, $action[1], $action[3]);
                            break;
                        case "allproposals":
                            return $this->proposals($connection, $action[1], $action[3]);
                        case "schedule":
                            return $this->schedule($connection, $action[1]);
                        default:
                            $_SESSION['errorMessage'][] = "You are not allowed to do this opperation";
                            return $this->redirect("/manager");
                    }
                } else {
                    $_SESSION['errorMessage'][] = "You are not allowed to do this opperation";
                    return $this->redirect("/manager");
                }
            } else {
                $_SESSION['errorMessage'][] = "You are not allowed to do this opperation. Missing token.";
                return $this->redirect("/manager");
            }
        }

        parent::printFlush($this->request->here());

        $this->set("active", "manager");
        $this->set("admin", parent::getAdmin() | parent::getSuperUser());
        $this->set("reviewer", parent::getReviewer());
        $this->set("program_manager", parent::getProgramManager());       

        if($this->request->session()->read("first-name")){
            $this->set('username', $_SESSION['first-name'] . " " . $_SESSION['last-name']);
        }

        $results = $connection->execute('SELECT id, name, logo, year, url, cfp_open, cfp_close, for_redhat, accommodation, schedule'
            . ' FROM cfp.events, cfp.managers WHERE id = event_id and user_id like ("' . $_SESSION['loggedUser'] . '") ORDER BY id desc')->fetchAll('assoc');
        foreach($results as $i=>$r){
            $results[$i]['forms'] = $connection->execute("SELECT id, type, placeholder FROM forms WHERE event_id = " . $r['id'] . " ORDER BY form_order")->fetchAll("assoc");
            $results[$i]['hasSMTP'] = parent::hasSMTP($r['id']);
        }
        $token = $this->request->getParam('_csrfToken');

        $_SESSION['token'] = $token;
        $this->set("token", $token);
        $this->set("events", $results);
        $this->set("inflector", parent::getInflector());

        try {
            $this->render("home");
        } catch (MissingTemplateException $exception) {
            if (Configure::read('debug')) {
                throw $exception;
            }
            throw new NotFoundException();
        }
    }

    public function export($connection, $event, $type, $form, $confirmed){
        $evinfo = $connection->execute("SELECT name, year, collect_data FROM events WHERE id = " . $event)->fetch("assoc");
        $formId = $connection->execute("SELECT id FROM forms WHERE event_id = " . $event . " AND type like ('" . $form . "')")->fetch("assoc");
        if($type){
            $this->response->download(str_replace(" ", "-", $evinfo['name']) . "-" . $evinfo['year'] . "-" . $form . "-speakers.csv");
            if($form == "session"){
                $users = $connection->execute("SELECT email, first_name, last_name, organization, bio, avatar, position, country FROM users WHERE id in"
                        . " (SELECT user_id FROM responses WHERE id IN (SELECT response_id FROM responses_to_topics, accepts as a where id = a.response_to_topic_id AND"
                        . " a.vote = 1 AND a.event_id = " . $event . " )) OR id IN (SELECT user_id FROM additional_speakers WHERE response_id IN "
                        . "(SELECT response_id FROM responses_to_topics, accepts as a where id = a.response_to_topic_id AND a.vote = 1 AND a.event_id = " . $event . " ))")->fetchAll("assoc");
            } else {
                $users = $connection->execute("SELECT email, first_name, last_name, organization, bio, avatar, position, country FROM users WHERE id in"
                        . " (SELECT user_id FROM answers WHERE id IN (SELECT answer_id FROM accepts WHERE"
                        . " vote = 1 AND event_id = " . $event . " ) AND form_id = " . $formId['id'] . ") OR id IN (SELECT user_id FROM additional_speakers WHERE answer_id IN "
                        . "(SELECT answer_id FROM accepts as a, answers where vote = 1 AND a.event_id = " . $event . " and form_id = " . $formId['id'] . "))")->fetchAll("assoc");
            }
            $this->set("data", $users);
        }else{
            $this->response->download(str_replace(" ", "-", $evinfo['name']) . "-" . $evinfo['year'] . "-" . $form . ($confirmed?"-confirmed":"") . "-proposals.csv");
            if($form == "session"){
                $sessions = $connection->execute("SELECT r.id, r.title, r.type, r.duration, r.difficulty, r.abstract, r.summary, r.notes, r.notes2, r.notes3, r.keywords, r.answer_id, r.user_id as speakers"
                        . ", GROUP_CONCAT(t.name SEPARATOR ', ') as topic FROM"
                        . " responses as r, accepts as a, responses_to_topics as c, topics as t WHERE a.vote = 1 AND a.response_to_topic_id = c.id" . ($confirmed?" AND r.confirmed = 1":"")
                        . " AND c.response_id = r.id AND r.event_id = " . $event . " AND r.event_id = a.event_id AND c.topic_id = t.id GROUP BY r.id")->fetchAll("assoc");
                foreach($sessions as $k => $s){
                    $primary = $connection->execute("SELECT CONCAT(first_name, ' ', last_name) as name FROM users WHERE id like '" . $s['speakers'] . "'")->fetchAll("assoc");
                    $secondary = $connection->execute("SELECT CONCAT(first_name, ' ', last_name) as name FROM users WHERE id IN (SELECT user_id FROM additional_speakers WHERE response_id = " . $s['id'] . ")")->fetchAll("assoc");
                    $primary = array_column($primary, "name");
                    $secondary = array_column($secondary, "name");
                    $sessions[$k]['speakers'] = array_merge($primary,$secondary);

                    if($s['answer_id'] != ""){
                        $answers = $connection->execute('SELECT q.question, ap.text FROM questions as q, answer_parts as ap, answers as a WHERE a.id = ' . $s['answer_id'] .
                                ' AND a.form_id = q.form_id and ap.question_id = q.id AND ap.answer_id = a.id')->fetchAll('assoc');
                        $sessions[$k]['answers'] = $answers;
                    }

                    if($evinfo['collect_data']){
                        $collectdata = $connection->execute('SELECT q.question, ap.text FROM questions as q, answer_parts as ap, answers as a WHERE a.collect_response_id = ' . $s['id'] .
                                ' AND a.form_id = q.form_id and ap.question_id = q.id AND ap.answer_id = a.id')->fetchAll('assoc');
                        if(count($collectdata) < 1){
                            $collectdata = $connection->execute('SELECT q.question, "-" as text FROM questions as q WHERE q.form_id = (SELECT IFNULL(id, 0) as id FROM forms WHERE type like "collect" AND event_id = ' . $event . ')' .
                                ' ORDER BY question_order')->fetchAll('assoc');
                        }

                        if(isset($sessions[$k]['answers'])){
                            $sessions[$k]['answers'] = array_merge($answers, $collectdata);
                        } else {
                            $sessions[$k]['answers'] = $collectdata;
                        }
                    }
                }
            } else {
                $sessions = $connection->execute("SELECT a.id, a.user_id as speakers FROM answers as a, accepts as c"
                        . " WHERE c.answer_id = a.id AND c.vote = 1 AND a.form_id = " . $formId['id'] . ($confirmed?" AND a.confirmed = 1":""))->fetchAll("assoc");
                foreach($sessions as $k => $s){
                    $primary = $connection->execute("SELECT CONCAT(first_name, ' ', last_name) as name FROM users WHERE id like '" . $s['speakers'] . "'")->fetchAll("assoc");
                    $secondary = $connection->execute("SELECT CONCAT(first_name, ' ', last_name) as name FROM users WHERE id IN (SELECT user_id FROM additional_speakers WHERE answer_id = " . $s['id'] . ")")->fetchAll("assoc");
                    $primary = array_column($primary, "name");
                    $secondary = array_column($secondary, "name");
                    $sessions[$k]['speakers'] = array_merge($primary,$secondary);

                    $answers = $connection->execute('SELECT q.question, q.take_as, ap.text FROM questions as q, answer_parts as ap, answers as a WHERE a.id = ' . $s['id'] .
                            ' AND a.form_id = q.form_id and ap.question_id = q.id AND ap.answer_id = a.id')->fetchAll('assoc');
                            
                    foreach($answers as $i=>$r){
                		if($r['take_as'] == 'topics'){
                            $curTops = $connection->execute('SELECT GROUP_CONCAT(" ", name) as name FROM topics WHERE id IN(' . $r['text'] . ')')->fetch("assoc");
                			$answers[$i]['text'] = $curTops['name'];
                		}
                		if($r['take_as'] == 'accommodation'){
                		    if($form == "accommodation"){
                                $curRooms = $connection->execute('SELECT CONCAT(h.name, " - ", r.name, " (", r.full_price, ")") as name FROM hotels as h, hotel_rooms as r WHERE r.id IN (' . $r['text'] . ') and r.hotel_id = h.id')->fetch("assoc");
                            } else {
                                $curRooms = $connection->execute('SELECT CONCAT(h.name, " - ", r.name, " (", r.covered_price, ")") as name FROM hotels as h, hotel_rooms as r WHERE r.id IN (' . $r['text'] . ') and r.hotel_id = h.id')->fetch("assoc");
                            }
                			$answers[$i]['text'] = $curRooms['name'];
                		}
                    }
                    $sessions[$k]['answers'] = $answers;

                    if($evinfo['collect_data']){
                        $collectdata = $connection->execute('SELECT q.question, ap.text FROM questions as q, answer_parts as ap, answers as a WHERE a.collect_answer_id = ' . $s['id'] .
                            ' AND a.form_id = q.form_id and ap.question_id = q.id AND ap.answer_id = a.id')->fetchAll('assoc');
                        if(count($collectdata) < 1){
                            $collectdata = $connection->execute('SELECT q.question, "-" as text FROM questions as q WHERE q.form_id = (SELECT IFNULL(id, 0) as id FROM forms WHERE type like "collect" AND event_id = ' . $event . ')' .
                                ' ORDER BY question_order')->fetchAll('assoc');
                        }

                        $sessions[$k]['answers'] = array_merge($answers, $collectdata);
                    }
                }    			
            }
            $this->set("data", $sessions);
        }

        $this->set("type", $type);
        $this->layout = 'ajax';
        $this->render("export");
        return;
    }

    public function stats($connection, $event, $form){
        $evinfo = $connection->execute("SELECT name, year FROM events WHERE id = " . $event)->fetchAll("assoc")[0];
        $fid = $connection->execute("SELECT id FROM forms WHERE event_id = " . $event . " AND type like ('" . $form . "')")->fetch("assoc");
        if($form == "session"){
            $stats = $connection->execute("SELECT r.id, r.title, u.first_name, u.last_name, t.name, sum(v.vote) as vote, a.vote as accept FROM users as u, responses as r, topics as t, responses_to_topics as rt, votes as v, accepts as a ".
                    "WHERE r.user_id = u.id and r.event_id = " . $event . " and rt.topic_id = t.id and rt.response_id = r.id and ".
                    "rt.id = v.response_to_topic_id and a.response_to_topic_id = rt.id group by r.id, t.name order by r.id asc, vote desc")->fetchAll('assoc');
        } else {
            $stats = $connection->execute("SELECT an.id, ap.text as title, u.first_name, u.last_name, sum(v.vote) as vote, a.vote as accept FROM users as u, answers as an, answer_parts as ap, votes as v, accepts as a ".
                     "WHERE an.user_id = u.id and an.event_id = " . $event . " and an.form_id = " .  $fid['id'] . " and an.id = ap.answer_id and ap.as_title = 1 and v.answer_id = an.id and ".
                     "a.answer_id = an.id group by an.id order by an.id asc, vote desc")->fetchAll('assoc');
        }
        $this->response->download(str_replace(" ", "-", $evinfo['name']) . "-" . $evinfo['year'] . "-" . $form . "-stats.csv");
        $this->set("data", $stats);
        $this->set("form", $form);
        $this->layout = 'ajax';
        $this->render("stats");
        return;
    }

    public function proposals($connection, $event, $form){
        $evinfo = $connection->execute("SELECT name, year, collect_data FROM events WHERE id = " . $event)->fetch("assoc");
        $formId = $connection->execute("SELECT id FROM forms WHERE event_id = " . $event . " AND type like ('" . $form . "')")->fetch("assoc");
        $topics = $connection->execute("SELECT topic_id, name FROM topics_to_events, topics WHERE id = topic_id AND event_id = " . $event)->fetchAll("assoc");
        $questions = $connection->execute("SELECT id, question, take_as FROM questions WHERE form_id = " . $formId['id'] . " AND type NOT LIKE ('description') ORDER BY question_order")->fetchAll("assoc");
        if($evinfo['collect_data']){
            $collectquestions = $connection->execute("SELECT CONCAT('9',id) as id, question,'self' as take_as FROM questions WHERE form_id = (SELECT IFNULL(id, 0) AS id FROM forms WHERE type LIKE ('collect') AND event_id = " . $event . ")" .
                " ORDER BY question_order")->fetchAll('assoc');
            $questions = array_merge($questions, $collectquestions);
        }

        if($form == "session"){
            $others = $connection->execute('SELECT r.id, r.title, r.type, r.duration, r.difficulty, r.abstract, r.summary, r.notes, r.notes2, r.notes3, r.keywords, r.answer_id,'
                    . ' CONCAT(u.first_name, " ", u.last_name, " [", u.email, "]") as speakers, GROUP_CONCAT(t.name SEPARATOR ", ") as topics FROM'
                    . ' cfp.topics as t, cfp.users as u, cfp.responses as r, cfp.responses_to_topics as c WHERE u.id = r.user_id AND c.event_id = ' . $event 
                    . ' AND r.id = c.response_id AND t.id = c.topic_id group by r.id ORDER BY c.id')->fetchAll('assoc');
            foreach($others as $key=>$o){
                $additional = $connection->execute('SELECT CONCAT(u.first_name, " ", u.last_name, " [", u.email, "]") as additional from users u, additional_speakers a WHERE u.id = a.user_id AND a.response_id = '
                        . $others[$key]['id'])->fetchAll('assoc');
                if (is_array($additional) && count($additional) > 0){
                    $others[$key]['speakers'] .= " (main speaker)";
                    foreach($additional as $k => $a) {
                        $others[$key]['speakers'] .= ", " . $a['additional'];
                    }
                }
                if($o['answer_id'] != ""){
                    $answers = $connection->execute('SELECT ap.question_id, ap.text FROM questions as q, answer_parts as ap, answers as a WHERE a.id = ' . $o['answer_id'] .
                            ' AND a.form_id = q.form_id and ap.question_id = q.id AND ap.answer_id = a.id')->fetchAll('assoc');
                    $others[$key]['answers'] = $answers;
                }

                if($evinfo['collect_data']){
                    $collectdata = $connection->execute('SELECT CONCAT("9",ap.question_id) as question_id, ap.text FROM questions as q, answer_parts as ap, answers as a WHERE a.collect_response_id = ' . $o['id'] .
                        ' AND a.form_id = q.form_id and ap.question_id = q.id AND ap.answer_id = a.id')->fetchAll('assoc');
                    if(isset($others[$key]['answers'])){
                        $others[$key]['answers'] = array_merge($answers, $collectdata);
                    } else {
                        $others[$key]['answers'] = $collectdata;
                    }
                }
            }
        } else {
            $others = $connection->execute("SELECT a.id, a.user_id as speakers FROM answers as a WHERE a.form_id = " . $formId['id'])->fetchAll("assoc");
            foreach($others as $k => $s){
                $primary = $connection->execute("SELECT CONCAT(first_name, ' ', last_name, ' [', email, ']') as name FROM users WHERE id like '" . $s['speakers'] . "'")->fetchAll("assoc");
                $secondary = $connection->execute("SELECT CONCAT(first_name, ' ', last_name, ' [', email, ']') as name FROM users WHERE id IN (SELECT user_id FROM"
                        . " additional_speakers WHERE answer_id = " . $s['id'] . ")")->fetchAll("assoc");
                $primary = array_column($primary, "name");
                $secondary = array_column($secondary, "name");
                if(count($secondary) > 0){
                    $primary[0] .= " (main speaker)";
                }
                $others[$k]['speakers'] = implode(",", array_merge($primary,$secondary));
                
                $answers = $connection->execute('SELECT ap.question_id, q.take_as, ap.text FROM questions as q, answer_parts as ap, answers as a WHERE a.id = ' . $s['id'] .
                            ' AND a.form_id = q.form_id and ap.question_id = q.id AND ap.answer_id = a.id')->fetchAll('assoc');
                foreach($answers as $i=>$r){
            		if($r['take_as'] == 'topics'){
                        $curTops = $connection->execute('SELECT GROUP_CONCAT(" ", name) as name FROM topics WHERE id IN(' . $r['text'] . ')')->fetch("assoc");
            			$answers[$i]['text'] = $curTops['name'];
            		}
            		if($r['take_as'] == 'accommodation'){
            		    if($form == "accommodation"){
                            $curRooms = $connection->execute('SELECT CONCAT(h.name, " - ", r.name, " (", r.full_price, ")") as name FROM hotels as h, hotel_rooms as r WHERE r.id IN (' . $r['text'] . ') and r.hotel_id = h.id')->fetch("assoc");
                        } else {
                            $curRooms = $connection->execute('SELECT CONCAT(h.name, " - ", r.name, " (", r.covered_price, ")") as name FROM hotels as h, hotel_rooms as r WHERE r.id IN (' . $r['text'] . ') and r.hotel_id = h.id')->fetch("assoc");
                        }
            			$answers[$i]['text'] = $curRooms['name'];
            		}
                }
                $others[$k]['answers'] = $answers;

                if($evinfo['collect_data']){
                    $collectdata = $connection->execute('SELECT CONCAT("9",ap.question_id) as question_id, ap.text FROM questions as q, answer_parts as ap, answers as a WHERE a.collect_answer_id = ' . $s['id'] .
                        ' AND a.form_id = q.form_id and ap.question_id = q.id AND ap.answer_id = a.id')->fetchAll('assoc');
                    $others[$k]['answers'] = array_merge($answers, $collectdata);
                }

            }
        }
        $this->set("topics", $topics);
        $this->set("data", $others);
        $this->set("form", $form);
        $this->set("questions", $questions);
        $this->response->download(str_replace(" ", "-", $evinfo['name']) . "-" . $evinfo['year'] . "-" . $form . "-all-proposals.csv");
        $this->layout = 'ajax';
        $this->render("proposals");
        return;
    }
    
    public function schedule($connection, $event){
        $evinfo = $connection->execute("SELECT name, year, schedule, open, close, timezone, schedule_version FROM events WHERE id = " . $event)->fetch("assoc");
               
        $roomsOrigin = $connection->execute('SELECT * FROM cfp.rooms WHERE event_id = ' . $event)->fetchAll('assoc');
        $rooms = array();
        foreach($roomsOrigin as $key => $val){
            $rooms[$val['id']] = $val['name'];
        }
        
        $placedResp = $connection->execute("SELECT s.id, s.room_id, s.date, s.start, s.end, r.id as response_id, r.title, r.duration, r.abstract as description, CONCAT(u.first_name,' ',u.last_name) as speaker,"
            . " u.avatar, u.organization, u.bio, t.name as topic FROM"
            . " responses as r, users as u, topics as t, responses_to_topics as rt, schedule as s WHERE"
            . " r.id IN (SELECT response_id FROM schedule WHERE event_id = " . $event . " and room_id is NOT NULL and version = " . $evinfo['schedule_version'] . ") AND r.event_id = " . $event
            . " AND u.id = r.user_id AND rt.response_id = r.id AND rt.topic_id = t.id AND r.id = s.response_id and s.version = " . $evinfo['schedule_version'])->fetchAll("assoc");

        $placedAnsw = $connection->execute("SELECT s.id, s.room_id, s.date, s.start, s.end, a.id as answer_id, CONCAT(u.first_name,' ',u.last_name) as speaker, u.avatar, u.organization, u.bio, f.type as topic"
            . " FROM answers as a, users as u, schedule as s, forms as f WHERE"
            . " a.id IN (SELECT answer_id FROM schedule WHERE event_id = " . $event . " and room_id is NOT NULL and version = " . $evinfo['schedule_version'] . ") AND a.event_id = " . $event
            . " AND u.id = a.user_id AND a.form_id = f.id AND a.id = s.answer_id and s.version = " . $evinfo['schedule_version'])->fetchAll("assoc");

        foreach($placedAnsw as $k=>$ua){
            $title = $connection->execute("SELECT IFNULL(text,'') as text FROM answer_parts WHERE answer_id = " . $ua['answer_id'] . " AND as_title = 1")->fetch("assoc");
            $placedAnsw[$k]['title'] = $title['text'];

            $duration = $connection->execute("SELECT IFNULL(text,5) as text FROM answer_parts WHERE answer_id = " . $ua['answer_id'] . " AND as_duration = 1")->fetch("assoc");
            $placedAnsw[$k]['duration'] = intval($duration['text']);

            $description = $connection->execute("SELECT IFNULL(text,'') as text FROM answer_parts WHERE answer_id = " . $ua['answer_id'] . " AND as_description = 1")->fetch("assoc");
            $placedAnsw[$k]['description'] = $description['text'];
        }
     
        $this->set("Responses", $placedResp);
        $this->set("Answers", $placedAnsw);
        $this->set("event", $evinfo);
        $this->set("rooms", $rooms);
        
        $this->response->download(str_replace(" ", "-", $evinfo['name']) . "-" . $evinfo['year'] . "-schedule.csv");
        $this->layout = 'ajax';
        $this->render("schedule");
        return;
    }
}
