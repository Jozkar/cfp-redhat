<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Datasource\ConnectionManager;

/**
 * Static content controller
 *
 * This controller will render views from Template/HomePage/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class ManageController extends AppController
{

    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Network\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function show(...$action)
    {
        if(!parent::getProgramManager() && !parent::getSuperUser()){
    		return $this->redirect("/");
    	}
    	
    	if(count($action) < 1 or !is_numeric($action[0])){
    		$_SESSION['errorMessage'][] = "Unsufficient data. Your link is probably broken.";
    		return $this->redirect("/manager");
    	}

    	if(!parent::checkFormType($action[1])){
    		$_SESSION['errorMessage'][] = "You don't have access to that review process. Permission denied.";
    		return $this->redirect("/reviewer");
    	}

    	parent::printFlush($this->request->here());
    	$this->set("active", "manager");
    	$this->set("admin", parent::getAdmin() | parent::getSuperUser());
    	$this->set("reviewer", parent::getReviewer());
    	$this->set("program_manager", parent::getProgramManager());

    	if($this->request->session()->read("first-name")){
    		$this->set('username', $_SESSION['first-name'] . " " . $_SESSION['last-name']);
    	}
    
    	$connection = ConnectionManager::get('cfp');
    
        $form = $connection->execute('SELECT type, placeholder FROM forms WHERE type = "' . $action[1] . '" and event_id = ' . $action[0])->fetch("assoc");
   
        $this->set("form", $form);
        $this->set("inflector", parent::getInflector());

    	$myEvent = $connection->execute('SELECT 1 FROM managers WHERE user_id like ("' . $_SESSION['loggedUser'] . '") and event_id = ' . $action[0])->fetchAll('assoc'); 

    	if(!parent::getSuperUser() && $myEvent != '' && count($myEvent) < 1){
    		$_SESSION['errorMessage'][] = "You aren't allowed to do this operation.";
    		return $this->redirect("/manager");
    	}

    	$event = $connection->execute('SELECT name, year, GROUP_CONCAT(topic_id) AS topics FROM cfp.events, cfp.topics_to_events WHERE id  = ' . $action[0] . ' AND event_id = id')->fetchAll('assoc');
    	if($action[1] == "session"){
    		$myTopics = $connection->execute('SELECT id, name FROM cfp.topics WHERE id IN (' . $event[0]['topics'] . ')')->fetchAll('assoc');
    		$responses = $connection->execute(
    			'SELECT 
    				r.id as response_id,
    				r.title,
    				r.type,
    				r.duration,
    				r.difficulty,
    				r.abstract,
    				r.summary,
    				r.keywords,
    				r.confirmed,
    				r.answer_id,
    				c.id,
    				f.id as form_id,
    				t.id as topic_id,
    				t.name as topic,
    				CONCAT(u.first_name, " ", u.last_name, " (", u.email, ")") as speaker,
    				u.country,
    				(SELECT 
    					GROUP_CONCAT(tp.name SEPARATOR ", ") 
    				 FROM 
    					cfp.topics as tp,
    					cfp.responses_to_topics as re 
    				 WHERE 
    					re.topic_id = tp.id
    					AND re.response_id = r.id
    				) as topics
    			 FROM 
    				cfp.topics as t,
    				cfp.users as u,
    				cfp.forms as f,
    				cfp.responses as r,
    				cfp.responses_to_topics as c 
    			LEFT JOIN
    				cfp.votes as v
    			ON
    				c.id = v.response_to_topic_id
    			WHERE 
    				u.id = r.user_id 
    				AND c.event_id = ' . $action[0] .'
    				AND r.id = c.response_id
    				AND t.id = c.topic_id
    				AND r.id = c.response_id 
    				AND f.event_id = c.event_id
    				AND f.type like ("session")
    			GROUP BY c.id 
    			ORDER BY t.name, c.id')->fetchAll('assoc');

            $scoreSimple = $connection->execute('SELECT sum(vote) as score, response_to_topic_id as id FROM votes WHERE event_id = ' . $action[0] . ' AND captain = 0 GROUP BY id')->fetchAll("assoc");
            $scoreCaptain = $connection->execute('SELECT response_to_topic_id as id, vote as captain FROM votes WHERE captain = 1 AND event_id = ' . $action[0])->fetchAll('assoc');

    		$votes = $connection->execute('SELECT response_to_topic_id as response_id, vote FROM cfp.accepts WHERE event_id = ' . $action[0])->fetchAll('assoc');

    	} else {
    		$answers = $connection->execute('SELECT 
                                                a.id, 
                                                CONCAT(u.first_name, " ", u.last_name, " (", u.email, ")") as speaker, 
                                                a.form_id,
    						                    a.confirmed,
                                                u.country,
                                                (SELECT
                                                    sum(vote)
                                                 FROM
                                                    votes as vts
                                                 WHERE
                                                    vts.answer_id = a.id
                                                    AND vts.captain = 0
                                                ) as score,
                                                (SELECT 
                                                    GROUP_CONCAT(" ", CONCAT(rev.first_name, " ", rev.last_name, " (", vt.vote, ")")) 
                                                 FROM 
                                                    cfp.users as rev,
                                                    cfp.votes as vt
                                                 WHERE
                                                    rev.id = vt.reviewer_id
                                                    AND vt.captain = 0
                                                    AND a.id = vt.answer_id
                                                ) as votes,
                                                (SELECT 
                                                    vo.vote 
    	                                         FROM 
                                                    votes as vo 
    	                                         WHERE 
                                                    vo.answer_id = a.id 
                                                    AND vo.captain = 1 
                                                    AND vo.event_id = ' . $action[0] .' 
    	                                        ) as captainscore

                                                 FROM forms as f, users as u, answers as a LEFT JOIN votes as v ON v.answer_id = a.id WHERE 
                                                 f.id = a.form_id AND f.type like ("' . $action[1] . '") and a.user_id = u.id and f.event_id = ' . $action[0]. ' 
                                                 GROUP BY a.id ORDER BY a.id')->fetchAll('assoc');

    		$responses = array();

    		foreach ($answers as $a){
    			$res = $connection->execute('SELECT q.question, ap.text, ap.as_title, q.take_as FROM questions as q, answer_parts as ap WHERE ap.answer_id = ' . $a['id'] .
    				' AND ap.question_id = q.id and q.form_id = ' . $a['form_id'])->fetchAll('assoc');
    			$newRes = array("answer_id"=>$a['id'], "speaker"=>$a['speaker'], "country"=>$a['country'], "score"=>$a['score'], "votes"=>$a['votes'], "captainscore"=>$a['captainscore'], "form_id"=>$a['form_id'],
    					"confirmed"=>$a['confirmed']);
    			foreach($res as $r){
                    if($r['as_title'] == 1){
        				$newRes['Title'] = $r['text'];
                    } else if($r['take_as'] == 'topics'){
                            $curTops = $connection->execute('SELECT GROUP_CONCAT(" ", name) as name FROM topics WHERE id IN(' . $r['text'] . ')')->fetch("assoc");
            				$newRes[$r['question']] = $curTops['name'];
                        } else if($r['as_duration'] == 1){
                            $newRes['Duration'] = $r['text'];
                        } else {
            				$newRes[$r['question']] = $r['text'];
                        }
                    }
    			
    			$responses[] = $newRes;
    		}
    		$votes = $connection->execute('SELECT answer_id, vote FROM cfp.accepts WHERE event_id = ' . $action[0])->fetchAll('assoc');
    	}
    	$token = $this->request->getParam('_csrfToken');

    	$accepted = array();
    	$rejected = array();
    	$waitlist = array();
    	$unreviewed = array();

    	
    	foreach($responses as $r){
            $added = false;
    		if($action[1] == "session"){
    			$r['comments'] = $connection->execute('SELECT author, comment, time, public FROM cfp.comments WHERE response_id = ' . $r['response_id'] . ' ORDER BY id desc')->fetchAll('assoc');
                $r['score'] = "";
                $r['captainscore'] = "";
                
                $voteslist = $connection->execute('SELECT GROUP_CONCAT(" ", CONCAT(r.first_name, " ", r.last_name, " (", v.vote, ")")) as votes FROM users as r, votes as v WHERE r.id = v.reviewer_id'.
                                                ' AND v.captain = 0 AND v.response_to_topic_id = ' . $r['id'])->fetch('assoc');

                $r['votes'] = $voteslist['votes'];

                foreach($scoreSimple as $s){
                    if($s['id'] == $r['id']){
                        $r['score'] = $s['score'];
                    }
                }

                foreach($scoreCaptain as $s){
                    if($s['id'] == $r['id']){
                        switch($s['captain']){
                            case 1: $r['captainscore'] = "Backup";
                                    break;
                            case 2: $r['captainscore'] = "Waitlist";
                                    break;
                            case -1: $r['captainscore'] = "Rejected";
                                     break;
                            case 3: $r['captainscore'] = "Accepted";
                                    break;
                        }
                    }
                }

    			$additional = $connection->execute('SELECT CONCAT(u.first_name, " ", u.last_name, " (", u.email, ")") as additional from users u, additional_speakers a WHERE u.id = a.user_id AND'
    				. ' a.response_id = ' . $r['response_id'])->fetchAll('assoc');
    			if (is_array($additional) && count($additional) > 0){
    				$r['speaker'] .= " (main speaker)";
    				foreach($additional as $k => $a) {
    					$r['speaker'] .= ", " . $a['additional'];
    				}
    			}

    			if($r['answer_id'] != ""){
    				$answers = $connection->execute('SELECT q.question, ap.text FROM questions as q, answer_parts as ap, answers as a WHERE a.id = ' . $r['answer_id'] .
    					' AND a.form_id = q.form_id and ap.question_id = q.id AND ap.answer_id = a.id')->fetchAll('assoc');
    				$r['answers'] = $answers;
    			}

    			foreach($votes as $v){
    				if($v['response_id'] == $r['id']){
    					$added = true;			
    					if ($v['vote'] > 0){
    						$accepted[$r['topic_id']][] = $r;
    					}else if($v['vote'] < 0){
    						$rejected[$r['topic_id']][] = $r;
    					} else {
                            $waitlist[$r['topic_id']][] = $r;
                        }
    				} 
    			}
    			if(!$added){
    					$unreviewed[$r['topic_id']][] = $r;
    			}
    		} else {
                switch($r['captainscore']){
                    case 1: $r['captainscore'] = "Backup";
                            break;
                    case 2: $r['captainscore'] = "Waitlist";
                            break;
                    case -1: $r['captainscore'] = "Rejected";
                             break;
                    case 3: $r['captainscore'] = "Accepted";
                            break;
                }

    			$r['comments'] = $connection->execute('SELECT author, comment, time, public FROM cfp.comments WHERE answer_id = ' . $r['answer_id'] . ' ORDER BY id desc')->fetchAll('assoc');
    			$additional = $connection->execute('SELECT CONCAT(u.first_name, " ", u.last_name, " (", u.email, ")") as additional from users u, additional_speakers a WHERE u.id = a.user_id AND a.answer_id = '
    				. $r['answer_id'])->fetchAll('assoc');
    			if (is_array($additional) && count($additional) > 0){
    				$r['speaker'] .= " (main speaker)";
    				foreach($additional as $k => $a) {
    					$r['speaker'] .= ", " . $a['additional'];
    				}
    			}
    			foreach($votes as $v){
    				if($v['answer_id'] == $r['answer_id']){
    					$added = true;
    					if ($v['vote'] > 0){
    						$r['vote'] = $v['vote'];
    						$accepted[] = $r;
    					}else if($v['vote'] < 0){
    						$r['vote'] = $v['vote'];
    						$rejected[] = $r;
    					}else{
                            $r['vote'] = $v['vote'];
                            $waitlist[] = $r;
                        }
    				}
    			}
    			if(!$added){
    				$unreviewed[] = $r;
    			}
    			$myTopics = null;
    		}
    	}
    
    	$this->request->session()->write("token", $token);
        $this->set("token", $token);
        $this->set("topics", $myTopics);
    	$this->set("eventInfo", $event[0]);		
    	$this->set("event", $action[0]);
    	$this->set("accepted", $accepted);
    	$this->set("rejected", $rejected);
    	$this->set("waitlist", $waitlist);
    	$this->set("unreviewed", $unreviewed);
    	$this->set("manager", $_SESSION['loggedUser']);

    	try {
    		if($action[1] == "session"){
    			$this->render("home");
    		} else {
    			$this->render("answers");
    		}
            } catch (MissingTemplateException $exception) {
                if (Configure::read('debug')) {
                    throw $exception;
                }
                throw new NotFoundException();
            }
    }
}
