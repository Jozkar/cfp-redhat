<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Datasource\ConnectionManager;

/**
 * Static content controller
 *
 * This controller will render views from Template/HomePage/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class TopicFixController extends AppController
{

    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Network\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function show(...$action)
    {
    if(!parent::getProgramManager() && !parent::getSuperUser()){
    	return $this->redirect("/");
    }
    	
    if(count($action) < 1 or !is_numeric($action[0])){
    	$_SESSION['errorMessage'][] = "Unsufficient data. Your link is probably broken.";
    	return $this->redirect("/manager");
    }	

    parent::printFlush($this->request->here());

    $this->set("active", "manager");
    $this->set("admin", parent::getAdmin() | parent::getSuperUser());
    $this->set("reviewer", parent::getReviewer());
    $this->set("program_manager", parent::getProgramManager());       
    
    if($this->request->session()->read("first-name")){
            $this->set('username', $_SESSION['first-name'] . " " . $_SESSION['last-name']);
    }

    $connection = ConnectionManager::get('cfp');

    $myEvent = $connection->execute('SELECT 1 FROM managers WHERE user_id like ("' . $_SESSION['loggedUser'] . '") and event_id = ' . $action[0])->fetchAll('assoc');

    if(!parent::getSuperUser() && $myEvent != '' && count($myEvent) < 1){
                $_SESSION['errorMessage'][] = "You aren't allowed to do this operation.";
                return $this->redirect("/manager");
        }

    $event = $connection->execute('SELECT name, year, GROUP_CONCAT(topic_id) AS topics FROM cfp.events, cfp.topics_to_events WHERE id  = ' . $action[0] . ' AND event_id = id')->fetchAll('assoc');


    $topics = $connection->execute('SELECT id, name FROM cfp.topics WHERE id IN (' . $event[0]['topics'] . ')')->fetchAll('assoc');
    $others = $connection->execute('SELECT r.id, r.title, r.type, r.duration, r.difficulty, r.abstract, r.summary, r.answer_id, r.notes, r.notes2, r.notes3, r.keywords, CONCAT(u.first_name, " ", u.last_name, " (", u.email, ")") as speaker, GROUP_CONCAT(t.name SEPARATOR ", ") as topic, GROUP_CONCAT(t.id SEPARATOR ",") as topic_id, f.id as form_id FROM cfp.forms as f, cfp.topics as t, cfp.users as u, cfp.responses as r, cfp.responses_to_topics as c WHERE u.id = r.user_id AND c.event_id = ' . $action[0] .' AND r.id = c.response_id AND t.id = c.topic_id AND f.event_id = c.event_id AND f.type like ("session") group by r.id ORDER BY c.id')->fetchAll('assoc');


    $token = $this->request->getParam('_csrfToken');

    $accepted = array();
    $rejected = array();
    $unreviewed = array();

    foreach($others as $key=>$o){
    	$others[$key]['comments'] = $connection->execute('SELECT author, comment, time, public FROM cfp.comments WHERE response_id = ' . $o['id'] . ' ORDER BY id desc')->fetchAll('assoc');
    	$additional = $connection->execute('SELECT CONCAT(u.first_name, " ", u.last_name, " (", u.email, ")") as additional from users u, additional_speakers a WHERE u.id = a.user_id AND'
    		.' a.response_id = ' . $others[$key]['id'])->fetchAll('assoc');
                if (is_array($additional) && count($additional) > 0){
                        $others[$key]['speaker'] .= " (main speaker)";
                        foreach($additional as $k => $a) {
                                $others[$key]['speaker'] .= ", " . $a['additional'];
                        }
    	}

    	$note = $connection->execute('SELECT q.question FROM questions as q, forms as f WHERE take_as like ("notes") and form_id = f.id and f.event_id = ' . $action[0] . ' and f.type like ("session")')->fetchAll('assoc');

    	if(count($note) > 0){
    		$others[$key][$note[0]['question']] = $o['notes'];
    	}
                       unset($others[$key]['notes']);
    	$note = $connection->execute('SELECT q.question FROM questions as q, forms as f WHERE take_as like ("notes2") and form_id = f.id and f.event_id = ' . $action[0] . ' and f.type like ("session")')->fetchAll('assoc');
    	if(count($note) > 0){
    		$others[$key][$note[0]['question']] = $o['notes2'];
    	}
    	unset($others[$key]['notes2']);
    	$note = $connection->execute('SELECT q.question FROM questions as q, forms as f WHERE take_as like ("notes3") and form_id = f.id and f.event_id = ' . $action[0] . ' and f.type like ("session")')->fetchAll('assoc');
    	if(count($note) > 0){
    		$others[$key][$note[0]['question']] = $o['notes3'];
    	}
    	unset($others[$key]['notes3']);

    	if($o['answer_id'] != ""){
    		$answers = $connection->execute('SELECT q.question, ap.text FROM questions as q, answer_parts as ap, answers as a WHERE a.id = ' . $o['answer_id'] .
    			' AND a.form_id = q.form_id and ap.question_id = q.id AND ap.answer_id = a.id')->fetchAll('assoc');
    		$others[$key]['answers'] = $answers;
    	}
    }

    $this->request->session()->write("token", $token);
        $this->set("token", $token);
        $this->set("topics", $topics);
    $this->set("eventInfo", $event[0]);		
    $this->set("event", $action[0]);
    $this->set("manager", $_SESSION['loggedUser']);
    $this->set("responses", $others);

    try {
            $this->render("home");
        } catch (MissingTemplateException $exception) {
            if (Configure::read('debug')) {
                throw $exception;
            }
            throw new NotFoundException();
        }
    }
}
