<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Datasource\ConnectionManager;
/**
 * Static content controller
 *
 * This controller will render views from Template/HomePage/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class AcceptProposalsController extends AppController
{

    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Network\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function show(...$path)
    {

        parent::printFlush($this->request->here());

        $this->set("active", "accept");
        $this->set("admin", parent::getAdmin() | parent::getSuperUser());
        $this->set("reviewer", parent::getReviewer());
        $this->set("program_manager", parent::getProgramManager());
        $connection = ConnectionManager::get('cfp');

        if($this->request->session()->read("first-name")){
        	$this->set('username', $_SESSION['first-name'] . " " . $_SESSION['last-name']);
        }

        if(count($path) > 0 && is_numeric($path[0])){
    	    //show form for confirmation
        	// TODO: additional speakers
        	$form = $connection->execute('SELECT id FROM cfp.forms WHERE event_id = ' . $path[0] . ' and type like ("confirmation")')->fetchAll('assoc');
        	$event = $connection->execute('SELECT name, year, id FROM events WHERE id = ' . $path[0])->fetchAll('assoc');
        	$exists = $connection->execute("SELECT 1 FROM cfp.answers WHERE user_id like '" . $_SESSION['loggedUser'] . "' AND form_id = " . $form[0]['id'] . " AND event_id = " . $path[0])->rowCount();
    
        	if(!$exists){
        		$questions = $connection->execute('SELECT * FROM cfp.questions WHERE form_id = ' . $form[0]['id'] . ' ORDER BY question_order')->fetchAll('assoc');

        		$options = $connection->execute('SELECT * FROM cfp.question_options WHERE form_id = ' . $form[0]['id'] . ' ORDER BY question_id, id')->fetchAll('assoc');

    	    	foreach($questions as $i=>$q){
    		    	$questions[$i]['options'] = array();
    			    foreach($options as $o){
    				    if($q['id'] == $o['question_id']){
    					    $questions[$i]['options'][] = $o;
        				}
        			}
    	    	}
        	} else {
        		$questions = $options = null;
    	    }

        	$answers = $connection->execute("SELECT a.id, f.type, f.placeholder FROM answers as a, forms as f, accepts as c" .
        		" WHERE c.answer_id = a.id AND c.vote = 1 AND a.confirmed = 0 AND a.informed = 1 AND f.id = a.form_id AND a.event_id = " . $path[0] . " AND a.user_id like ('" . $_SESSION['loggedUser'] . "')")
    	    	->fetchAll('assoc');
            foreach($answers as $k => $a){
                $title = $connection->execute("SELECT ap.text as title FROM answer_parts as ap, answers as a WHERE a.id = " . $a['id'] . " and a.id = ap.answer_id AND ap.as_title = 1")->fetch('assoc');
                if(isset($title['title'])){
                    $answers[$k]['Title'] = $title['title'];
                }
            }

        	$additionalAnswers = $connection->execute("SELECT a.id, f.type, f.placeholder FROM answers as a, forms as f, accepts as c, additional_speakers as s" .
        		" WHERE c.answer_id = a.id AND c.vote = 1 AND s.confirmed = 0 AND s.informed = 1 AND f.id = a.form_id AND a.event_id = " . $path[0] . " AND a.id = s.answer_id" .
    	    	" AND s.user_id like ('" . $_SESSION['loggedUser'] . "')")->fetchAll('assoc');
            foreach($additionalAnswers as $k => $a){
                $title = $connection->execute("SELECT ap.text as title FROM answer_parts as ap, answers as a WHERE a.id = " . $a['id'] . " and a.id = ap.answer_id AND ap.as_title = 1")->fetch('assoc');
                if(isset($title['title'])){
                    $additionalAnswers[$k]['Title'] = $title['title'];
                }
            }

        	$responses = $connection->execute("SELECT distinct r.id, r.title FROM responses as r, accepts as a, responses_to_topics as t" .
        		" WHERE a.response_to_topic_id = t.id AND a.vote = 1 AND r.confirmed = 0 AND r.informed = 1 AND a.event_id = " . $path[0] . " AND t.response_id = r.id AND r.user_id like ('" . $_SESSION['loggedUser'] . "')")
    	    	->fetchAll('assoc');
        	$additionalResponses = $connection->execute("SELECT distinct r.id, r.title FROM responses as r, accepts as a, responses_to_topics as t, additional_speakers as s" .
        		" WHERE a.response_to_topic_id = t.id AND a.vote = 1 AND s.confirmed = 0 AND s.informed = 1 AND a.event_id = " . $path[0] . " AND t.response_id = r.id AND r.id = s.response_id" .
    	    	" AND s.user_id like ('" . $_SESSION['loggedUser'] . "')")->fetchAll('assoc');

        	$this->set("answers", $answers);
        	$this->set("additionalAnswers", $additionalAnswers);
    	    $this->set("responses", $responses);
        	$this->set("additionalResponses", $additionalResponses);
        	$this->set("questions", $questions);
    	    $this->set("event", $event[0]);
        	$this->set("form", $form[0]);

        	try{
        		$this->render("confirm");
    	    } catch (MissingTemplateException $exception){
    		    if (Configure::read('debug')) {
    			    throw $exception;
        		}
        		throw new NotFoundException();
    	    }

        } else {
        	$responses = $connection->execute("SELECT distinct e.id, e.name, e.year, e.logo FROM cfp.events as e, cfp.responses as r, cfp.accepts as c, cfp.responses_to_topics as t" .
    	    	" WHERE e.id = c.event_id and c.vote = 1 and r.confirmed = 0 and t.id = c.response_to_topic_id and t.response_id = r.id and r.informed = 1 and r.user_id like ('" . $_SESSION['loggedUser'] . "')")
    		    ->fetchAll("assoc"); 
        	$additResponses = $connection->execute("SELECT distinct e.id, e.name, e.year, e.logo FROM cfp.events as e, cfp.responses as r, cfp.accepts as c," .
                " cfp.responses_to_topics as t, cfp.additional_speakers as s" .
        		" WHERE e.id = c.event_id and c.vote = 1 and s.confirmed = 0 and t.id = c.response_to_topic_id and t.response_id = r.id and r.id = s.response_id and s.informed = 1" .
    	    	" and s.user_id like ('" . $_SESSION['loggedUser'] . "')")->fetchAll("assoc");

        	$answers = $connection->execute("SELECT distinct e.id, e.name, e.year, e.logo FROM cfp.events as e, cfp.answers as a, cfp.accepts as c" .
        		" WHERE e.id = c.event_id and c.vote = 1 and a.confirmed = 0 and a.informed = 1 and a.id = c.answer_id and a.user_id like ('" . $_SESSION['loggedUser'] . "')")->fetchAll("assoc");
    	    $additAnswers = $connection->execute("SELECT distinct e.id, e.name, e.year, e.logo FROM cfp.events as e, cfp.answers as a, cfp.accepts as c, cfp.additional_speakers as s" .
    		    " WHERE e.id = c.event_id and c.vote = 1 and s.confirmed = 0 and s.informed = 1 and a.id = c.answer_id and a.id = s.answer_id and s.user_id like ('" . $_SESSION['loggedUser'] . "')")->fetchAll("assoc");

        	$events = parent::array_multidim_unique(array_merge(($responses!=null?$responses:array()),($additResponses!=null?$additResponses:array()),($answers!=null?$answers:array()),($additAnswers!=null?$additAnswers:array())),
                 "id");

        	$this->set("events", $events);

        	$confirmedResponses = $connection->execute("SELECT distinct e.id, e.name, e.year, e.logo, e.accommodation FROM cfp.events as e, cfp.responses as r," .
                " cfp.accepts as c, cfp.responses_to_topics as t" .
    	    	" WHERE e.id = c.event_id and c.vote = 1 and r.confirmed = 1 and t.id = c.response_to_topic_id and t.response_id = r.id and r.informed = 1 and r.user_id like ('" . $_SESSION['loggedUser'] . "')")
    		    ->fetchAll("assoc");
        	$confirmedAdditResponses = $connection->execute("SELECT distinct e.id, e.name, e.year, e.logo, e.accommodation FROM cfp.events as e, cfp.responses as r," .
                " cfp.accepts as c, cfp.responses_to_topics as t, cfp.additional_speakers as s" .
        		" WHERE e.id = c.event_id and c.vote = 1 and s.confirmed = 1 and t.id = c.response_to_topic_id and t.response_id = r.id and r.id = s.response_id and s.informed = 1" .
    	    	" and s.user_id like ('" . $_SESSION['loggedUser'] . "')")->fetchAll("assoc");
        	$confirmedAnswers = $connection->execute("SELECT distinct e.id, e.name, e.year, e.logo, e.accommodation FROM cfp.events as e, cfp.answers as a, cfp.accepts as c" .
        		" WHERE e.id = c.event_id and c.vote = 1 and a.confirmed = 1 and a.informed = 1 and a.id = c.answer_id and a.user_id like ('" . $_SESSION['loggedUser'] . "')")->fetchAll("assoc");
    	    $additAnswers = $connection->execute("SELECT distinct e.id, e.name, e.year, e.logo, e.accommodation FROM cfp.events as e, cfp.answers as a, cfp.accepts as c, cfp.additional_speakers as s" .
    		    " WHERE e.id = c.event_id and c.vote = 1 and s.confirmed = 1 and s.informed = 1 and a.id = c.answer_id and a.id = s.answer_id and s.user_id like ('" . $_SESSION['loggedUser'] . "')")->fetchAll("assoc");

        	$confirmedEvents = parent::array_multidim_unique(array_merge(($confirmedResponses!=null?$confirmedResponses:array()),($confirmedAdditResponses!=null?$confirmedAdditResponses:array()),
    					($confirmedAnswers!=null?$confirmedAnswers:array()),($confirmedAdditAnswers!=null?$confirmedAdditAnswers:array())), "id");
    					
    		foreach($confirmedEvents as $i => $r){
                if($connection->execute("SELECT 1 FROM forms WHERE event_id = " . $r['id'] . " AND type in ('accommodation', 'covered accommodation') AND (cfp_close is null OR cfp_close >= CURDATE())")->rowCount() > 0){
                    $confirmedEvents[$i]['booking'] = true;
                } else {
                	$confirmedEvents[$i]['booking'] = false;
              	}
            }

        	$this->set("confirmedEvents", $confirmedEvents);

            try {
                $this->render("home");
            } catch (MissingTemplateException $exception) {
                if (Configure::read('debug')) {
                    throw $exception;
                }
                throw new NotFoundException();
            }
        }
    }

    public function confirmed(...$path)
    {
        parent::printFlush($this->request->here());

        $this->set("active", "accept");
        $this->set("admin", parent::getAdmin() | parent::getSuperUser());
        $this->set("reviewer", parent::getReviewer());
        $this->set("program_manager", parent::getProgramManager());
        $connection = ConnectionManager::get('cfp');

        if($this->request->session()->read("first-name")){
    	    $this->set('username', $_SESSION['first-name'] . " " . $_SESSION['last-name']);
        }

        $event = $connection->execute('SELECT name, year, id, logo, collect_data, confirmation_info FROM events WHERE id = ' . $path[0])->fetchAll('assoc');

        $answers = $connection->execute("SELECT a.id, f.type, f.placeholder FROM answers as a, forms as f, accepts as c" .
        	" WHERE c.answer_id = a.id AND c.vote = 1 AND a.confirmed = 1 AND a.informed = 1 AND f.id = a.form_id AND a.event_id = " . $path[0] . " AND a.user_id like ('" . $_SESSION['loggedUser'] . "')")
    	    ->fetchAll('assoc');
        foreach($answers as $k => $a){
            $title = $connection->execute("SELECT ap.text as title FROM answer_parts as ap, answers as a WHERE a.id = " . $a['id'] . " and a.id = ap.answer_id AND ap.as_title = 1")->fetch('assoc');
            if(isset($title['title'])){
                $answers[$k]['Title'] = $title['title'];
            }
        }

        $additionalAnswers = $connection->execute("SELECT a.id, f.type, f.placeholder FROM answers as a, forms as f, accepts as c, additional_speakers as s" .
        	" WHERE c.answer_id = a.id AND c.vote = 1 AND s.confirmed = 1 AND s.informed = 1 AND f.id = a.form_id AND a.event_id = " . $path[0] . " AND a.id = s.answer_id" .
    	    " AND s.user_id like ('" . $_SESSION['loggedUser'] . "')")->fetchAll('assoc');

        foreach($additionalAnswers as $k => $a){
            $title = $connection->execute("SELECT ap.text as title FROM answer_parts as ap, answers as a WHERE a.id = " . $a['id'] . " and a.id = ap.answer_id AND ap.as_title = 1")->fetch('assoc');
            if(isset($title['title'])){
                $additionalAnswers[$k]['Title'] = $title['title'];
            }
        }


        $responses = $connection->execute("SELECT distinct r.id, r.title FROM responses as r, accepts as a, responses_to_topics as t" .
        	" WHERE a.response_to_topic_id = t.id AND a.vote = 1 AND r.confirmed = 1 AND r.informed = 1 AND a.event_id = " . $path[0] . " AND t.response_id = r.id AND r.user_id like ('" . $_SESSION['loggedUser'] . "')")
    	    ->fetchAll('assoc');
        $additionalResponses = $connection->execute("SELECT distinct r.id, r.title FROM responses as r, accepts as a, responses_to_topics as t, additional_speakers as s" .
        	" WHERE a.response_to_topic_id = t.id AND a.vote = 1 AND s.confirmed = 1 AND s.informed = 1 AND a.event_id = " . $path[0] . " AND t.response_id = r.id AND r.id = s.response_id" .
    	    " AND s.user_id like ('" . $_SESSION['loggedUser'] . "')")->fetchAll('assoc');

        $this->set("answers", $answers);
        $this->set("additionalAnswers", $additionalAnswers);
        $this->set("responses", $responses);
        $this->set("additionalResponses", $additionalResponses);
        $this->set("event", $event[0]);

        try{
    	    $this->render("list");
        } catch (MissingTemplateException $exception){
        	if (Configure::read('debug')) {
    	    	throw $exception;
        	}
        	throw new NotFoundException();
        }
    }
}
