<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Datasource\ConnectionManager;
use Cake\Mailer\Email;
/**
 * Static content controller
 *
 * This controller will render views from Template/HomePage/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class AdditionalController extends AppController
{

    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Network\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function show(...$action)
    {

        $connection = ConnectionManager::get('cfp');

        if(isset($_POST['save'])){
            if(isset($_SESSION['token']) && isset($_POST['_Token']) && $_POST['_Token'] == $_SESSION['token']){
                if(isset($action[3]) && is_numeric($action[1]) && is_numeric($action[2]) && is_numeric($action[3])){
                    if(filter_var($_POST['user'], FILTER_VALIDATE_EMAIL) !== false){
                        $user = "'" . str_replace(array("'", "\""), array("\'", "\\\""), $_POST['user']) . "'";
                        if($action[0] == "add"){
                            return $this->setSpeaker($connection,$user, $action[1], $action[2],$action[3], true);
                        } else if($action[0] == "delete"){
                            return $this->setSpeaker($connection,$user, $action[1], $action[2],$action[3], false);
                        }
                    } else {
                        $_SESSION['errorMessage'][] = "You have to specify valid email address.";
                        return $this->redirect("/additional/" . $action[1] . "/" . $action[2] . "/" . $action[3]);
                    }
                } else {
                    $_SESSION['errorMessage'][] = "You are not allowed to do this operation";
                    return $this->redirect("/myproposals");
                }
            } else {
                $_SESSION['errorMessage'][] = "Your token expired.";
                return $this->redirect("/myproposals");
            }
        }

        if(count($action) < 1 || !is_numeric($action[0])){
            $_SESSION['errorMessage'][] = "Unsufficient data. Your link is probably broken.";
            return $this->redirect("/myproposals");
        }

        try{
            $form = $connection->execute("SELECT * FROM cfp.forms WHERE id = " . $action[0])->fetchAll("assoc");
            if(count($form) == 0){
                throw new \Exception("Corrupted data. Your link is probably broken.");
            } else {
                $form = $form[0];
            }
            $event = $connection->execute("SELECT id, name, year, for_redhat, cfp_close FROM cfp.events WHERE id = " . $form['event_id'])->fetchAll("assoc")[0];
            if(strtotime(date("Y-m-d", time())) > strtotime($event['cfp_close']) && !parent::getAdmin() && !parent::getProgramManagerForEvent($form['event_id'])){
                $_SESSION['errorMessage'][] = "CfP is closed. You can't add or delete any additional speaker.";
                return $this->redirect('/');
            }
        }catch(\Exception $e){
            $_SESSION['errorMessage'][] = $e->getMessage();
            return $this->redirect("/");
        }

        if($form['speaker_limit'] != 0){
            if(count($action) >= 3 && is_numeric($action[1]) && is_numeric($action[2])){
                if($action[1] > 0){
                    $speaker = $connection->execute("SELECT user_id FROM responses WHERE id = " . $action[1])->fetchAll("assoc");
                    if(count($speaker) == 0){
                        $_SESSION['errorMessage'][] = "Corrupted data. Your link is probably broken.";
                        return $this->redirect("/myproposals");
                    }

                    $column = 'response_id';
                    $findId = $action[1];
                } else {
                    if($action[2] > 0){
                        $speaker = $connection->execute("SELECT user_id FROM answers WHERE id = " . $action[2])->fetchAll("assoc");
                        if(count($speaker) == 0){
                            $_SESSION['errorMessage'][] = "Corrupted data. Your link is probably broken.";
                            return $this->redirect("/myproposals");
                        }

                        $column = 'answer_id';
                        $findId = $action[2];
                    } else {
                        $_SESSION['errorMessage'][] = "Corrupted data. Your link is probably broken.";
                        return $this->redirect("/myproposals");
                    }
                }

                $additionals = $connection->execute("SELECT u.first_name, u.last_name, a.user_id, u.avatar ".
                        "FROM users as u RIGHT JOIN additional_speakers as a ON u.id = a.user_id WHERE a." . $column . " = " . $findId)->fetchAll("assoc");

                $found = false;
                foreach($additionals as $ad){
                    if($ad['user_id'] == $_SESSION['loggedUser']){
                        $found = true;
                    }
                }

                if($speaker[0]['user_id'] != $_SESSION['loggedUser'] && !parent::getAdmin() && !parent::getProgramManager() && !$found){
                    $_SESSION['errorMessage'][] = "You're not allowed to do this operation. Permission denied.";
                    return $this->redirect("/myproposals");
                }
            } else {
                $_SESSION['errorMessage'][] = "Unsufficient data. Your link is probably broken.";
                return $this->redirect("/myproposals");
            }
        } else {
            $_SESSION['errorMessage'][] = "This event doesn't allow to set anybody as additional speaker.";
            return $this->redirect("/myproposals");
        }

        parent::printFlush($this->request->here());
        $this->set("eventId", $action[0]);
        $this->set("admin", parent::getAdmin() | parent::getSuperUser());
        $this->set("reviewer", parent::getReviewer());
        $this->set("program_manager", parent::getProgramManager());


        if($event['for_redhat'] == 1){
            $allUsers = $connection->execute("SELECT first_name, last_name, id, email, avatar FROM users WHERE id like ('%@redhat.com')")->fetchAll("assoc");
        } else {
            $allUsers = $connection->execute("SELECT first_name, last_name, id, email, avatar FROM users")->fetchAll("assoc");
        }

        $this->set("users", $allUsers);
        $this->set("speakers", $additionals);
        $this->set("form", $form);
        $this->set("response", $action[1]);
        $this->set("answer", $action[2]);
        $this->set("active", "");
        $token = $this->request->getParam('_csrfToken');

        $_SESSION['token'] = $token;
        $this->set("token", $token);
        $this->set('username', $_SESSION['first-name']." ".$_SESSION['last-name']);

        try {
            $this->render('list');
        } catch (MissingTemplateException $exception) {
            if (Configure::read('debug')) {
                throw $exception;
            }
            throw new NotFoundException();
        }
    }

    public function setSpeaker($connect, $user, $form, $response, $answer, $action){

        try{
            $column = "";
            $table = "";
            $val = "";
            if($response > 0){
                $column = "response_id";
                $table = "responses";
                $val = $response;
            } else {
                if($answer > 0){
                    $column = "answer_id";
                    $table = "answers";
                    $val = $answer;
                } else {
                    throw new \Exception("Unsupported operation.");
                }
            }
            $owner = $connect->execute("SELECT user_id FROM " . $table . " WHERE id = " . $val)->fetchAll('assoc');
            if($owner[0]['user_id'] == $_POST['user']){
                $_SESSION['errorMessage'][] = "You can't add or remove proposals owner.";
                return $this->redirect("/additional/". $form . "/" . $response . "/" . $answer);    		
            }
            $event = $connect->execute("SELECT e.for_redhat FROM cfp.events as e, cfp.forms as f WHERE f.event_id = e.id AND f.id = " . $form)->fetchAll('assoc');
            if(count($event) > 0){
                $event = $event[0];
            }
            if($action){
                $check = $connect->execute("SELECT user_id FROM cfp.additional_speakers WHERE user_id like (" . $user . ") AND " . $column . " = " . $val);
                if(count($check) > 0){
                    $_SESSION['warningMessage'][] = "This user is already set as additional speaker for this proposal.";
                    return $this->redirect("/additional/" . $form . "/" . $response . "/" . $answer);
                }
                $cnt = $connect->execute("SELECT count(user_id) as cnt FROM cfp.additional_speakers WHERE " . $column . " = " . $val)->fetchAll('assoc');
                $maxForm = $connect->execute("SELECT speaker_limit FROM cfp.forms WHERE id = " . $form)->fetchAll("assoc");

                if($maxForm[0]['speaker_limit'] != 0 && ($maxForm[0]['speaker_limit'] < 0 || $cnt[0]['cnt'] < $maxForm[0]['speaker_limit'])){
                    if(isset($event['for_redhat']) && $event['for_redhat'] == 1){
                        if(strpos($user, "@redhat.com'") !== false){
                            $connect->execute("INSERT INTO cfp.additional_speakers (user_id, " . $column . ") VALUES (" . $user . ", " . $val . ")");
                            $newSpeaker = $connect->execute("SELECT id FROM users WHERE email like (" . $user . ")")->fetchAll("assoc");
                            if(count($newSpeaker) == 0){
                                $this->sendInvite($connect, $form, $user, true);
                            } else {
                                $this->sendInvite($connect, $form, $user, false);
                            }
                        } else {
                            $_SESSION['errorMessage'][] = "You can set only users from @redhat.com domain as additional speaker.";
                            return $this->redirect("/additional/". $form . "/" . $response . "/" . $answer);
                        }
                    } else {
                        $connect->execute("INSERT INTO cfp.additional_speakers (user_id, " . $column . ") VALUES (" . $user . ", " . $val . ")");
                        $newSpeaker = $connect->execute("SELECT id FROM users WHERE email like (" . $user . ")")->fetchAll("assoc");
                        if(count($newSpeaker) == 0){
                            $this->sendInvite($connect, $form, $user, true);
                        } else {
                            $this->sendInvite($connect, $form, $user, false);
                        }
                    }
                    $_SESSION['successMessage'][] = "Speaker has been successfully added.";
                } else {
                    $_SESSION['errorMessage'][] = "You already reached limit for number of additional speakers.";
                }
            } else {
                $connect->execute("DELETE FROM cfp.additional_speakers WHERE " . $column . " = " . $val . " and user_id like(" . $user . ")");    	
                $_SESSION['successMessage'][] = "Speaker has been successfully removed.";
            }
        }catch(\Exception $e){
            if($action){
                $_SESSION['errorMessage'][] = "User can't be set as additional speaker.";
            } else {
                $_SESSION['errorMessage'][] = "Additional speaker can't be removed.";
            }
        }    
        return $this->redirect("/additional/". $form . "/" . $response . "/" . $answer);
    }

    public function sendInvite($connect, $form, $user, $createRequest){

        $user = str_replace("'", "", $user);

        $event = $connect->execute("SELECT e.id, e.name, e.year  FROM events as e, forms as f WHERE e.id = f.event_id and f.id = " . $form)->fetchAll('assoc')[0];
        if(!parent::hasSMTP($event['id'])){
            $_SESSION['warningMessage'][] = "We aren't able to send invite email to additional speaker. Please, notify him/her to sign in to this website.";
            return;
        }

        $smtp = $connect->execute('SELECT * FROM cfp.smtps WHERE event_id = ' . $event['id'])->fetchAll('assoc')[0];
        $smtp['password'] = parent::decrypt($smtp['password']);
        $smtp['username'] = parent::decrypt($smtp['username']);

        try{
            Email::configTransport("custom", ['className'=>"Smtp", "timeout"=>60, "tls"=>($smtp['tls']==1?true:false), 'host'=>$smtp['host'], 'port'=>$smtp['port'], 'username'=>$smtp['username'],
             'password'=>$smtp['password'], 'context'=>['ssl'=>['allow_self_signed'=>true, 'verify_peer'=>false, 'verify_peer_name'=>false]]]);

            $email = new Email();
            $email->dropTransport("Smtp");
            $email->emailFormat("html");
            $email->setFrom([$smtp['username'] => $smtp['nick']]);
            if($smtp['replyto'] != ''){
                $email->setReplyTo($smtp['replyto']);
            }
            $email->setTo($user);
            if($createRequest){
                $email->setSubject($event['name']." (" . $event['year'] . ") - invitation");
                $email->template("invite");
                $email->setViewVars(['event' => $event, "speaker" => $_SESSION['first-name']." ".$_SESSION['last-name'], "email" => $user, "domain" => $_SERVER['HTTP_HOST']]);
                $_SESSION['warningMessage'][] = "User `" . $user . "` need to accept invitation.";
            } else {
                $email->setSubject($event['name']." (" . $event['year'] . ") - notification");
                $email->template("notification");
                $email->setViewVars(['event' => $event, "speaker" => $_SESSION['first-name']." ".$_SESSION['last-name'], "domain" => $_SERVER['HTTP_HOST']]);
            }
            $email->send();
        }catch(\Exception $e){
            $_SESSION['errorMessage'][] = $e->getMessage();
        }
    }
}
