<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Datasource\ConnectionManager;

/**
 * Static content controller
 *
 * This controller will render views from Template/HomePage/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class FaqController extends AppController
{

    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Network\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function show(...$path)
    {
    parent::printFlush($this->request->here());

    $this->set("active", "home");
    $this->set("admin", parent::getAdmin() | parent::getSuperUser());
    $this->set("reviewer", parent::getReviewer());
    $this->set("program_manager", parent::getProgramManager());       
    	
    if($this->request->session()->read("first-name")){
           $this->set('username', $_SESSION['first-name'] . " " . $_SESSION['last-name']);
    }
    
    $questions = array(
    	array("question"=>"How can I modify my profile information?", "answer"=>"Visit your <a href='/profile'>profile page</a>. Link to this page is available on top right corner of every page."),
    	array("question"=>"I can't save my new avatar image.", "answer"=>"Make sure, you're entering valid URL address and this address is publicly available e.g. from your mobile phone web browser."),
    	array("question"=>"How can I create new proposal?", "answer"=>"Visit <a href='/'>CfP catalog</a> page and choose event, which now accepting new proposals (has a link 'Submit a proposal'. Here you choose type of the proposal and fill prepared form. Keep in mind, all fields in the form marked with asterisk (*) are required and you will not be able to submit your proposal until you fill them all."),
    	array("question"=>"How can I edit my proposal?", "answer"=>"Visit <a href='/myproposals'>My proposals</a> and find the proposal you want to edit. If the event still accepts new proposals, you can see the 'Edit' link under your proposal. Otherwise, you're not allowed to edit your proposal after CfP close deadline."),
    	array("question"=>"Can I set additional speaker to my proposal?", "answer"=>"Yes, you can. There are two ways. First, after submitting your proposal, you're redirected to confirmation page, where you can find red button 'Manage additional speakers for current proposal'. Second is via <a href='/myproposals'>My proposals</a> page, where you can find link 'Additional speakers' on every card with proposal. You can manage additional speakers only until end of CfP open period."),
    	array("question"=>"I found some issue / I have idea for enhancement of service", "answer" => "Please, contact author directly on <a href='mailto:jridky@redhat.com'>jridky@redhat.com</a>."),
    );

    $this->set("questions", $questions);

    try {
    	$this->render("home");
    } catch (MissingTemplateException $exception) {
    	if (Configure::read('debug')) {
    		throw $exception;
    	}
    	throw new NotFoundException();
    }
    }   
}
