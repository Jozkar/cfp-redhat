<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Datasource\ConnectionManager;
/**
 * Static content controller
 *
 * This controller will render views from Template/HomePage/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class MyProposalsController extends AppController
{

    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Network\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function show(...$path)
    {

    parent::printFlush($this->request->here());

    $this->set("active", "proposal");
    $this->set("admin", parent::getAdmin() | parent::getSuperUser());
    $this->set("reviewer", parent::getReviewer());
    $this->set("program_manager", parent::getProgramManager());
        $connection = ConnectionManager::get('cfp');

    $responses = $connection->execute("SELECT DISTINCT r.id, r.title, r.user_id, r.event_id, r.answer_id, e.name, e.year, e.logo, e.cfp_close, f.cfp_close as form_close, f.id as form_id FROM".
            " cfp.forms as f, cfp.events as e, cfp.responses as r ".
    		" WHERE r.event_id = e.id AND r.user_id like ('" . $_SESSION['loggedUser'] . "') AND f.event_id = r.event_id AND f.type like ('session') UNION".
                 	" SELECT DISTINCT r1.id, r1.title, r1.user_id, r1.event_id, r1.answer_id, e1.name, e1.year, e1.logo, e1.cfp_close, f1.cfp_close as form_close, f1.id as form_id FROM cfp.forms as f1, cfp.events as e1,".
    		" cfp.responses as r1, additional_speakers as a1 WHERE r1.event_id = e1.id AND a1.user_id like ('" . $_SESSION['loggedUser'] . "') AND a1.response_id = r1.id".
    		" AND f1.event_id = r1.event_id AND f1.type like ('session') ORDER BY id desc")->fetchAll("assoc");

    $answers =  $connection->execute("SELECT DISTINCT an1.id, an1.event_id, an1.user_id, e1.name, e1.year, e1.logo, e1.cfp_close, f1.type, f1.id as form_id, f1.cfp_close as form_close FROM".
    		" cfp.forms as f1, cfp.events as e1, cfp.answers as an1 WHERE an1.event_id = e1.id AND an1.user_id like ('" . $_SESSION['loggedUser'] . 
    		"') AND f1.type NOT in ('session', 'confirmation', 'collect', 'accommodation', 'covered accommodation') AND an1.id NOT IN".
    		" (SELECT answer_id FROM cfp.responses WHERE user_id like ('" . $_SESSION['loggedUser'] . "') AND answer_id IS NOT NULL) AND f1.id = an1.form_id UNION".
    		" SELECT DISTINCT an.id, an.event_id, an.user_id, e.name, e.year, e.logo, e.cfp_close, f.type, f.cfp_close as form_close, f.id as form_id FROM".
    		" cfp.forms as f, cfp.events as e, cfp.answers as an, cfp.additional_speakers as a WHERE a.user_id like ('" . $_SESSION['loggedUser'] . "') AND a.answer_id = an.id".
    		" AND an.event_id = e.id AND an.id NOT IN (SELECT answer_id FROM cfp.responses WHERE user_id like ('" . $_SESSION['loggedUser'] . "') AND answer_id IS NOT NULL)".
    		" AND f.id = an.form_id order by type asc, id desc")->fetchAll("assoc");

    foreach($answers as $k => $a){
        $t = $connection->execute("SELECT a.id, ap.text as title FROM answer_parts as ap, answers as a WHERE a.id = " . $a['id'] . " and a.id = ap.answer_id AND ap.as_title = 1")->fetch('assoc');
        if(isset($t['title']) && trim($t['title']) != ""){
            $answers[$k]['Title'] = $t['title'];
        }
    }

    if($this->request->session()->read("first-name")){
            $this->set('username', $_SESSION['first-name'] . " " . $_SESSION['last-name']);
    }

    $this->set("responses", $responses);
    $this->set("answers", $answers);

        try {
            $this->render("home");
        } catch (MissingTemplateException $exception) {
            if (Configure::read('debug')) {
                throw $exception;
            }
            throw new NotFoundException();
        }
    }
}
