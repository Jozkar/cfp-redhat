<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Datasource\ConnectionManager;
use Doctrine\Common\Inflector\Inflector;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppAdminController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */

    private $superuser = false, $admin = false, $program = false, $reviewer = false, $email = "";
    private $allowList = array("/profile","/","/review","/admin","/administrators", "/manager", "/reviewer", "/topicfix", "/event");
    private $allowSpecial = array("managers", "reviewers", "review", "manage", "capreviewers");

    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');

        /*
         * Enable the following components for recommended CakePHP security settings.
         * see https://book.cakephp.org/3.0/en/controllers/components/security.html
         */
#       $this->loadComponent('Security');
#       $this->loadComponent('Csrf');
        $domain = $_SERVER['HTTP_HOST'];
        $session = $this->request->session();
        $request = $this->request->here();
        $connection = ConnectionManager::get('cfp');

        $connection->execute("SET GLOBAL sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''))");
    	
        if($session->read("loggedUser")==NULL && $request != "/login" && $domain != "localhost"){
    		 $this->user = null;
             header("Location: /login");
    	     die();
        }else{
    	if($domain != "localhost"){
    		if($request == "/login"){
    			$this->viewBuilder()->setLayout("basic");
    		} else {
    			$results = $connection->execute('SELECT email, superuser, admin, program_manager, reviewer, id FROM cfp.users WHERE id like ("' .$_SESSION['loggedUser'] . '")')->fetchAll('assoc');
    			if($results){
    				$this->superuser = $results[0]['superuser'];
    				$this->admin = $results[0]['admin'];
    				$this->program_manager = $results[0]['program_manager'];
    				$this->reviewer = $results[0]['reviewer'];
    				$this->email = $results[0]['email'];
    			}
    		}
    	} else {
    		$results = $connection->execute('SELECT email, superuser, admin, program_manager, reviewer, id FROM cfp.users WHERE id like ("jridky@redhat.com")')->fetchAll('assoc');
    		if($results){
    			$this->superuser = $results[0]['superuser'];
    			$this->admin = $results[0]['admin'];
                $this->program_manager = $results[0]['program_manager'];
                $this->reviewer = $results[0]['reviewer'];
                $this->email = $results[0]['email'];
            }
    		$results = $connection->execute('SELECT id,first_name,last_name FROM cfp.users WHERE email like "jridky@redhat.com"')->fetchAll('assoc');
            if($results){
                $session->write("loggedUser", $results[0]['id']);
                $session->write("first-name", $results[0]['first_name']);
                $session->write("last-name", $results[0]['last_name']);
                $session->write("email", "jridky@redhat.com");
            }
    	}
    }   
    }

    public function printFlush($page){
    	if(!in_array($page, $this->allowList)){
    		$found = false;
    		foreach ($this->allowSpecial as $site){
    			if(stripos($page, $site) !== false){
    				$found = true;
    			}
    		}
    		if(!$found) {
    			return;
    		}
    	}
    	if(isset($_SESSION['successMessage'])){
    		foreach($_SESSION['successMessage'] as $m){
    			$this->Flash->success($m);
    		}
    		unset($_SESSION['successMessage']);
    	}

    	if(isset($_SESSION['errorMessage'])){
    		foreach($_SESSION['errorMessage'] as $m){
    			$this->Flash->error($m);
    		}
    		unset($_SESSION['errorMessage']);
    	}

    	if(isset($_SESSION['warningMessage'])){
    		foreach($_SESSION['warningMessage'] as $m){
    			$this->Flash->warning($m);
    		}
    		unset($_SESSION['warningMessage']);
    	}
    }

    public function getSuperUser(){
        return $this->superuser;
    }

    public function getAdmin(){
    	return $this->admin;
    }

    public function getReviewer(){
    	return $this->reviewer;
    }

    public function getProgramManager(){
    	return $this->program_manager;
    }

    public function toPulral($name){
        return $this->getInflector()->pluralize($name);
    }

    public function getInflector(){
        return new Inflector();
    }

    public function getAdminForEvent($event){
            $connection = ConnectionManager::get('cfp');
            $row = $connection->execute("SELECT 1 FROM admins WHERE event_id = " . $event . " and user_id like ('" . $_SESSION['loggedUser'] . "')")->rowCount();
            return $row > 0 | $this->superuser;
    }

    public function isRedHatter(){
    	return (strpos($this->email, "@redhat.com") !== false? true:false);
    }
}
