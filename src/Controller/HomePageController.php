<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Datasource\ConnectionManager;
/**
 * Static content controller
 *
 * This controller will render views from Template/HomePage/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class HomePageController extends AppController
{

    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Network\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function display(...$path)
    {

        $count = count($path);
        if (!$count) {
            return $this->redirect('/');
        }
        if (in_array('..', $path, true) || in_array('.', $path, true)) {
            throw new ForbiddenException();
        }
        $page = $subpage = null;

        if (!empty($path[0])) {
            $page = $path[0];
        }
        if (!empty($path[1])) {
            $subpage = $path[1];
        }

    parent::printFlush($this->request->here());

    $redhat = parent::isRedHatter();
    $this->set("active", "home");
    $this->set("admin", parent::getAdmin() | parent::getSuperUser());
    $this->set("reviewer", parent::getReviewer());
    $this->set("program_manager", parent::getProgramManager());
    $connection = ConnectionManager::get('cfp');
    if($redhat){
    	$upcomming = $connection->execute('SELECT name, logo, cfp_open, year, url FROM cfp.events WHERE cfp_open > CURDATE() ORDER BY cfp_open')->fetchAll('assoc');
    	$results = $connection->execute('SELECT e.id, name, logo, e.cfp_close, year, url, redhat_event, max(f.cfp_close) as form_close, accommodation FROM cfp.events as e LEFT JOIN cfp.forms as f ON e.id = f.event_id'
            . ' WHERE (cfp_open <= CURDATE() and e.cfp_close >= CURDATE()) OR ((SELECT max(fe.cfp_close) FROM forms as fe WHERE fe.event_id = e.id AND fe.type not in ("accommodation", "covered accommodation")) >= CURDATE()) GROUP BY e.id ORDER BY e.cfp_open')->fetchAll('assoc');
    	$old = $connection->execute('SELECT id, name, logo, e.cfp_close, year, url, schedule FROM cfp.events as e WHERE'
            . ' e.cfp_close < CURDATE() and ((SELECT IFNULL(max(fe.cfp_close),2000-01-01) FROM forms as fe WHERE e.id = fe.event_id AND fe.type not in ("accommodation", "covered accommodation")) < CURDATE()) order by e.cfp_close desc')->fetchAll('assoc');
    } else {
    	$upcomming = $connection->execute('SELECT name, logo, cfp_open, year, url FROM cfp.events WHERE cfp_open > CURDATE() and for_redhat = 0 ORDER BY cfp_open')->fetchAll('assoc');
    	$results = $connection->execute('SELECT e.id, name, logo, e.cfp_close, year, url, redhat_event, max(f.cfp_close) as form_close, accommodation FROM cfp.events as e LEFT JOIN cfp.forms as f ON e.id = f.event_id'
            .' WHERE (cfp_open <= CURDATE() and e.cfp_close >= CURDATE() and for_redhat = 0) OR ((SELECT max(fe.cfp_close) FROM forms as fe WHERE fe.event_id=e.id AND fe.type not in ("accommodation", "covered accommodation")) >= CURDATE() and for_redhat = 0) GROUP BY e.id '
            .' ORDER BY e.cfp_open')->fetchAll('assoc');
    	$old = $connection->execute('SELECT id, name, logo, e.cfp_close, year, url, schedule FROM cfp.events as e WHERE'
            . ' e.cfp_close < CURDATE() and ((SELECT IFNULL(max(fe.cfp_close),2000-01-01) FROM forms as fe WHERE e.id = fe.event_id AND fe.type not in ("accommodation", "covered accommodation")) < CURDATE()) and for_redhat = 0 order by cfp_close desc')->fetchAll('assoc');
    }

    foreach($results as $i => $r){
        if($connection->execute("SELECT 1 FROM forms WHERE event_id = " . $r['id'] . " AND type like ('quiz')")->rowCount() > 0){
            $results[$i]['name'] = $r['name'] . " + QUIZ";
        }   
        if($connection->execute("SELECT 1 FROM forms WHERE event_id = " . $r['id'] . " AND type in ('accommodation', 'covered accommodation') AND (cfp_close is null OR cfp_close >= CURDATE())")->rowCount() > 0){
            $results[$i]['booking'] = true;
        } else {
        	$results[$i]['booking'] = false;
      	}
    }
    
    foreach($old as $i => $r){
        if($connection->execute("SELECT 1 FROM forms WHERE event_id = " . $r['id'] . " AND type in ('accommodation', 'covered accommodation') AND (cfp_close is null OR cfp_close >= CURDATE())")->rowCount() > 0){
            $old[$i]['booking'] = true;
        } else {
        	$old[$i]['booking'] = false;
      	}
    }

    $this->set("events", $results);
    $this->set("oldEvents", $old);
    $this->set("upcommingEvents", $upcomming);

    if($this->request->session()->read("first-name")){
            $this->set('username', $_SESSION['first-name'] . " " . $_SESSION['last-name']);
    }

        try {
            $this->render("home");
        } catch (MissingTemplateException $exception) {
            if (Configure::read('debug')) {
                throw $exception;
            }
            throw new NotFoundException();
        }
    }
}
