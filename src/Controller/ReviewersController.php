<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Datasource\ConnectionManager;
/**
 * Static content controller
 *
 * This controller will render views from Template/HomePage/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class ReviewersController extends AppController
{

    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Network\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function show(...$action)
    {

    	if(!parent::getAdmin() && !parent::getProgramManager() && !parent::getSuperUser()){
    		return $this->redirect("/");
    	}

    	if(count($action) < 1){
    		$_SESSION['errorMessage'][] = "Unsufficient data. Your link is probably broken.";
    		if(parent::getAdmin() | parent::getSuperUser()){
    			return $this->redirect("/admin");
    		} else {
    			return $this->redirect("/manager");
    		}
    	}
    	$connection = ConnectionManager::get('cfp');
    	
    	if(isset($_POST['save'])){
    		if(isset($_SESSION['token']) && isset($_POST['_Token']) && $_POST['_Token'] == $_SESSION['token']){
    			if(isset($action[1]) && isset($_POST['user']) && $_POST['user'] != "" && $action[1] == "add"){
    				// get user id
    				$_POST['user'] = "'" . str_replace(array("'", "\""), array("\'", "\\\""), $_POST['user']) . "'";
    				$newReviewer = $connection->execute("SELECT id FROM users WHERE email like (" . $_POST['user'] . ")")->fetchAll("assoc");
    				if(!isset($_POST['formType'])){
    					$_POST['formType'] = 'session';
    					$_SESSION['warningMessage'][] = "Group of proposals set to 'Sessions'.";
    				}
    				if(!isset($_POST['topic']) && $_POST['formType'] == 'session'){
    					$_SESSION['errorMessage'][] = "You haven't specified any topic to review.";
    					return $this->redirect("/reviewers/" . $action[0]);
    				} else {
    					return $this->modify($connection,$newReviewer[0]["id"],$action[0],$_POST['topic'], 1, (isset($_POST['captain'])?1:0), $_POST['formType']);
    				}
    			} else {
    				$_SESSION['errorMessage'][] = "You are not allowed to do this opperation";
    				return $this->redirect("/reviewers/" . $action[0]);
    			}
    		} else {
    			$_SESSION['errorMessage'][] = "You are not allowed to do this opperation";
    				return $this->redirect("/reviewers/" . $action[0]);
    		}
    	}

    	parent::printFlush($this->request->here());
    	$this->set("eventId", $action[0]);
    	$this->set("admin", parent::getAdmin() | parent::getSuperUser());
    	$this->set("reviewer", parent::getReviewer());
    	$this->set("program_manager", parent::getProgramManager());

    	$formAction = "add";			

    	// check, if are passed any arguments
    	if(!empty($action) && count($action) > 1){
    		// supported are arguments /.../event/action/userid/captain/token/type
    		if( count($action) <= 5 ){
    			//not enough arguments error message  + redirect
    			$_SESSION['errorMessage'][] = "Unsufficient data. Your link is probably broken.";
    			return $this->redirect("/reviewers/".$action[0]); 
    		}
    		//for delete action
    		if ($action[1] == "delete") {
    			if($_SESSION['token'] == $action[4]){
    				// set reviewer column in db to 0
    				return $this->modify($connection,$action[2],$action[0], null, 0, $action[3], $action[5]);
    			} else {
    				$_SESSION['errorMessage'][] = "This action can't be performet - invalid token.";
                    return $this->redirect("/reviewers/".$action[0]);
    			}
    		}
    	}

    	$eventInfo = $connection->execute("SELECT name, year, GROUP_CONCAT(topic_id) AS topics FROM events, topics_to_events WHERE id = " . $action[0] . " AND event_id = id")->fetchAll("assoc");
    	$tops = $connection->execute("SELECT * FROM topics WHERE id IN (". $eventInfo[0]['topics'] . ")")->fetchAll("assoc");
    	$allUsers = $connection->execute("SELECT first_name, last_name, id, email, avatar FROM users")->fetchAll("assoc");
    	$reviewerUsers = $connection->execute("SELECT first_name, last_name, id, email, avatar, topics, captain, form_type FROM users, reviewers as m WHERE m.user_id = id and m.event_id = " . $action[0])->fetchAll("assoc");
    	$forms = $connection->execute("SELECT type, placeholder FROM forms WHERE event_id = " . $action[0] . " AND type not in ('confirmation','collect')")->fetchAll("assoc");
    	$topics = array();
    	foreach($tops as $t){
    		$topics[$t['id']] = $t;
    	}

    	$this->set("eventInfo", $eventInfo[0]);
    	$this->set("topics", $topics);		
    	$this->set("reviewers", $reviewerUsers);
    	$this->set("users", $allUsers);
    	$this->set("action", $formAction);
    	$this->set("forms", $forms);
    	$this->set("active", "");
        $this->set("inflector", parent::getInflector());
    	$token = $this->request->getParam('_csrfToken');

    	$_SESSION['token'] = $token;
    	$this->set("token", $token);
    	$this->set('username', $_SESSION['first-name']." ".$_SESSION['last-name']);
    
    	try {
    	    $this->render('list');
    	} catch (MissingTemplateException $exception) {
    	    if (Configure::read('debug')) {
    	        throw $exception;
    	    }
    	    throw new NotFoundException();
    	}
    }
    
    public function modify($connect, $key, $event, $topicList, $val, $captain, $form){
    	$key = "'" . str_replace(array("'", "\""), array("\'", "\\\""), $key) . "'";
    	
    	try{
    		if($val){
    			$connect->execute("UPDATE cfp.users SET `reviewer`=" . $val . " WHERE `id` like (" . $key . ")");

    			$topics = $connect->execute("SELECT topics FROM cfp.reviewers WHERE event_id=" . $event . " and captain=".$captain." and user_id like (" . $key . ") and form_type like ('" . $form . "')")->fetchAll("assoc");
    			if(count($topics) < 1){
                    if($form == "session"){
        				$connect->execute("INSERT INTO cfp.reviewers (event_id, user_id, topics, captain, form_type) VALUES (" . $event .", " . $key . ", '" . implode(",", $topicList) . "'," . $captain . ", '" . $form . "')");
                    } else {
        				$connect->execute("INSERT INTO cfp.reviewers (event_id, user_id, captain, form_type) VALUES (" . $event .", " . $key . ", " . $captain . ", '" . $form . "')");
                    }
    				$_SESSION['successMessage'][] = "Reviewer has been successfully created.";
    			} else {
    				if($form == 'session'){
    					$res = array_merge(explode(",",$topics[0]['topics']), $topicList);
    					$res = array_unique($res);
    					$connect->execute("UPDATE cfp.reviewers SET topics='" . implode(",", $res) . "' WHERE event_id=" . $event . " AND captain=".$captain." AND user_id like (" . $key . ") and form_type like ('session')");
    					$_SESSION['successMessage'][] = "Reviewer has been successfully updated.";
    				} else {
    					$_SESSION['warningMessage'][] = "Nothing to update.";
    				}
    			}

    		} else {
    			$connect->execute("DELETE FROM cfp.reviewers WHERE event_id = " . $event . " and captain=" . $captain . " and user_id like(" . $key . ") and form_type like ('" . $form . "')");		
    			$_SESSION['successMessage'][] = "Reviewer has been successfully removed.";
    			$test = $connect->execute("SELECT * FROM cfp.reviewers WHERE user_id like(" . $key . ")");		
    			if(count($test) < 1) {
    				$connect->execute("UPDATE cfp.users SET `reviewer`= 0 WHERE `id` like (" . $key . ")");
    			}
    		}
    	}catch(\Exception $e){
    		if($val){
    			$_SESSION['errorMessage'][] = "Reviewer can't be created.";
    		} else {
    			$_SESSION['errorMessage'][] = "Reviewer can't be removed.";
    		}
    	}	
    	$this->redirect("/reviewers/".$event);
    }
}
