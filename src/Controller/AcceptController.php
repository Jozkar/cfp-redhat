<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class AcceptController extends Controller
{

    public function initialize()
    {

        parent::initialize();

        $this->loadComponent('RequestHandler');
    }
    
    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Network\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function add(...$path)
    {

        $token = $this->request->getData("token");
        $event_id = $this->request->getData("event");
        $response_id = $this->request->getData("response");
        $answer_id = $this->request->getData("answer");
        $user_id = $this->request->getData("user");
        $vote = $this->request->getData("vote");

    	if($token == null){
    		die(json_encode(array("result"=>"error", "reason"=>"No token received")));
    	}

    	if($token != $this->request->session()->read('token')){
    		die(json_encode(array("result"=>"error", "reason"=>"Token expired. Please, refresh your page.")));
    	}


    	if($event_id == '' || ($response_id == '' && $answer_id == '' && $user_id == '') || $vote == '' || !is_numeric($event_id) || (!is_numeric($response_id) && !is_numeric($answer_id) && !is_string($user_id)) || !is_numeric($vote)){
    		die(json_encode(array("result"=>"error", "reason"=>"Unsufficient data. Your vote wasn't accepted.")));
    	}
      
    	$connection = ConnectionManager::get('cfp');

        if($user_id != ""){
            $connection->execute('DELETE FROM cfp.hotel_booking WHERE event_id = ' . $event_id . ' and user_id = "' . $user_id . '"');

            if($connection->execute('INSERT INTO cfp.hotel_booking (event_id, vote, user_id) VALUES (' . $event_id . ', ' . $vote . ', "' . $user_id . '")')->errorCode() != 0){
                die(json_encode(array("result"=>"error", "reason"=>"Database error. Your vote wasn't accepted.")));
            } else {
                die(json_encode(array("result"=>"success")));
            }
        }elseif($answer_id != ""){
    		$connection->execute('DELETE FROM cfp.accepts WHERE event_id = ' . $event_id . ' and answer_id = ' . $answer_id);

    		if($connection->execute('INSERT INTO cfp.accepts (event_id, vote, answer_id) VALUES (' . $event_id . ', ' . $vote . ', ' . $answer_id . ')')->errorCode() != 0){
    			die(json_encode(array("result"=>"error", "reason"=>"Database error. Your vote wasn't accepted.")));
    		} else {
    			die(json_encode(array("result"=>"success")));
    		}
    	} else {
    		$connection->execute('DELETE FROM cfp.accepts WHERE event_id = ' . $event_id . ' and response_to_topic_id = ' . $response_id);
    	    
    		if($connection->execute('INSERT INTO cfp.accepts (event_id, vote, response_to_topic_id) VALUES (' . $event_id . ', ' . $vote . ', ' . $response_id . ')')->errorCode() != 0){		
    			die(json_encode(array("result"=>"error", "reason"=>"Database error. Your vote wasn't accepted.")));
    		} else {
    			die(json_encode(array("result"=>"success")));
    		}
    	}
    }
}
