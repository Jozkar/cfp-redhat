<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Datasource\ConnectionManager;
use Doctrine\Common\Inflector\Inflector;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */

    private $superuser = false, $admin = false, $program = false, $reviewer = false, $email = "";
    private $allowList = array("/profile","/","/review","/admin","/administrators", "/manager", "/reviewer", "/topicfix", "/event", "/signins", "/login", "/acceptproposals", "/codes", "/hotelrooms");
    private $allowSpecial = array("managers", "reviewers", "review", "manage", "capreviewers","emails", "forms", "answer", "proposal", "confirm", "additional", "submit", "codes", "collect", "slots", "hotel", "booking", "room", "schedule");

    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');

        /*
         * Enable the following components for recommended CakePHP security settings.
         * see https://book.cakephp.org/3.0/en/controllers/components/security.html
         */
        $this->loadComponent('Security');
        $this->loadComponent('Csrf');
        $domain = $_SERVER['HTTP_HOST'];
        $session = $this->request->session();
        $request = $this->request->here();
        $connection = ConnectionManager::get('cfp');

        $connection->execute("SET GLOBAL sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''))");

        if($session->read("loggedUser")==NULL && $request != "/login" && strpos($request, "/schedule/") === false && $domain != "localhost:81"){
             $_SESSION['desiredDestination'] = $request;
    		 $this->user = null;
             header("Location: /login");
    	     die();
        }else{
        	if($domain != "localhost:81"){
        		if($request == "/login"){
        			$this->viewBuilder()->setLayout("basic");
        		} else {
        			$results = $connection->execute('SELECT email, superuser, admin, program_manager, reviewer, id FROM cfp.users WHERE id like ("' .$_SESSION['loggedUser'] . '")')->fetchAll('assoc');
        			if(count($results) > 0){
                        $this->superuser = $results[0]['superuser'];
        				$this->admin = $results[0]['admin'];
        				$this->program_manager = $results[0]['program_manager'];
        				$this->reviewer = $results[0]['reviewer'];
        				$this->email = $results[0]['email'];
        			}else{
        				if($request != "/faq" && $request != "/profile" && $request != "/profile/add" && strpos($request, "/schedule/") === false){
        					$_SESSION['warningMessage'][] = "Please, fill and save your profile information first.";
        					header("Location: /profile");
        					die();
        				}
        			}
        		}
        	} else {
        		$results = $connection->execute('SELECT email, superuser, admin, program_manager, reviewer, id FROM cfp.users WHERE id like ("jridky@redhat.com")')->fetchAll('assoc');
        		if($results){
                    $this->superuser = $results[0]['superuser'];
        			$this->admin = $results[0]['admin'];
                    $this->program_manager = $results[0]['program_manager'];
                    $this->reviewer = $results[0]['reviewer'];
                    $this->email = $results[0]['email'];
                }
        		$results = $connection->execute('SELECT id,first_name,last_name FROM cfp.users WHERE email like "jridky@redhat.com"')->fetchAll('assoc');
                if($results){
                    $session->write("loggedUser", $results[0]['id']);
                    $session->write("first-name", $results[0]['first_name']);
                    $session->write("last-name", $results[0]['last_name']);
                    $session->write("email", "jridky@redhat.com");
                }
        	}
        }   
    }

    public function printFlush($page){
    	if(!in_array($page, $this->allowList)){
    		$found = false;
    		foreach ($this->allowSpecial as $site){
    			if(stripos($page, $site) !== false){
    				$found = true;
    			}
    		}
    		if(!$found) {
    			return;
    		}
    	}
    	if(isset($_SESSION['successMessage'])){
    		foreach($_SESSION['successMessage'] as $m){
    			$this->Flash->success($m);
    		}
    		unset($_SESSION['successMessage']);
    	}

    	if(isset($_SESSION['errorMessage'])){
    		foreach($_SESSION['errorMessage'] as $m){
    			$this->Flash->error($m);
    		}
    		unset($_SESSION['errorMessage']);
    	}

    	if(isset($_SESSION['warningMessage'])){
    		foreach($_SESSION['warningMessage'] as $m){
    			$this->Flash->warning($m);
    		}
    		unset($_SESSION['warningMessage']);
    	}
    }

    public function getSuperUser(){
        return $this->superuser;
    }

    public function getAdmin(){
    	return $this->admin;
    }

    public function getReviewer(){
    	return $this->reviewer;
    }

    public function getProgramManager(){
    	return $this->program_manager;
    }

    public function getProgramManagerForEvent($event){
    	$connection = ConnectionManager::get('cfp');
    	$row = $connection->execute("SELECT 1 FROM managers WHERE event_id = " . $event . " and user_id like ('" . $_SESSION['loggedUser'] . "')")->rowCount();
    	return $row > 0 | $this->superuser;
    }

    public function getAdminForEvent($event){
        $connection = ConnectionManager::get('cfp');
        $row = $connection->execute("SELECT 1 FROM admins WHERE event_id = " . $event . " and user_id like ('" . $_SESSION['loggedUser'] . "')")->rowCount();
        return $row > 0 | $this->superuser;
    }

    public function getReviewerForEvent($event){
        $connection = ConnectionManager::get('cfp');
        $row = $connection->execute("SELECT 1 FROM reviewers WHERE event_id = " . $event . " and user_id like ('" . $_SESSION['loggedUser'] . "')")->rowCount();
        return $row > 0 | $this->superuser;
    }

    public function hasSMTP($event){
        $connection = ConnectionManager::get('cfp');
        $row = $connection->execute("SELECT 1 FROM smtps WHERE event_id = " . $event . " and host IS NOT NULL and host not like ('')")->rowCount();
        return $row > 0;
    }

    public function getCurrentEmail(){
    	return $this->email;
    }

    public function isRedHatter(){
    	return (strpos($this->email, "@redhat.com") !== false? true:false);
    }

    public function toPulral($name){
        return $this->getInflector()->pluralize($name);
    }

    public function getInflector(){
        return new Inflector();
    }

    public function getKeywords($type){
    	switch($type){
    		case "all": return array("URLFNAME", "URLLNAME", "URLMAIL", "FNAME", "LNAME", "EMAIL", "EVENT", "YEAR");
    		case "accept":
            case "waitList":
    		case "reject": return array("URLFNAME", "URLLNAME", "URLMAIL", "FNAME", "LNAME", "EMAIL", "EVENT", "YEAR", "PROPOSALS", "SUBMTITLE", "TRACK");
    		case "confirmation": return array("FNAME", "LNAME", "EMAIL", "EVENT", "YEAR", "FORMTYPE", "ANSWERS", "EDITLINK");
    		case "confirmedSpeakers": return array("FNAME", "LNAME", "EMAIL", "EVENT", "YEAR", "CODE");
    		case "unconfirmedSpeakers": return array("FNAME", "LNAME", "EMAIL", "EVENT", "YEAR", "PROPOSALS");
    		case "accommodation":
    		case "covered accommodation": return array("FNAME", "LNAME", "EMAIL", "EVENT", "YEAR", "ANSWERS", "HOTEL", "HMAIL");
    		default: return array();
    	}
    }

    public function checkFormType($type){
    	switch($type){
    		case "session":
    		case "booth":
    		case "meetup":
    		case "workshop":
    		case "confirmation":
            case "collect":
            case "quiz":
            case "contest":
            case "lightning talk":
            case "activity":
            case "accommodation":
            case "covered accommodation":
    			return true;
    		default:
    			return false;
    	}
    }

    public function array_multidim_unique($array, $key) {
        $temp_array = array();
        $i = 0;
        $key_array = array();
   
        foreach($array as $val) {
            if (!in_array($val[$key], $key_array)) {
                $key_array[$i] = $val[$key];
                $temp_array[$i] = $val;
            }
            $i++;
        }
        usort($temp_array, function($a, $b){ return ($a['year'] == $b['year']?$a['name'] <=> $b['name']:$a["year"] < $b["year"]); });
        return $temp_array;
    }

    public function encrypt($what) {
    	return openssl_encrypt($what,$cipher=env("CFP_ENC_METHOD","aes-256-cbc-hmac-sha256"), $cipherKey=env("CAKEPHP_SECRET_TOKEN",NULL), $options=0, $iv=env("CAKEPHP_SECURITY_SALT",NULL));
    }

    public function decrypt($what) {
    	return openssl_decrypt($what, $cipher=env("CFP_ENC_METHOD","aes-256-cbc-hmac-sha256"), $cipherKey=env("CAKEPHP_SECRET_TOKEN",NULL), $options=0, $iv=env("CAKEPHP_SECURITY_SALT",NULL));
    }
}
