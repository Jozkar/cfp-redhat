<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Datasource\ConnectionManager;
use Cake\Mailer\Email;

/**
 * Static content controller
 *
 * This controller will render views from Template/HomePage/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class ConfirmProposalsController extends AppController
{

    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Network\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function show(...$path)
    {
        if(empty($path) || count($path) < 2 || !is_numeric($path[1])){
            $_SESSION['errorMessage'][] = "Unsufficient data. Your link is probably broken.";
            return $this->redirect("/");
        }

        $connection = ConnectionManager::get('cfp');

        if($path[0] == "accept"){
            $form = $connection->execute('SELECT * FROM forms WHERE id = ' . $path[1])->fetchAll('assoc');

            if(count($form) < 1){
                $_SESSION['errorMessage'][] = "Form not found. Your link is probably broken.";
                return $this->redirect("/");
            }

            $form = $form[0];

            
            $event = $connection->execute('SELECT id, year, name, redhat_event, GROUP_CONCAT(topic_id) AS topics, cfp_close FROM cfp.events, cfp.topics_to_events WHERE id = ' . $form['event_id'] 
                . ' AND event_id = id')->fetchAll('assoc')[0];

            if(!parent::hasSMTP($form['event_id'])){
                $_SESSION['warningMessage'][] = "We aren't able to send you any email due unsufficient settings for this event. Please, contact event organizers directly.";
                return $this->redirect("/acceptproposals");
            }

            if($event['redhat_event'] == 0){
                $_SESSION['errorMessage'][] = "This event isn't organized by Red Hat. You can't confirm anything.";
                return $this->redirect("/acceptproposals");
            }

            $questions = $connection->execute('SELECT * FROM cfp.questions WHERE form_id = ' . $path[1] . ' ORDER BY question_order')->fetchAll('assoc');
            return $this->add($connection, $questions, $form);
        } else if($path[0] == "reject"){
            return $this->delete($connection, $path[1]);
        } else {
            $_SESSION['errorMessage'][] = "This opperation isn't supported.";
            return $this->redirect("/acceptproposals/");
        }
    }

    public function sendConfirm($connection, $form, $aid, $primary){

        $event = $connection->execute('SELECT name, year, id, code_to FROM events WHERE id = ' . $form['event_id'])->fetchAll('assoc')[0];
        $mail = $connection->execute('SELECT * FROM cfp.emails WHERE event_id = ' . $form['event_id'] . ' and type = "confirmedSpeakers"')->fetchAll('assoc')[0];
        $smtp = $connection->execute('SELECT * FROM cfp.smtps WHERE event_id = ' . $form['event_id'])->fetchAll('assoc')[0];
        $smtp['password'] = parent::decrypt($smtp['password']);
        $smtp['username'] = parent::decrypt($smtp['username']);
        Email::configTransport("custom", ['className'=>"Smtp", "timeout"=>60, "tls"=>($smtp['tls']==1?true:false), 'host'=>$smtp['host'], 'port'=>$smtp['port'], 'username'=>$smtp['username'], 
        'password'=>$smtp['password'], 'context'=>['ssl'=>['allow_self_signed'=>true, 'verify_peer'=>false, 'verify_peer_name'=>false]]]);
        $code = $connection->execute("SELECT code FROM cfp.codes WHERE user_id like('" . $_SESSION['loggedUser'] . "') AND event_id = " . $form['event_id'])->fetchAll("assoc");

        if($event['code_to'] == 0){
            if($primary){ 
                $eligible = true;
            } else {
                $eligible = false;
            }    	
        } else {
            $eligible = true;
        }

        if(count($code) < 1 && $eligible){
            $connection->execute("UPDATE cfp.codes SET user_id = '" . $_SESSION['loggedUser'] . "' WHERE user_id IS NULL AND event_id = " . $form['event_id'] . " ORDER BY code ASC limit 1");
            $code = $connection->execute("SELECT code FROM cfp.codes WHERE user_id like('" . $_SESSION['loggedUser'] . "') AND event_id = " . $form['event_id'])->fetchAll("assoc");
        }

        $tx = str_replace(parent::getKeywords('confirmedSpeakers'), array($_SESSION['first-name'], $_SESSION['last-name'], $_SESSION['loggedUser'], $event['name'], $event['year'], ($eligible && count($code) > 0?$code[0]['code']:"NULL")), $mail['text']);

        $text = explode("SECONDARYSPEAKER", $tx);

        if(count($text) > 1){
            if($primary){
                $text = $text[0];
            } else {
                $text = $text[1];
            }
        } else {
            $text = $text[0];
        }

        $email = new Email();
        $email->dropTransport("Smtp");
        $email->emailFormat($mail['format']);
        $email->setFrom([$smtp['username'] => $smtp['nick']]);
        if($smtp['replyto'] != ''){
            $email->setReplyTo($smtp['replyto']);
        }
        $email->setTo($_SESSION['loggedUser']);
        $email->setSubject($mail['subject']);
        $email->send($text);
    }

    public function add($connection, $questions, $form) {
        //check for existing response from this user based on it's type
        $exists = $connection->execute("SELECT id FROM cfp.answers WHERE user_id like '" . $_SESSION['loggedUser'] . "' AND form_id = " . $form['id'] . " AND event_id = " . $form['event_id'])->fetchAll("assoc");
        $hasCode = $connection->execute("SELECT 1 FROM cfp.codes WHERE user_id like('" . $_SESSION['loggedUser'] . "') AND event_id = " . $form['event_id'])->rowCount();
        if( count($exists) < 1 ){
            if(count((isset($_POST['answers'])?$_POST['answers']:array())) + count((isset($_POST['additionalanswers'])?$_POST['additionalanswers']:array()))
            + count((isset($_POST['responses'])?$_POST['responses']:array())) + count((isset($_POST['additionalresponses'])?$_POST['additionalresponses']:array())) > 0){
                $answerId = -1;
                foreach($questions as $q){
                    switch($q['type']){
                        case "text":
                        case "textarea":
                        case "radio":
                            if(strpos($q['take_as'], "self") !== false){
                                $answer = "'" . str_replace(array("'", "\""), array("\'", "\\\""), $_POST[$q['take_as']]) . "'";
                                try {
                                    if($answerId < 0){
                                        $connection->execute("INSERT INTO answers (event_id, user_id, form_id) VALUES (" .$form['event_id']. ", '" .$_SESSION['loggedUser']. "', " .$form['id']. ")");
                                        $answerId = $connection->execute("SELECT LAST_INSERT_ID() as id")->fetchAll('assoc')[0]['id'];
                                    }
                                    // INSERT INTO answers
                                    $connection->execute("INSERT INTO answer_parts (question_id, answer_id, text) VALUES (" . $q['id'] . ", " . $answerId . ", " . $answer . ")");
                                }catch(\Exception $e){
                                    $_SESSION['errorMessage'][] = $e->getMessage();
                                    $connection->execute("DELETE FROM answer_parts WHERE answer_id = ". $answerId);
                                    $connection->execute("DELETE FROM answers WHERE id = " . $answerId);
                                    return $this->redirect("/acceptproposals/" . $form['event_id']);
                                }
                            }
                            break;
                        case "checkbox":
                            if(isset($_POST[$q['take_as']]) && count($_POST[$q['take_as']]) > 0){
                                $answers = array();
                                foreach($_POST[$q['take_as']] as $t){
                                    $answers[] = str_replace(array("'", "\""), array("\'", "\\\""), $t);
                                }
                                try{
                                    if($answerId < 0){;
                                        $connection->execute("INSERT INTO answers (event_id, user_id, form_id) VALUES (" .$form['event_id']. ", '" .$_SESSION['loggedUser']. "', " .$form['id']. ")");
                                        $answerId = $connection->execute("SELECT LAST_INSERT_ID() as id")->fetchAll('assoc')[0]['id'];
                                    }
                                    //INSERT INTO answers
                                    $connection->execute("INSERT INTO answer_parts (question_id, answer_id, text) VALUES (" . $q['id'] . ", " . $answerId . ", '" . implode(",", $answers) . "')");
                                }catch(\Exception $e){
                                    $_SESSION['errorMessage'][] = $e->getMessage();
                                    $connection->execute("DELETE FROM answer_parts WHERE answer_id = ". $answerId);
                                    $connection->execute("DELETE FROM answers WHERE id = " . $answerId);
                                    return $this->redirect("/acceptproposals/" . $form['event_id']);
                                }
                            } else {
                                if($q['required']){
                                    $_SESSION['errorMessage'][] = "You didn't specified required fields. Your proposal wasn't accepted.";
                                    if($answerId > 0){
                                        $connection->execute("DELETE FROM answer_parts WHERE answer_id = ". $answerId);
                                        $connection->execute("DELETE FROM answers WHERE id = " . $answerId);
                                    }
                                    return $this->redirect("/acceptproposals/" . $form['event_id']);
                                }
                            }
                            break;
                        default: 
                            break;
                    }
                }
            } else {
                $exists = array(false);
                $hasCode = 1;
            }
        } else {
            $answerId = $exists[0]['id'];
        }

        $answers = $connection->execute("SELECT a.id FROM answers as a, forms as f, accepts as c" .
                " WHERE c.answer_id = a.id AND c.vote = 1 AND a.confirmed = 0 AND a.informed = 1 AND a.event_id = " . $form['event_id'] . " AND a.user_id like ('" . $_SESSION['loggedUser'] . "')")->fetchAll('assoc');
        foreach($answers as $a){
            if(in_array($a['id'], $_POST['answers'])){
                $connection->execute("UPDATE answers SET confirmed = 1 WHERE event_id = " . $form['event_id'] . " AND id = " . $a['id']);
            } else {
                $connection->execute("UPDATE answers SET confirmed = -1 WHERE event_id = " . $form['event_id'] . " AND id = " . $a['id']);
                $connection->execute("UPDATE additional_speakers SET confirmed = -1 WHERE answer_id = " . $a['id']);
            }
        }

        $additionalAnswers = $connection->execute("SELECT a.id FROM answers as a, forms as f, accepts as c, additional_speakers as s" .
                " WHERE c.answer_id = a.id AND c.vote = 1 AND s.confirmed = 0 AND s.informed = 1 AND f.id = a.form_id AND a.event_id = " . $form['event_id'] . " AND a.id = s.answer_id" .
                " AND s.user_id like ('" . $_SESSION['loggedUser'] . "')")->fetchAll('assoc');
        foreach($additionalAnswers as $a){
            if(in_array($a['id'], $_POST['additionalanswers'])){
                $connection->execute("UPDATE additional_speakers SET confirmed = 1 WHERE answer_id = " . $a['id'] . " AND user_id like ('" . $_SESSION['loggedUser'] . "')");
            } else {
                $connection->execute("UPDATE additional_speakers SET confirmed = -1 WHERE answer_id = " . $a['id'] . " AND user_id like ('" . $_SESSION['loggedUser'] . "')");
            }
        }

        $responses = $connection->execute("SELECT r.id FROM responses as r, accepts as a, responses_to_topics as t" .
                " WHERE a.response_to_topic_id = t.id AND a.vote = 1 AND r.confirmed = 0 AND r.informed = 1 AND a.event_id = " . $form['event_id'] . " AND t.response_id = r.id AND r.user_id like ('" . $_SESSION['loggedUser'] . "')")
            ->fetchAll('assoc');
        foreach($responses as $r){
            if(in_array($r['id'], $_POST['responses'])){
                $connection->execute("UPDATE responses SET confirmed = 1 WHERE event_id = " . $form['event_id'] . " AND id = " . $r['id']);
            } else {
                $connection->execute("UPDATE responses SET confirmed = -1 WHERE event_id = " . $form['event_id'] . " AND id = " . $r['id']);
                $connection->execute("UPDATE additional_speakers SET confirmed = -1 WHERE response_id = " . $r['id']);
            }
        }

        $additionalResponses = $connection->execute("SELECT distinct r.id, r.title FROM responses as r, accepts as a, responses_to_topics as t, additional_speakers as s" .
                " WHERE a.response_to_topic_id = t.id AND a.vote = 1 AND s.confirmed = 0 AND s.informed = 1 AND a.event_id = " . $form['event_id'] . " AND t.response_id = r.id AND r.id = s.response_id" .
                " AND s.user_id like ('" . $_SESSION['loggedUser'] . "')")->fetchAll('assoc');
        foreach($additionalResponses as $r){
            if(in_array($r['id'], $_POST['additionalresponses'])){
                $connection->execute("UPDATE additional_speakers SET confirmed = 1 WHERE response_id = " . $r['id'] . " AND user_id like ('" . $_SESSION['loggedUser'] . "')");
            } else {
                $connection->execute("UPDATE additional_speakers SET confirmed = -1 WHERE response_id = " . $r['id'] . " AND user_id like ('" . $_SESSION['loggedUser'] . "')");
            }
        }

        try{
            if(count($exists) < 1 or (count($responses) > 0 and $hasCode < 1)){
                $this->sendConfirm($connection, $form, $answerId, count($responses) > 0);
                $_SESSION['successMessage'][] = "Check your email address (" . $_SESSION['loggedUser'] . ") for additional information.";
            }
            $_SESSION['successMessage'][] = "Your answer has been accepted.";

        }catch(\Exception $e){
            $_SESSION['errorMessage'][] = $e->getMessage();
        }
        return $this->redirect("/acceptproposals");
    }

    public function delete($connection, $eventId){
        $event = $connection->execute('SELECT id, year, name, redhat_event, GROUP_CONCAT(topic_id) AS topics, cfp_close FROM cfp.events, cfp.topics_to_events WHERE id = ' . $eventId
            . ' AND event_id = id')->fetchAll('assoc')[0];

        if($event['redhat_event'] == 0){
            $_SESSION['errorMessage'][] = "This event isn't organized by Red Hat. You can't confirm anything.";
            return $this->redirect("/");
        }

        $connection->execute("UPDATE responses SET confirmed = -1 WHERE event_id = " . $eventId . " AND user_id like ('" . $_SESSION['loggedUser'] . "')");
        $connection->execute("UPDATE answers SET confirmed = -1 WHERE user_id like ('" . $_SESSION['loggedUser'] . "') and event_id = " . $eventId);
        $connection->execute("UPDATE additional_speakers SET confirmed = -1 WHERE response_id in (SELECT id FROM responses WHERE user_id like ('" . $_SESSION['loggedUser'] . "') and event_id = " . $eventId . ") or " .
                "answer_id in (SELECT id FROM answers WHERE user_id like ('" . $_SESSION['loggedUser'] . "') and event_id = " . $eventId . ")");
        $connection->execute("UPDATE additional_speakers SET confirmed = -1 WHERE user_id like ('" . $_SESSION['loggedUser'] . "') and (response_id in (SELECT id FROM responses WHERE event_id = " . $eventId . ") or " .
                "answer_id in (SELECT id FROM answers WHERE event_id = " . $eventId . "))");

        $_SESSION['successMessage'][] = "Your rejection was accepted.";
        return $this->redirect("/acceptproposals");

    }
}
