<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Datasource\ConnectionManager;

/**
 * Static content controller
 *
 * This controller will render views from Template/HomePage/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class ReviewController extends AppController
{

    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Network\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function show(...$action)
    {
        if(!parent::getReviewer()){
    		return $this->redirect("/");
    	}

    	if(count($action) < 1 or !is_numeric($action[0])){
    		$_SESSION['errorMessage'][] = "Unsufficient data. Your link is probably broken.";
    		return $this->redirect("/reviewer");
    	}

        parent::printFlush($this->request->here());
        $connection = ConnectionManager::get('cfp');
    	$this->set("active", "reviewer");
    	$this->set("admin", parent::getAdmin() | parent::getSuperUser());
    	$this->set("reviewer", parent::getReviewer());
    	$this->set("program_manager", parent::getProgramManager());
    	$form = $connection->execute('SELECT type, placeholder FROM forms WHERE type = "' . $action[1] . '" and event_id = ' . $action[0])->fetch("assoc");

        $this->set("form", $form);
        $this->set("inflector", parent::getInflector());

    	if($this->request->session()->read("first-name")){
    	        $this->set('username', $_SESSION['first-name'] . " " . $_SESSION['last-name']);
    	}

    	if(!parent::checkFormType($action[1])){
    		$_SESSION['errorMessage'][] = "You don't have access to that review process. Permission denied.";
    		return $this->redirect("/reviewer");
    	}

    	$event = $connection->execute('SELECT name, year, blind_review FROM cfp.events WHERE id  = ' . $action[0])->fetchAll('assoc');
    	$topics = $connection->execute('SELECT topics FROM cfp.reviewers WHERE user_id like ("' . $_SESSION['loggedUser'] . '") and captain=0 and event_id = ' . $action[0] . " and form_type like ('" . $action[1] . "')")->fetchAll('assoc');
    	if( count($topics) < 1){
    		$_SESSION['errorMessage'][] = "You don't have access to that page. Permission denied.";
    		return $this->redirect("/reviewer");
    	}
    	if($action[1] == 'session'){
    		$myTopics = $connection->execute('SELECT id, name FROM cfp.topics WHERE id in (' . $topics[0]['topics'] . ')')->fetchAll('assoc');
    		$responses = $connection->execute('SELECT r.title, r.id as response_id, r.type, r.duration, r.difficulty, r.abstract, r.summary, r.notes, r.keywords, r.notes2, r.notes3, r.answer_id,'.
    			' CONCAT(u.first_name, " ", u.last_name, " (", u.email, ")") as speaker, u.country, c.id, t.id as topic_id, t.name as topic, sum(v.vote) as score FROM'.
    			' cfp.topics as t, cfp.users as u, cfp.responses as r, cfp.responses_to_topics as c LEFT JOIN votes as v on c.id = v.response_to_topic_id WHERE'.
    			' u.id = r.user_id AND c.event_id = ' . $action[0] .' AND t.id in (' . $topics[0]['topics'] . ') AND r.id = c.response_id AND t.id = c.topic_id'.
    			' GROUP BY c.id ORDER BY t.name, c.id')->fetchAll('assoc');
    		$votes = $connection->execute('SELECT response_to_topic_id as response_id, vote FROM cfp.votes WHERE reviewer_id like ("' . $_SESSION['loggedUser'] . '") and event_id = ' . $action[0])->fetchAll('assoc');
    	} else {
    		$answers = $connection->execute('SELECT a.id, CONCAT(u.first_name, " ", u.last_name, " (", u.email, ")") as speaker, a.form_id FROM answers as a, forms as f, users as u WHERE '.
    						'f.id = a.form_id AND f.type like ("' . $action[1] . '") and a.user_id = u.id and f.event_id = ' . $action[0])->fetchAll('assoc');
    		$responses = array();

    		foreach ($answers as $a){
    			$res = $connection->execute('SELECT q.question, ap.text, ap.as_title, q.take_as FROM questions as q, answer_parts as ap WHERE ap.answer_id = ' . $a['id'] .
    							' AND ap.question_id = q.id and q.form_id = ' . $a['form_id'])->fetchAll('assoc');
    			$newRes = array("answer_id"=>$a['id'], "speaker"=>$a['speaker']);
    			foreach($res as $r){
                    if($r['as_title'] == 1){
        				$newRes['Title'] = $r['text'];
                    } else if($r['take_as'] == 'topics'){
                            $curTops = $connection->execute('SELECT GROUP_CONCAT(" ", name) as name FROM topics WHERE id IN(' . $r['text'] . ')')->fetch("assoc");
    			$newRes[$r['question']] = $curTops['name'];
        	} else if($r['as_duration'] == 1) {
        	        		$newRes['Duration'] = $r['text'];
        	} else {
            				$newRes[$r['question']] = $r['text'];
    		
                    }
    			}
    			$responses[] = $newRes;
    		}
    		$votes = $connection->execute('SELECT answer_id, vote FROM cfp.votes WHERE reviewer_id like ("' . $_SESSION['loggedUser'] . '") and event_id = ' . $action[0])->fetchAll('assoc');
    	}
    	$token = $this->request->getParam('_csrfToken');

    	$accepted = array();
    	$rejected = array();
    	$skipped = array();
    	$unreviewed = array();


    	foreach($responses as $r){
    		$added = false;
    		if($action[1] == 'session'){
    			$r['comments'] = $connection->execute('SELECT author, comment, time, public FROM cfp.comments WHERE response_id = ' . $r['response_id'] . ' ORDER BY id desc')->fetchAll('assoc');
    			$additional = $connection->execute('SELECT CONCAT(u.first_name, " ", u.last_name, " (", u.email, ")") as additional from users u, additional_speakers a WHERE u.id = a.user_id AND a.response_id = '
    				. $r['response_id'])->fetchAll('assoc');
    			if (is_array($additional) && count($additional) > 0){
    				$r['speaker'] .= " (main speaker)";
    				foreach($additional as $k => $a) {
    					$r['speaker'] .= ", " . $a['additional'];
    				}
    			}
    			$note = $connection->execute('SELECT q.question FROM questions as q, forms as f WHERE take_as like ("notes") and form_id = f.id and f.event_id = ' . $action[0] . ' and f.type like ("session")')->fetchAll('assoc');

    			if(count($note) > 0){
    				$r[$note[0]['question']] = $r['notes'];
    			}
    			unset($r['notes']);
    			$note = $connection->execute('SELECT q.question FROM questions as q, forms as f WHERE take_as like ("notes2") and form_id = f.id and f.event_id = ' . $action[0] . ' and f.type like ("session")')->fetchAll('assoc');
    			if(count($note) > 0){
    				$r[$note[0]['question']] = $r['notes2'];
    			}
    			unset($r['notes2']);
    			$note = $connection->execute('SELECT q.question FROM questions as q, forms as f WHERE take_as like ("notes3") and form_id = f.id and f.event_id = ' . $action[0] . ' and f.type like ("session")')->fetchAll('assoc');
    			if(count($note) > 0){
    				$r[$note[0]['question']] = $r['notes3'];
    			}
    			unset($r['notes3']);
    			if($r['answer_id'] != ""){
    				$answers = $connection->execute('SELECT q.question, ap.text FROM questions as q, answer_parts as ap, answers as a WHERE a.id = ' . $r['answer_id'] .
    					' AND a.form_id = q.form_id and ap.question_id = q.id AND ap.answer_id = a.id')->fetchAll('assoc');
    				$r['answers'] = $answers;
    			}

    			$response_topic = $connection->execute('SELECT GROUP_CONCAT(t.name SEPARATOR ", ") as topics FROM cfp.topics as t, cfp.responses_to_topics as r WHERE r.topic_id = t.id AND r.response_id = ' . $r['response_id'])->fetchAll('assoc');
    			$r['topics'] = $response_topic[0]['topics'];
    			foreach($votes as $v){
    				if($v['response_id'] == $r['id']){
    					$added = true;
                        $r['vote'] = $v['vote'];
    					if ($v['vote'] > -1){
                            if($v['vote'] == 0){
                                $skipped[$r['topic_id']][] = $r;
                            } else {
        						$accepted[$r['topic_id']][] = $r;
                            }
    					}else{
    						$rejected[$r['topic_id']][] = $r;
    					}
    				}
    			}
    			if(!$added){
    					$unreviewed[$r['topic_id']][] = $r;
    			}
    		} else {
    			$r['comments'] = $connection->execute('SELECT author, comment, time, public FROM cfp.comments WHERE answer_id = ' . $r['answer_id'] . ' ORDER BY id desc')->fetchAll('assoc');
    			$additional = $connection->execute('SELECT CONCAT(u.first_name, " ", u.last_name, " (", u.email, ")") as additional from users u, additional_speakers a WHERE u.id = a.user_id AND a.answer_id = '
    					. $r['answer_id'])->fetchAll('assoc');
    			if (is_array($additional) && count($additional) > 0){
    				$r['speaker'] .= " (main speaker)";
    				foreach($additional as $k => $a) {
    					$r['speaker'] .= ", " . $a['additional'];
    				}
    			}
    			foreach($votes as $v){
    				if($v['answer_id'] == $r['answer_id']){
    					$added = true;
                        $r['vote'] = $v['vote'];
    					if ($v['vote'] > -1){
                            if($v['vote'] == 0){
                                $skipped[] = $r;
                            } else {
        						$accepted[] = $r;
                            }
    					}else{
    						$rejected[] = $r;
    					}
    				}
    			}
    			if(!$added){
    				$unreviewed[] = $r;
    			}
    			$myTopics = null;
    		}
    	}

    $this->request->session()->write("token", $token);
    $this->set("token", $token);
    $this->set("topics", $myTopics);
    $this->set("eventInfo", $event[0]);
    $this->set("event", $action[0]);
    $this->set("accepted", $accepted);
    $this->set("rejected", $rejected);
    $this->set("skipped", $skipped);
    $this->set("unreviewed", $unreviewed);
    $this->set("reviewer", $_SESSION['loggedUser']);
    $this->set("blindRev", $event[0]['blind_review']);

    try {
    	if($action[1] == 'session'){
    		$this->render("home");
    	} else {
    		$this->render('answers');
    	}
        } catch (MissingTemplateException $exception) {
            if (Configure::read('debug')) {
                throw $exception;
            }
            throw new NotFoundException();
        }
    }
}
