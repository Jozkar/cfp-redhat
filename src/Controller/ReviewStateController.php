<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Datasource\ConnectionManager;

/**
 * Static content controller
 *
 * This controller will render views from Template/HomePage/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class ReviewStateController extends AppController
{

    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Network\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function show(...$action)
    {
    if(!parent::getReviewer()){
    	return $this->redirect("/");
    }
    	
    if(count($action) < 1 or !is_numeric($action[0])){
    	$_SESSION['errorMessage'][] = "Unsufficient data. Your link is probably broken.";
    	return $this->redirect("/reviewer");
    }	

    parent::printFlush($this->request->here());

    $connection = ConnectionManager::get('cfp');

    $this->set("active", "reviewer");
    $this->set("admin", parent::getAdmin() | parent::getSuperUser());
    $this->set("reviewer", parent::getReviewer());
    $this->set("program_manager", parent::getProgramManager());       
    $form = $connection->execute('SELECT type, placeholder FROM forms WHERE type = "' . $action[1] . '" and event_id = ' . $action[0])->fetch("assoc");

    $this->set("form", $form);
    $this->set("inflector", parent::getInflector());

    if($this->request->session()->read("first-name")){
            $this->set('username', $_SESSION['first-name'] . " " . $_SESSION['last-name']);
    }

    if(!parent::checkFormType($action[1])){
    	$_SESSION['errorMessage'][] = "You don't have access to that review process. Permission denied.";
    	return $this->redirect("/reviewer");
    }

    $event = $connection->execute('SELECT name, year, GROUP_CONCAT(topic_id) AS topics FROM cfp.events, cfp.topics_to_events WHERE id  = ' . $action[0] . ' AND event_id = id')->fetchAll('assoc');
    $topics = $connection->execute('SELECT topics FROM cfp.reviewers WHERE user_id like ("' . $_SESSION['loggedUser'] . '") and captain=1 and form_type like ("' . $action[1] . '") and event_id = ' . $action[0])->fetchAll('assoc');

    if(count($topics) < 1){
    	$_SESSION['errorMessage'][] = "You don't have access to that page. Permission denied.";
    	return $this->redirect("/reviewer");
    }

    if($action[1] == 'session'){
    	$myTopics = $connection->execute('SELECT id, name FROM cfp.topics where id in (' . $topics[0]['topics'] . ')')->fetchAll('assoc');

        $reviewers = array();
        foreach($myTopics as $mt){
            $reviewers[$mt['id']] = $connection->execute('SELECT u.id, CONCAT(u.first_name, " ", u.last_name, " (", u.id, ")") as name, 0 as accepted, 0 as rejected, 0 as skipped, 0 as unreviewed FROM users as u, reviewers as r' .
                ' WHERE r.user_id = u.id AND r.captain = 0 AND FIND_IN_SET(' . $mt['id'] . ', r.topics) AND r.event_id = ' . $action[0] . ' AND r.form_type like ("' . $action[1] . '")')->fetchAll("assoc");
        }

        $votes = $connection->execute('SELECT r.topic_id, vote, reviewer_id FROM responses_to_topics as r, votes WHERE response_to_topic_id = r.id AND captain = 0 AND r.event_id = ' . $action[0] .
             ' ORDER BY reviewer_id')->fetchAll('assoc'); 

        $prop = $connection->execute('SELECT topic_id, count(topic_id) as cnt FROM responses_to_topics, responses WHERE topic_id IN (' . $topics[0]['topics'] . ')' .
             ' AND response_id = responses.id AND responses_to_topics.event_id = ' . $action[0] . ' GROUP BY topic_id ORDER BY topic_id')->fetchAll('assoc');
        $proposals = array();
        $reviewer = array();
        foreach($prop as $p){
            $proposals[$p['topic_id']] = array("total" => $p['cnt'], "done" => 0);
            $localRev = array();
            foreach($votes as $v){
                if($v['topic_id'] == $p['topic_id']){
                    if(isset($localRev[$v['reviewer_id']])){
                        if($v['vote'] > 0){
                            $localRev[$v['reviewer_id']]['accepted']++;
                        } else if($v['vote'] == 0){
                            $localRev[$v['reviewer_id']]['skipped']++;
                        } else {
                            $localRev[$v['reviewer_id']]['rejected']++;
                        }
                    } else {
                        $localRev[$v['reviewer_id']] = array("accepted" => 0, "skipped" => 0, "rejected" => 0);
                        if($v['vote'] > 0){
                            $localRev[$v['reviewer_id']]['accepted']++;
                        } else if($v['vote'] == 0){
                            $localRev[$v['reviewer_id']]['skipped']++;
                        } else {
                            $localRev[$v['reviewer_id']]['rejected']++;
                        }
                    }
                }
            }
            foreach($reviewers[$p['topic_id']] as $k => $r){
                $reviewers[$p['topic_id']][$k]['unreviewed'] = $p['cnt'];
                foreach($localRev as $lrk => $lr){
                    if($lrk == $r['id']){
                        $reviewers[$p['topic_id']][$k]['accepted'] = $lr['accepted'];
                        $reviewers[$p['topic_id']][$k]['skipped'] = $lr['skipped'];
                        $reviewers[$p['topic_id']][$k]['rejected'] = $lr['rejected'];
                        $reviewers[$p['topic_id']][$k]['unreviewed'] = $p['cnt'] - $lr['rejected'] - $lr['skipped'] - $lr['accepted'];
                        if($reviewers[$p['topic_id']][$k]['unreviewed'] == 0){
                            $proposals[$p['topic_id']]['done']++;
                        }
                    }
                }
            }
        }

    } else {

        $reviewers = $connection->execute('SELECT u.id, CONCAT(u.first_name, " ", u.last_name, " (", u.id, ")") as name, 0 as accepted, 0 as rejected, 0 as skipped, 0 as unreviewed FROM users as u, reviewers as r WHERE' .
            '  r.user_id = u.id AND r.captain = 0 AND r.event_id = ' . $action[0] . ' AND r.form_type like ("' . $action[1] . '") ORDER BY u.id')->fetchAll('assoc');

        $votes = $connection->execute('SELECT vote, reviewer_id FROM votes WHERE answer_id IN (SELECT id FROM answers WHERE form_id = (SELECT id FROM forms WHERE event_id = ' . $action[0] . ' AND type like ("' . $action[1] . '")))' .
            ' AND captain = 0 AND event_id = ' . $action[0] . ' ORDER BY reviewer_id')->fetchAll('assoc');

        $prop = $connection->execute('SELECT count(id) as cnt FROM answers WHERE form_id = (SELECT id FROM forms WHERE type like("' . $action[1] . '") AND event_id = ' . $action[0] . ')')->fetchAll('assoc');
        $myTopics = null;

        $reviewer = array();
        $localRev = array();
        $proposals = array("total" => $prop[0]['cnt'], "done" => 0);
        foreach($votes as $v){
            if(isset($localRev[$v['reviewer_id']])){
                if($v['vote'] > 0){
                    $localRev[$v['reviewer_id']]['accepted']++;
                } else if($v['vote'] == 0){
                    $localRev[$v['reviewer_id']]['skipped']++;
                } else {
                    $localRev[$v['reviewer_id']]['rejected']++;
                }
            } else {
                $localRev[$v['reviewer_id']] = array("accepted" => 0, "skipped" => 0, "rejected" => 0);
                if($v['vote'] > 0){
                    $localRev[$v['reviewer_id']]['accepted']++;
                } else if($v['vote'] == 0){
                    $localRev[$v['reviewer_id']]['skipped']++;
                } else {
                    $localRev[$v['reviewer_id']]['rejected']++;
                }
            }
        }
        foreach($reviewers as $k => $r){
            $reviewers[$k]['unreviewed'] = $prop[0]['cnt'];
            foreach($localRev as $lrk => $lr){
                if($lrk == $r['id']){
                    $reviewers[$k]['accepted'] = $lr['accepted'];
                    $reviewers[$k]['skipped'] = $lr['skipped'];
                    $reviewers[$k]['rejected'] = $lr['rejected'];
                    $reviewers[$k]['unreviewed'] = $prop[0]['cnt'] - $lr['rejected'] - $lr['skipped'] - $lr['accepted'];
                    if($reviewers[$k]['unreviewed'] == 0){
                       $proposals['done']++;
                    }
                }
            }
        }
    }

    $token = $this->request->getParam('_csrfToken');

    $this->request->session()->write("token", $token);
    $this->set("token", $token);
    $this->set("topics", $myTopics);
    $this->set("eventInfo", $event[0]);		
    $this->set("event", $action[0]);
    $this->set("reviewers", $reviewers);
    $this->set("proposals", $proposals);

    try {
    	if($action[1] == 'session'){
     		$this->render("home");
    	} else {
    		$this->render("answers");
    	}
        } catch (MissingTemplateException $exception) {
            if (Configure::read('debug')) {
                throw $exception;
            }
            throw new NotFoundException();
        }
    }
}
