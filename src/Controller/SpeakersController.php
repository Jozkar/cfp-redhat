<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Datasource\ConnectionManager;
/**
 * Static content controller
 *
 * This controller will render views from Template/HomePage/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class SpeakersController extends AppController
{

    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Network\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function show(...$action)
    {

    if(!parent::getAdmin() && !parent::getProgramManager() && !parent::getSuperUser()){
    	return $this->redirect("/");
    }

    if(count($action) < 1 || !is_numeric($action[0])){
    	$_SESSION['errorMessage'][] = "Unsufficient data. Your link is probably broken.";
    	if(parent::getAdmin() | parent::getSuperUser()){
    		return $this->redirect("/admin");
    	} else {
    		return $this->redirect("/manager");
    	}
    }

    $connection = ConnectionManager::get('cfp');
    	
    $this->set("admin", parent::getAdmin() | parent::getSuperUser());
    $this->set("reviewer", parent::getReviewer());
    $this->set("program_manager", parent::getProgramManager());


    $eventInfo = $connection->execute("SELECT id, name, year, logo FROM events WHERE id = " . $action[0])->fetchAll("assoc");
    $allUsers = $connection->execute("SELECT email, first_name, last_name, organization, avatar, bio, position, country FROM users WHERE id in (SELECT user_id FROM responses WHERE id IN (SELECT response_id FROM responses_to_topics WHERE event_id = " . $action[0] . " )) OR id IN (SELECT user_id FROM additional_speakers WHERE response_id IN (SELECT response_id FROM responses_to_topics WHERE event_id = " . $action[0] . " ))")->fetchAll("assoc");

    if(count($action) >= 2 && ($action[1] == "export" || $action[1] == "exportAll")) {
    	$this->stats($eventInfo[0], $allUsers, $action[1]);
    } else {
    	$this->set("eventInfo", $eventInfo[0]);
    	$this->set("users", $allUsers);
    	$this->set("active", "");
    	$this->set('username', $_SESSION['first-name']." ".$_SESSION['last-name']);

    	try {
    	    $this->render('list');
    	} catch (MissingTemplateException $exception) {
    	    if (Configure::read('debug')) {
    	        throw $exception;
    	    }
    	    throw new NotFoundException();
    	}
    }
    }

    public function stats($evinfo, $users, $type){
    if($type == "export") {
    	$this->response->download(str_replace(" ", "-", $evinfo['name']) . "-" . $evinfo['year'] . "-speakers-email.csv");
    } else {
    	$this->response->download(str_replace(" ", "-", $evinfo['name']) . "-" . $evinfo['year'] . "-speakers-all.csv");
    }
    $this->set("data", $users);
    $this->set("type", $type);
    $this->layout = 'ajax';
    $this->render("speakers");
    return;
    }
}
