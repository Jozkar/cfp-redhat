<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class FormUpdateController extends Controller
{

    public function initialize()
    {

        parent::initialize();

        $this->loadComponent('RequestHandler');
    }
    
    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Network\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function manage(...$path)
    {

        $token = $this->request->getData("token");
        $type = $this->request->getData("type");
    $form_id = $this->request->getData("formId");
    $action = $this->request->getData("action");
    $id = $this->request->getData("id");

    if($token == null){
    	die(json_encode(array("result"=>"error", "reason"=>"No token received")));
    }

    if($token != $this->request->session()->read('token')){
    	die(json_encode(array("result"=>"error", "reason"=>"Token expired. Please, refresh your page.")));
    }

    $connection = ConnectionManager::get('cfp');
    if(count($path) < 1){
    	if($action == "delete"){
    		$this->delete($connection, $id, $form_id, $type);
    	}
    	if($action == "order"){
    		$order = $this->request->getData("order");
    		$this->reorder($connection, $form_id, $order);
    	}
    	switch($type){
    		case "hidden":
    		case "textarea":
    		case "text":
    			$question = $this->request->getData("question");
    			$helpText = $this->request->getData("helpText");
    			$storeAs = $this->request->getData("storage");
    			$size = $this->request->getData("size");
    			$required = $this->request->getData("required");
                $asTitle = $this->request->getData("asTitle");
                $asDescription = $this->request->getData("asDescription");
                $asDuration = $this->request->getData("asDuration");
                $defValue = $this->request->getData("defValue");
    			if($this->check($connection, $id, $form_id)){
    				if($action == "update"){
    					$this->update($connection, $id, $form_id, $type, $question, $helpText, $storeAs, $size, $required, $asTitle, $asDescription, $asDuration, $defValue, true);
    				} else {
    					die(json_encode(array("result"=>"error", "reason"=>"You're trying to add new field with existing ID. That indicates, someone else is modifying this form at the same time. Please refresh this page to get curretn form content.")));
    				}
    			} else {
    				$this->add($connection, $id, $form_id, $type, $question, $helpText, $storeAs, $size, $required, $asTitle, $asDescription, $asDuration, $defValue, true);
    			}
    			break;
    		case "description":
    			$title = $this->request->getData("title");
    			$description = $this->request->getData("description");
    			if($this->check($connection, $id, $form_id)){
    				if($action == "update"){
    					$this->updateTitle($connection, $id, $form_id, $title, $description);
    				} else {
    					die(json_encode(array("result"=>"error", "reason"=>"You're trying to add new field with existing ID. That indicates, someone else is modifying this form at the same time. Please refresh this page to get curretn form content.")));
    				}
    			}else{
    				$this->addTitle($connection, $id, $form_id, $title, $description);
    			}
    			break;
    		case "radio":
    		case "checkbox":
    			$question = $this->request->getData("question");
    			$helpText = $this->request->getData("helpText");
    			$storeAs = $this->request->getData("storage");
    			$options = $this->request->getData("options");
    			$asDuration = $this->request->getData("asDuration");
    			$values = $this->request->getData("values");
    			$required = $this->request->getData("required");
    			if($this->check($connection, $id, $form_id)){
    				if($action == "update"){
    					$this->update($connection, $id, $form_id, $type, $question, $helpText, $storeAs, 0, $required, 0, 0, $asDuration, "", false);
    					if($storeAs != "topics"){
    						$connection->execute("DELETE FROM question_options WHERE question_id = " . $id . " AND form_id = " . $form_id);
    						$this->addOptions($connection, $id, $form_id, $options, $values, false);
    					}
    					die(json_encode(array("result"=>"success", "id"=>$id, "action"=>"update")));
    				} else {
    					die(json_encode(array("result"=>"error", "reason"=>"You're trying to add new field with existing ID. That indicates, someone else is modifying this form at the same time. Please refresh this page to get curretn form content.")));
    				}		
    			} else {
    				$this->add($connection, $id, $form_id, $type, $question, $helpText, $storeAs, 0, $required, 0, 0, $asDuration, "", false);
    				if($storeAs != "topics"){
    					$this->addOptions($connection, $id, $form_id, $options, $values, true);
    				}
    				die(json_encode(array("result"=>"success", "id"=>$id, "action"=>"add")));
    			}

    		
    			break;
    		default: die(json_encode(array("result"=>"error", "reason"=>"Unsupported field type.")));
    			 break;
    	}
    }
    }

    public function check($connection, $id, $formId){
    $res = $connection->execute("SELECT 1 FROM cfp.questions WHERE form_id = " . $formId . " AND id = " . $id );
    if(count($res) > 0){
    	return true;
    } else {
    	return false;
    }
    }

    public function delete($connection, $id, $formId, $type){
    if($connection->execute("DELETE FROM cfp.questions WHERE id = " . $id . " AND form_id = " . $formId)->errorCode() != 0){
    	die('{"result": "error", "reason": "Database error. This question couldn\'t be removed."}');
    }else{
    	if($type == "radio" || $type == "checkbox"){
    		$connection->execute("DELETE FROM cfp.question_options WHERE question_id = " . $id . " AND form_id = " . $formId);
    	}

    	die(json_encode(array("result"=>"success", "id"=> $id, "action"=>"delete")));
    }
    }

    public function addTitle($connection, $id, $formId, $title, $description){
    if($connection->execute("INSERT INTO questions (id, form_id, question, type, help_text, question_order) VALUES (" . $id . ", " . $formId . ", '" . str_replace("'", "\'", nl2br($title)) .
    		     "', 'description', '" . str_replace("'", "\'", nl2br($description)) . "', " . $id . ")")->errorCode() != 0){
    	die(json_encode(array("result"=>"error", "reason"=>"Database error. This title/description can't be stored.")));
    } else {
    	die(json_encode(array("result"=>"success", "id"=>$id, "action"=>"add")));
    }
    }

    public function updateTitle($connection, $id, $formId, $title, $description){
    if($connection->execute("UPDATE questions SET question = '" . str_replace("'", "\'", nl2br($title)) . "', help_text = '" . str_replace("'", "\'", nl2br($description)) . "' WHERE id = " . $id . " AND form_id = " . $formId)
    	->errorCode() != 0){
    	die(json_encode(array("result"=>"error", "reason"=>"Database error. This title/description couldn't be updated.")));
    } else {
    	die(json_encode(array("result"=>"success", "id"=>$id, "action"=>"update")));
    }
    }

    public function add($connection, $id, $formId, $type, $question, $helpText, $storeAs, $size, $required, $asTitle, $asDescription, $asDuration, $defValue, $dieOnSuccess){
    if($connection->execute("INSERT INTO questions (id, form_id, question, type, help_text, take_as, question_order, size, required, as_title, as_description, as_duration, default_value) VALUES (" . $id . ", " . $formId . ", '" .
     str_replace("'", "\'", nl2br($question)) . "', '" . $type . "', '" . str_replace("'", "\'", nl2br($helpText)) . "', '" . $storeAs . "', " . $id . ", " . $size . ", " . $required . ", " . $asTitle . ", " .
     $asDescription . ", " . $asDuration . ", '" . $defValue . "')")->errorCode() != 0){
    	die(json_encode(array("result"=>"error", "reason"=>"Database error. This question can't be stored.")));
    } else {
        if($asTitle == 1 && ($type == "checkbox" || $type == "radio" || $type == "hidden")){
            $asTitle = 0;
        }

        if($asTitle == 1){
            $connection->execute("UPDATE questions SET as_title = 0 WHERE form_id = " . $formId);
            $connection->execute("UPDATE questions SET as_title = 1 WHERE id = " . $id . " and form_id = " . $formId);
            $connection->execute("UPDATE answer_parts SET as_title = 0 WHERE answer_id in (SELECT id FROM answers WHERE form_id = " . $formId . ")");
        } else {
            $connection->execute("UPDATE questions SET as_title = 0 WHERE id = " . $id . " and form_id = " . $formId);
        }
        
        if($asDescription == 1 && ($type == "checkbox" || $type == "radio" || $type == "hidden")){
            $asDescription = 0;
        }

        if($asDescription == 1){
            $connection->execute("UPDATE questions SET as_description = 0 WHERE form_id = " . $formId);
            $connection->execute("UPDATE questions SET as_description = 1 WHERE id = " . $id . " and form_id = " . $formId);
            $connection->execute("UPDATE answer_parts SET as_description = 0 WHERE answer_id in (SELECT id FROM answers WHERE form_id = " . $formId . ")");
        } else {
            $connection->execute("UPDATE questions SET as_description = 0 WHERE id = " . $id . " and form_id = " . $formId);
        }

        if($asDuration == 1 && ($type == "text" || $type == "textarea")){
            $asDuration = 0;
        }

        if($asDuration == 1){
            $connection->execute("UPDATE questions SET as_duration = 0 WHERE form_id = " . $formId);
            $connection->execute("UPDATE questions SET as_duration = 1 WHERE id = " . $id . " and form_id = " . $formId);
            $connection->execute("UPDATE answer_parts SET as_duration = 0 WHERE answer_id in (SELECT id FROM answers WHERE form_id = " . $formId . ")");
        } else {
            $connection->execute("UPDATE questions SET as_duration = 0 WHERE id = " . $id . " and form_id = " . $formId);
        }
    	if($dieOnSuccess){
    		die(json_encode(array("result"=>"success", "id"=>$id, "action"=>"add")));
    	}
    }
    }

    public function update($connection, $id, $formId, $type, $question, $helpText, $storeAs, $size, $required, $asTitle, $asDescription, $asDuration, $defValue, $dieOnSuccess){
        $oldType = $connection->execute("SELECT type FROM questions WHERE id = " . $id . " AND form_id = " . $formId)->fetchAll('assoc')[0];
        if($connection->execute("UPDATE questions SET question='" . str_replace("'", "\'", nl2br($question)) . "', type='". $type . "', help_text='" . str_replace("'", "\'", nl2br($helpText)) . "', take_as='"
             . $storeAs . "', size=" . $size . ", required=" . $required . ", as_title = " . $asTitle . ", as_description = " . $asDescription . ", as_duration = " . $asDuration . ", default_value = '" . $defValue
             . "' WHERE id = " . $id . " AND form_id = " . $formId)->errorCode() != 0){
    	    die(json_encode(array("result"=>"error", "reason"=>"Database error. This question can't be updated.")));
        } else {
        	if(($oldType['type'] == "checkbox" || $oldType['type'] == "radio") && ($type != "checkbox" && $type != "radio")){
    	    	$connection->execute("DELETE FROM question_options WHERE question_id = " . $id . " AND from_id = " . $form_id);
    	    }

            if($asTitle == 1 && ($type == "checkbox" || $type == "radio" || $type == "hidden")){
                $asTitle = 0;
            }

            if($asTitle == 1){
                $connection->execute("UPDATE questions SET as_title = 0 WHERE form_id = " . $formId);
                $connection->execute("UPDATE questions SET as_title = 1 WHERE id = " . $id . " and form_id = " . $formId);
                $connection->execute("UPDATE answer_parts SET as_title = 0 WHERE answer_id in (SELECT id FROM answers WHERE form_id = " . $formId . ")");
                $connection->execute("UPDATE answer_parts SET as_title = 1 WHERE answer_id in (SELECT id FROM answers WHERE form_id = " . $formId . ") and question_id = " . $id);
            } else {
                $connection->execute("UPDATE questions SET as_title = 0 WHERE id = " . $id . " and form_id = " . $formId);
                $connection->execute("UPDATE answer_parts SET as_title = 0 WHERE answer_id in (SELECT id FROM answers WHERE form_id = " . $formId . ") and question_id = " . $id);
            }

    	    if($asDescription == 1 && ($type == "checkbox" || $type == "radio" || $type == "hidden")){
                $asDescription = 0;
            }

            if($asDescription == 1){
                $connection->execute("UPDATE questions SET as_description = 0 WHERE form_id = " . $formId);
                $connection->execute("UPDATE questions SET as_description = 1 WHERE id = " . $id . " and form_id = " . $formId);
                $connection->execute("UPDATE answer_parts SET as_description = 0 WHERE answer_id in (SELECT id FROM answers WHERE form_id = " . $formId . ")");
                $connection->execute("UPDATE answer_parts SET as_description = 1 WHERE answer_id in (SELECT id FROM answers WHERE form_id = " . $formId . ") and question_id = " . $id);
            } else {
                $connection->execute("UPDATE questions SET as_description = 0 WHERE id = " . $id . " and form_id = " . $formId);
                $connection->execute("UPDATE answer_parts SET as_description = 0 WHERE answer_id in (SELECT id FROM answers WHERE form_id = " . $formId . ") and question_id = " . $id);
            }

            if($asDuration == 1 && ($type == "text" || $type == "textarea")){
                $asDuration = 0;
            }

            if($asDuration == 1){
                $connection->execute("UPDATE questions SET as_duration = 0 WHERE form_id = " . $formId);
                $connection->execute("UPDATE questions SET as_duration = 1 WHERE id = " . $id . " and form_id = " . $formId);
                $connection->execute("UPDATE answer_parts SET as_duration = 0 WHERE answer_id in (SELECT id FROM answers WHERE form_id = " . $formId . ")");
                $connection->execute("UPDATE answer_parts SET as_duration = 1 WHERE answer_id in (SELECT id FROM answers WHERE form_id = " . $formId . ") and question_id = " . $id);
            } else {
                $connection->execute("UPDATE questions SET as_duration = 0 WHERE id = " . $id . " and form_id = " . $formId);
                $connection->execute("UPDATE answer_parts SET as_duration = 0 WHERE answer_id in (SELECT id FROM answers WHERE form_id = " . $formId . ") and question_id = " . $id);
            }

            if($dieOnSuccess){
    		    die(json_encode(array("result"=>"success", "id"=>$id, "action"=>"update")));
    	    }
        }
    }

    public function addOptions($connection, $id, $formId, $options, $values, $type) {
    foreach ($options as $i=>$o){
    	if($connection->execute("INSERT INTO question_options (id, question_id, form_id, label, value) VALUES (" . $i . ", " . $id . ", " . $formId . ", '" . str_replace("'", "\'", $o) . "', "
    			. "'" . str_replace("'", "\'", $values[$i]) . "')")->errorCode() != 0){
    		$connection->execute("DELETE FROM question_options WHERE question_id = " . $id . " AND form_id = " . $formId);
    		die(json_encode(array("result"=>"error", "reason"=>"Options couldn't be stored. Please try it again and check for using of unusual characters.")));
    	}
    }
    die(json_encode(array("result"=>"success", "id"=>$id, "action"=>($type?"add":"update"))));
    }

    public function reorder($connection, $formId, $order) {
    try{
    	foreach ($order as $i=>$q){
    		$connection->execute("UPDATE questions SET question_order = " . $i . " WHERE id = " . $q . " AND form_id = " . $formId);
    	}
    }catch(\Exception $e){
    	die(json_encode(array("result"=>"error", "reason"=>$e->getMessage())));
    }
    die(json_encode(array("result"=>"success", "action"=>"order")));
    }
}
