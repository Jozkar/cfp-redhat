<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Datasource\ConnectionManager;
/**
 * Static content controller
 *
 * This controller will render views from Template/HomePage/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class CaptainReviewersController extends AppController
{

    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Network\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function show(...$action)
    {

    	if(!parent::getReviewer()){
    		return $this->redirect("/");
    	}

    	if(count($action) < 1){
    		$_SESSION['errorMessage'][] = "Unsufficient data. Your link is probably broken.";
    		return $this->redirect("/reviewer");
    	}
    	$connection = ConnectionManager::get('cfp');

    	$isCaptain = $connection->execute("SELECT 1 FROM reviewers WHERE user_id like ('" . $_SESSION['loggedUser'] . "') AND captain=1 AND event_id = " . $action[0])->fetchAll("assoc");
    	$captainsTopics = $connection->execute("SELECT topics FROM reviewers WHERE user_id like ('" . $_SESSION['loggedUser'] . "') AND captain=1 AND form_type like ('session') AND event_id = " . $action[0])->fetchAll("assoc");

    	if(count($isCaptain) < 1){
    		return $this->redirect("/reviewer");
    	}		

    	if(isset($_POST['save'])){
    		if(isset($_SESSION['token']) && isset($_POST['_Token']) && $_POST['_Token'] == $_SESSION['token']){
    			if(isset($action[1]) && isset($_POST['user']) && $_POST['user'] != "" && $action[1] == "add"){
    				// get user id
    				$_POST['user'] = "'" . str_replace(array("'", "\""), array("\'", "\\\""), $_POST['user']) . "'";
    				$newReviewer = $connection->execute("SELECT id FROM users WHERE email like (" . $_POST['user'] . ")")->fetchAll("assoc");
    				if(!isset($_POST['formType'])){
    					$_POST['formType'] = 'session';
    					$_SESSION['warningMessage'][] = "Group of proposals set to 'Sessions'.";
    				}
    				if(!isset($_POST['topic']) && $_POST['formType'] == 'session'){
    					$_SESSION['errorMessage'][] = "You haven't specified any topic to review.";
    					return $this->redirect("/capreviewers/" . $action[0]);
    				} else {
    					return $this->modify($connection,$newReviewer[0]["id"],$action[0],$_POST['topic'], $_POST['formType']);
    				}
    			} else {
    				$_SESSION['errorMessage'][] = "You are not allowed to do this opperation";
    				return $this->redirect("/capreviewers/" . $action[0]);
    			}
    		} else {
    			$_SESSION['errorMessage'][] = "You are not allowed to do this opperation. Token expired.";
    				return $this->redirect("/capreviewers/" . $action[0]);
    		}
    	}

    	parent::printFlush($this->request->here());
    	$this->set("eventId", $action[0]);
    	$this->set("admin", parent::getAdmin() | parent::getSuperUser());
    	$this->set("reviewer", parent::getReviewer());
    	$this->set("program_manager", parent::getProgramManager());

    	$formAction = "add";			

    	// check, if are passed any arguments
    	if(!empty($action) && count($action) > 1){
    		//for delete action
    		if ($action[1] == "delete") {
    			$_SESSION['errorMessage'][] = "This action can't be performet - permission denied.";
                return $this->redirect("/capreviewers/".$action[0]);
    		}
    	}

    	$eventInfo = $connection->execute("SELECT name, year, GROUP_CONCAT(topic_id) AS topics FROM events, topics_to_events WHERE id = " . $action[0] . ' AND event_id = id')->fetchAll("assoc");
    	$allowedTopics = $connection->execute("SELECT id, name FROM topics WHERE id IN (" . (count($captainsTopics) > 0?$captainsTopics[0]['topics']:'-9999') . ")")->fetchAll("assoc");
    	$topi = $connection->execute("SELECT * FROM topics WHERE id IN (" . $eventInfo[0]['topics'] . ")")->fetchAll("assoc");
    	$allUsers = $connection->execute("SELECT distinct first_name, last_name, id, email, avatar FROM users LEFT OUTER JOIN reviewers as m ON (m.user_id = id)")->fetchAll("assoc");
    	$forms = $connection->execute("SELECT f.type, f.placeholder FROM forms as f, reviewers as r WHERE r.form_type = f.type AND f.event_id = r.event_id AND r.event_id = " . $action[0] . " and r.captain=1 and r.user_id like ('" . $_SESSION['loggedUser'] . "')")->fetchAll("assoc");
    	$searchQuery = array();
    	if(count($captainsTopics) > 0){
    		foreach(explode(",", $captainsTopics[0]['topics']) as $top) {
    			$searchQuery[] = " find_in_set(".$top.", m.topics) > 0 ";
    		}
    	}
    	if(count($searchQuery) < 1){
    		$searchQuery[] = "1";
    	}
    	$reviewerUsers = $connection->execute("SELECT distinct first_name, last_name, id, email, avatar, topics, form_type FROM users, reviewers as m WHERE m.user_id = id and m.event_id = " . $action[0]
    			 . " and m.captain = 0 and m.form_type in (SELECT form_type FROM reviewers WHERE event_id = " . $action[0] . " and captain=1 and user_id like ('" . $_SESSION['loggedUser'] . "')) and (" . 
    			"(find_in_set('session', (SELECT GROUP_CONCAT(form_type) FROM reviewers WHERE event_id = " . $action[0] . " and captain=1 and user_id like ('" . $_SESSION['loggedUser'] . "'))) > 0 and (" . 
    			implode("OR", $searchQuery) . ")) or m.form_type not like ('session') )")->fetchAll("assoc");
    	$allTopics = array();
    	foreach($topi as $t){
    		$allTopics[$t['id']] = $t;
    	}

    	$this->set("eventInfo", $eventInfo[0]);
    	$this->set("topics", $allTopics);		
    	$this->set("allowedTopics", $allowedTopics);	
    	$this->set("reviewers", $reviewerUsers);
    	$this->set("users", $allUsers);
    	$this->set("action", $formAction);
    	$this->set("forms", $forms);
    	$this->set("active", "");
        $this->set("inflector",parent::getInflector());
    	$token = $this->request->getParam('_csrfToken');

    	$_SESSION['token'] = $token;
    	$this->set("token", $token);
    	$this->set('username', $_SESSION['first-name']." ".$_SESSION['last-name']);
    
    	try {
    	    $this->render('list');
    	} catch (MissingTemplateException $exception) {
    	    if (Configure::read('debug')) {
    	        throw $exception;
    	    }
    	    throw new NotFoundException();
    	}
    }
    
    public function modify($connect, $key, $event, $topicList, $form){
    	$key = "'" . str_replace(array("'", "\""), array("\'", "\\\""), $key) . "'";
    	
    	try{
    		$connect->execute("UPDATE cfp.users SET `reviewer`=1 WHERE `id` like (" . $key . ")");
    		$topics = $connect->execute("SELECT topics FROM cfp.reviewers WHERE event_id=" . $event . " and captain=0 and user_id like (" . $key . ") and form_type like ('" . $form . "')")->fetchAll("assoc");
    		if(count($topics) < 1){
    			$connect->execute("INSERT INTO cfp.reviewers (event_id, user_id, topics, captain, form_type) VALUES (" . $event .", " . $key . ", '" . implode(',', $topicList) . "',0, '".$form."')");
    		} else {
    			if($form == 'session'){
    				$res = array_merge(explode(",",$topics[0]['topics']), $topicList);
    				$res = array_unique($res);
    				$connect->execute("UPDATE cfp.reviewers SET topics='" . implode(",", $res) . "' WHERE event_id=" . $event . " AND captain=0 AND user_id like (" . $key . ")");
    			} else {
    				$_SESSION['warningMessage'][] = "Nothing to update.";
    			}
    		}
    		if(!isset($_SESSION['warningMessage'])) {
    			$_SESSION['successMessage'][] = "Reviewer has been successfully created.";
    		}
    	}catch(\Exception $e){
    		$_SESSION['errorMessage'][] = "Reviewer can't be created.";
    	}	
    	$this->redirect("/capreviewers/".$event);
    }
}
