<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class VoteController extends Controller
{

    public function initialize()
    {

        parent::initialize();

        $this->loadComponent('RequestHandler');
    }
    
    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Network\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function add(...$path)
    {

        $token = $this->request->getData("token");
        $event_id = $this->request->getData("event");
        $response_id = $this->request->getData("response");
        $answer_id = $this->request->getData("answer");
        $vote = $this->request->getData("vote");
        $reviewer_id = str_replace('%', '', $this->request->getData("reviewer"));
        $captain = $this->request->getData("captain");

    	if($token == null){
    		die(json_encode(array("result"=>"error", "reason"=>"No token received")));
    	}

    	if($token != $this->request->session()->read('token')){
    		die(json_encode(array("result"=>"error", "reason"=>"Token expired. Please, refresh your page.")));
    	}

    	if(count($path) < 1){
    		if($event_id == '' || ($response_id == '' && $answer_id == '') || $vote == '' || ($reviewer_id == '' && $captain == '') ||
                 !is_numeric($event_id) || (!is_numeric($response_id) && !is_numeric($answer_id)) || !is_numeric($vote)){
    			die(json_encode(array("result"=>"error", "reason"=>"Unsufficient data. Your vote wasn't accepted.")));
    		}
      
    		$connection = ConnectionManager::get('cfp');
    		if($answer_id != ""){
    			$connection->execute('DELETE FROM cfp.votes WHERE event_id = ' . $event_id . ' and answer_id = ' . $answer_id . ' and reviewer_id like ("' . $reviewer_id . '") and captain = ' . ($captain != ''?"1":"0"));

    			if($connection->execute('INSERT INTO cfp.votes (event_id, vote, reviewer_id, answer_id, captain) VALUES (' . $event_id . ', ' . $vote . ', "'. $reviewer_id . '", ' . $answer_id . ',' . ($captain != ''?"1":"0") . ')')->errorCode() != 0){
    				die(json_encode(array("result"=>"error", "reason"=>"Database error. Your vote wasn't accepted.")));
    			} else {
    				die(json_encode(array("result"=>"success", "message"=>'INSERT INTO cfp.votes (event_id, vote, reviewer_id, answer_id, captain) VALUES (' . $event_id . ', ' . $vote . ', "'. $reviewer_id . '", ' . $answer_id . ',' . ($captain != ''?"1":"0") . ')')));
    			}
    		} else {
    			$connection->execute('DELETE FROM cfp.votes WHERE event_id = ' . $event_id . ' and response_to_topic_id = ' . $response_id . ' and reviewer_id like ("' . $reviewer_id . '") and captain = ' . ($captain != ''?"1":"0"));
    	    
    			if($connection->execute('INSERT INTO cfp.votes (event_id, vote, reviewer_id, response_to_topic_id, captain) VALUES ('. $event_id .', '. $vote .', "'. $reviewer_id .'", '. $response_id .',' . 
                    ($captain != ''?"1":"0") . ')')->errorCode() != 0){
    				die(json_encode(array("result"=>"error", "reason"=>"Database error. Your vote wasn't accepted.")));
    			} else {
    				die(json_encode(array("result"=>"success")));
    			}
    		}
    	} else {
    		if($event_id == '' || $response_id == '' || !is_numeric($event_id) || !is_numeric($response_id)){
    			die(json_encode(array("result"=>"error", "reason"=>"Unsufficient data. Your change wasn't accepted.")));
    		}
      
    		$connection = ConnectionManager::get('cfp');

    		if($path[0] == 'settopic'){
    			if(count($_POST['topic']) == 1){
    				$res = $connection->execute("SELECT id FROM responses_to_topics WHERE event_id = " . $event_id . " AND response_id=" . $response_id . " AND topic_id = " . $_POST['topic']);
    				if (count($res) < 1){
    					$connection->execute('INSERT INTO cfp.responses_to_topics (event_id, response_id, topic_id) VALUES (' . $event_id . ', ' . $response_id . ', '. $_POST['topic'] . ')');
    				}
    				$connection->execute('DELETE FROM cfp.votes WHERE event_id = ' . $event_id . ' AND response_to_topic_id in (SELECT id FROM responses_to_topics WHERE event_id = ' . $event_id . ' AND response_id = ' . $response_id . ' AND topic_id <> ' . $_POST['topic'] . ')');
    				$connection->execute('DELETE FROM cfp.responses_to_topics WHERE event_id = ' . $event_id . ' AND response_id = ' . $response_id . ' AND topic_id <> ' . $_POST['topic']);
    				die(json_encode(array("result"=>"success")));
    			} else {
    				die(json_encode(array("result"=>"error", "reason"=>"You have to specify only one topic for this proposal.")));
    			}
    		} else {
    			foreach($_POST['topic'] as $t){
    			    	$res = $connection->execute("SELECT id FROM responses_to_topics WHERE event_id = " . $event_id . " AND response_id=" . $response_id . " AND topic_id = " . $t);
    				if (count($res) < 1){
    					$connection->execute('INSERT INTO cfp.responses_to_topics (event_id, response_id, topic_id) VALUES (' . $event_id . ', ' . $response_id . ', '. $t . ')');
    				}
    			}
    			die(json_encode(array("result"=>"success")));
    		}
    	}
    }
}
