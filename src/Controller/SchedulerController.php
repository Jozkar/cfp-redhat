<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Datasource\ConnectionManager;
/**
 * Static content controller
 *
 * This controller will render views from Template/HomePage/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class SchedulerController extends AppController
{

    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Network\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function show(...$action)
    {
        if(count($action) < 1 or !is_numeric($action[0])){
			$_SESSION['errorMessage'][] = "Unsufficient data. Your link is probably broken.";
			return $this->redirect("/");
		}
		
		$connection = ConnectionManager::get('cfp');
        
        $event = $connection->execute("SELECT name, id, year, schedule, open, close, timezone, schedule_version+1 as version FROM events WHERE id = " . $action[0])->fetch("assoc");
        		
		if(!parent::getProgramManagerForEvent($event['id']) && !parent::getSuperUser()){
            return $this->redirect("/manager/");
        }
        
        if(isset($_POST['save'])){
            if(isset($_POST['_Token']) && $_POST['_Token'] == $_SESSION['token']){
                   return $this->update($connection, $action[0]);
            } else {
                $_SESSION['errorMessage'][] = "You are not allowed to do this opperation";
                return $this->redirect("/scheduler/" . $action[0]);
            }
        }

        if(isset($_POST['sync'])){
            if(isset($_POST['_Token']) && $_POST['_Token'] == $_SESSION['token']){
                   return $this->sync($connection, $action[0]);
            } else {
                $_SESSION['errorMessage'][] = "You are not allowed to do this opperation";
                return $this->redirect("/scheduler/" . $action[0]);
            }
        }

        if(isset($_POST['publish'])){
            if(isset($_POST['_Token']) && $_POST['_Token'] == $_SESSION['token']){
                   return $this->publish($connection, $action[0]);
            } else {
                $_SESSION['errorMessage'][] = "You are not allowed to do this opperation";
                return $this->redirect("/scheduler/" . $action[0]);
            }
        }


        parent::printFlush($this->request->here());
        $this->set("admin", parent::getAdmin() | parent::getSuperUser());
        $this->set("reviewer", parent::getReviewer());
        $this->set("program_manager", parent::getProgramManager());

        $rooms = $connection->execute('SELECT * FROM cfp.rooms WHERE event_id = ' . $action[0])->fetchAll('assoc');
        foreach($rooms as $i=>$r){
            $blocks = $connection->execute('SELECT * FROM cfp.room_availability WHERE room_id = ' . $r['id'])->fetchAll('assoc');
            
            $rooms[$i]['available'] = $blocks;
        }

        $eversion = $connection->execute("SELECT schedule_version FROM events WHERE id = " . $action[0])->fetch("assoc");

        $unplacedResp = $connection->execute("SELECT s.id, r.id as response_id, r.title, r.duration, CONCAT(u.first_name,' ',u.last_name) as speaker, t.name as topic, t.color, t.text_color FROM"
            . " responses as r, users as u, topics as t, responses_to_topics as rt, schedule as s WHERE"
            . " r.id IN (SELECT response_id FROM schedule WHERE event_id = " . $action[0] . " and room_id is NULL and version = " . ($eversion['schedule_version'] + 1) . ") AND r.event_id = " . $action[0]
            . " AND u.id = r.user_id AND rt.response_id = r.id AND rt.topic_id = t.id AND r.id = s.response_id and s.version = " . ($eversion['schedule_version'] + 1))->fetchAll("assoc");

        $unplacedAnsw = $connection->execute("SELECT s.id, a.id as answer_id, CONCAT(u.first_name,' ',u.last_name) as speaker, f.type as topic, f.color, f.text_color FROM answers as a, users as u, schedule as s, forms as f WHERE"
            . " a.id IN (SELECT answer_id FROM schedule WHERE event_id = " . $action[0] . " and room_id is NULL and version = " . ($eversion['schedule_version'] + 1) . ") AND a.event_id = " . $action[0]
            . " AND u.id = a.user_id AND a.form_id = f.id AND a.id = s.answer_id and s.version = " . ($eversion['schedule_version'] + 1))->fetchAll("assoc");

        foreach($unplacedAnsw as $k=>$ua){
            $title = $connection->execute("SELECT IFNULL(text,'') as text FROM answer_parts WHERE answer_id = " . $ua['answer_id'] . " AND as_title = 1")->fetch("assoc");
            $unplacedAnsw[$k]['title'] = $title['text'];

            $duration = $connection->execute("SELECT IFNULL(text,5) as text FROM answer_parts WHERE answer_id = " . $ua['answer_id'] . " AND as_duration = 1")->fetch("assoc");
            $unplacedAnsw[$k]['duration'] = intval($duration['text']);
        }

        $placedResp = $connection->execute("SELECT s.id, s.room_id, s.date, s.start, s.end, r.id as response_id, r.title, r.duration, CONCAT(u.first_name,' ',u.last_name) as speaker, t.name as topic, t.color, t.text_color FROM"
            . " responses as r, users as u, topics as t, responses_to_topics as rt, schedule as s WHERE"
            . " r.id IN (SELECT response_id FROM schedule WHERE event_id = " . $action[0] . " and room_id is NOT NULL and version = " . ($eversion['schedule_version'] + 1) . ") AND r.event_id = " . $action[0]
            . " AND u.id = r.user_id AND rt.response_id = r.id AND rt.topic_id = t.id AND r.id = s.response_id and s.version = " . ($eversion['schedule_version'] + 1))->fetchAll("assoc");

        $placedAnsw = $connection->execute("SELECT s.id, s.room_id, s.date, s.start, s.end, a.id as answer_id, CONCAT(u.first_name,' ',u.last_name) as speaker, f.type as topic, f.color, f.text_color"
            . " FROM answers as a, users as u, schedule as s, forms as f WHERE"
            . " a.id IN (SELECT answer_id FROM schedule WHERE event_id = " . $action[0] . " and room_id is NOT NULL and version = " . ($eversion['schedule_version'] + 1) . ") AND a.event_id = " . $action[0]
            . " AND u.id = a.user_id AND a.form_id = f.id AND a.id = s.answer_id and s.version = " . ($eversion['schedule_version'] + 1))->fetchAll("assoc");

        foreach($placedAnsw as $k=>$ua){
            $title = $connection->execute("SELECT IFNULL(text,'') as text FROM answer_parts WHERE answer_id = " . $ua['answer_id'] . " AND as_title = 1")->fetch("assoc");
            $placedAnsw[$k]['title'] = $title['text'];

            $duration = $connection->execute("SELECT IFNULL(text,5) as text FROM answer_parts WHERE answer_id = " . $ua['answer_id'] . " AND as_duration = 1")->fetch("assoc");
            $placedAnsw[$k]['duration'] = intval($duration['text']);
        }

        $breaks = $connection->execute("SELECT id, title, description, room_id, date, start, end FROM schedule WHERE event_id = " . $action[0] . " AND answer_id IS NULL and response_id IS NULL and version = "
            . ($eversion['schedule_version'] + 1))->fetchAll("assoc");

        $this->set("uResponses", $unplacedResp);
        $this->set("Responses", $placedResp);
        $this->set("uAnswers", $unplacedAnsw);
        $this->set("Answers", $placedAnsw);
        $this->set("breaks", $breaks);

        $this->set("event", $event);
        $this->set("rooms", $rooms);
        $this->set("active", "manager");
        $token = $this->request->getParam('_csrfToken');

        $_SESSION['token'] = $token;
        $this->set("token", $token);
        $this->set('username', $_SESSION['first-name']." ".$_SESSION['last-name']);

        try {
            $this->render('scheduler');
        } catch (MissingTemplateException $exception) {
            if (Configure::read('debug')) {
                throw $exception;
            }
            throw new NotFoundException();
        }
    }

    public function update($connect, $eventID){
        foreach($_POST as $key=>$val){
            if(!is_numeric($_POST[$key]) && !is_array($_POST[$key])){
                $_POST[$key] = "'" . $this->sanity($val) . "'";
            }
        }
        
        $eversion = $connect->execute("SELECT schedule_version FROM events WHERE id = " . $eventID)->fetch("assoc");

        try{
            foreach($_POST['remove'] as $r){
                $connect->execute("DELETE FROM schedule WHERE id = " . $r . " AND version = " . ($eversion['schedule_version'] + 1));
            }

            foreach($_POST['unassign'] as $u){
                $connect->execute("UPDATE schedule SET room_id = NULL, start = NULL, end = NULL, date = NULL WHERE id = " . $u . " AND version =  " . ($eversion['schedule_version'] + 1));
            }

            foreach($_POST['slot'] as $sid => $s){
                $start = date_create($s['start']);
                $end = date_create($s['end']);

                if(!is_numeric($sid)){
                    $connect->execute("INSERT INTO cfp.schedule (start, end, date, title, event_id, room_id, version) VALUES ('" . $start->format("H:i:s") . "', '" . $end->format("H:i:s") . "', '"
                        . $start->format("Y-m-d") . "', '" . $this->sanity($s['title']) . "'," . $eventID . ", " . $s['room'] . ", " . ($eversion['schedule_version'] + 1) . ")");
                } elseif(isset($s['title'])) {
                    $connect->execute("UPDATE cfp.schedule SET `room_id`=" . $s['room']. ", `start`='" . $start->format("H:i:s") . "', `end`='" . $end->format("H:i:s") .
                    "', `date`='" . $start->format("Y-m-d") . "', `title`='" . $this->sanity($s['title']) . "' WHERE id = " . $sid . " AND version=" . ($eversion['schedule_version'] + 1));
                } else {
                    $connect->execute("UPDATE cfp.schedule SET `room_id`=" . $s['room']. ", `start`='" . $start->format("H:i:s") . "', `end`='" . $end->format("H:i:s") . 
                    "', `date`='" . $start->format("Y-m-d") . "' WHERE id = " . $sid . " AND version=" . ($eversion['schedule_version'] + 1));
                }

            }
            $_SESSION['successMessage'][] = "Schedule draft has been successfully modified.";

        }catch(\Exception $e){
            $_SESSION['errorMessage'][] = "Schedule couldn't be updated. " . $e->getMessage();
        }
        return $this->redirect("/scheduler/" . $eventID);    
    }

    public function sync($connect, $eventID){
        $forms = $connect->execute("SELECT id, type FROM forms WHERE schedule = 1 AND event_id = " . $eventID)->fetchAll("assoc");
        $eversion = $connect->execute("SELECT schedule_version FROM events WHERE id = " . $eventID)->fetch("assoc");

        foreach($forms as $f){
            if($f['type'] == "session"){
                $connect->execute("DELETE FROM schedule WHERE response_id IN (SELECT IFNULL(r.id, 0) FROM responses as r, accepts as a, responses_to_topics as rt WHERE"
                    . " r.event_id = " . $eventID . " AND rt.response_id = r.id AND rt.id = a.response_to_topic_id AND a.vote < 1) AND version = " . ($eversion['schedule_version'] + 1));

                $talks = $connect->execute("SELECT r.id FROM responses as r, accepts as a, responses_to_topics as rt WHERE r.id NOT IN (SELECT IFNULL(response_id,0) FROM schedule WHERE"
                    . " event_id = " . $eventID . " AND version = " . ($eversion['schedule_version'] + 1) . ")"
                    . " AND r.event_id = " . $eventID . " AND rt.response_id = r.id AND rt.id = a.response_to_topic_id AND a.vote = 1")->fetchAll("assoc");
                foreach($talks as $t){
                    if($t['id'] > 0){
                        $connect->execute("INSERT INTO schedule (response_id, event_id, version) VALUES (" . $t['id'] . ", " . $eventID . ", " . ($eversion['schedule_version'] + 1) . ")");
                    }
                }
            } else {
                $connect->execute("DELETE FROM schedule WHERE answer_id IN (SELECT IFNULL(a.id, 0) FROM answers as a, accepts as ac WHERE"
                    . " a.form_id = " . $f['id'] . " AND a.event_id = " . $eventID . " AND a.id = ac.answer_id AND ac.vote < 1) and version = ". ($eversion['schedule_version'] + 1));

                $talks = $connect->execute("SELECT a.id FROM answers as a, accepts as ac WHERE a.id NOT IN (SELECT IFNULL(answer_id,0) FROM schedule WHERE event_id = " . $eventID
                    . " AND version = " . ($eversion['schedule_version'] + 1) .")"
                    . " AND a.form_id = " . $f['id'] . " AND a.event_id = " . $eventID . " AND a.id = ac.answer_id AND ac.vote = 1")->fetchAll("assoc");
                foreach($talks as $t){
                    if($t['id'] > 0){
                        $connect->execute("INSERT INTO schedule (answer_id, event_id, version) VALUES (" . $t['id'] . ", " . $eventID . ", " . ($eversion['schedule_version'] + 1) . ")");
                    }
                }
            }
        }

        $_SESSION['successMessage'][] = "Sessions synced.";
        return $this->redirect("/scheduler/" . $eventID);
    }

    public function publish($connect, $eventID){

        $eversion = $connect->execute("SELECT schedule_version FROM events WHERE id = " . $eventID)->fetch("assoc");
        if($eversion != null && is_numeric($eversion['schedule_version'])){

            $connect->execute("UPDATE events SET schedule_version = " . ($eversion['schedule_version'] + 1) . " WHERE id = " . $eventID);
            $connect->execute("DELETE FROM schedule WHERE version < " . ($eversion['schedule_version'] + 1) . " AND event_id = " . $eventID);
            $connect->execute("INSERT INTO schedule (event_id, room_id, answer_id, response_id, date, start, end, title, description, version, id) "
                . "SELECT event_id, room_id, answer_id, response_id, date, start, end, title, description, version+1, id FROM schedule WHERE event_id = " . $eventID);

            $_SESSION['successMessage'][] = "Schedule published.";
        } else {
            $_SESSION['successMessage'][] = "Can't publish the schedule. Event doesn't exist.";
        }
        return $this->redirect("/scheduler/" . $eventID);
    }

    public function sanity($string){
        return str_replace(array("'", "\""), array("\'", "\\\""), $string);
    }
}
