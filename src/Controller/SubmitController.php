<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Datasource\ConnectionManager;

/**
 * Static content controller
 *
 * This controller will render views from Template/HomePage/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class SubmitController extends AppController
{

    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Network\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function show(...$path)
    {
    if(empty($path) || count($path) < 1 || !is_numeric($path[0])){
    	$_SESSION['errorMessage'][] = "Unsufficient data. Your link is probably broken.";
    	return $this->redirect("/");
    }

    parent::printFlush($this->request->here());

    $this->set("active", "home");
    $this->set("admin", parent::getAdmin() | parent::getSuperUser());
    $this->set("reviewer", parent::getReviewer());
    $this->set("program_manager", parent::getProgramManager());       
    	
    if($this->request->session()->read("first-name")){
    	$this->set('username', $_SESSION['first-name'] . " " . $_SESSION['last-name']);
    }

    $connection = ConnectionManager::get('cfp');

    $results = $connection->execute('SELECT id, type, cfp_close, placeholder FROM cfp.forms WHERE event_id = ' . $path[0] . ' AND type not in ("confirmation", "collect", "accommodation", "covered accommodation") ORDER BY form_order')->fetchAll('assoc');
    $event = $connection->execute('SELECT id, year, name, redhat_event, cfp_close FROM cfp.events WHERE id = ' . $path[0] . ' ORDER BY id desc')->fetch('assoc');
    $form_close = $connection->execute('SELECT max(cfp_close) as cfp_close FROM cfp.forms WHERE event_id = ' . $path[0] . ' AND type not in ("confirmation", "collect", "accommodation", "covered accommodation")')->fetch('assoc');

    if(strtotime(date("Y-m-d", time())) > strtotime(($form_close['cfp_close'] > $event['cfp_close']?$form_close['cfp_close']:$event['cfp_close'])) && !parent::getAdmin() && !parent::getProgramManagerForEvent($event['id'])){
    	$_SESSION['errorMessage'][] = "CfP is closed. You can't add, modify or delete any proposal.";
    	return $this->redirect('/');
    }

    if(!parent::getAdmin() && !parent::getProgramManagerForEvent($event['id'])){
        foreach($results as $i => $r){
            if($form_close['cfp_close'] == ""){
                if(strtotime(date("Y-m-d", time())) > strtotime($event['cfp_close'])){
                    unset($results[$i]);
                }
            } else {
                if($r['cfp_close'] == ""){
                    if(strtotime(date("Y-m-d", time())) > strtotime($event['cfp_close'])){
                        unset($results[$i]);
                    }
                } else if (strtotime(date("Y-m-d", time())) > strtotime($r['cfp_close'])){
                    unset($results[$i]);              
                }
            }
        }
    }

    if($event['redhat_event'] == 0){
    	$_SESSION['errorMessage'][] = "This event isn't organized by Red Hat. You can't find any form for it.";
    	return $this->redirect("/");
    }

    $this->set("event", $event);
    $this->set("forms", $results);
    $this->set("inflector", parent::getInflector());
    try {
            $this->render("home");
        } catch (MissingTemplateException $exception) {
            if (Configure::read('debug')) {
                throw $exception;
            }
            throw new NotFoundException();
        }
    }
}
