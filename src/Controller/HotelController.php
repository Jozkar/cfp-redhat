<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Datasource\ConnectionManager;
/**
 * Static content controller
 *
 * This controller will render views from Template/HomePage/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class HotelController extends AppController
{

    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Network\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function show(...$action)
    {
        if(count($action) < 1 or !is_numeric($action[0])){
			$_SESSION['errorMessage'][] = "Unsufficient data. Your link is probably broken.";
			return $this->redirect("/");
		}
		
		if(!parent::getProgramManagerForEvent($action[0]) && !parent::getSuperUser()){
            return $this->redirect("/hotels/" . $action[0]);
        }
        $connection = ConnectionManager::get('cfp');
        
        if(isset($_POST['save'])){
            if(isset($_POST['_Token']) && $_POST['_Token'] == $_SESSION['token']){
                if(isset($action[1]) && $action[1] == "add"){
                   return $this->add($connection, $action[0]);
                } else {
                   return $this->update($connection,$action[0],$action[2]);
                }
            } else {
                $_SESSION['errorMessage'][] = "You are not allowed to do this opperation";
                return $this->redirect("/hotels/" . $action[0]);
            }
        }

        parent::printFlush($this->request->here());
        $this->set("admin", parent::getAdmin() | parent::getSuperUser());
        $this->set("reviewer", parent::getReviewer());
        $this->set("program_manager", parent::getProgramManager());

        $formAction = "add";            
		$results = array();
		
        // check, if are passed any arguments
        if(count($action) > 1){
            // supported are arguments /.../event-id/action
            if((count($action) <= 2 && $action[1] != "add") || (count($action) >= 3 && !is_numeric($action[0])  && !is_numeric($action[2]))){
                //not enough arguments error message  + redirect
                $_SESSION['errorMessage'][] = "Unsufficient data. Your link is probably broken.";
                return $this->redirect("/manager"); 
            }
            
            //for edit action
            if($action[1] == "edit"){
                $results = $connection->execute('SELECT * FROM cfp.hotels WHERE event_id = ' . $action[0] . " AND id = " . $action[2])->fetch('assoc');
                $formAction = "update";

            //for delete action
            } else if ($action[1] == "delete") {
                if(count($action) > 3){
                    if($_SESSION['token'] == $action[3]){
                        return $this->delete($connection,$action[0],$action[2]);
                    } else {
                        $_SESSION['errorMessage'][] = "This action can't be performet - invalid token.";
                        return $this->redirect("/hotels/" . $action[0]);
                    }
                } else {
                    $_SESSION['errorMessage'][] = "This action can't be performet - unsufficient data. Your link is probably broken.";
                    return $this->redirect("/hotels/" . $action[0]);
                }
            }
        }
        
        $event = $connection->execute("SELECT name, year, id FROM events WHERE id = " .$action[0])->fetch("assoc");

        $this->set("event", $event);
        $this->set("hotel", $results);
        $this->set("action", $formAction);
        $this->set("active", "manager");
        $token = $this->request->getParam('_csrfToken');

        $_SESSION['token'] = $token;
        $this->set("token", $token);
        $this->set('username', $_SESSION['first-name']." ".$_SESSION['last-name']);

        try {
            $this->render('hotel');
        } catch (MissingTemplateException $exception) {
            if (Configure::read('debug')) {
                throw $exception;
            }
            throw new NotFoundException();
        }
    }

    public function add($connect, $eventID){
        foreach($_POST as $key=>$val){
            if(!is_numeric($_POST[$key]) && !is_array($_POST[$key])){
                $_POST[$key] = "'" . $this->sanity($val) . "'";
            }
        }
        try{
            $res = $connect->execute("INSERT INTO cfp.hotels (name, email, web, description, event_id, use_for_paid_form) VALUES ("
                . $_POST['name']. ", ". $_POST['email'] . ", " . $_POST['web'] . ", " .$_POST['description'] . ", " . $eventID . ", " . (isset($_POST['use_for_paid_form'])?1:0) . ")");
            $_SESSION['successMessage'][] = "Hotel has been successfully added.";
        }catch(\Exception $e){
            $_SESSION['errorMessage'][] = "Hotel can't be added.";
        }    
        return $this->redirect("/hotels/".$eventID);
    }

    public function update($connect, $eventID, $hotelID){
        if(parent::getProgramManagerForEvent($eventID)){

            foreach($_POST as $key=>$val){
                if(!is_numeric($_POST[$key]) && !is_array($_POST[$key])){
                    $_POST[$key] = "'" . $this->sanity($val) . "'";
                }
            }
            try{
                $connect->execute("UPDATE cfp.hotels SET `name`=" . $_POST['name']. ", `email`=" . $_POST['email'] . ", `web`=" . $_POST['web'] . ", `description`=" . $_POST['description'] . 
                    ", `use_for_paid_form` = " . (isset($_POST['use_for_paid_form'])?1:0) . " WHERE `id` = " . $hotelID . " AND event_id = " . $eventID);
                $_SESSION['successMessage'][] = "Hotel has been successfully modified.";
            }catch(\Exception $e){
                $_SESSION['errorMessage'][] = "Hotel can't be updated. " . $e->getMessage();
            }
            return $this->redirect("/hotels/" . $eventID);    
        } else {
            $_SESSION['errorMessage'][] = "You are not allowed to update this hotel.";
            return $this->redirect("/hotels/" . $eventID);
        }
    }

    public function delete($connect, $eventID, $hotelID){
        if(parent::getProgramManagerForEvent($eventID)){
            try{
                $connect->execute("DELETE FROM cfp.hotel_rooms WHERE hotel_id = " . $hotelID);
                $connect->execute("DELETE FROM cfp.hotels WHERE event_id = " . $eventID . " AND id = " . $hotelID);
                $_SESSION['successMessage'][] = "Hotel has been successfully removed.";
            }catch(\Exception $e){
                $_SESSION['errorMessage'][] = "Hotel can't be removed. " . $e->getMessage();
            }
            return $this->redirect("/hotels/" . $eventID);
        } else {
            $_SESSION['successMessage'][] = "You are not allowed to delete this hotel.";
            return $this->redirect("/hotels/" . $eventID);
        }
    }

    public function sanity($string){
        return str_replace(array("'", "\""), array("\'", "\\\""), $string);
    }
}
