<?php
    /**
     * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
     * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
     *
     * Licensed under The MIT License
     * For full copyright and license information, please see the LICENSE.txt
     * Redistributions of files must retain the above copyright notice.
     *
     * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
     * @link      https://cakephp.org CakePHP(tm) Project
     * @since     0.2.9
     * @license   https://opensource.org/licenses/mit-license.php MIT License
     */
    namespace App\Controller;

    use Cake\Controller\Controller;
    use Cake\Event\Event;
    use Cake\Core\Configure;
    use Cake\Datasource\ConnectionManager;

    /**
     * Static content controller
     *
     * This controller will render views from Template/Pages/
     *
     * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
     */
    class TopicUpdateController extends Controller
    {

        public function initialize()
        {

            parent::initialize();

            $this->loadComponent('RequestHandler');
        }
        
        /**
         * Displays a view
         *
         * @param array ...$path Path segments.
         * @return \Cake\Http\Response|null
         * @throws \Cake\Network\Exception\ForbiddenException When a directory traversal attempt.
         * @throws \Cake\Network\Exception\NotFoundException When the view file could not
         *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
         */
        public function add(...$path)
        {

            $token = $this->request->getData("token");
            $name = $this->request->getData("name");
            $color = $this->request->getData("color");
            $topic_id = $this->request->getData("id");

            if($token == null){
                die(json_encode(array("result"=>"error", "reason"=>"No token received")));
            }

            if($token != $this->request->session()->read('token')){
                die(json_encode(array("result"=>"error", "reason"=>"Token expired. Please, refresh your page.")));
            }

            
            if($name == '' || $topic_id == '' || !is_numeric($topic_id)){
                die(json_encode(array("result"=>"error", "reason"=>"Unsufficient data. Topic update wasn't accepted.")));
            }
          
            $connection = ConnectionManager::get('cfp');
            
            if($topic_id < 0 && $topic_id != -1){    
                if($connection->execute('INSERT INTO cfp.topics (name, color, text_color) VALUES ("' . $name . '", "' . $color . '", "' . $this->getTextColor($color) . '")')->errorCode() != 0){        
                    die(json_encode(array("result"=>"error", "reason"=>"Database error. New topic wasn't inserted.")));
                } else {
                    die(json_encode(array("result"=>"success")));
                }
            }else{
                if($connection->execute('UPDATE cfp.topics SET name = "' . $name . '", color = "' . $color . '", text_color = "' . $this->getTextColor($color) . '" WHERE id = ' . $topic_id)->errorCode() != 0){
                    die(json_encode(array("result"=>"error", "reason"=>"Database error. Topic update wasn't accepted.")));
                } else {
                    die(json_encode(array("result"=>"success")));
                }
            }
             
    }

    public function getTextColor($color){
        list($r,$g,$b) = sscanf($color, "#%02x%02x%02x");

        if((($r*0.299) + ($g*0.587) + ($b*0.114)) > 186){
            return "#333333";
        } else {
            return "#ffffff";
        }
    }
}
