<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Datasource\ConnectionManager;
use Cake\Mailer\Email;

/**
 * Static content controller
 *
 * This controller will render views from Template/HomePage/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class SlotsController extends AppController
{

    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Network\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function show(...$path)
    {
        if(!parent::getAdmin() && !parent::getProgramManager() && !parent::getSuperUser()){
            return $this->redirect("/manager");
        }
    
        parent::printFlush($this->request->here());

        $this->set("active", "manager");
        $this->set("admin", parent::getAdmin() | parent::getSuperUser());
        $this->set("reviewer", parent::getReviewer());
        $this->set("program_manager", parent::getProgramManager());       
        
        if($this->request->session()->read("first-name")){
           $this->set('username', $_SESSION['first-name'] . " " . $_SESSION['last-name']);
        }

        $connection = ConnectionManager::get('cfp');

        if(!parent::getAdminForEvent($path[0]) && !parent::getProgramManagerForEvent($path[0])){
            $_SESSION['errorMessage'][] = "You are not allowed to do this opperation.";
            return $this->redirect("/manager");
        }

        if(isset($_POST['save'])){
            if(isset($_POST["_Token"]) && $_POST["_Token"] == $_SESSION['token']){
                return $this->update($connection);
            } else {
                $_SESSION['errorMessage'][] = "You are not allowed to do this opperation. Missing token.";
                if(isset($path[0]) && is_numeric($path[0])){
                    return $this->redirect("/slots/".$path[0]);
                }else{
                    return $this->redirect("/manager");
                }
            }
        }

        $event = $connection->execute('SELECT id, name, year FROM cfp.events WHERE id = ' . $path[0])->fetch('assoc');
        $topics= $connection->execute('SELECT id, name, timeAllocation FROM cfp.topics, cfp.topics_to_events WHERE event_id = ' . $path[0] . ' AND topic_id = id')->fetchAll('assoc');
        $token = $this->request->getParam('_csrfToken');

        $_SESSION['token'] = $token;
        $this->set("token", $token);
        $this->set("event", $event);
        $this->set("topics", $topics);

        try {
            $this->render("home");
        } catch (MissingTemplateException $exception) {
            if (Configure::read('debug')) {
                throw $exception;
            }
            throw new NotFoundException();
        }
    }

    public function update($connect){
        foreach($_POST as $key=>$val){
            if(!is_numeric($val)){
                $_POST[$key] = "'" . str_replace(array("'", "\""), array("\'", "\\\""), $val) . "'";
            }
        }
        try{
            foreach($_POST as $k => $p){
                if(is_numeric($k)){
                   $connect->execute("UPDATE cfp.topics_to_events SET `timeAllocation`=" . $p . " WHERE `event_id` = " . $_POST['event'] . " and topic_id = " . $k);
                }
            }
            $_SESSION['successMessage'][] = "Time allocations updated.";
        }catch(\Exception $e){
            $_SESSION['errorMessage'][] = "Time allocations can't be updated. Check form fields for unusual characters.";
        }
        if(isset($_POST['event']) && is_numeric($_POST['event'])){
            return $this->redirect("/slots/".$_POST['event']);    
        }else{
            return $this->redirect("/manager");
        }
    }   
}
