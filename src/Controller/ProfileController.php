<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Datasource\ConnectionManager;
/**
 * Static content controller
 *
 * This controller will render views from Template/HomePage/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class ProfileController extends AppController
{

    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Network\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function show(...$action)
    {

    $connection = ConnectionManager::get('cfp');
    
    if(isset($_POST['save'])){
    	if(isset($_POST['_Token']) && $_POST['_Token'] == $_SESSION['token']){
    		if(isset($action[0]) && $action[0] == "add"){
    			$this->add($connection);
    		} else {
    			$this->update($connection);
    		}
    	} else {
    		$_SESSION['errorMessage'][] = "You are not allowed to do this opperation";
    		$this->redirect("/profile");
    	}
    }

    if(isset($action[0]) && $action[0] == "delete" && isset($action[1]) && $action[1] == $_SESSION['token']){
    	return $this->delete($connection);
    }

    parent::printFlush($this->request->here());
    $this->set("active", "profile");
    $this->set("admin", parent::getAdmin() | parent::getSuperUser());
    $this->set("reviewer", parent::getReviewer());
    $this->set("program_manager", parent::getProgramManager());
    $results = $connection->execute('SELECT * FROM cfp.users WHERE id like ("' . $_SESSION['loggedUser'] . '")')->fetchAll('assoc');
    $action = "update";
    if(!$results){
    	$results = array(
    		"first_name"=>$_SESSION['first-name'],
    		"last_name"=>$_SESSION['last-name'],
    		"email"=>$_SESSION['email'],
    		"avatar"=>"/webroot/img/avatars/noperson.png",
    		"organization"=>"", "position"=>"", "country"=>"", "bio"=>"");
    	$action = "add";
    } else {
    	$results = $results[0];
    }

    $_SESSION['first-name'] = $results['first_name'];
    $_SESSION['last-name'] = $results['last_name'];

    $token = $this->request->getParam('_csrfToken');

    $_SESSION['token'] = $token;
        $this->set("user", $results);
    $this->set("action", $action);
    $this->set("token", $token);
        $this->set('username', $results['first_name']." ".$results['last_name']);

    try {
        $this->render('profile');
    } catch (MissingTemplateException $exception) {
        if (Configure::read('debug')) {
            throw $exception;
        }
        throw new NotFoundException();
    }
    }
    
    public function add($connect){
    foreach($_POST as $key=>$val){
    	if($key == "avatar"){
    		if($val != "" && !$this->checkURL($val)){
    			$_POST[$key] = "";
    			$val = "";
    			$_SESSION['warningMessage'][] = "Your avatar has been changed to default one, due provided URL address isn't publicly available URL of image.";
    		}
    	}
    	$_POST[$key] = "'" . str_replace(array("'", "\""), array("\'", "\\\""), $val) . "'";
    }
    try{
    	$res = $connect->execute("INSERT INTO cfp.users (id, first_name, last_name, email, bio, organization, position, country, avatar) VALUES ('" . $_SESSION['loggedUser'] 
    		. "', " . $_POST['first_name']. ", " . $_POST['last_name'] . ", '" . $_SESSION['email'] . "', " . $_POST['bio'] . ", ". $_POST['organization'] . ", "
                    . $_POST['position'] . ", " .$_POST['country'] . ", " . (isset($_POST['avatar']) && $_POST['avatar'] != '\'\''?$_POST['avatar']:"'/webroot/img/avatars/noperson.png'"). ")");
    	$_SESSION['successMessage'][] = "Your profile has been successfully created.";
    	$_SESSION['first-name'] = $_POST['first_name'];
    	$_SESSION['last-name'] = $_POST['last_name'];
    }catch(\Exception $e){
    	$_SESSION['errorMessage'][] = "Your profile can't be created. Maybe it already exists.";
    }	
    $this->redirect("/profile");
    }

    public function update($connect){
    foreach($_POST as $key=>$val){
    	if($key == "avatar"){
    		if($val != "" && !$this->checkURL($val)){
    			$_POST[$key] = "";
    			$val = "";
    			$_SESSION['errorMessage'][] = "Provided avatar URL address isn't publicly available URL of image.";
    		}
    	}
    	$_POST[$key] = "'" . str_replace(array("'", "\""), array("\'", "\\\""), $val) . "'";
    }
    try{
    	$res = $connect->execute("UPDATE cfp.users SET `first_name`=" . $_POST['first_name']. ", `last_name`=" . $_POST['last_name'] . ", `bio`=" . $_POST['bio'] . 
    		", `organization`=". $_POST['organization'] . ", `country`=" . $_POST['country'] . ", `position`=" . $_POST['position'] . ", "
    		. (isset($_POST['avatar']) && $_POST['avatar'] != '\'\'' && $_POST['avatar'][1] == "h"?"avatar=".$_POST['avatar']:'avatar="/webroot/img/avatars/noperson.png"').
    		" WHERE `id` like ('" . $_SESSION['loggedUser'] . "')");
    	if(isset($_POST['avatar']) && ($_POST['avatar'] == '\'\'' || $_POST['avatar'][1] != "h")){
    		$_SESSION['warningMessage'][] = "Your avatar has been changed to default one.";
    	}
    	$_SESSION['successMessage'][] = "Your profile has been successfully modified.";
    }catch(\Exception $e){
    	$_SESSION['errorMessage'][] = "Your profile can't be updated. Check form fields for unusual characters.";
    }
    $this->redirect("/profile");    
    }

    public function delete($connect){
    $connect->execute("DELETE FROM cfp.managers WHERE user_id like ('" . $_SESSION['loggedUser'] . "')");
    $connect->execute("DELETE FROM cfp.reviewers WHERE user_id like ('" . $_SESSION['loggedUser'] . "')");
    $connect->execute("DELETE FROM cfp.additional_speakers WHERE user_id like ('" . $_SESSION['loggedUser'] . "')");
    $connect->execute("DELETE FROM cfp.votes WHERE reviewer_id like ('" . $_SESSION['loggedUser'] . "')");
    $connect->execute("DELETE FROM cfp.answer_parts WHERE answer_id in (SELECT id FROM cfp.answers WHERE user_id like ('" . $_SESSION['loggedUser'] . "'))");
    $connect->execute("DELETE FROM cfp.answers WHERE user_id like ('" . $_SESSION['loggedUser'] . "')");

    $rtt = $connect->execute("SELECT id FROM cfp.responses_to_topics WHERE response_id in (SELECT id FROM cfp.responses WHERE user_id like ('" . $_SESSION['loggedUser'] . "'))")->fetchAll("assoc");
    $connect->execute("DELETE FROM cfp.comments WHERE response_id in (SELECT id FROM cfp.responses WHERE user_id like ('" . $_SESSION['loggedUser'] . "'))");
    $connect->execute("DELETE FROM cfp.responses WHERE user_id like ('" . $_SESSION['loggedUser'] . "')");
    $connect->execute("DELETE FROM cfp.users WHERE id like ('" . $_SESSION['loggedUser'] . "')");

    foreach($rtt as $r){
    	$connect->execute("DELETE FROM cfp.accepts WHERE response_to_topic_id = " . $r['id']);
    	$connect->execute("DELETE FROM cfp.votes WHERE response_to_topic_id = " . $r['id']);
    	$connect->execute("DELETE FROM cfp.responses_to_topics WHERE id = " . $r['id']);
    }

    return $this->redirect("/logout");
    }

    public function checkURL($url){
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch , CURLOPT_RETURNTRANSFER, 1);
    $data = curl_exec($ch);
    $headers = curl_getinfo($ch);
    curl_close($ch);

    return ($headers['http_code'] == "200" && strpos($headers['content_type'], "image") !== false);
    }
}
