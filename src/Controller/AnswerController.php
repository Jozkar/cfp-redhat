<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Datasource\ConnectionManager;

/**
 * Static content controller
 *
 * This controller will render views from Template/HomePage/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class AnswerController extends AppController
{

    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Network\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function show(...$action)
    {
    	
    if(count($action) < 1 or !is_numeric($action[0])){
    	$_SESSION['errorMessage'][] = "Unsufficient data. Your link is probably broken.";
    	return $this->redirect("/");
    }	

    parent::printFlush($this->request->here());

    $this->set("active", "home");
    $this->set("admin", parent::getAdmin() | parent::getSuperUser());
    $this->set("reviewer", parent::getReviewer());
    $this->set("program_manager", parent::getProgramManager());       
    	
    if($this->request->session()->read("first-name")){
            $this->set('username', $_SESSION['first-name'] . " " . $_SESSION['last-name']);
    }

    $connection = ConnectionManager::get('cfp');

    if(count($action) == 1 && is_numeric($action[0])){
    	$speakers = $connection->execute('SELECT CONCAT(u.first_name, " ", u.last_name ) as speaker FROM cfp.users as u, cfp.answers as a WHERE u.id = a.user_id AND a.id = ' . $action[0])->fetchAll('assoc');
    	$questions = $connection->execute('SELECT q.question, ap.text, ap.as_title, q.take_as FROM cfp.questions as q, answer_parts as ap, answers as a WHERE a.id = ' . $action[0] . ' AND a.form_id = q.form_id'.
    		' AND ap.question_id = q.id AND ap.answer_id = a.id AND a.id NOT IN (SELECT answer_id FROM cfp.responses WHERE answer_id IS NOT NULL ) ORDER BY q.question_order')->fetchAll('assoc');
    	$formType = $connection->execute('SELECT type FROM forms as f, answers as a WHERE a.form_id = f.id and a.id = ' . $action[0])->fetchAll('assoc');
        $event = $connection->execute('SELECT for_redhat FROM events as e, answers as a WHERE a.event_id = e.id AND a.id = ' . $action[0])->fetch("assoc");
    	$permission = $connection->execute('SELECT distinct 1 FROM reviewers as r, managers as m, answers as a WHERE a.id = ' . $action[0]
    					   . ' AND ((r.event_id = a.event_id and r.user_id like ("' . $_SESSION["loggedUser"] . '")) or (m.event_id = a.event_id and m.user_id like ("'
    					   . $_SESSION['loggedUser'] . '")))')->fetchAll('assoc');

    	foreach($questions as $i=>$r){
    		if($r['take_as'] == 'topics'){
                $curTops = $connection->execute('SELECT GROUP_CONCAT(" ", name) as name FROM topics WHERE id IN(' . $r['text'] . ')')->fetch("assoc");
    			$questions[$i]['text'] = $curTops['name'];
    		}
    		if($r['take_as'] == 'accommodation'){
    		    if($formType['type'] == "accommodation"){
                    $curRooms = $connection->execute('SELECT CONCAT(h.name, " - ", r.name, " (", r.full_price, ")") as name FROM hotels as h, hotel_rooms as r WHERE r.id IN (' . $r['text'] . ') and r.hotel_id = h.id')->fetch("assoc");
                } else {
                    $curRooms = $connection->execute('SELECT CONCAT(h.name, " - ", r.name, " (", r.covered_price, ")") as name FROM hotels as h, hotel_rooms as r WHERE r.id IN (' . $r['text'] . ') and r.hotel_id = h.id')->fetch("assoc");
                }
    			$questions[$i]['text'] = $curRooms['name'];
    		}
    		if($r['as_title'] == 1){
                        $questions['Title'] = $r['text'];
    	                unset($questions[$i]);
            }
        }
    
        if(!isset($questions['Title'])){
            $questions['Title'] = ucfirst($formType[0]['type']) . " proposal no. " . $action[0];
        }
    
    	foreach($speakers as $key=>$o){
    		if(parent::getAdmin() || ($permission != null && $permission[0]['1'])){
                            if($permission != null && $permission[0]['1']){
    				$userType = $formType[0]['type'] . " proposals reviewer";
    				if(parent::getProgramManager()){
    					$userType = "program manager";
    				}
    			} else {
    				$userType = "admin";
    			}
    			$questions['comments'] = $connection->execute('SELECT author, comment, time, public FROM cfp.comments WHERE answer_id = ' . $action[0] . ' ORDER BY id desc')->fetchAll('assoc');
    			$permission = true;
    		} else {
    			if(count($talk)>0 && $talk[0]['id'] == $_SESSION['loggedUser']){
    				$permission = true;
    				$userType = "speaker";
    			} else {
    				$permission = false;
    			}
    			$questions['comments'] = $connection->execute('SELECT author, comment, time, public FROM cfp.comments WHERE answer_id = ' . $action[0] . ' AND public = 1 ORDER BY id desc')->fetchAll('assoc');
    		}
    		$additional = $connection->execute('SELECT CONCAT(u.first_name, " ", u.last_name) as additional from users u, additional_speakers a WHERE u.id = a.user_id AND a.answer_id = ' . $action[0])->fetchAll('assoc');
    		if (is_array($additional) && count($additional) > 0){
    			$speakers[$key]['speaker'] .= " (main speaker)";
    			foreach($additional as $k => $a) {
    				$speakers[$key]['speaker'] .= ", " . $a['additional'];
    			}
    		}
    	}

        if($event['for_redhat'] == 1 && !parent::isRedHatter()){
            $questions = [];
        }

    	if(count($questions) > 0){
    		$questions[] = array("question"=>"Speaker", "text"=>$speakers[0]['speaker']);
    		$this->set("form", $questions);
    		$this->set("answer", $action[0]);
    		$token = $this->request->getParam('_csrfToken');
                        $this->request->session()->write("token", $token);
    		$this->set("token", $token);
    		$this->set("permission", $permission);
    		$this->set("userType", $userType);

    	}
    	try {
    		$this->render('talk');
    	} catch (MissingTemplateException $exception) {
    		if (Configure::read('debug')) {
    			throw $exception;
    		}
    		throw new NotFoundException();
    	}
    } 
    }
}
