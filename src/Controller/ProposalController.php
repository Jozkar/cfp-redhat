<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Datasource\ConnectionManager;
use Cake\Mailer\Email;

/**
 * Static content controller
 *
 * This controller will render views from Template/HomePage/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class ProposalController extends AppController
{

    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Network\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function show(...$path)
    {
        if(empty($path) || count($path) < 1 || !is_numeric($path[0])){
            $_SESSION['errorMessage'][] = "Unsufficient data. Your link is probably broken.";
            return $this->redirect("/");
        }

        $this->set("active", "home");
        $this->set("admin", parent::getAdmin() | parent::getSuperUser());
        $this->set("reviewer", parent::getReviewer());
        $this->set("program_manager", parent::getProgramManager());

        parent::printFlush($this->request->here());

        if($this->request->session()->read("first-name")){
            $this->set('username', $_SESSION['first-name'] . " " . $_SESSION['last-name']);
        }

        $connection = ConnectionManager::get('cfp');

        $form = $connection->execute('SELECT * FROM cfp.forms WHERE id = ' . $path[0])->fetchAll('assoc');
        if(count($form) < 1){
            $_SESSION['errorMessage'][] = "This form doesn't exist.";
            return $this->redirect('/');
        }
        $form = $form[0];
        $event = $connection->execute('SELECT id, year, name, redhat_event, GROUP_CONCAT(topic_id) AS topics, cfp_close FROM cfp.events, cfp.topics_to_events WHERE id = ' . $form['event_id']
            . ' AND event_id = id')->fetchAll('assoc')[0];

        $this->set("current_program_manager", parent::getProgramManagerForEvent($form['event_id']));

        if(strtotime(date("Y-m-d", time())) > strtotime(($form['cfp_close'] != "" && $form['cfp_close'] != $event['cfp_close']?$form['cfp_close']:$event['cfp_close'])) && !parent::getAdmin() && !parent::getProgramManagerForEvent($form['event_id'])){
            $_SESSION['errorMessage'][] = "CfP is closed. You can't add, modify or delete any proposal.";
            return $this->redirect('/');
        }

        if($event['redhat_event'] == 0){
            $_SESSION['errorMessage'][] = "This event isn't organized by Red Hat. You can't find any form for it.";
            return $this->redirect("/");
        }

        $questions = $connection->execute('SELECT * FROM cfp.questions WHERE form_id = ' . $path[0] . ' ORDER BY question_order')->fetchAll('assoc');

        if(count($path) > 1 && !is_numeric($path[1])){
            if($path[1] == "add"){
                return $this->add($connection, $questions, $event, $form);
            } else if($path[1] == "update"){
                if(count($path) > 3 && is_numeric($path[2]) && is_numeric($path[3])){
                    return $this->update($connection, $questions, $event, $form, $path[2], $path[3]);
                } else {
                    $_SESSION['errorMessage'][] = "Your link is probably broken. No changes has been made.";
                    return $this->redirect("/proposal/" . $path[0]);
                }
            } else if($path[1] == "delete"){
                if(count($path) > 3 && is_numeric($path[2]) && is_numeric($path[3])){
                    return $this->delete($connection, $path[2], $path[3]);
                } else {
                    $_SESSION['errorMessage'][] = "Your link is probably broken. No changes has been made.";
                    return $this->redirect("/myproposals");
                }    		

            } else {
                $_SESSION['errorMessage'][] = "This opperation isn't supported.";
                return $this->redirect("/proposal/" . $path[0]);
            }

        }else{
            $options = $connection->execute('SELECT * FROM cfp.question_options WHERE form_id = ' . $path[0] . ' ORDER BY question_id, id')->fetchAll('assoc');
            $topics = $connection->execute('SELECT * FROM cfp.topics WHERE id IN (' . ($event['topics']!=""?$event['topics']:-5) . ') ORDER BY name, id')->fetchAll('assoc');

            foreach($questions as $i=>$q){
                $questions[$i]['options'] = array();
                foreach($options as $o){
                    if($q['id'] == $o['question_id']){
                        $questions[$i]['options'][] = $o;
                    }
                }
            }

            if(count($path) > 2 && is_numeric($path[1]) && is_numeric($path[2])){
                //check if user modifies it's own submission
                $myOwn = $connection->execute("SELECT 1 FROM responses WHERE id = " . $path[1] . " and user_id like '" . $_SESSION['loggedUser'] . "'")->rowCount();
                $additSpeaker = $connection->execute("SELECT 1 FROM additional_speakers WHERE response_id = " . $path[1] . " and user_id like '" . $_SESSION['loggedUser'] . "'")->rowCount();
                $myOwnAnswer = $connection->execute("SELECT 1 FROM answers WHERE id = " . $path[2] . " and user_id like '" . $_SESSION['loggedUser'] . "'")->rowCount();
                $additSpeakerAnswer = $connection->execute("SELECT 1 FROM additional_speakers WHERE answer_id = " . $path[2] . " and user_id like '" . $_SESSION['loggedUser'] . "'")->rowCount();

                $values = [];
                //doing update of my answer/response
                if($path[1] > 0){
                    if($myOwn || $additSpeaker || parent::getAdmin() || parent::getProgramManagerForEvent($form['event_id'])){
                        $answerId = $connection->execute("SELECT answer_id FROM responses WHERE id = " . $path[1])->fetchAll('assoc');
                        if(count($answerId) > 0) {
                            $answerId = $answerId[0]['answer_id'];
                        }else{
                            $answerId = NULL;
                        }
                        foreach($questions as $q){
                            if($q['type'] == "description"){
                                $values[] = NULL;
                                continue;
                            }
                            if(strpos($q['take_as'], "self") !== false){
                                $v = $connection->execute("SELECT text FROM cfp.answer_parts WHERE question_id = " . $q['id'] . " AND answer_id = " . ($answerId != NULL?$answerId:"-1"))->fetchAll("assoc");
                                if(count($v) > 0){
                                    $values[] = $v[0]['text'];
                                } else {
                                    $values[] = NULL;
                                }
                            } else if($q['take_as'] == "topics"){
                                $v = $connection->execute("SELECT topic_id FROM responses_to_topics WHERE response_id = " . $path[1])->fetchAll('assoc');
                                $t = [];
                                foreach($v as $top){
                                    $t[] = $top['topic_id'];
                                }
                                $values[] = implode(",", $t);
                            } else {
                                $v = $connection->execute("SELECT " . $q['take_as'] . " FROM cfp.responses WHERE id = " . $path[1])->fetchAll('assoc');
                                if(count($v) > 0){
                                    $values[] = $v[0][$q['take_as']];
                                } else {
                                    $values[] = NULL;
                                }
                            }
                        }
                        $this->set("values", $values);
                        $this->set("response", $path[1]);
                        $this->set("answer", $path[2]);
                    }
                } else {
                    if($myOwnAnswer || $additSpeakerAnswer || parent::getAdmin() || parent::getProgramManagerForEvent($form['event_id'])){
                        foreach($questions as $q){
                            $v = $connection->execute("SELECT text FROM cfp.answer_parts WHERE question_id = " . $q['id'] . " AND answer_id = " . $path[2])->fetchAll("assoc");
                            if(count($v) > 0){
                                $values[] = $v[0]['text'];
                            } else {
                                $values[] = NULL;
                            }
                        }
                        $this->set("values", $values);
                        $this->set("response", $path[1]);
                        $this->set("answer", $path[2]);
                    }
                }    		
            } else {
                if($form['one_response'] == 0 && !parent::getAdmin() && !parent::getProgramManagerForEvent($form['event_id'])){
                    //check for existing response from this user based on it's type
                    if($form['type'] == "session"){
                        $exists = $connection->execute("SELECT 1 FROM cfp.responses WHERE user_id like '" . $_SESSION['loggedUser'] . "' AND event_id = " . $form['event_id'])->rowCount();
                        if( $exists > 0 ){
                            $_SESSION['errorMessage'][] = "You have already submitted your proposal for this event.";
                            return $this->redirect("/submit/" . $form['event_id']);
                        }
                    } else {
                        $exists = $connection->execute("SELECT 1 FROM cfp.answers WHERE user_id like '" . $_SESSION['loggedUser'] . "' AND form_id = " . $form['id'] . " AND event_id = " . $form['event_id'])->rowCount();
                        if( $exists > 0 ){
                            $_SESSION['errorMessage'][] = "You have already submitted your proposal for this event.";
                            return $this->redirect("/submit/" . $form['event_id']);
                        }
                    }
                }

                $this->set("values", null);
            }

            $this->set("event", $event);
            $this->set("questions", $questions);
            $this->set("topics", $topics);
            $this->set("form", $form);

            if (parent::getAdmin() || parent::getProgramManagerForEvent($form['event_id'])){
                $allUsers = $connection->execute("SELECT first_name, last_name, id, email FROM users")->fetchAll("assoc");
                $this->set("users", $allUsers);
            } else {
                $this->set("users", null);
            }

            try {
                $this->render("home");
            } catch (MissingTemplateException $exception) {
                if (Configure::read('debug')) {
                    throw $exception;
                }
                throw new NotFoundException();
            }
        }
    }

    public function confirmation(...$path){
        if(empty($path) || count($path) < 1 || !is_numeric($path[0])){
            $_SESSION['errorMessage'][] = "Unsufficient data. Your link is probably broken.";
            return $this->redirect("/");
        }

        $this->set("active", "home");
        $this->set("admin", parent::getAdmin());
        $this->set("reviewer", parent::getReviewer());
        $this->set("program_manager", parent::getProgramManager());
        parent::printFlush($this->request->here());

        if($this->request->session()->read("first-name")){
            $this->set('username', $_SESSION['first-name'] . " " . $_SESSION['last-name']);
        }

        $connection = ConnectionManager::get('cfp');

        $form = $connection->execute('SELECT * FROM cfp.forms WHERE id = ' . $path[0])->fetchAll('assoc');
        if(count($form) < 1){
            $_SESSION['errorMessage'][] = "This form doesn't exist.";
            return $this->redirect('/');
        }
        $form = $form[0];
        $event = $connection->execute('SELECT id, year, name FROM cfp.events WHERE id = ' . $form['event_id'])->fetchAll('assoc')[0];


        $this->set("event", $event);
        $this->set("response", $path[1]);
        $this->set("answer", $path[2]);
        $this->set("form", $form);
        $this->set("hasSMTP", parent::hasSMTP($event['id']));

        try {
            $this->render("thanks");
        } catch (MissingTemplateException $exception) {
            if (Configure::read('debug')) {
                throw $exception;
            }
            throw new NotFoundException();
        }
    }

    public function sendConfirm($connection, $form, $answers, $rid, $aid){

        $event = $connection->execute('SELECT name, year, id FROM events WHERE id = ' . $form['event_id'])->fetchAll('assoc')[0];
        $mail = $connection->execute('SELECT * FROM cfp.emails WHERE event_id = ' . $event['id'] . ' and type = "confirmation" and form_type = "' . ($form['type'] == 'quiz'?'quiz':'event') . '"')->fetchAll('assoc')[0];
        $smtp = $connection->execute('SELECT * FROM cfp.smtps WHERE event_id = ' . $event['id'])->fetchAll('assoc')[0];
        $smtp['password'] = parent::decrypt($smtp['password']);
        $smtp['username'] = parent::decrypt($smtp['username']);
        Email::configTransport("custom", ['className'=>"Smtp", "timeout"=>60, "tls"=>($smtp['tls']==1?true:false), 'host'=>$smtp['host'], 'port'=>$smtp['port'], 'username'=>$smtp['username'], 
        'password'=>$smtp['password'], 'context'=>['ssl'=>['allow_self_signed'=>true, 'verify_peer'=>false, 'verify_peer_name'=>false]]]);

        $ans = "";
        foreach($answers as $a){
            if($mail['format'] == "html"){
                $ans .= "<h3>" . $a['question'] . "</h3>" . $a['answer'] . "<br>";
            } else {
                $ans .= "Q: " . $a['question'] . "\nA: " . $a['answer'] . "\n\n";
            }
        }
        $l = $_SERVER["REQUEST_SCHEME"] . "://" . $_SERVER["HTTP_HOST"] . "/proposal/" . $form['id'] . "/" . $rid . "/" . $aid;
        if($mail['format'] == "html"){
            $link = "<a href='" . $l . "'>" . $l . "</a>";
        } else {
            $link = $l;
        }
        $text = str_replace(parent::getKeywords('confirmation'), array($_SESSION['first-name'], $_SESSION['last-name'], $_SESSION['loggedUser'], $event['name'], $event['year'], $form['type'], $ans, $link), $mail['text']);
        $email = new Email();
        $email->dropTransport("Smtp");
        $email->emailFormat($mail['format']);
        $email->setFrom([$smtp['username'] => $smtp['nick']]);
        if($smtp['replyto'] != ''){
            $email->setReplyTo($smtp['replyto']);
        }
        $email->setTo($_SESSION['loggedUser']);
        $email->setSubject($mail['subject']);
        $email->send($text);
    }

    public function add($connection, $questions, $event, $form) {
        if(parent::getAdmin() || parent::getProgramManagerForEvent($form['event_id'])){
            if(isset($_POST['replaceUser']) && $_POST['replaceUser'] != ""){
                $userCheck = $connection->execute("SELECT 1 FROM cfp.users WHERE id like ('" . str_replace(array("'", "\""), array("\'", "\\\""), $_POST['replaceUser']) . "')")->rowCount();
                if($userCheck > 0){
                    $backup = $_SESSION['loggedUser'];
                    $_SESSION['loggedUser'] = $_POST['replaceUser'];
                }
            }
        }

        if($form['one_response'] == 0){
            //check for existing response from this user based on it's type
            if($form['type'] == "session"){
                $exists = $connection->execute("SELECT 1 FROM cfp.responses WHERE user_id like '" . $_SESSION['loggedUser'] . "' AND event_id = " . $form['event_id'])->rowCount();
                if( $exists > 0 ){
                    $_SESSION['errorMessage'][] = "You have already submitted your proposal for this event.";
                    if(parent::getAdmin() || parent::getProgramManagerForEvent($form['event_id'])){
                        if($userCheck > 0){
                            $_SESSION['loggedUser'] = $backup;
                        }
                    }
                    return $this->redirect("/submit/" . $form['event_id']);
                }
            } else {
                $exists = $connection->execute("SELECT 1 FROM cfp.answers WHERE user_id like '" . $_SESSION['loggedUser'] . "' AND form_id = " . $form['id'] . " AND event_id = " . $form['event_id'])->rowCount();
                if( $exists > 0 ){
                    $_SESSION['errorMessage'][] = "You have already submitted your proposal for this event.";
                    if(parent::getAdmin() || parent::getProgramManagerForEvent($form['event_id'])){
                        if($userCheck > 0){
                            $_SESSION['loggedUser'] = $backup;
                        }
                    }
                    return $this->redirect("/submit/" . $form['event_id']);
                }
            }
        }

        $confirmation = [];
        $takeAsArray = [];
        $insertValues = [];
        $addToTopics = [];
        $answerId = -1;
        $newId = -1;
        foreach($questions as $q){
            switch($q['type']){
                case "text":
                case "textarea":
                case "radio":
                case "hidden":
                    if($q['take_as'] == "topics"){
                        $addToTopics[] = $_POST[$q['take_as']];
                        $topicName = $connection->execute("SELECT name FROM cfp.topics WHERE id = " . $_POST[$q['take_as']])->fetchAll("assoc");
                        if(count($topicName) > 0){
                            $confirmation [] = array("question"=>$q['question'], "answer"=>htmlspecialchars($topicName[0]['name']));
                            if($form['type'] !== "session"){
                                if($answerId < 0){
                                    $connection->execute("INSERT INTO answers (event_id, user_id, form_id) VALUES (" . $event['id'] . ", '" . $_SESSION['loggedUser'] . "', " . $form['id'] . ")");
                                    $answerId = $connection->execute("SELECT LAST_INSERT_ID() as id")->fetchAll('assoc')[0]['id'];
                                }
                                // INSERT INTO answers
                                $connection->execute("INSERT INTO answer_parts (question_id, answer_id, text, as_title, as_description, as_duration) VALUES ("
                                    . $q['id'] . ", " . $answerId . ", " . $_POST[$q['take_as']] . ", " . $q['as_title'] . ", " . $q['as_description'] . ", " . $q['as_duration'] . ")");
                            }
                        }
                    } else if(strpos($q['take_as'], "self") !== false){
                        $answer = "'" . str_replace(array("'", "\""), array("\'", "\\\""), $_POST[$q['take_as']]) . "'";
                        try {
                            if($answerId < 0){
                                $connection->execute("INSERT INTO answers (event_id, user_id, form_id) VALUES (" . $event['id'] . ", '" . $_SESSION['loggedUser'] . "', " . $form['id'] . ")");
                                $answerId = $connection->execute("SELECT LAST_INSERT_ID() as id")->fetchAll('assoc')[0]['id'];
                            }
                            // INSERT INTO answers
                            $connection->execute("INSERT INTO answer_parts (question_id, answer_id, text, as_title, as_description, as_duration) VALUES (" . $q['id'] . ", " . $answerId . ", " . $answer . ", " . $q['as_title']
                                . ", " . $q['as_description'] . ", " . $q['as_duration'] . ")");
                            if($q['type'] != "hidden"){
                                $confirmation[] = array("question"=>$q['question'], "answer"=>nl2br(htmlspecialchars($_POST[$q['take_as']])));
                            }
                        }catch(\Exception $e){
                            $_SESSION['errorMessage'][] = $e->getMessage();
                            $connection->execute("DELETE FROM answer_parts WHERE answer_id = ". $answerId);
                            $connection->execute("DELETE FROM answers WHERE id = " . $answerId);
                            if(parent::getAdmin() || parent::getProgramManagerForEvent($form['event_id'])){
                                if($userCheck > 0){
                                    $_SESSION['loggedUser'] = $backup;
                                }
                            }
                            return $this->redirect("/proposal/" . $form['id']);
                        }
                    } else {
                        $takeAsArray[] = $q['take_as'];
                        if(isset($_POST[$q['take_as']]) && $_POST[$q['take_as']] != ""){
                            $insertValues[] = "'" . str_replace(array("'", "\""), array("\'", "\\\""), $_POST[$q['take_as']]) . "'";
                        } else {
                            if($q['required']){
                                $_SESSION['errorMessage'][] = "You didn't specified required fields. Your proposal wasn't accepted.";
                                if($answerId > 0){
                                    $connection->execute("DELETE FROM answer_parts WHERE answer_id = ". $answerId);
                                    $connection->execute("DELETE FROM answers WHERE id = " . $answerId);
                                }
                                if(parent::getAdmin() || parent::getProgramManagerForEvent($form['event_id'])){
                                    if($userCheck > 0){
                                        $_SESSION['loggedUser'] = $backup;
                                    }
                                }
                                return $this->redirect("/proposal/" . $form['id']);
                            } else {
                                $insertValues[] = "NULL";
                            }
                        }
                        $confirmation [] = array("question"=>$q['question'], "answer"=>nl2br(htmlspecialchars($_POST[$q['take_as']])));
                    }
                    break;
                case "checkbox":
                    $confVal = [];
                    if(isset($_POST[$q['take_as']]) && count($_POST[$q['take_as']]) > 0){
                        if($q['take_as'] == "topics"){
                            foreach ($_POST['topics'] as $t){
                                $addToTopics[] = $t;
                            }
                            $tN = $connection->execute("SELECT name FROM cfp.topics WHERE id IN (" . implode(",",$_POST['topics']) . ")")->fetchAll("assoc");
                            $topicName = array();
                            foreach($tN as $t){
                                $topicName[] = $t['name'];
                            }
                            if($form['type'] !== "session"){
                                if($answerId < 0){
                                    $connection->execute("INSERT INTO answers (event_id, user_id, form_id) VALUES (" . $event['id'] . ", '" . $_SESSION['loggedUser'] . "', " . $form['id'] . ")");
                                    $answerId = $connection->execute("SELECT LAST_INSERT_ID() as id")->fetchAll('assoc')[0]['id'];
                                }
                                // INSERT INTO answers
                                $connection->execute("INSERT INTO answer_parts (question_id, answer_id, text) VALUES (" . $q['id'] . ", " . $answerId . ", '" . implode(",", $addToTopics) . "')");
                            }

                            $confirmation [] = array("question"=>$q['question'], "answer"=>implode(", ", $topicName));
                        } else {
                            if(strpos($q['take_as'], "self") !== false){
                                $answers = array();
                                foreach($_POST[$q['take_as']] as $t){
                                    $answers[] = str_replace(array("'", "\""), array("\'", "\\\""), $t);
                                    $confVal[] = $t;
                                }
                                try{
                                    if($answerId < 0){;
                                        $connection->execute("INSERT INTO answers (event_id, user_id, form_id) VALUES (" . $event['id'] . ", '" . $_SESSION['loggedUser'] . "', " . $form['id'] . ")");
                                        $answerId = $connection->execute("SELECT LAST_INSERT_ID() as id")->fetchAll('assoc')[0]['id'];
                                    }
                                    //INSERT INTO answers
                                    $connection->execute("INSERT INTO answer_parts (question_id, answer_id, text) VALUES (" . $q['id'] . ", " . $answerId . ", '" . implode(",", $answers) . "')");
                                }catch(\Exception $e){
                                    $_SESSION['errorMessage'][] = $e->getMessage();
                                    $connection->execute("DELETE FROM answer_parts WHERE answer_id = ". $answerId);
                                    $connection->execute("DELETE FROM answers WHERE id = " . $answerId);
                                    if(parent::getAdmin() || parent::getProgramManagerForEvent($form['event_id'])){
                                        if($userCheck > 0){
                                            $_SESSION['loggedUser'] = $backup;
                                        }
                                    }
                                    return $this->redirect("/proposal/" . $form['id']);
                                }
                            } else {
                                $takeAsArray[] = $q['take_as'];
                                $tmpValues = array();
                                foreach($_POST[$q['take_as']] as $t){
                                    $tmpValues[] = str_replace(array("'", "\""), array("\'", "\\\""), $t);
                                    $confVal[] = $t;
                                }
                                $insertValues[] = "'" . implode(",", $tmpValues) . "'";
                            }
                            $confirmation[] = array("question"=>$q['question'], "answer"=>implode(",",$confVal));
                        }
                    } else {
                        if($q['required']){
                            $_SESSION['errorMessage'][] = "You didn't specified required fields. Your proposal wasn't accepted.";
                            if($answerId > 0){
                                $connection->execute("DELETE FROM answer_parts WHERE answer_id = ". $answerId);
                                $connection->execute("DELETE FROM answers WHERE id = " . $answerId);
                            }
                            if(parent::getAdmin() || parent::getProgramManagerForEvent($form['event_id'])){
                                if($userCheck > 0){
                                    $_SESSION['loggedUser'] = $backup;
                                }
                            }
                            return $this->redirect("/proposal/" . $form['id']);
                        }
                    }
                    break;
                default: 
                    break;
            }
        }
        if($form['type'] == 'session'){
            $takeAsArray[] = "event_id";
            $takeAsArray[] = "user_id";
            $insertValues[] = $event['id'];
            $insertValues[] = "'" . $_SESSION['loggedUser'] . "'";
            if(count($addToTopics) < 1){
                $addToTopics[] = -1;
            }
            if($answerId > 0){
                $takeAsArray[] = "answer_id";
                $insertValues[] = $answerId;
            }
            try{
                $connection->execute("INSERT INTO cfp.responses (" . implode(",", $takeAsArray) . ") VALUES (" . implode(",", $insertValues) . ")");
                $newId = $connection->execute("SELECT LAST_INSERT_ID() as id")->fetchAll('assoc')[0]['id'];
                foreach($addToTopics as $att){
                    $connection->execute("INSERT INTO cfp.responses_to_topics (response_id, topic_id, event_id) VALUES (" . $newId . ", " . $att . ", " . $event['id'] . ")");
                }
            }catch(\Exception $e){
                $_SESSION['errorMessage'][] = $e->getMessage();
                if($answerId > 0){
                    $connection->execute("DELETE FROM answer_parts WHERE answer_id = ". $answerId);
                    $connection->execute("DELETE FROM answers WHERE id = " . $answerId);
                }
                if($newId > 0){
                    $connection->execute("DELETE FROM cfp.responses WHERE id = " . $newId);
                    $connection->execute("DELETE FROM cfp.responses_to_topics WHERE event_id = " . $event['id'] . " AND response_id = " . $newId);
                }
                if(parent::getAdmin() || parent::getProgramManagerForEvent($form['event_id'])){
                    if($userCheck > 0){
                        $_SESSION['loggedUser'] = $backup;
                    }
                }
                return $this->redirect("/proposal/" . $form['id']);
            }
        }
        try{
            if(parent::hasSMTP($form['event_id'])){
                $this->sendConfirm($connection, $form, $confirmation, $newId, $answerId);
            }
        }catch(\Exception $e){
            $_SESSION['errorMessage'][] = $e->getMessage();
        }

        if(parent::getAdmin() || parent::getProgramManagerForEvent($form['event_id'])){
            if($userCheck > 0){
                $_SESSION['loggedUser'] = $backup;
            }
        }

        return $this->redirect("/confirm/".$form['id']."/".$newId."/".$answerId);
    }

    public function update($connection, $questions, $event, $form ,$responseId, $answerId) {
        $takeAsArray = [];
        $insertValues = [];
        $addToTopics = [];
        foreach($questions as $q){
            switch($q['type']){
                case "text":
                case "textarea":
                case "radio":
                    if($q['take_as'] == "topics"){
                        $addToTopics[] = $_POST[$q['take_as']];
                    } else if(strpos($q['take_as'], "self") !== false){
                        $answer = "'" . str_replace(array("'", "\""), array("\'", "\\\""), $_POST[$q['take_as']]) . "'";
                        try {
                            if($answerId < 0){
                                $connection->execute("INSERT INTO answers (event_id, user_id, form_id) VALUES (" . $event['id'] . ", '" . $_SESSION['loggedUser'] . "', " . $form['id'] . ")");
                                $answerId = $connection->execute("SELECT LAST_INSERT_ID() as id")->fetchAll('assoc')[0]['id'];
                                $connection->execute("UPDATE responses SET answer_id = " . $answerId . " WHERE id = " . $responseId);
                                $connection->execute("INSERT INTO answer_parts (question_id, answer_id, text, as_title, as_description, as_duration) VALUES (" . $q['id'] . ", " . $answerId . ", " . $answer . ", " . $q['as_title']
                                    . ", " . $q['as_description'] . "," . $q['as_duration'] . ")");
                            } else {
                                $row = $connection->execute("SELECT 1 FROM answer_parts WHERE question_id = " . $q['id'] . " AND answer_id = " . $answerId)->rowCount();
                                if($row > 0){
                                    $connection->execute("UPDATE answer_parts SET text=" . $answer . " WHERE question_id = " . $q['id'] . " AND answer_id = " . $answerId);
                                } else {
                                    $connection->execute("INSERT INTO answer_parts (question_id, answer_id, text, as_title, as_description, as_duration) VALUES (" . $q['id'] . ", " . $answerId . ", " . $answer . ", " . $q['as_title']
                                        . ", " . $q['as_description'] . ", " . $q['as_duration'] . ")");
                                }
                            }
                        }catch(\Exception $e){
                            $_SESSION['errorMessage'][] = $e->getMessage();
                            return $this->redirect("/proposal/" . $form['id']);
                        }
                    } else {
                        $takeAsArray[] = $q['take_as'];
                        if(isset($_POST[$q['take_as']]) && $_POST[$q['take_as']] != ""){
                            $insertValues[] = "'" . str_replace(array("'", "\""), array("\'", "\\\""), $_POST[$q['take_as']]) . "'";
                        } else {
                            if($q['required']){
                                $_SESSION['errorMessage'][] = "You didn't specified required fields. Your proposal was partially updated.";
                                return $this->redirect("/proposal/" . $form['id']);
                            } else {
                                $insertValues[] = "NULL";
                            }
                        }
                    }
                    break;
                case "checkbox":
                    if(isset($_POST[$q['take_as']]) && count($_POST[$q['take_as']]) > 0){
                        if($q['take_as'] == "topics"){
                            foreach ($_POST['topics'] as $t){
                                $addToTopics[] = $t;
                            }
                        } else {
                            if(strpos($q['take_as'], "self") !== false){
                                $answers = array();
                                foreach($_POST[$q['take_as']] as $t){
                                    $answers[] = str_replace(array("'", "\""), array("\'", "\\\""), $t);
                                }
                                try{
                                    if($answerId < 0){
                                        $connection->execute("INSERT INTO answers (event_id, user_id, form_id) VALUES (" . $event['id'] . ", '" . $_SESSION['loggedUser'] . "', " . $form['id'] . ")");
                                        $answerId = $connection->execute("SELECT LAST_INSERT_ID() as id")->fetchAll('assoc')[0]['id'];
                                        $connection->execute("UPDATE responses SET answer_id = " . $answerId . " WHERE id = " . $responseId);
                                        $connection->execute("INSERT INTO answer_parts (question_id, answer_id, text) VALUES (" . $q['id'] . ", " . $answerId . ", '" . implode(",", $answers) . "')");
                                    } else {
                                        $row = $connection->execute("SELECT 1 FROM answer_parts WHERE question_id = " . $q['id'] . " AND answer_id = " . $answerId)->rowCount();
                                        if($row > 0){
                                            $connection->execute("UPDATE answer_parts SET text='" . implode(",", $answers) . "' WHERE question_id = " . $q['id'] . " AND answer_id = " . $answerId);
                                        } else {
                                            $connection->execute("INSERT INTO answer_parts (question_id, answer_id, text) VALUES (" . $q['id'] . ", " . $answerId . ", '" . implode(",", $answers) . "')");
                                        }
                                    }
                                }catch(\Exception $e){
                                    $_SESSION['errorMessage'][] = $e->getMessage();
                                    return $this->redirect("/proposal/" . $form['id']);
                                }
                            } else {
                                $takeAsArray[] = $q['take_as'];
                                $tmpValues = array();
                                foreach($_POST[$q['take_as']] as $t){
                                    $tmpValues[] = str_replace(array("'", "\""), array("\'", "\\\""), $t);
                                }
                                $insertValues[] = "'" . implode(",", $tmpValues) . "'";
                            }
                        }
                    } else {
                        if($q['required']){
                            $_SESSION['errorMessage'][] = "You didn't specified required fields. Your proposal was partially updated.";
                            return $this->redirect("/proposal/" . $form['id']);
                        }
                    }
                    break;
                default: 
                    break;
            }
        }
        if($form['type'] == 'session'){
            if(count($addToTopics) < 1){
                $addToTopics[] = -1;
            }
            try{
                $body = [];
                foreach($takeAsArray as $i=>$t){
                    $body[] = $t . "=" . $insertValues[$i];
                }
                
                $connection->execute("UPDATE cfp.responses SET " . implode(",", $body) . " WHERE id =" . $responseId);

                $currentTopics = $connection->execute("SELECT topic_id FROM cfp.responses_to_topics WHERE response_id = " . $responseId)->fetchAll('assoc');
                $current = [];
                foreach($currentTopics as $c){
                    $current[] = $c["topic_id"];
                }

                foreach($addToTopics as $att){
                    if(($k = array_search($att, $current)) !== false){
                        unset($current[$k]);
                    } else {
                        $connection->execute("INSERT INTO cfp.responses_to_topics (response_id, topic_id, event_id) VALUES (" . $responseId . ", " . $att . ", " . $event['id'] . ")");
                    }
                }
                if(count($current) > 0){
                    $connection->execute('DELETE FROM cfp.votes WHERE event_id = ' . $event['id'] . ' AND response_to_topic_id in (SELECT id FROM responses_to_topics WHERE event_id = ' . $event['id'] . 
                                ' AND response_id  = ' . $responseId . ' AND topic_id IN (' . implode(",", $current) . '))');
                    $connection->execute("DELETE FROM cfp.responses_to_topics WHERE response_id = " . $responseId . " AND topic_id IN (" . implode(",", $current) . ")");
                }
            }catch(\Exception $e){
                $_SESSION['errorMessage'][] = $e->getMessage();
                return $this->redirect("/proposal/" . $form['id']);
            }
        }

        if(!isset($_POST['minorUpdate'])){
            if($form['type'] == "session"){
                $connection->execute('DELETE FROM cfp.votes WHERE event_id = ' . $event['id'] . ' AND response_to_topic_id in (SELECT id FROM responses_to_topics WHERE event_id = ' . $event['id'] .
                            ' AND response_id  = ' . $responseId . ')');
            } else {
                $connection->execute('DELETE FROM cfp.votes WHERE event_id = ' . $event['id'] . ' AND answer_id = ' . $answerId);
            }
        }

        $_SESSION['successMessage'][] = "Your proposal was successfully updated.";
        return $this->redirect("/myproposals");
    }

    public function delete($conn, $response, $ans){
        if($response > -1){
            $answer = $conn->execute("SELECT answer_id FROM cfp.responses WHERE id = " . $response . " AND user_id like ('" . $_SESSION['loggedUser'] . "')")->fetchAll("assoc");
            if (count($answer) < 1){
                $_SESSION['errorMessage'][] = "You can't remove proposal of someone else.";
                return $this->redirect("/myproposals");
            }

            $conn->execute("DELETE FROM cfp.responses WHERE id = " . $response);
            $conn->execute("DELETE FROM cfp.comments WHERE response_id = " . $response);
            $conn->execute("DELETE FROM cfp.additional_speakers WHERE response_id = " . $response);
            $conn->execute("DELETE FROM cfp.accepts WHERE response_to_topic_id IN (SELECT id FROM responses_to_topics WHERE response_id = " . $response . ")");
            $conn->execute("DELETE FROM cfp.votes WHERE response_to_topic_id IN (SELECT id FROM responses_to_topics WHERE response_id = " . $response . ")");
            $conn->execute("DELETE FROM cfp.responses_to_topics WHERE response_id = " . $response);
            $conn->execute("DELETE FROM cfp.schedule WHERE response_id = " . $response);

            if($answer[0]['answer_id'] > 0){
                $conn->execute("DELETE FROM cfp.answers WHERE id = " . $answer[0]['answer_id']);
                $conn->execute("DELETE FROM cfp.answer_parts WHERE answer_id = " . $answer[0]['answer_id']);
            }
        }else{
            $answer = $conn->execute("SELECT id FROM cfp.answers WHERE id = " . $ans . " AND user_id like ('" . $_SESSION['loggedUser'] . "')")->fetchAll("assoc");
            if (count($answer) < 1){
                $_SESSION['errorMessage'][] = "You can't remove proposal of someone else.";
                return $this->redirect("/myproposals");
            }

            $conn->execute("DELETE FROM cfp.answers WHERE id = " . $ans);
            $conn->execute("DELETE FROM cfp.answer_parts WHERE answer_id = " . $ans);
            $conn->execute("DELETE FROM cfp.schedule WHERE answer_id = " . $ans);
            $conn->execute("DELETE FROM cfp.additional_speakers WHERE answer_id = " . $ans);
            $conn->execute("DELETE FROM cfp.accepts WHERE answer_id = " . $ans);
            $conn->execute("DELETE FROM cfp.votes WHERE answer_id = " . $ans);
            $conn->execute("DELETE FROM cfp.comments WHERE answer_id = " . $ans);
        }

        $_SESSION['successMessage'][] = "Your proposal had been removed.";
        return $this->redirect("/myproposals");
    }
}
