<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Core\Configure;
use Google_Client;
use Jumbojett\OpenIDConnectClient;
use Cake\Datasource\ConnectionManager;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class AuthController extends Controller
{

    public function initialize()
    {

        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('Security');
    }
    
    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Network\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function google(...$path)
    {
        $connection = ConnectionManager::get('cfp');
        $gcred = $connection->execute('SELECT client_id, client_secret ,callback FROM cfp.signins WHERE service like "google" and domain like "' . $_SERVER['HTTP_HOST'] . '"')->fetchAll('assoc');
        if(!$gcred){
        	$this->login();
        }

        $id_code = $_GET["code"];

        $client = new Google_Client();
        $client->setClientId($this->decrypt($gcred[0]['client_id']));
        $client->setClientSecret($this->decrypt($gcred[0]['client_secret']));
        $client->setScopes("email");
        $client->setRedirectUri($gcred[0]['callback']);
        if(!isset($id_code) || $id_code == null){
        	header("Location: ".$client->createAuthUrl());
        	die();
        }else{
        	try{
        		$token = $client->fetchAccessTokenWithAuthCode($id_code);
        		$client->setAccessToken($token);
        		$payload = $client->verifyIdToken();
        	}catch(\Exception $e){
        		$_SESSION['errorMessage'][] = $e->getMessage();
        		return $this->redirect("/login");
        	}

        	if ($payload) {
        		$session = $this->request->session();
        		//create SESSION content
        		$session->write("loggedUser", $payload['email']);
        		$session->write("first-name", $payload['given_name']);
        		$session->write("last-name", $payload['family_name']);
        		$session->write("email", $payload['email']);
        	} else {
        		// Invalid ID token
        		$_SESSION['errorMessage'][] = "Invalid answer from Google, please contact site administrator.";
        		return $this->redirect("/login");
        	}

        	$connection = ConnectionManager::get('cfp');
            $results = $connection->execute('SELECT id,first_name,last_name FROM cfp.users WHERE id like "' . $payload['email'] . '"')->fetchAll('assoc');
        	if(!$results){
        		return $this->redirect("/profile");
        	} else {
        		$session->write("first-name", $results[0]['first_name']);
        		$session->write("last-name", $results[0]['last_name']);
                if(isset($_SESSION['desiredDestination']) && $_SESSION['desiredDestination'] != ""){
                    $desDes = $_SESSION['desiredDestination'];
                    unset($_SESSION['desiredDestination']);
                    return $this->redirect($desDes);
                }            
        		return $this->redirect("/");
        	}
        }
    }

    public function github(...$action){
        $this->loadComponent('Security');
        $this->loadComponent('Csrf');

        $connection = ConnectionManager::get('cfp');
        $session = $this->request->session();
        if(count($action) < 1){
    	    $githubtoken = $this->request->getParam('_csrfToken');
        	$ghcred = $connection->execute('SELECT client_id, callback FROM cfp.signins WHERE service like "github" AND domain like "' . $_SERVER['HTTP_HOST'] . '"')->fetchAll('assoc');
        	if(!$ghcred){
    	    	login();
        	}else{
        		$params = array(
        			'client_id' => $this->decrypt($ghcred[0]['client_id']),
    	    		'redirect_uri' => $ghcred[0]['callback'],
    		    	'scope' => 'read:user,user:email',
    			    'state' => $githubtoken
        		);
        		header('Location: https://github.com/login/oauth/authorize?' . http_build_query($params));
    	    	die();
        	}
        } else {
    	    if(isset($_GET['code']) && isset($_GET['state'])){
    		    $ghcred = $connection->execute('SELECT client_id, client_secret, name FROM cfp.signins WHERE service like "github" AND domain like "' . $_SERVER['HTTP_HOST'] . '"')->fetchAll('assoc');
        		if(count($ghcred) < 1){
        			$_SESSION['errorMessage'][]="Unsupported operation. GitHub login isn't enabled.";
    	    		return $this->redirect('/');
    		    }
        		$fields = ["client_id" => $this->decrypt($ghcred[0]['client_id']),
    			   "client_secret" => $this->decrypt($ghcred[0]['client_secret']),
    			   "code"=>$_GET['code'],
    			   "state"=>$_GET['state']];

        		$headers[] = 'Authorization: Bearer ' . $_GET['state'];
    	    	$headers[] = 'Accept: application/json';

        		$ch = curl_init();
        		curl_setopt($ch,CURLOPT_URL, "https://github.com/login/oauth/access_token");
    	    	curl_setopt($ch,CURLOPT_POST, true);
    		    curl_setopt($ch,CURLOPT_POSTFIELDS, http_build_query($fields));
        		curl_setopt($ch,CURLOPT_HTTPHEADER, $headers);
        		curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
    	    	$result = curl_exec($ch);

        		$accesstoken = json_decode($result);
        		$headers[0] = "Authorization: token " . $accesstoken->{"access_token"};
    	    	$headers[] = "User-Agent: " . str_replace(" ", "-", $ghcred[0]['name']);

                $us = curl_init();
        		curl_setopt($us,CURLOPT_URL,"https://api.github.com/user");
    	    	curl_setopt($us,CURLOPT_HTTPHEADER, $headers);
    		    curl_setopt($us,CURLOPT_RETURNTRANSFER, true);
        		$user = json_decode(curl_exec($us));
        		$names = explode(" ", $user->{'name'}, 1);

    	    	$ml = curl_init();
    		    curl_setopt($ml,CURLOPT_URL,"https://api.github.com/user/emails");
        		curl_setopt($ml,CURLOPT_HTTPHEADER, $headers);
        		curl_setopt($ml,CURLOPT_RETURNTRANSFER, true);
    	    	$mails = json_decode(curl_exec($ml));
    		    $primary = "";
                $session->write("first-name", $names[0]);
                $session->write("last-name", (count($names) <= 1?"":$names[1]));
    	    	$results = $connection->execute('SELECT id,first_name,last_name FROM cfp.users WHERE id like "' . $user->{"email"} . '"')->fetchAll('assoc');
    		    if(!$results){
    			    foreach($mails as $mail){
    				    $results = $connection->execute('SELECT id,first_name,last_name FROM cfp.users WHERE id like "' . $mail->{"email"} . '"')->fetchAll('assoc');
        				if($mail->{"primary"}){
        					$primary=$mail->{"email"};
    	    			}
    		    		if($results){
    			            $session->write("loggedUser", $results[0]['id']);
    			            $session->write("first-name", $results[0]['first_name']);
    		              	$session->write("last-name", $results[0]['last_name']);
    		       	        $session->write("email", $results[0]['id']);
    		                return $this->redirect("/");
    		    		}
    			    }
        		}else{
        			$session->write("loggedUser", $results[0]['id']);
    	    		$session->write("first-name", $results[0]['first_name']);
    		    	$session->write("last-name", $results[0]['last_name']);
    			    $session->write("email", $results[0]['id']);
        			return $this->redirect("/");
        		}
    	    	$session->write("email", $primary);
    		    $session->write("loggedUser", $primary);
        		return $this->redirect("/profile");
        	}elseif(isset($_GET['error'])){
    	    	$_SESSION['errorMessage'][]=$_GET['error_description'];
    		    return $this->redirect("/login");
        	}
    	
        	$this->login();
        }
    }
    
    public function fas(...$path)
    {
        $connection = ConnectionManager::get('cfp');
        $fcred = $connection->execute('SELECT client_id, client_secret, callback FROM cfp.signins WHERE service like "fas" and domain like "' . $_SERVER['HTTP_HOST'] . '"')->fetch('assoc');
        if(!$fcred){
        	$this->login();
        }
       
        $oidc = new OpenIDConnectClient('https://id.fedoraproject.org/openidc/',
                                $this->decrypt($fcred['client_id']),
                                $this->decrypt($fcred['client_secret']));
        $oidc->providerConfigParam(array('token_endpoint'=>'https://id.fedoraproject.org/openidc/Token',
                                         'userinfo_endpoint'=>'https://id.fedoraproject.org/openidc/UserInfo',
                                         'authorization_endpoint'=>'https://id.fedoraproject.org/openidc/Authorization'));
        
        $oidc->addScope("email");
        $oidc->addScope("profile");        
        $oidc->setRedirectURL($fcred['callback']);
        
        $oidc->authenticate();

    	try{
    		$email = $oidc->requestUserInfo('email');
    	}catch(\Exception $e){
    		$_SESSION['errorMessage'][] = $e->getMessage();
    		return $this->redirect("/login");
    	}

    	if ($email) {
    		$session = $this->request->session();
    		//create SESSION content
    		$session->write("loggedUser", $email);
    		$session->write("first-name", "FAS");
    		$session->write("last-name", "user");
    		$session->write("email", $email);
    	} else {
    		// Invalid ID token
    		$_SESSION['errorMessage'][] = "Invalid answer from FAS, please contact site administrator.";
    		return $this->redirect("/login");
    	}

    	$connection = ConnectionManager::get('cfp');
        $results = $connection->execute('SELECT id,first_name,last_name FROM cfp.users WHERE id like "' . $email . '"')->fetch('assoc');
    	if(!$results){
    		return $this->redirect("/profile");
    	} else {
    		$session->write("first-name", $results['first_name']);
    		$session->write("last-name", $results['last_name']);
            if(isset($_SESSION['desiredDestination']) && $_SESSION['desiredDestination'] != ""){
                $desDes = $_SESSION['desiredDestination'];
                unset($_SESSION['desiredDestination']);
                return $this->redirect($desDes);
            }            
    		return $this->redirect("/");
    	}
        
    }

    public function logout()
    {
        $this->request->session()->destroy();
        $this->redirect('/');
    }

    public function login(){
        if(isset($_SESSION['loggedUser'])){
    	    return $this->redirect("/");
        }
        $this->viewBuilder()->setLayout("basic");
        $this->printFlush();
        $connection = ConnectionManager::get('cfp');
        $results = $connection->execute('SELECT service FROM cfp.signins WHERE domain like "' . $_SERVER['HTTP_HOST'] . '"')->fetchAll('assoc');

        if($results){
        	$this->set("services", $results);
        }else{
        	$this->set("services", null);
        }
        
        $this->set("sName", array("google"=>"Google", "github"=>"GitHub", "fas"=>"FAS"));
        
        try {
            $this->render("login");
        } catch (MissingTemplateException $exception) {
            if (Configure::read('debug')) {
                throw $exception;
            }
            throw new NotFoundException();
        }
    }

    public function encrypt($what) {
        return openssl_encrypt($what,$cipher=env("CFP_ENC_METHOD","aes-256-cbc-hmac-sha256"), $cipherKey=env("CAKEPHP_SECRET_TOKEN",NULL), $options=0, $iv=env("CAKEPHP_SECURITY_SALT",NULL));
    }

    public function decrypt($what) {
        return openssl_decrypt($what, $cipher=env("CFP_ENC_METHOD","aes-256-cbc-hmac-sha256"), $cipherKey=env("CAKEPHP_SECRET_TOKEN",NULL), $options=0, $iv=env("CAKEPHP_SECURITY_SALT",NULL));
    }

    public function printFlush(){
        if(isset($_SESSION['successMessage'])){
            foreach($_SESSION['successMessage'] as $m){
                $this->Flash->success($m);
            }
            unset($_SESSION['successMessage']);
        }

        if(isset($_SESSION['errorMessage'])){
            foreach($_SESSION['errorMessage'] as $m){
                $this->Flash->error($m);
            }
            unset($_SESSION['errorMessage']);
        }

        if(isset($_SESSION['warningMessage'])){
            foreach($_SESSION['warningMessage'] as $m){
                $this->Flash->warning($m);
            }
            unset($_SESSION['warningMessage']);
        }
    }
}
