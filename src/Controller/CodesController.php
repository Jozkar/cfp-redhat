<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Datasource\ConnectionManager;
use Cake\Mailer\Email;
/**
 * Static content controller
 *
 * This controller will render views from Template/HomePage/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class CodesController extends AppController
{

    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Network\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function show(...$action)
    {

        if(!parent::getAdmin() && !parent::getProgramManager() && !parent::getSuperUser()){
            return $this->redirect("/");
        }

        if(count($action) < 1 || !is_numeric($action[0])){
            $_SESSION['errorMessage'][] = "Unsufficient data. Your link is probably broken.";
            if(parent::getAdmin() || parent::getSuperUser()){
                return $this->redirect("/admin");
            } else {
                return $this->redirect("/manager");
            }
        }

        parent::printFlush($this->request->here());

        $connection = ConnectionManager::get('cfp');

        if(isset($_POST['save'])){
            if(isset($_SESSION['token']) && isset($_POST['_Token']) && $_POST['_Token'] == $_SESSION['token']){
                if(isset($action[1]) && $action[1] == "add"){
                    // get user id
                    $_POST['prefix'] = str_replace(array("'", "\""), array("\'", "\\\""), $_POST['prefix']);
                    if(isset($_POST['start']) && $_POST['start'] > 0){
                        $start = $_POST['start'];
                    } else {
                        $start = $connection->execute("SELECT code FROM cfp.codes WHERE event_id = " . $action[0])->rowCount();
                        $start = $start + 1;
                    }
                    for($i = 1; $i <= $_POST['count'] && $start < 100000; $start++){
                        try{
                            $connection->execute("INSERT INTO cfp.codes (event_id, code) VALUES (" . $action[0] . ", '" . sprintf("%s%'.05d", $_POST['prefix'], $start) . "')");
                            $i++;
                        }catch(\Exception $e){
                            continue;
                        }    					
                    }
                    $_SESSION['successMessage'][] = "Your codes has been generated.";
                    return $this->redirect("/codes/" . $action[0]);
                } else {
                    $_SESSION['errorMessage'][] = "Unknown operation.";
                    return $this->redirect("/codes/" . $action[0]);
                }
            } else {
                $_SESSION['errorMessage'][] = "You are not allowed to do this opperation";
                return $this->redirect("/codes/" . $action[0]);
            }
        }


        $this->set("admin", parent::getAdmin() | parent::getSuperUser());
        $this->set("reviewer", parent::getReviewer());
        $this->set("program_manager", parent::getProgramManager());


        $eventInfo = $connection->execute("SELECT id, name, year, logo FROM events WHERE id = " . $action[0])->fetchAll("assoc");
        $codes = $connection->execute("SELECT CONCAT(u.first_name, ' ', u.last_name) as name, u.id as email, c.code FROM cfp.users as u RIGHT JOIN cfp.codes as c ON c.user_id = u.id WHERE c.event_id = " . $action[0])
            ->fetchAll("assoc");

        if(count($action) >= 2) {
            if($action[1] == "resend"){
                return $this->confirmation($eventInfo[0], $connection, $action[2]);
            } else {
                $this->stats($eventInfo[0], $codes);
            }
        } else {
            $token = $this->request->getParam('_csrfToken');

            $_SESSION['token'] = $token;
            $this->set("token", $token);
            $this->set("eventInfo", $eventInfo[0]);
            $this->set("codes", $codes);
            $this->set("active", "");
            $this->set('username', $_SESSION['first-name']." ".$_SESSION['last-name']);

            try {
                $this->render('list');
            } catch (MissingTemplateException $exception) {
                if (Configure::read('debug')) {
                    throw $exception;
                }
                throw new NotFoundException();
            }
        }
    }

    public function stats($evinfo, $codes){
        $this->response->download(str_replace(" ", "-", $evinfo['name']) . "-" . $evinfo['year'] . "-codes.csv");
        $this->set("data", $codes);
        $this->layout = 'ajax';
        $this->render("codes");
        return;
    }

    public function confirmation($event, $connection, $primary){
        if(!parent::hasSMTP($event['id'])){
            $_SESSION['warningMessage'][] = "No SMTP credentials are available for this event, so no email can be sent. Please, contact event administrator.";
            return $this->redirect("/codes/" . $event['id']);            
        }
        $form = $connection->execute('SELECT id FROM cfp.forms WHERE event_id = ' . $event['id'] . ' and type = "confirmation"')->fetchAll('assoc')[0];
        $mail = $connection->execute('SELECT * FROM cfp.emails WHERE event_id = ' . $event['id'] . ' and type = "confirmedSpeakers"')->fetchAll('assoc')[0];
        $smtp = $connection->execute('SELECT * FROM cfp.smtps WHERE event_id = ' . $event['id'])->fetchAll('assoc')[0];
        $smtp['password'] = parent::decrypt($smtp['password']);
        $smtp['username'] = parent::decrypt($smtp['username']);
        Email::configTransport("custom", ['className'=>"Smtp", "timeout"=>60, "tls"=>($smtp['tls']==1?true:false), 'host'=>$smtp['host'], 'port'=>$smtp['port'], 'username'=>$smtp['username'], 
        'password'=>$smtp['password'], 'context'=>['ssl'=>['allow_self_signed'=>true, 'verify_peer'=>false, 'verify_peer_name'=>false]]]);
        $answers = $connection->execute("SELECT u.first_name, u.last_name, u.id as email FROM cfp.answers as a, cfp.users as u WHERE a.user_id = u.id AND a.form_id = " . $form['id'] . " and a.event_id = " . $event['id'])
            ->fetchAll("assoc");

        foreach($answers as $a){
            $c = $connection->execute("SELECT code FROM codes WHERE user_id like ('" . $a['email'] . "') AND event_id = " . $event['id'])->fetchAll("assoc");
            if(count($c) < 1){
                if($primary) {
                    continue;
                }
                $c = array("code", "");
            } else {
                if(!$primary){
                    continue;
                }
                $c = $c[0];
            }
            $tx = str_replace(parent::getKeywords('confirmedSpeakers'), array($a['first_name'], $a['last_name'], $a['email'], $event['name'], $event['year'], $c['code']), $mail['text']);

            $text = explode("SECONDARYSPEAKER", $tx);

            if(count($text) > 1){
                if($primary){
                    $text = $text[0];
                } else {
                    $text = $text[1];
                }
            } else {
                $text = $text[0];
            }

            $email = new Email();
            $email->dropTransport("Smtp");
            $email->emailFormat($mail['format']);
            $email->setFrom([$smtp['username'] => $smtp['nick']]);
            if($smtp['replyto'] != ''){
                $email->setReplyTo($smtp['replyto']);
            }
            $email->setTo($a['email']);
            $email->setSubject($mail['subject']);
            $email->send($text);
        }

        $_SESSION['successMessage'][] = "Emails are on the way.";
        return $this->redirect("/codes/" . $event['id']);
    }
}
