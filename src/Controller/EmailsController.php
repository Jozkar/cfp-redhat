<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Datasource\ConnectionManager;
use Cake\Mailer\Email;

/**
 * Static content controller
 *
 * This controller will render views from Template/HomePage/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class EmailsController extends AppController
{

    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Network\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function show(...$path)
    {
    if(!parent::getAdmin() && !parent::getProgramManager() && !parent::getSuperUser()){
        return $this->redirect("/");
    }
    
    parent::printFlush($this->request->here());

    $this->set("active", "manager");
    $this->set("admin", parent::getAdmin() | parent::getSuperUser());
    $this->set("reviewer", parent::getReviewer());
    $this->set("program_manager", parent::getProgramManager());       
        
    if($this->request->session()->read("first-name")){
           $this->set('username', $_SESSION['first-name'] . " " . $_SESSION['last-name']);
    }

    $connection = ConnectionManager::get('cfp');

    if(!count($path) >= 1 || !is_numeric($path[0])){
        $_SESSION['errorMessage'][] = "Unsufficient data. Your link is probably broken.";
        return $this->redirect("/manager");
    }

    if(!parent::getAdminForEvent($path[0]) && !parent::getProgramManagerForEvent($path[0])){
        $_SESSION['errorMessage'][] = "You are not allowed to do this opperation.";
        return $this->redirect("/admin");
    }

    if(isset($_POST['save'])){
        if(isset($_POST["_Token"]) && $_POST["_Token"] == $_SESSION['token']){
            return $this->update($connection);
        } else {
            $_SESSION['errorMessage'][] = "You are not allowed to do this opperation. Missing token.";
            if(isset($path[0]) && is_numeric($path[0])){
                return $this->redirect("/emails/".$path[0]);
            }else{
                return $this->redirect("/manager");
            }
        }
    }

    $event = $connection->execute('SELECT id, name, year FROM cfp.events WHERE id = ' . $path[0])->fetchAll('assoc')[0];
    $event['hasSMTP'] = parent::hasSMTP($event['id']);
    $token = $this->request->getParam('_csrfToken');

    $_SESSION['token'] = $token;
    $this->set("token", $token);


    if(count($path) == 1 && is_numeric($path[0])){
        $form_types = $connection->execute('SELECT distinct form_type FROM cfp.emails WHERE event_id = ' . $path[0] . ' and form_type not like ("event") order by form_type')->fetchAll('assoc');
        array_unshift($form_types, array("form_type"=>"event"));
        $results = array();
        foreach($form_types as $t){
            $results[$t['form_type']] = $connection->execute('SELECT * FROM cfp.emails WHERE event_id = ' . $path[0] . ' and form_type like ("' . $t['form_type'] . '") order by type')->fetchAll('assoc');
        }
        $types = array("all"=>array("logo"=>"pficon-info list-view-pf-icon-info", "title"=>"Email with general information"),
                   "to-accept"=>array("logo"=>"pficon-info list-view-pf-icon-success", "title"=>"Email with general information to accepted speakers"),
                   "accept"=>array("logo"=>"pficon-ok list-view-pf-icon-success", "title"=>"Accept email"),
                   "to-reject"=>array("logo"=>"pficon-info list-view-pf-icon-danger", "title"=>"Email with general information to rejected speakers"),
                   "reject"=>array("logo"=>"pficon-error-circle-o list-view-pf-icon-danger", "title"=>"Reject email"),
                   "confirmation"=>array("logo"=>"pficon-orders list-view-pf-icon-warning", "title"=>"Confirmation email when CfP submitted"),
                   "confirmedSpeakers"=>array("logo"=>"pficon-messages list-view-pf-icon-success", "title"=>"Email when speaker confirms accepted submission"),
                   "unconfirmedSpeakers"=>array("logo"=>"pficon-help list-view-pf-icon-info", "title"=>"Email for accepted, but yet unconfirmed speakers"),
                   "waitList"=>array("logo"=>"pficon-pending list-view-pf-icon-warning", "title"=>"Email to those, who are at wait list"),
                   "hotel-paid"=>array("logo"=>"pficon-on-running list-view-pf-icon-warning", "title"=>"Email to hotels for speakers, where we cover accommodation"),
                   "hotel-unpaid"=>array("logo"=>"pficon-paused list-view-pf-icon-info", "title"=>"Email to hotels for speakers, where we don't cover accommodation"),
                   "to-hotel-paid"=>array("logo"=>"pficon-info list-view-pf-icon-success", "title"=>"Email with general information to speakers with covered accommodation"),
                   "to-hotel-unpaid"=>array("logo"=>"pficon-info list-view-pf-icon-success", "title"=>"Email with general information to speakers with accommodation covered by themselves."));
 
        $this->set("event", $event);
        $this->set("forms", $form_types);
        $this->set("emails", $results);
        $this->set("types", $types);

        try {
            $this->render("home");
        } catch (MissingTemplateException $exception) {
            if (Configure::read('debug')) {
                throw $exception;
            }
            throw new NotFoundException();
        }
    }else{
        if(count($path) > 1 && is_numeric($path[0]) && isset($path[1]) && isset($path[2]) && isset($path[3])){
            if($path[1] == 'edit' && in_array($path[2], array('all','accept','reject','confirmation', 'confirmedSpeakers', 'waitList', 'unconfirmedSpeakers', 'to-accept', 'to-reject', 'hotel-paid', 'hotel-unpaid', 'to-hotel-paid', 'to-hotel-unpaid')) 
                    && in_array($path[3], array('booth','meetup','workshop','confirmation','event','session', 'quiz', 'contest', 'lightning_talk', "activity", "covered_accommodation", "accommodation"))){
                $email = $connection->execute('SELECT * FROM cfp.emails WHERE event_id = ' . $path[0] . ' and type = "' . $path[2] .'" and form_type = "' . str_replace("_", " ", $path[3]) . '"')->fetchAll('assoc');

                $substitues = array("all"=>array("FNAME"=>"User's first name", "LNAME"=>"User's last name", "EMAIL"=>"User's email", 
                                 "URLFNAME"=>"URL encoded user's first name", "URLLNAME"=>"URL encoded user's last name", "URLMAIL"=>"URL encoded user's email",
                                 "EVENT"=>"Event name", "YEAR"=>"Event's full year"),
                            "accept"=>array("FNAME"=>"User's first name", "LNAME"=>"User's last name", "EMAIL" => "User's email", 
                                    "URLFNAME"=>"URL encoded user's first name", "URLLNAME"=>"URL encoded user's last name", "URLMAIL"=>"URL encoded user's email", 
                                    "EVENT"=>"Event name", "YEAR"=>"Event's full year", "SUBMTITLE"=>"Title of accepted proposal - for non-group email only",
                                    "TRACK"=>"Proposal's track - for non-group email only", 
                                    "PROPOSALS"=>"List of accepted proposals (in format: 'title' ('time' minutes) for 'track' track (where you're 'speaker type' speaker) - for group email only"),
                            "to-accept"=>array("FNAME"=>"User's first name", "LNAME"=>"User's last name", "EMAIL" => "User's email", 
                                    "URLFNAME"=>"URL encoded user's first name", "URLLNAME"=>"URL encoded user's last name", "URLMAIL"=>"URL encoded user's email", 
                                    "EVENT"=>"Event name", "YEAR"=>"Event's full year"),
                            "reject"=>array("FNAME"=>"User's first name", "LNAME"=>"User's last name", "EMAIL" => "User's email", 
                                    "URLFNAME"=>"URL encoded user's first name", "URLLNAME"=>"URL encoded user's last name", "URLMAIL"=>"URL encoded user's email",
                                    "EVENT"=>"Event name", "YEAR"=>"Event's full year", "SUBMTITLE"=>"Title of rejected proposal - for non-group email only",
                                    "TRACK"=>"Proposal's track - for non-group email only", 
                                    "PROPOSALS"=>"List of rejected proposals (in format: 'title' ('time' minutes) for 'track' track (where you're 'speaker type' speaker) - for group email only"),
                            "to-reject"=>array("FNAME"=>"User's first name", "LNAME"=>"User's last name", "EMAIL" => "User's email", 
                                    "URLFNAME"=>"URL encoded user's first name", "URLLNAME"=>"URL encoded user's last name", "URLMAIL"=>"URL encoded user's email",
                                    "EVENT"=>"Event name", "YEAR"=>"Event's full year"),
                            "confirmation"=>array("FNAME"=>"User's first name", "LNAME"=>"User's last name", "EMAIL" => "User's email","EVENT"=>"Event name", 
                                      "YEAR"=>"Event's full year", "FORMTYPE"=>"One of session/booth/workshop/meetup/contest/lightning talk/activity based on filled CfP form",
                                      "ANSWERS"=>"Content of submitted form (This has to be used)", "EDITLINK"=>"Direct link to edit current proposal."),
                            "confirmedSpeakers"=>array("FNAME"=>"User's first name", "LNAME"=>"User's last name", "EMAIL" => "User's email","EVENT"=>"Event name",
                                           "YEAR"=>"Event's full year", "CODE"=>"Unique user code (can be used as promo code to Hotels)",
                                           "SECONDARYSPEAKER"=>"Delimiter to distinguish between messsage for primary and secondary speakers. " .
                                                   "Everything above is for primary speakers. Everything below is for secondary speakers."),
                            "waitList"=>array("FNAME"=>"User's first name", "LNAME"=>"User's last name", "EMAIL" => "User's email",
                                    "URLFNAME"=>"URL encoded user's first name", "URLLNAME"=>"URL encoded user's last name", "URLMAIL"=>"URL encoded user's email",
                                    "EVENT"=>"Event name", "YEAR"=>"Event's full year", "SUBMTITLE"=>"Title of rejected proposal - for non-group email only",
                                    "TRACK"=>"Proposal's track - for non-group email only",
                                    "PROPOSALS"=>"List of rejected proposals (in format: 'title' ('time' minutes) for 'track' track (where you're 'speaker type' speaker) - for group email only"),                                      
                            "unconfirmedSpeakers"=>array("FNAME"=>"User's first name", "LNAME"=>"User's last name", "EMAIL" => "User's email","EVENT"=>"Event name",
                                         "YEAR"=>"Event's full year", "PROPOSALS"=>"List of unreviewed proposals (in format: 'title' (where you're 'speaker type' speaker)"),
                            "to-hotel-paid"=>array("FNAME"=>"User's first name", "LNAME"=>"User's last name", "EMAIL" => "User's email","EVENT"=>"Event name",
                                         "YEAR"=>"Event's full year", "ANSWERS"=>"Content of submitted form (This has to be used)", "HOTEL"=>"Hotel name", "HOTELEMAIL"=>"Email address to hotel"),
                            "to-hotel-unpaid"=>array("FNAME"=>"User's first name", "LNAME"=>"User's last name", "EMAIL" => "User's email","EVENT"=>"Event name",
                                         "YEAR"=>"Event's full year", "ANSWERS"=>"Content of submitted form (This has to be used)", "HOTEL"=>"Hotel name", "HOTELEMAIL"=>"Email address to hotel"),
                            "hotel-paid"=>array("FNAME"=>"User's first name", "LNAME"=>"User's last name", "EMAIL" => "User's email","EVENT"=>"Event name",
                                         "YEAR"=>"Event's full year"),
                            "hotel-unpaid"=>array("FNAME"=>"User's first name", "LNAME"=>"User's last name", "EMAIL" => "User's email","EVENT"=>"Event name",
                                         "YEAR"=>"Event's full year"));

                $token = $this->request->getParam('_csrfToken');
                $_SESSION['token'] = $token;
                $this->set("token", $token);
                $this->set("email", $email[0]);
                $this->set("event", $event);        
                $this->set("substitues", $substitues);

                try {
                    $this->render("form");
                } catch (MissingTemplateException $exception) {
                    if (Configure::read('debug')) {
                        throw $exception;
                    }
                    throw new NotFoundException();
                }

            }elseif($path[1] == 'test' && in_array($path[2], array('all','accept','reject','confirmation','confirmedSpeakers','waitList', 'unconfirmedSpeakers', 'to-accept', 'to-reject', 'hotel-paid', 'hotel-unpaid', 'to-hotel-paid', 'to-hotel-unpaid')) 
                    && in_array($path[3], array('booth','meetup','workshop','confirmation','event','session', 'quiz', 'contest', 'lightning_talk', 'activity', 'covered_accommodation', 'accommodation'))){
                if(!parent::hasSMTP($path[0])){
                    $_SESSION['warningMessage'][] = "No SMTP credentials are available for this event, so no email can be sent. Please, contact event administrator.";
                    return $this->redirect("/emails/".$path[0]);
                }

                $mail = $connection->execute('SELECT * FROM cfp.emails WHERE event_id = ' . $path[0] . ' and type = "' . $path[2] .'" and form_type = "' . str_replace("_", " ", $path[3]) .'"')->fetchAll('assoc')[0];
                $keywords = array("URLFNAME", "URLLNAME", "URLMAIL", "FNAME", "LNAME", "EMAIL", "EVENT", "YEAR", "PROPOSALS", "SUBMTITLE", "TRACK","ANSWERS", "FORMTYPE", "EDITLINK", "CODE", "HOTEL", "HOTELEMAIL");
                $substitutions = array(urlencode($_SESSION['first-name']), urlencode($_SESSION['last-name']), urlencode(parent::getCurrentEmail()),
                            $_SESSION['first-name'],$_SESSION['last-name'],parent::getCurrentEmail(), $event['name'], $event['year'], 
                            "My testing proposal (30 minutes) for Testing track - (where you are primary speaker)".($mail['format']=="html"?"<br>":"\n").
                            "My second proposal (90 minutes) for Testing track - (where you are aditional speaker)", "My testing proposal", "Testing",
                            ($mail['format']=="html"?"<h3>FirstQuestion</h3>My answer<br><h3>Other question</h3>Other answer<br>":"Q: FirstQuestion\nA: My answer\n\nQ: Other question\nA: Other answer\n\n"),
                            "session", ($mail['format']=="html"?"<a href='#'>https://some.long.link.to.my.proposal.net</a>":"https://some.long.link.to.my.proposal"), "SOME1523CODE", "Hotel 4 star", "hotel@hotel.com");
                $smtp = $connection->execute('SELECT * FROM cfp.smtps WHERE event_id = ' . $path[0])->fetchAll('assoc')[0];
                $smtp['password'] = openssl_decrypt($smtp['password'], $cipher=env("CFP_ENC_METHOD","aes-256-cbc-hmac-sha256"), $cipherKey=env("CAKEPHP_SECRET_TOKEN",NULL), $options=0, $iv=env("CAKEPHP_SECURITY_SALT",NULL));
                $smtp['username'] = openssl_decrypt($smtp['username'], $cipher=env("CFP_ENC_METHOD","aes-256-cbc-hmac-sha256"), $cipherKey=env("CAKEPHP_SECRET_TOKEN",NULL), $options=0, $iv=env("CAKEPHP_SECURITY_SALT",NULL));
                Email::configTransport("custom", ['className'=>"Smtp", "timeout"=>60, "tls"=>($smtp['tls']==1), 'host'=>$smtp['host'], 'port'=>$smtp['port'], 'username'=>$smtp['username'], 
                'password'=>$smtp['password'], 'context'=>['ssl'=>['allow_self_signed'=>true, 'verify_peer'=>false, 'verify_peer_name'=>false]]]);
                $mail['text'] = str_replace($keywords, $substitutions, $mail['text']);
                $email = new Email();
                $email->emailFormat($mail['format']);
                $email->setFrom([$smtp['username'] => $smtp['nick']]);
                if($smtp['replyto'] != ''){
                    $email->setReplyTo($smtp['replyto']);
                }
                $email->setTo(parent::getCurrentEmail());
                $email->setSubject($mail['subject']);
                $email->send($mail['text']);
                $_SESSION['successMessage'][] = "Testing email has been successfully sent to ".parent::getCurrentEmail().".";
                return $this->redirect("/emails/".$path[0]);

            }else{
                $_SESSION['errorMessage'][] = "Unsupported data. Your link is probably broken.";
                            return $this->redirect("/emails/".$path[0]);
            }            
        } else {
            $_SESSION['errorMessage'][] = "Unsufficient data. Your link is probably broken.";
            return $this->redirect("/manager");
        }
    }
    }

    public function update($connect){
        foreach($_POST as $key=>$val){
            if(!is_numeric($val)){
                $_POST[$key] = "'" . str_replace(array("'", "\""), array("\'", "\\\""), $val) . "'";
            }
        }
        try{
            $res = $connect->execute("UPDATE cfp.emails SET `format`=" . (isset($_POST['format'])?"'html'":"'text'") . ", `subject`=" . $_POST['subject'] . ", `text`=" . $_POST['text'] . 
                   ", `sumarize`=" . (isset($_POST['sumarize'])?1:0) . " WHERE `event_id` = " . $_POST['event'] . " and type = " . $_POST['type'] . " and form_type = " . $_POST['form_type']);
            $_SESSION['successMessage'][] = "Email templates been successfully modified.";
        }catch(\Exception $e){
            $_SESSION['errorMessage'][] = "Email template can't be updated. Check form fields for unusual characters.";
        }
        if(isset($_POST['event']) && is_numeric($_POST['event'])){
            return $this->redirect("/emails/".$_POST['event']);    
        }else{
            return $this->redirect("/manager");
        }
    }   
}
