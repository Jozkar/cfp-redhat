<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Datasource\ConnectionManager;

/**
 * Static content controller
 *
 * This controller will render views from Template/HomePage/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class CaptainReviewController extends AppController
{

    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Network\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function show(...$action)
    {
    if(!parent::getReviewer()){
    	return $this->redirect("/");
    }
    	
    if(count($action) < 1 or !is_numeric($action[0])){
    	$_SESSION['errorMessage'][] = "Unsufficient data. Your link is probably broken.";
    	return $this->redirect("/reviewer");
    }	

    parent::printFlush($this->request->here());

    $this->set("active", "reviewer");
    $this->set("admin", parent::getAdmin() | parent::getSuperUser());
    $this->set("reviewer", parent::getReviewer());
    $this->set("program_manager", parent::getProgramManager());       

    if($this->request->session()->read("first-name")){
            $this->set('username', $_SESSION['first-name'] . " " . $_SESSION['last-name']);
    }

    if(!parent::checkFormType($action[1])){
    	$_SESSION['errorMessage'][] = "You don't have access to that review process. Permission denied.";
    	return $this->redirect("/reviewer");
    }

    $connection = ConnectionManager::get('cfp');
    $event = $connection->execute('SELECT name, year, GROUP_CONCAT(topic_id) AS topics FROM cfp.events, cfp.topics_to_events WHERE id  = ' . $action[0] . ' AND event_id = id')->fetchAll('assoc');
    $topics = $connection->execute('SELECT topics FROM cfp.reviewers WHERE user_id like ("' . $_SESSION['loggedUser'] . '") and captain=1 and form_type like ("' . $action[1] . '") and event_id = ' . $action[0])->fetchAll('assoc');
    $form = $connection->execute('SELECT type, placeholder FROM forms WHERE type = "' . $action[1] . '" and event_id = ' . $action[0])->fetch("assoc");

    $this->set("form", $form);

    if(count($topics) < 1){
    	$_SESSION['errorMessage'][] = "You don't have access to that page. Permission denied.";
    	return $this->redirect("/reviewer");
    }

    if($action[1] == 'session'){
    	$myTopics = $connection->execute('SELECT id, name, timeAllocation, timeAllocation as timeAllocationResult FROM cfp.topics, cfp.topics_to_events where id in (' . $topics[0]['topics'] . ') and topic_id = id AND event_id = ' 
            . $action[0])->fetchAll('assoc');
    	$allTopics = $connection->execute('SELECT id, name FROM cfp.topics WHERE id IN (' . $event[0]['topics'] . ')')->fetchAll('assoc');
    	$responses = $connection->execute(
    	'SELECT 
    		r.id as response_id,
    		r.title,
    		r.type,
    		r.duration,
    		r.difficulty,
    		r.abstract,
    		r.summary,
    		r.keywords,
    		r.answer_id,
    		r.confirmed,
    		c.id,
    		t.id as topic_id,
    		t.name as topic,
    		CONCAT(u.first_name, " ", u.last_name, " (", u.email, ")") as speaker,
    		u.country,
    		(SELECT 
    			GROUP_CONCAT(tp.name SEPARATOR ", ") 
    		 FROM 
    			cfp.topics as tp,
    			cfp.responses_to_topics as re
    		 WHERE
    			re.topic_id = tp.id 
    			AND re.response_id = r.id
    		) as topics,
    		CONCAT(
    			(SELECT
    				count(*)
    			 FROM
    				votes as vt
    			 WHERE
    				vt.response_to_topic_id = c.id
                    AND vt.captain = 0
    			),
    			"/",
    			(SELECT
    				count(*)
    			 FROM
    				reviewers as rew
    			 WHERE
    				FIND_IN_SET(t.id, rew.topics)
                    AND rew.captain = 0
    				AND rew.event_id = ' . $action[0] . '
                    AND rew.form_type like ("session")
    			)
    		) as reviews
    	FROM
    		cfp.topics as t,
    		cfp.users as u,
    		cfp.responses as r,
    		cfp.responses_to_topics as c 
    	LEFT JOIN
    		votes as v 
    	ON
    		c.id = v.response_to_topic_id
    	WHERE
    		u.id = r.user_id
    		AND c.event_id = ' . $action[0] .'
    		AND t.id in (' . $topics[0]['topics'] . ')
    		AND r.id = c.response_id
    		AND t.id = c.topic_id
    	GROUP BY 
    		c.id 
    	ORDER BY 
    		t.name, c.id')->fetchAll('assoc');
    	$others = $connection->execute('SELECT r.id, r.title, r.type, r.duration, r.difficulty, r.abstract, r.summary, r.answer_id, '.
    					' r.keywords, CONCAT(u.first_name, " ", u.last_name, " (", u.email, ")") as speaker, GROUP_CONCAT(t.name SEPARATOR ", ") '.
    					'as topic, GROUP_CONCAT(t.id SEPARATOR ",") as topic_id FROM cfp.topics as t, cfp.users as u, cfp.responses as r, cfp.responses_to_topics as c'.
    					' WHERE u.id = r.user_id AND c.event_id = ' . $action[0] .' AND r.id = c.response_id AND t.id = c.topic_id group by r.id ORDER BY c.id')->fetchAll('assoc');
    	$score = $connection->execute('SELECT sum(vote) as score, response_to_topic_id as id FROM votes WHERE event_id = ' . $action[0] . ' AND captain = 0 GROUP BY id')->fetchAll('assoc');
    	$votes = $connection->execute('SELECT response_to_topic_id as response_id, vote FROM cfp.votes WHERE captain = 1 and event_id = ' . $action[0])->fetchAll('assoc');
    } else {
    	$answers = $connection->execute('SELECT 
    					a.id, 
    					CONCAT(u.first_name, " ", u.last_name, " (", u.email, ")") as speaker, 
    					a.form_id,
    					a.confirmed,
    					u.country,
                        (SELECT
                            sum(vote)
                         FROM
                            votes as vts
                         WHERE
                            vts.answer_id = a.id
                            AND vts.captain = 0
                        ) as score,
    					(SELECT 
    						GROUP_CONCAT(" ", CONCAT(rev.first_name, " ", rev.last_name, " (", vt.vote, ")")) 
    					 FROM 
    						cfp.users as rev,
    						cfp.votes as vt
    					 WHERE
    						rev.id = vt.reviewer_id
                            AND vt.captain = 0
    						AND a.id = vt.answer_id
    						) as votes,
    					CONCAT(
    						(SELECT
    							count(*)
    						 FROM
    							votes as vt
    						 WHERE
    							vt.answer_id = a.id
                                AND vt.captain = 0
    						),
    						"/",
    						(SELECT
    							count(*)
    						 FROM
    							reviewers as rew
    						 WHERE
    							f.type like rew.form_type
    							AND rew.captain = 0
    							AND rew.event_id = ' . $action[0] . '
    						)
    					) as reviews
    					FROM forms as f, users as u, answers as a LEFT JOIN votes as v ON v.answer_id = a.id WHERE 
    					f.id = a.form_id AND f.type like ("' . $action[1] . '") and a.user_id = u.id and f.event_id = ' . $action[0]. ' 
    					GROUP BY a.id ORDER BY a.id')->fetchAll('assoc');
    	$responses = array();
    	foreach ($answers as $a){
    		$res = $connection->execute('SELECT q.question, ap.text, ap.as_title, q.take_as FROM questions as q, answer_parts as ap WHERE ap.answer_id = ' . $a['id'] .  
    						' AND ap.question_id = q.id and q.form_id = ' . $a['form_id'])->fetchAll('assoc');
    		$newRes = array("answer_id"=>$a['id'], "speaker"=>$a['speaker'], "country"=>$a['country'], "score"=>$a['score'], "votes"=>$a['votes'], "reviews"=>$a['reviews'], "confirmed"=>$a['confirmed']);
    		foreach($res as $r){
    			if($r['as_title'] == 1){
        				$newRes['Title'] = $r['text'];
                    } else if($r['take_as'] == 'topics'){
                            $curTops = $connection->execute('SELECT GROUP_CONCAT(" ", name) as name FROM topics WHERE id IN(' . $r['text'] . ')')->fetch("assoc");
    			$newRes[$r['question']] = $curTops['name'];
    		} else if($r['as_duration'] == 1){
                $newRes['Duration'] = $r['text'];
            } else {
        				$newRes[$r['question']] = $r['text'];
    		}
    		}
    		$responses[] = $newRes;
    	}
    	$votes = $connection->execute('SELECT answer_id, vote FROM cfp.votes WHERE captain = 1 and event_id = ' . $action[0])->fetchAll('assoc');

    }

    $token = $this->request->getParam('_csrfToken');
    $accepted = array();
    $rejected = array();
    $unreviewed = array();

    foreach($responses as $r){
    	$added = false;
    	if ($action[1] == 'session'){
    		$r['comments'] = $connection->execute('SELECT author, comment, time, public FROM cfp.comments WHERE response_id = ' . $r['response_id'] . ' ORDER BY id desc')->fetchAll('assoc');
    		$r['score'] = "";
                    $voteslist = $connection->execute('SELECT GROUP_CONCAT(" ", CONCAT(r.first_name, " ", r.last_name, " (", v.vote, ")")) as votes FROM users as r, votes as v WHERE r.id = v.reviewer_id'.
                                                ' AND v.captain = 0 AND v.response_to_topic_id = ' . $r['id'])->fetch('assoc');
                    $r['votes'] = $voteslist['votes'];

    		foreach($score as $s){
                        if($s['id'] == $r['id']){
                            $r['score'] = $s['score'];
                        }
                    }
    		$additional = $connection->execute('SELECT CONCAT(u.first_name, " ", u.last_name, " (", u.email, ")") as additional from users u, additional_speakers a WHERE u.id = a.user_id AND'
    			.' a.response_id = '. $r['response_id'])->fetchAll('assoc');
                           if (is_array($additional) && count($additional) > 0){
                                   $r['speaker'] .= " (main speaker)";
                                foreach($additional as $k => $a) {
                                           $r['speaker'] .= ", " . $a['additional'];
                                   }
                           }
    		if($r['answer_id'] != ""){
    			$answers = $connection->execute('SELECT q.question, ap.text FROM questions as q, answer_parts as ap, answers as a WHERE a.id = ' . $r['answer_id'] .
    				' AND a.form_id = q.form_id and ap.question_id = q.id AND ap.answer_id = a.id')->fetchAll('assoc');
    			$r['answers'] = $answers;
    		}

    		foreach($votes as $v){
    			if($v['response_id'] == $r['id']){
    				$added = true;			
    				if ($v['vote'] > 0){
    					$r['vote'] = $v['vote'];
    					$accepted[$r['topic_id']][] = $r;
                        if($v['vote'] == 3){
                            foreach($myTopics as $tid => $mt){
                                if($mt['id'] == $r['topic_id'] && is_numeric($r['duration']) && $mt['timeAllocation'] > 0){
                                    $myTopics[$tid]['timeAllocationResult'] = $mt['timeAllocationResult'] - $r['duration'];
                                    break;
                                }
                            }
                        }
    				}else{
    					$r['vote'] = $v['vote'];
    					$rejected[$r['topic_id']][] = $r;
    				}
    			} 
    		}
    		if(!$added){
    			$unreviewed[$r['topic_id']][] = $r;
    		}
    	} else {
    		$r['comments'] = $connection->execute('SELECT author, comment, time, public FROM cfp.comments WHERE answer_id = ' . $r['answer_id'] . ' ORDER BY id desc')->fetchAll('assoc');
    		$additional = $connection->execute('SELECT CONCAT(u.first_name, " ", u.last_name, " (", u.email, ")") as additional from users u, additional_speakers a WHERE u.id = a.user_id AND a.answer_id = '
    				. $r['answer_id'])->fetchAll('assoc');
    		if (is_array($additional) && count($additional) > 0){
    			$r['speaker'] .= " (main speaker)";
    			foreach($additional as $k => $a) {
    				$r['speaker'] .= ", " . $a['additional'];
    			}
    		}
    		foreach($votes as $v){
    			if($v['answer_id'] == $r['answer_id']){
    				$added = true;
    				if ($v['vote'] > 0){
    					$r['vote'] = $v['vote'];
    					$accepted[] = $r;
    				}else{
    					$r['vote'] = $v['vote'];
    					$rejected[] = $r;
    				}
    			}
    		}
    		if(!$added){
    			$unreviewed[] = $r;
    		}
    		$myTopics = null;
    		$allTopics = null;
    	}
    }

    if($action[1] == 'session'){
    	foreach($others as $key=>$o){
    		$others[$key]['comments'] = $connection->execute('SELECT author, comment, time, public FROM cfp.comments WHERE response_id = ' . $o['id'] . ' ORDER BY id desc')->fetchAll('assoc');
    		$additional = $connection->execute('SELECT CONCAT(u.first_name, " ", u.last_name, " (", u.email, ")") as additional from users u, additional_speakers a WHERE u.id = a.user_id AND'
    			.' a.response_id = ' . $others[$key]['id'])->fetchAll('assoc');
                        if (is_array($additional) && count($additional) > 0){
                                   $others[$key]['speaker'] .= " (main speaker)";
                                   foreach($additional as $k => $a) {
                                           $others[$key]['speaker'] .= ", " . $a['additional'];
                                   }
                        }
    		if($o['answer_id'] != ""){
    			$answers = $connection->execute('SELECT q.question, ap.text FROM questions as q, answer_parts as ap, answers as a WHERE a.id = ' . $o['answer_id'] .
    				' AND a.form_id = q.form_id and ap.question_id = q.id AND ap.answer_id = a.id')->fetchAll('assoc');
    			$others[$key]['answers'] = $answers;
    		}
    	}
    } else {
    	$others = null;
    }

    $this->request->session()->write("token", $token);
    $this->set("token", $token);
    $this->set("topics", $myTopics);
    $this->set("allTopics", $allTopics);
    $this->set("eventInfo", $event[0]);		
    $this->set("event", $action[0]);
    $this->set("accepted", $accepted);
    $this->set("rejected", $rejected);
    $this->set("unreviewed", $unreviewed);
    $this->set("captain", 1);
    $this->set("reviewer", $_SESSION['loggedUser']);
    $this->set("rest", $others);
    $this->set("inflector", parent::getInflector());

    try {
    	if($action[1] == 'session'){
     		$this->render("home");
    	} else {
    		$this->render("answers");
    	}
        } catch (MissingTemplateException $exception) {
            if (Configure::read('debug')) {
                throw $exception;
            }
            throw new NotFoundException();
        }
    }
}
