<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Datasource\ConnectionManager;
/**
 * Static content controller
 *
 * This controller will render views from Template/HomePage/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class SigninController extends AppController
{

    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Network\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function show(...$action)
    {

    	if(!parent::getSuperUser()){
    		return $this->redirect("/");
    	}
    	$connection = ConnectionManager::get('cfp');
    	
    	if(isset($_POST['save'])){
    		if(isset($_POST['_Token']) && $_POST['_Token'] == $_SESSION['token'] && isset($_POST['action'])){
    			if($_POST['action'] == "update"){
    				return $this->update($connection);
    			}else{
    				return $this->add($connection);
    			}
    		} else {
    			$_SESSION['errorMessage'][] = "You are not allowed to do this opperation";
    			return $this->redirect("/signins");
    		}
    	}

    	parent::printFlush($this->request->here());
    	$this->set("admin", parent::getAdmin() | parent::getSuperUser());
    	$this->set("reviewer", parent::getReviewer());
    	$this->set("program_manager", parent::getProgramManager());

    	// check, if are passed any arguments
    	if(!empty($action) && count($action) >=2){
    		// supported are arguments /.../action/vent-id
    		if(!in_array($action[1], ["add","edit","remove"])){
    			//not enough arguments error message  + redirect
    			$_SESSION['errorMessage'][] = "Unsufficient data. Your link is probably broken.";
    			return $this->redirect("/signins"); 
    		}

    		switch($action[1]){
    			case "edit":
    				$results = $connection->execute('SELECT * FROM cfp.signins WHERE service like ("' . $action[2] . '") and domain like ("' . $action[0] . '")')->fetchAll('assoc')[0];
    				$results['client_id'] = parent::decrypt($results['client_id']);
    				$results['client_secret'] = parent::decrypt($results['client_secret']);
    				$this->set("credentials", $results);
    				$this->set("action", "update");
    				$this->set("domain", $action[0]);
                    $this->set("service", $action[1]);
    				break;
    			case "add":
    				$this->set("credentials", null);
                    $this->set("domain", $action[0]);
    				$this->set("action", "create");
    				$this->set("service", $action[2]);
    				break;
    			case "remove":
    				$connection->execute("DELETE FROM cfp.signins WHERE service like ('" . $action[2] . "') and domain like ('" . $action[0] . "')");
    				$_SESSION['successMessage'][] = "Domain " . $action[0] . " (" . $action[2] . " service) has been successfully removed.";
    				return $this->redirect("/signins/" . $action[0]);
    				break;
    			default:
    				$_SESSION['errorMessage'][] = "Unsupported operation. Your link is probably broken.";
    				return $this->redirect("/signins/" . $action[0]);
    				break;
    		}

    		$_SESSION['token'] = $this->request->getParam('_csrfToken');
    		$this->set("token", $_SESSION['token']);
    		$this->set("active","admin");
            $this->set('username', $_SESSION['first-name']." ".$_SESSION['last-name']);

    		try{
    			 $this->render("form");
    		}catch(MissingTemplateException $exception) {
    			if (Configure::read('debug')) {
    				throw $exception;
    			}
    			throw new NotFoundException();
    		}
    		
    	}else{
            if(count($action) >=1){
        		$dom = $connection->execute("SELECT domain, service FROM signins WHERE domain like ('" . $action[0] . "') ORDER BY domain, service")->fetchAll('assoc');
                $this->set("domain", $action[0]);
        		$this->set("domains", $dom);
        		$this->set("active","admin");
    	    	$this->set('username', $_SESSION['first-name']." ".$_SESSION['last-name']);
    	
    		    try {
    		        $this->render('home');
        		} catch (MissingTemplateException $exception) {
        		    if (Configure::read('debug')) {
    	            	throw $exception;
    		        }
    		        throw new NotFoundException();
        		}
            } else {
                $_SESSION['errorMessage'][] = "Unknown domain. Your link is probably broken.";
                return $this->redirect("/domains/");
            }
    	}
    }
    
    public function update($connect){
    foreach($_POST as $key=>$val){
    	if(!is_numeric($val) && !is_array($_POST[$key])){
    		if($key == "client_id" || $key == "client_secret"){
    			$_POST[$key] = "'".parent::encrypt($val)."'";
    		}elseif($key != "origin_domain"){
    			$_POST[$key] = "'" . str_replace(array("'", "\""), array("\'", "\\\""), $val) . "'";
    		}
    	}
    }
    try{
    	$res = $connect->execute("UPDATE cfp.signins SET `domain`=" . $_POST['domain']. ", `client_id`=" . $_POST['client_id'] . ", `client_secret`=" . $_POST['client_secret'] . ", `callback`=" . $_POST['callback'] .
                                         ", `name`=" . $_POST['name'] . " WHERE `service` = " . $_POST['service'] . " AND `domain` = " . $_POST['old_domain']);
    	$_SESSION['successMessage'][] = "Credentials has been successfully modified.";
    }catch(\Exception $e){
    	$_SESSION['errorMessage'][] = "Credentials can't be updated.";
    }
    return $this->redirect("/signins/" . $_POST['origin_domain']);    
    }

    public function add($connect){
    foreach($_POST as $key=>$val){
    	if(!is_numeric($val) && !is_array($_POST[$key])){
    		if($key == "client_id" || $key == "client_secret"){
    			$_POST[$key] = "'".parent::encrypt($val)."'";
    		}elseif($key != "origin_domain"){
    			$_POST[$key] = "'" . str_replace(array("'", "\""), array("\'", "\\\""), $val) . "'";
    		}
    	}
    }
    try{
    	$check = $connect->execute("SELECT 1 FROM cfp.signins WHERE service like (" . $_POST['service'] . ") AND domain like (" . $_POST['domain'] . ")")->fetchAll('assoc');
    	if(!$check){
    		$connect->execute("INSERT INTO cfp.signins (domain, client_id, client_secret, callback, name, service) VALUES (" . $_POST['domain'] . ", " . $_POST['client_id'] . ", " . $_POST['client_secret'] .
    				     ", " . $_POST['callback'] . ", " . $_POST['name'] . ", " . $_POST['service'] . ")");
    	}else{
    		throw new Exception();
    	}
    }catch(\Exception $e){
    	$_SESSION['errorMessage'][] = "Provided credentials can't be saved.";
    }
    return $this->redirect("/signins/" . $_POST['origin_domain']);
    }
}
