<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Datasource\ConnectionManager;

/**
 * Static content controller
 *
 * This controller will render views from Template/HomePage/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class AnswersController extends AppController
{

    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Network\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function show(...$action)
    {
    	
    if(count($action) < 1 or !is_numeric($action[0])){
    	$_SESSION['errorMessage'][] = "Unsufficient data. Your link is probably broken.";
    	return $this->redirect("/");
    }

    if(!parent::getAdmin() && !parent::getProgramManager()){
    	$_SESSION['errorMessage'][] = "You're not allowed to visit this page.";
    	return $this->redirect("/");
    }

    parent::printFlush($this->request->here());

    $this->set("active", "home");
    $this->set("admin", parent::getAdmin() | parent::getSuperUser());
    $this->set("reviewer", parent::getReviewer());
    $this->set("program_manager", parent::getProgramManager());
    	
    if($this->request->session()->read("first-name")){
            $this->set('username', $_SESSION['first-name'] . " " . $_SESSION['last-name']);
    }

    $connection = ConnectionManager::get('cfp');
    
    $form = $connection->execute('SELECT * FROM forms as f WHERE id = ' . $action[0])->fetchAll('assoc');
    if(count($form) < 1){
    	$_SESSION['errorMessage'][] = "Event doesn't exist.";
    	return $this->redirect("/");
    }
    $event = $connection->execute('SELECT id, name, year FROM events WHERE id = ' . $form[0]['event_id'])->fetch('assoc');
    
    $this->set("event", $event);
    $this->set("form", $form[0]);
    $this->set("current_program_manager", parent::getProgramManagerForEvent($form[0]['event_id']));
    
    if(count($action) > 4 && $action[1] == "delete" && is_numeric($action[2]) && is_numeric($action[3]) && $action[4] == $_SESSION['token']){
        if(!parent::getSuperUser() && !parent::getProgramManagerForEvent($action[2])){
            $_SESSION['errorMessage'][] = "You don't have access to do this operation. Permission denied.";
            return $this->redirect("/answers/" . $action[0]);
        }
        try{
            $connection->execute("DELETE FROM answers WHERE id=" . $action[3] . " AND event_id=" . $action[2]);
            $connection->execute("DELETE FROM answer_parts WHERE answer_id=" . $action[3]);
            $connection->execute("DELETE FROM comments WHERE answer_id=" . $action[3]);
            $connection->execute("DELETE FROM accepts WHERE answer_id=" . $action[3]);
            $connection->execute("DELETE FROM votes WHERE answer_id=" . $action[3]);
            $collectanswer = $connection->execute("SELECT id FROM answers WHERE collect_answer_id=" . $action[3])->fetch("assoc");
            if(is_numeric($collectanswer['id'])){
                $connection->execute("DELETE FROM answers WHERE id=" . $collectanswer['id']);
                $connection->execute("DELETE FROM answer_parts WHERE answer_id=" . $collectanswer['id']);                
            }
        }catch(\Exception $e){
            $_SESSION['errorMessage'][] = $e->getMessage();
            return $this->redirect("/answers/" . $action[0]);
        }

        $_SESSION['successMessage'][] = "Answer no." . $action[3] . " has been removed.";
        return $this->redirect("/answers/" . $action[0]);           
    }

    $all = $connection->execute('SELECT a.id as answer_id, CONCAT(u.first_name, " ", u.last_name) as speaker, a.form_id, f.type, a.collect_response_id, a.collect_answer_id FROM answers as a, forms as f, users as u WHERE' . 
    	' f.id = ' . $action[0] . ' and f.id = a.form_id and a.user_id = u.id and f.type not like ("session")')->fetchAll('assoc');

    foreach($all as $k=>$a){
    	$res = $connection->execute('SELECT q.question, ap.text, ap.as_title, q.take_as FROM cfp.questions as q, answer_parts as ap WHERE ap.answer_id = ' . $a['answer_id'] . 
    		' AND ap.question_id = q.id AND q.form_id = ' . $action[0] . ' ORDER BY q.question_order')->fetchAll('assoc');
    	foreach($res as $r){
            if($r['as_title'] == 1){
                $all[$k]["Title"] = $r['text'];
            } else {
            	if($r['take_as'] == 'topics'){
                                $curTops = $connection->execute('SELECT GROUP_CONCAT(" ", name) as name FROM topics WHERE id IN(' . $r['text'] . ')')->fetch("assoc");
            		$all[$k][$r['question']] = $curTops['name'];
            	} else {
                		$all[$k][$r['question']] = $r['text'];
            	}
            	if($r['take_as'] == 'accommodation'){
        		    if($a['type'] == "accommodation"){
                        $curRooms = $connection->execute('SELECT CONCAT(h.name, " - ", r.name, " (full price - ", r.full_price, ")") as name FROM hotels as h, hotel_rooms as r WHERE r.id IN (' . $r['text'] . ') and r.hotel_id = h.id')->fetch("assoc");
                    } else {
                        $curRooms = $connection->execute('SELECT CONCAT(h.name, " - ", r.name, " (covered price - ", r.covered_price, ")") as name FROM hotels as h, hotel_rooms as r WHERE r.id IN (' . $r['text'] . ') and r.hotel_id = h.id')->fetch("assoc");
                    }
        			$all[$k][$r['question']] = $curRooms['name'];
        		}
            }
    	}

        if($a['collect_answer_id'] > 0){
            $proposal = $connection->execute("SELECT a.id, ap.text as title FROM answer_parts as ap, answers as a WHERE a.id = " . $a['collect_answer_id'] . " and a.id = ap.answer_id AND ap.as_title = 1")->fetch('assoc');

            if(!isset($proposal['title']) || trim($proposal['title']) == ""){
                $answer = $connection->execute("SELECT a.id, f.type FROM answers as a, forms as f WHERE f.id = a.form_id AND a.id = " . $a['collect_answer_id'])->fetch('assoc');
                $answer['title'] = ucfirst($answer['type']). " proposal no. " . $answer['id'];
                $proposal = $answer;
                $all[$k]["Title"] = $proposal['title'] . " (" . $proposal['id'] . ")";
            }
        } else {
            unset($all[$k]['collect_answer_id']);
        }

            if($a['collect_response_id'] > 0){
            $proposal = $connection->execute("SELECT distinct r.id, r.title FROM responses as r WHERE r.id = " . $a['collect_response_id'])->fetch('assoc');
            $all[$k]["Proposal title"] = $proposal['title'] . " (" . $proposal['id'] . ")";
        } else {
            unset($all[$k]['collect_response_id']);
        }
    }
    
    $token = $this->request->getParam('_csrfToken');
    $this->request->session()->write("token", $token);
	$this->set("token", $token);
    $this->set("inflector", parent::getInflector());
    $this->set("all", $all);
    
    try {
    	$this->render('home');
    } catch (MissingTemplateException $exception) {
    	if (Configure::read('debug')) {
    		throw $exception;
    	}
    	throw new NotFoundException();
    }
     
    }
}
