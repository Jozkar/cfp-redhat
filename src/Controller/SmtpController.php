<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Datasource\ConnectionManager;
/**
 * Static content controller
 *
 * This controller will render views from Template/HomePage/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class SmtpController extends AppController
{

    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Network\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function show(...$action)
    {

        if(!parent::getAdmin() && !parent::getSuperUser()){
            return $this->redirect("/");
        }
        $connection = ConnectionManager::get('cfp');
       
        if(!isset($action[0]) || !is_numeric($action[0])){
            $_SESSION['errorMessage'][] = "Unsufficient data. Your link is probably broken.";
            return $this->redirect("/admin");
        }

        if(!parent::getAdminForEvent($action[0])){
            $_SESSION['errorMessage'][] = "You are not allowed to do this opperation.";
            return $this->redirect("/admin");
        }
 
        if(isset($_POST['save'])){
            if(isset($_POST['_Token']) && $_POST['_Token'] == $_SESSION['token']){
                return $this->update($connection,$action[0]);
            } else {
                $_SESSION['errorMessage'][] = "You are not allowed to do this opperation";
                return $this->redirect("/admin");
            }
        }

        parent::printFlush($this->request->here());
        $this->set("admin", parent::getAdmin() | parent::getSuperUser());
        $this->set("reviewer", parent::getReviewer());
        $this->set("program_manager", parent::getProgramManager());

        $results = $connection->execute('SELECT * FROM cfp.smtps WHERE event_id = ' . $action[0])->fetchAll('assoc')[0];
        if(isset($results)){
            $results['password'] = parent::decrypt($results['password']);
            $results['username'] = parent::decrypt($results['username']);
            $this->set("smtp", $results);
        }else{
            $this->set("smtp", "");
        }

        $this->set("event", $action[0]);
        $this->set("active", "admin");
        $token = $this->request->getParam('_csrfToken');

        $_SESSION['token'] = $token;
        $this->set("token", $token);
        $this->set('username', $_SESSION['first-name']." ".$_SESSION['last-name']);
    
        try {
            $this->render('mail');
        } catch (MissingTemplateException $exception) {
            if (Configure::read('debug')) {
                throw $exception;
            }
            throw new NotFoundException();
        }
    }
    
    public function update($connect, $id){
        foreach($_POST as $key=>$val){
            if(!is_numeric($val) && !is_array($_POST[$key])){
                if($key == "password" || $key == "username"){
                    $_POST[$key] = "'".parent::encrypt($val)."'";
                }else{
                    $_POST[$key] = "'" . str_replace(array("'", "\""), array("\'", "\\\""), $val) . "'";
                }
            }
        }
        try{
            $res = $connect->execute("UPDATE cfp.smtps SET `port`=" . $_POST['port']. ", `host`=" . $_POST['host'] . ", `username`=" . $_POST['username'] . ", `nick`=" . $_POST['nick'] .
                    ", `password`=" . $_POST['password'] . ", `tls`=" . (isset($_POST['tls'])?1:0) . ", `replyto` = " . $_POST['replyto'] . " WHERE `event_id` = " . $id);
            $_SESSION['successMessage'][] = "Event has been successfully modified.";
        }catch(\Exception $e){
            $_SESSION['errorMessage'][] = "Event can't be updated.";
        }
        return $this->redirect("/admin");    
    }
}
