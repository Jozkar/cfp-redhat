<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Datasource\ConnectionManager;
/**
 * Static content controller
 *
 * This controller will render views from Template/HomePage/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class RoomController extends AppController
{

    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Network\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function show(...$action)
    {
        if(count($action) < 1 or !is_numeric($action[0])){
			$_SESSION['errorMessage'][] = "Unsufficient data. Your link is probably broken.";
			return $this->redirect("/");
		}
		
		$connection = ConnectionManager::get('cfp');
        
        $event = $connection->execute("SELECT name, id, year, schedule, open, close, timezone FROM events WHERE id = " . $action[0])->fetch("assoc");
        		
		if(!parent::getProgramManagerForEvent($event['id']) && !parent::getSuperUser()){
            return $this->redirect("/rooms/" . $action[0]);
        }
        $connection = ConnectionManager::get('cfp');
        
        if(isset($_POST['save'])){
            if(isset($_POST['_Token']) && $_POST['_Token'] == $_SESSION['token']){
                if(isset($action[1]) && $action[1] == "add"){
                   return $this->add($connection, $event['id']);
                } elseif(isset($action[2]) && is_numeric($action[2])) {
                   return $this->update($connection, $action[0], $action[2]);
                } else {
                    $_SESSION['errorMessage'][] = "You are not allowed to do this opperation";
                    return $this->redirect("/rooms/" . $action[0]);
                }
            } else {
                $_SESSION['errorMessage'][] = "You are not allowed to do this opperation";
                return $this->redirect("/rooms/" . $action[0]);
            }
        }

        parent::printFlush($this->request->here());
        $this->set("admin", parent::getAdmin() | parent::getSuperUser());
        $this->set("reviewer", parent::getReviewer());
        $this->set("program_manager", parent::getProgramManager());

        $formAction = "add";            
		$results = array();
		
        // check, if are passed any arguments
        if(count($action) > 1){
            // supported are arguments /.../event-id/action/room-id/
            if((count($action) <= 2 && $action[1] != "add") || (count($action) >= 3 && !is_numeric($action[0])  && !is_numeric($action[2]))){
                //not enough arguments error message  + redirect
                $_SESSION['errorMessage'][] = "Unsufficient data. Your link is probably broken.";
                return $this->redirect("/manager"); 
            }
            
            //for edit action
            if($action[1] == "edit"){
                $results = $connection->execute('SELECT * FROM cfp.rooms WHERE id = ' . $action[2])->fetch('assoc');
                $blocks = $connection->execute('SELECT * FROM cfp.room_availability WHERE room_id = ' . $action[2])->fetchAll('assoc');
                $formAction = "update";

            //for delete action
            } else if ($action[1] == "delete") {
                if(count($action) > 3){
                    if($_SESSION['token'] == $action[3]){
                        return $this->delete($connection,$action[0],$action[2]);
                    } else {
                        $_SESSION['errorMessage'][] = "This action can't be performet - invalid token.";
                        return $this->redirect("/rooms/" . $action[0]);
                    }
                } else {
                    $_SESSION['errorMessage'][] = "This action can't be performet - unsufficient data. Your link is probably broken.";
                    return $this->redirect("/rooms/" . $action[0]);
                }
            }
        }
        
        $this->set("event", $event);
        $this->set("room", $results);
        $this->set("availability", $blocks);
        $this->set("action", $formAction);
        $this->set("active", "manager");
        $token = $this->request->getParam('_csrfToken');

        $_SESSION['token'] = $token;
        $this->set("token", $token);
        $this->set('username', $_SESSION['first-name']." ".$_SESSION['last-name']);

        try {
            $this->render('room');
        } catch (MissingTemplateException $exception) {
            if (Configure::read('debug')) {
                throw $exception;
            }
            throw new NotFoundException();
        }
    }

    public function add($connect, $eventID){
        foreach($_POST as $key=>$val){
            if(!is_numeric($_POST[$key]) && !is_array($_POST[$key])){
                $_POST[$key] = "'" . $this->sanity($val) . "'";
            }
        }
        try{
            $res = $connect->execute("INSERT INTO cfp.rooms (event_id, name, description, capacity, speaker_info) VALUES (" . $eventID . ", "
                . $_POST['name']. ", ". $_POST['description'] . ", " . $_POST['capacity'] . ", " . $_POST['info'] . ")");
            $roomID = $connect->execute("SELECT LAST_INSERT_ID() as id")->fetch('assoc')['id'];          
            $_SESSION['successMessage'][] = "Room has been successfully added.";

            $this->setRoomBlocks($connect, $_POST['roomblock'], $roomID);
        }catch(\Exception $e){
            $_SESSION['errorMessage'][] = "Room can't be added. " . $e->getMessage();
        }    
        return $this->redirect("/rooms/".$eventID);
    }

    public function update($connect, $eventID, $roomID){
        foreach($_POST as $key=>$val){
            if(!is_numeric($_POST[$key]) && !is_array($_POST[$key])){
                $_POST[$key] = "'" . $this->sanity($val) . "'";
            }
        }
        try{
            $connect->execute("UPDATE cfp.rooms SET `name`=" . $_POST['name']. ", `capacity`=" . $_POST['capacity'] . ", `description`=" . $_POST['description'] . 
                ", `speaker_info`=" . $_POST['info'] . " WHERE id = " . $roomID);
            $_SESSION['successMessage'][] = "Room has been successfully modified.";

            $this->setRoomBlocks($connect, $_POST['roomblock'], $roomID);
        }catch(\Exception $e){
            $_SESSION['errorMessage'][] = "Room can't be updated. " . $e->getMessage();
        }
        return $this->redirect("/rooms/" . $eventID);    
    }

    public function delete($connect, $eventID, $roomID){
        try{
            $connect->execute("DELETE FROM cfp.rooms WHERE id = " . $roomID);
            $connect->execute("DELETE FROM cfp.room_availability WHERE room_id = " . $roomID);
            $_SESSION['successMessage'][] = "Room has been successfully removed.";
        }catch(\Exception $e){
            $_SESSION['errorMessage'][] = "Room can't be removed. " . $e->getMessage();
        }
        return $this->redirect("/rooms/" . $eventID);
    }

    public function setRoomBlocks($connect, $blocks, $roomID){
        $connect->execute("DELETE FROM room_availability WHERE room_id = " . $roomID);

        foreach($blocks as $b){ 
            $start = date_create($b['start']);
            $end = date_create($b['end']);
            if($b['allday'] == "true"){
                $end->add(date_interval_create_from_date_string('-1 day'));
                if($start != $end){
                    $end->add(date_interval_create_from_date_string('1 day'));
                    while($start != $end){
                        $connect->execute("INSERT INTO cfp.room_availability (room_id, date, all_day) VALUES (" . $roomID . ", '" . $start->format("Y-m-d") . "', 1)");
                        $start->add(date_interval_create_from_date_string('1 day'));
                    }
                } else {
                    $connect->execute("INSERT INTO cfp.room_availability (room_id, date, all_day) VALUES (" . $roomID . ", '" . $start->format("Y-m-d") . "', 1)");
                }
            } else {
                $connect->execute("INSERT INTO cfp.room_availability (room_id, date, start, end, all_day) VALUES (" . $roomID . ", '" . $start->format("Y-m-d")
                    . "', '" . $start->format("H:i:s") . "', '" . $end->format("H:i:s") . "', 0)");
            }
        }
        $_SESSION['successMessage'][] = "Availability blocks has been saved.";
    }

    public function sanity($string){
        return str_replace(array("'", "\""), array("\'", "\\\""), $string);
    }
}
