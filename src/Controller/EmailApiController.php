<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\Mailer\Email;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class EmailApiController extends ApiController
{

    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Network\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function add(...$path)
    {

        $token = $this->request->getData("token");
        $event_id = $this->request->getData("event");
        $action = $this->request->getData("action");
        $form = $this->request->getData("form");

    	if($token == null){
    		die(json_encode(array("result"=>"error", "reason"=>"No token received")));
    	}

    	if($token != $this->request->session()->read('token')){
    		die(json_encode(array("result"=>"error", "reason"=>"Token expired. Please, refresh your page.")));
    	}

    	if($event_id == '' || $action == '' || $form == '' || !is_numeric($event_id) || !parent::checkFormType($form)){
    		die(json_encode(array("result"=>"error", "reason"=>"Unsufficient data. Email request wasn't accepted.")));
    	}

        $connection = ConnectionManager::get('cfp');

        $secCheck = $connection->execute("SELECT 1 FROM managers WHERE event_id = " . $event_id . " AND user_id like ('" . $_SESSION['loggedUser'] . "')")->rowCount();
        if(!$secCheck && !parent::getAdmin()){
            die(json_encode(array("result"=>"error", "reason"=>"You are not allowed to do this opperation."."SELECT 1 FROM managers WHERE event_id = " . $event_id . " AND user_id like ('" . $_SESSION['loggedUser'] . "')")));
        }
      

        switch($action){
            case "accept":
                if(parent::hasSMTP($event_id)){
                    $this->sendMails($connection, $event_id, 1, $form);
                } else {
                    die(json_encode(array("result"=>"error", "reason"=>"You can't send any email, until event administrator set up SMTP credentials for this event.")));
                }
                break;
            case "reject":
                if(parent::hasSMTP($event_id)){
                    $this->sendMails($connection, $event_id, -1, $form);
                } else {
                    die(json_encode(array("result"=>"error", "reason"=>"You can't send any email, until event administrator set up SMTP credentials for this event.")));
                }
                break;
            case "waitList":
                if(parent::hasSMTP($event_id)){
                    $this->sendMails($connection, $event_id, 0, $form);
                } else {
                    die(json_encode(array("result"=>"error", "reason"=>"You can't send any email, until event administrator set up SMTP credentials for this event.")));
                }
                break;
            case "unconfirmedSpeakers":
                if(parent::hasSMTP($event_id)){
                    $this->sendUnconfirmed($connection, $event_id, $form);
                } else {
                    die(json_encode(array("result"=>"error", "reason"=>"You can't send any email, until event administrator set up SMTP credentials for this event.")));
                }
                break;
            case "to-accept":
            case "to-reject":
            case "to-hotel-paid":
            case "to-hotel-unpaid":
            case "all":
                if(parent::hasSMTP($event_id)){
                    $this->sendInfo($connection, $event_id, $form, $action);
                } else {
                    die(json_encode(array("result"=>"error", "reason"=>"You can't send any email, until event administrator set up SMTP credentials for this event.")));
                }
                break;
            default:
                 die(json_encode(array("result"=>"error", "reason"=>"You are not allowed to do this opperation2.")));
        }
        die(json_encode(array("result"=>"success","message"=>"All emails sent.")));
    }

    public function sendMails($connection, $event, $result, $form){
        $evInfo = $connection->execute("SELECT name, year FROM events WHERE id = " . $event)->fetchAll('assoc')[0];
        if($form == "session"){
            $speakers = $connection->execute("SELECT email, id, first_name, last_name FROM users WHERE id in (SELECT user_id FROM responses WHERE informed=0 AND id IN (SELECT response_id FROM responses_to_topics, accepts as a where id = a.response_to_topic_id AND a.vote = " . $result . " AND a.event_id = " . $event . " )) OR id IN (SELECT user_id FROM additional_speakers WHERE informed=0 AND response_id IN (SELECT response_id FROM responses_to_topics, accepts as a where id = a.response_to_topic_id AND a.vote = " . $result . " AND a.event_id = " . $event . " ))")->fetchAll("assoc");
        } else {
            $fid = $connection->execute("SELECT id FROM forms WHERE event_id = " . $event . " AND type like ('" . $form . "')")->fetch("assoc")['id'];
            $speakers = $connection->execute("SELECT email, id, first_name, last_name FROM users WHERE id in (SELECT user_id FROM answers WHERE informed=0 AND id IN " .
                    "(SELECT answer_id FROM accepts as a WHERE a.vote = " . $result . " AND a.event_id = " . $event . " AND a.answer_id in (SELECT id FROM answers WHERE form_id = " . $fid . "))) " .
                    "OR id IN (SELECT user_id FROM additional_speakers WHERE informed=0 AND answer_id IN (SELECT answer_id FROM accepts as a WHERE a.vote = " . $result . " AND a.event_id = " . $event . 
                    " AND a.answer_id in (SELECT id FROM answers WHERE form_id = " . $fid . ")))")->fetchAll("assoc");
        }
        $mail = $connection->execute('SELECT * FROM cfp.emails WHERE event_id = ' . $event . ' and type = "' . ($result > 0?"accept":($result < 0?"reject":"waitList")) . '" and form_type = "' . $form .'"')->fetchAll('assoc')[0];
        $smtp = $connection->execute('SELECT * FROM cfp.smtps WHERE event_id = ' . $event)->fetchAll('assoc')[0];
        $smtp['password'] = parent::decrypt($smtp['password']);
        $smtp['username'] = parent::decrypt($smtp['username']);
        Email::configTransport("custom", ['className'=>"Smtp", "timeout"=>60, "tls"=>($smtp['tls']==1?true:false), 'host'=>$smtp['host'], 'port'=>$smtp['port'], 'username'=>$smtp['username'], 
        'password'=>$smtp['password'], 'context'=>['ssl'=>['allow_self_signed'=>true, 'verify_peer'=>false, 'verify_peer_name'=>false]]]);
        foreach($speakers as $s){
            if($form == "session"){
                $accepts = $connection->execute("SELECT distinct r.title, r.id, r.duration, t.name FROM responses as r, accepts as a, responses_to_topics as c, topics as t WHERE " .
                        "r.user_id like ('" . $s['id']. "') AND a.vote = " . $result . " AND a.response_to_topic_id = c.id AND c.response_id = r.id AND r.event_id = " . $event .
                        " AND r.event_id = a.event_id AND c.topic_id = t.id AND r.informed = 0")->fetchAll("assoc"); 
                $secondary = $connection->execute("SELECT distinct r.title, r.id, r.duration, t.name FROM responses as r, accepts as a, responses_to_topics as c, additional_speakers as s, topics as t WHERE " .
                        "a.vote = " . $result . " AND a.response_to_topic_id = c.id AND c.response_id = r.id AND r.event_id = " . $event .
                        " AND r.event_id = a.event_id AND c.topic_id = t.id AND s.response_id = r.id and s.user_id like ('" . $s['id'] . "') AND s.informed = 0")->fetchAll('assoc');
            } else {
                $accepts = $connection->execute("SELECT distinct a.id, (SELECT text FROM answer_parts as ap WHERE ap.as_title = 1 AND ap.answer_id = a.id) as title, '' as duration, '' as name FROM" .
                        " answers as a, accepts as c WHERE a.id = c.answer_id AND c.vote = " . $result .
                        " AND a.event_id = " . $event . " AND a.user_id like ('" . $s['id'] . "') AND a.informed = 0 AND a.form_id = " . $fid)->fetchAll("assoc");
                $secondary = $connection->execute("SELECT distinct a.id, (SELECT text FROM answer_parts as ap WHERE ap.as_title = 1 AND ap.answer_id = a.id) as title, '' as duration, '' as name FROM" .
                        " answers as a, accepts as c, additional_speakers as s WHERE " .
                        "c.vote = " . $result . " AND a.id = c.answer_id AND a.event_id = " . $event . " AND s.answer_id = a.id AND s.user_id like ('" . $s['id'] . "') AND s.informed = 0 AND " .
                        "a.form_id = " . $fid)
                    ->fetchAll("assoc");
            }

            if($mail['sumarize']){
                $sumarized = array();
                if($form == "session"){
                    foreach($accepts as $talk){
                        $sumarized[] = $talk['title'] . " (" . $talk['duration'] . " minutes) for " . $talk['name'] . " track (where you're primary speaker)";
                    }
                    foreach($secondary as $talk){
                        $sumarized[] = $talk['title'] . " (" . $talk['duration'] . " minutes) for " . $talk['name'] . " track (where you're additional speaker)";
                    }
                } else {
                    foreach($accepts as $talk){
                        $sumarized[] = ($talk['title'] != ''?$talk['title']:ucfirst($form) . " proposal no. " . $talk['id']) . " (where you're primary speaker)";
                    }
                    foreach($secondary as $talk){
                        $sumarized[] = ($talk['title'] != ''?$talk['title']:ucfirst($form) . " proposal no. " . $talk['id']) . " (where you're additional speaker)";
                    }
                }

                $text = str_replace(parent::getKeywords(($result>0?'accept':($result < 0?'reject':'waitList'))), array(urlencode($s['first_name']), urlencode($s['last_name']), urlencode($s['id']),
                            $s['first_name'], $s['last_name'], $s['id'], $evInfo['name'], $evInfo['year'], implode(($mail['format']=='html'?"<br>":"\n"), $sumarized), "", ""), $mail['text']);
                $email = new Email();
                $email->dropTransport("Smtp");
                $email->setFrom([$smtp["username"]=>$smtp['nick']]);
                if($smtp['replyto'] != ''){
                    $email->setReplyTo($smtp['replyto']);
                }
                $email->emailFormat($mail['format']);
                $email->setTo($s['email']);
                $email->setSubject($mail['subject']);
                $email->send($text);
                if($result != 0){
                    foreach($accepts as $a){
                        $connection->execute("UPDATE ".($form == "session"?"responses":"answers")." SET informed = 1 WHERE id = " . $a['id']);
                    }
                    foreach($secondary as $sec){
                        $connection->execute("UPDATE additional_speakers SET informed = 1 WHERE ".($form == "session"?"response":"answer")."_id = " . $sec['id'] . " AND user_id like ('" . $s['id'] . "')");
                    }
                }
            } else {
                foreach ($accepts as $talk){
                    $title = "";
                    if($form == "session"){
                        $title = $talk['title'] . " (" . $talk['duration'] . " minutes) for " . $talk['name'] . " track (where you're primary speaker)";
                    } else {
                        if($talk['title'] != ""){
                            $title = $talk['title'] . " (where you're primary speaker)";
                        } else {
                            $title = ucfirst($form) . " proposal no. " . $talk['id'] . " (where you're primary speaker)";
                        }
                    }
                    $text = str_replace(parent::getKeywords(($result>0?'accept':($result < 0?'reject':'waitList'))), array(urlencode($s['first_name']), urlencode($s['last_name']), urlencode($s['id']),
                                $s['first_name'], $s['last_name'], $s['id'], $evInfo['name'], $evInfo['year'], "", $title,
                                $talk['name']), $mail['text']);
                    $email = new Email();
                    $email->dropTransport("Smtp");
                    $email->setFrom([$smtp["username"]=>$smtp['nick']]);
                    if($smtp['replyto'] != ''){
                        $email->setReplyTo($smtp['replyto']);
                    }
                    $email->emailFormat($mail['format']);
                    $email->setTo($s['email']);
                    $email->setSubject($mail['subject']);
                    $email->send($text);
                    if($result != 0){
                        $connection->execute("UPDATE ".($form == "session"?"responses":"answers")." SET informed = 1 WHERE id = " . $talk['id']);
                    }
                }
                foreach ($secondary as $talk){
                    $title = "";
                    if($form == "session"){
                        $title = $talk['title'] . " (" . $talk['duration'] . " minutes) for " . $talk['name'] . " track (where you're primary speaker)";
                    } else {
                        if($talk['title'] != ""){
                            $title = $talk['title'] . " (where you're additional speaker)";
                        } else {
                            $title = ucfirst($form) . " proposal no. " . $talk['id'] . " (where you're additional speaker)";
                        }
                    }

                    $text = str_replace(parent::getKeywords(($result>0?'accept':($result<0?'reject':'waitList'))), array(urlencode($s['first_name']), urlencode($s['last_name']), urlencode($s['id']),
                                $s['first_name'], $s['last_name'], $s['id'], $evInfo['name'], $evInfo['year'], "", $title,
                                $talk['name']), $mail['text']);
                    $email = new Email();
                    $email->dropTransport("Smtp");
                    $email->setFrom([$smtp["username"]=>$smtp['nick']]);
                    if($smtp['replyto'] != ''){
                        $email->setReplyTo($smtp['replyto']);
                    }
                    $email->emailFormat($mail['format']);
                    $email->setTo($s['email']);
                    $email->setSubject($mail['subject']);
                    $email->send($text);
                    if($result != 0){
                        $connection->execute("UPDATE additional_speakers SET informed = 1 WHERE ".($form == "session"?"response":"answer")."_id = " . $talk['id'] . " AND user_id like ('" . $s['id'] . "')");
                    }
                }
            }
        }
    }

    public function sendUnconfirmed($connection, $event, $form){
        $evInfo = $connection->execute("SELECT name, year FROM events WHERE id = " . $event)->fetchAll('assoc')[0];
        $fid = $connection->execute("SELECT id FROM forms WHERE event_id = " . $event . " AND type like ('" . $form . "')")->fetch("assoc")['id'];
        if($form == "session"){
            $primarys = $connection->execute("SELECT distinct user_id FROM responses WHERE event_id = " . $event)->fetchAll("assoc");
            $secondarys = $connection->execute("SELECT distinct user_id FROM additional_speakers WHERE response_id IN (SELECT id FROM responses WHERE event_id = " . $event . ")")->fetchAll("assoc");
        } else {
            $primarys = $connection->execute("SELECT distinct user_id FROM answers WHERE event_id = " . $event . " AND form_id = " . $fid)->fetchAll("assoc");
            $secondarys = $connection->execute("SELECT distinct user_id FROM additional_speakers WHERE answer_id IN (SELECT id FROM answers WHERE event_id = " . $event . " AND form_id = " . $fid . ")")->fetchAll("assoc");
        }
        $speakers = array();

        foreach($primarys as $p){
            if(!in_array($p['user_id'], $speakers)){
                $speakers[] = $p['user_id'];
            }
        }
        foreach($secondarys as $s){
            if(!in_array($s['user_id'], $speakers)){
                $speakers[] = $s['user_id'];
            }
        }

        foreach($speakers as $i=>$s){
            if($form == "session"){
                $check = $connection->execute("SELECT id FROM responses WHERE id IN (SELECT response_id FROM responses_to_topics WHERE id IN (SELECT response_to_topic_id FROM accepts WHERE event_id = " . $event . 
                        " AND response_to_topic_id IS NOT NULL AND vote = 1) and event_id = " . $event . ") and user_id like ('" . $s . "') and confirmed = 0")->rowCount();

                $check2 = $connection->execute("SELECT response_id FROM additional_speakers WHERE response_id IN (SELECT id FROM responses WHERE id IN (SELECT response_id FROM responses_to_topics WHERE " .
                        "id IN (SELECT response_to_topic_id FROM accepts WHERE event_id = " . $event . " AND response_to_topic_id IS NOT NULL AND vote = 1) AND event_id = " . $event . ") AND event_id = " . $event . 
                        ") AND user_id like ('" . $s . "') and confirmed = 0")->rowCount();
            } else {
                $check = $connection->execute("SELECT id FROM answers WHERE id IN (SELECT answer_id FROM accepts WHERE event_id = " . $event . " AND answer_id IS NOT NULL AND vote = 1) and event_id = " . $event .
                        " and user_id like ('" . $s . "') and confirmed = 0 and form_id = " . $fid)->rowCount();

                $check2 = $connection->execute("SELECT answer_id FROM additional_speakers WHERE answer_id IN (SELECT id FROM answers WHERE id IN (SELECT answer_id FROM accepts WHERE event_id = " . $event .
                        " AND answer_id IS NOT NULL and vote = 1) AND event_id = " . $event . " AND form_id = " . $fid . " AND confirmed = 0 AND user_id like ('" . $s . "'))")->rowCount();
            }

            if($check + $check2 == 0){
                unset($speakers[$i]);
                continue;
            }
        }

        $mail = $connection->execute('SELECT * FROM cfp.emails WHERE event_id = ' . $event . ' and type = "unconfirmedSpeakers" and form_type like ("'. $form . '")')->fetchAll('assoc')[0];
        $smtp = $connection->execute('SELECT * FROM cfp.smtps WHERE event_id = ' . $event)->fetchAll('assoc')[0];
        $smtp['password'] = parent::decrypt($smtp['password']);
        $smtp['username'] = parent::decrypt($smtp['username']);
        Email::configTransport("custom", ['className'=>"Smtp", "timeout"=>60, "tls"=>($smtp['tls']==1?true:false), 'host'=>$smtp['host'], 'port'=>$smtp['port'], 'username'=>$smtp['username'], 
        'password'=>$smtp['password'], 'context'=>['ssl'=>['allow_self_signed'=>true, 'verify_peer'=>false, 'verify_peer_name'=>false]]]);
        foreach($speakers as $s){
            $sp = $connection->execute("SELECT first_name, last_name, email FROM users WHERE id LIKE ('" . $s . "')")->fetchAll("assoc")[0];
            $sum = array();
            if($form == "session"){
                $prim = $connection->execute("SELECT title FROM responses WHERE user_id LIKE ('" . $s . "') and event_id = " . $event . " AND id IN (SELECT response_id FROM responses_to_topics WHERE " .
                        "id IN (SELECT response_to_topic_id FROM accepts WHERE event_id = " . $event . " AND response_to_topic_id IS NOT NULL AND vote = 1)) and confirmed = 0")->fetchAll("assoc");
                foreach($prim as $p){
                    $sum[] = $p['title'] . " (where you're primary speaker)";
                }
                $secd = $connection->execute("SELECT title FROM responses WHERE id IN (SELECT response_id FROM additional_speakers WHERE user_id LIKE ('" . $s . "') AND response_id IN " .
                        "(SELECT id FROM responses WHERE id IN (SELECT response_id FROM responses_to_topics WHERE id IN (SELECT response_to_topic_id FROM accepts WHERE " .
                        "event_id = " . $event . " AND response_to_topic_id IS NOT NULL AND vote = 1))) and confirmed = 0) and event_id = " . $event)->fetchAll("assoc");
                foreach($secd as $se){
                    $sum[] = $se['title'] . " (where you're additional speaker)";
                }
            } else {
                $prim = $connection->execute("SELECT id FROM answers WHERE user_id LIKE ('" . $s . "') AND event_id = " . $event . " AND confirmed = 0 AND form_id = " . $fid . " AND id IN " .
                        "(SELECT answer_id FROM accepts WHERE event_id = " . $event . " AND answer_id IS NOT NULL AND vote = 1)")->fetchAll("assoc");
                foreach($prim as $p){
                    $sum[] = ucfirst($form) . " proposal no. " . $p['id'] . " (where you're primary speaker)";
                }
                $secd = $connection->execute("SELECT answer_id FROM additional_speakers WHERE answer_id IN (SELECT id FROM answers WHERE id IN (SELECT answer_id FROM accepts WHERE " .
                        "event_id = " . $event . " AND answer_id IS NOT NULL AND vote = 1) AND event_id = " . $event . " AND form_id = " . $fid . " AND user_id LIKE ('" . $s . "')) and confirmed = 0")->fetchAll("assoc");
                foreach($secd as $se){
                    $sum[] = ucfirst($form) . " proposal no. " . $se['id'] . " (where you're additional speaker)";
                }
            }

            $text = str_replace(parent::getKeywords('unconfirmedSpeakers'), array($sp['first_name'], $sp['last_name'], $sp['email'], $evInfo['name'], $evInfo['year'], implode(($mail['format']=='html'?"<br>":"\n"), $sum)),
                    $mail['text']);
            $email = new Email();
            $email->dropTransport("Smtp");
            $email->setFrom([$smtp["username"]=>$smtp['nick']]);
            if($smtp['replyto'] != ''){
                $email->setReplyTo($smtp['replyto']);
            }
            $email->emailFormat($mail['format']);
            $email->setTo(($sp != null?$sp['email']:$s));
            $email->setSubject($mail['subject']);
            $email->send($text);
        }
    }

    public function sendInfo($connection, $event, $form, $type){
        if($form == "session"){
            switch ($type){
                case "all":
                    $pspeakers = $connection->execute("SELECT distinct user_id from responses WHERE event_id = " . $event)->fetchAll("assoc");
                    $additionalspeakers = $connection->execute("SELECT distinct user_id from additional_speakers WHERE response_id IN (SELECT id FROM responses WHERE event_id = " . $event . ")")->fetchAll('assoc');
                    break;
                case "to-accept":
                    $pspeakers = $connection->execute("SELECT user_id FROM responses WHERE id IN (SELECT response_id FROM responses_to_topics, accepts as a where id = a.response_to_topic_id AND"
                            . " a.vote = 1 AND a.event_id = " . $event . " )")->fetchAll("assoc");
                    $additionalspeakers = $connection->execute("SELECT user_id FROM additional_speakers WHERE response_id IN "
                            . "(SELECT response_id FROM responses_to_topics, accepts as a where id = a.response_to_topic_id AND a.vote = 1 AND a.event_id = " . $event . " )")->fetchAll("assoc");
                    break;
                case "to-reject":
                    $pspeakers = $connection->execute("SELECT user_id FROM responses WHERE id IN (SELECT response_id FROM responses_to_topics, accepts as a where id = a.response_to_topic_id AND"
                            . " a.vote = -1 AND a.event_id = " . $event . " ) AND user_id NOT IN (SELECT user_id FROM responses WHERE id IN (SELECT response_id FROM responses_to_topics, accepts as a"
                            . " WHERE id = a.response_to_topic_id AND a.vote = 1 AND a.event_id = " . $event . ")) and user_id NOT IN (SELECT user_id FROM additional_speakers WHERE response_id IN "
                            . "(SELECT response_id FROM responses_to_topics, accepts as a where id = a.response_to_topic_id AND a.vote = 1 AND a.event_id = " . $event . " ))")->fetchAll("assoc");
                    $additionalspeakers = $connection->execute("SELECT user_id FROM additional_speakers WHERE response_id IN "
                            . "(SELECT response_id FROM responses_to_topics, accepts as a where id = a.response_to_topic_id AND a.vote = -1 AND a.event_id = " . $event . " )"
                            . " AND user_id NOT IN (SELECT user_id FROM additional_speakers WHERE response_id IN (SELECT response_id FROM responses_to_topics, accepts as a where id = a.response_to_topic_id"
                            . " AND a.vote = 1 AND a.event_id = " . $event . " )) AND user_id NOT IN (SELECT user_id FROM additional_speakers WHERE response_id IN "
                            . "(SELECT response_id FROM responses_to_topics, accepts as a where id = a.response_to_topic_id AND a.vote = 1 AND a.event_id = " . $event . " ))")->fetchAll("assoc");
                    break;
            }
        } else {
            $fid = $connection->execute("SELECT id FROM forms WHERE event_id = " . $event . " and type like ('" . $form . "')")->fetch("assoc")['id'];
            switch ($type){
                case "all":
                    $pspeakers = $connection->execute("SELECT distinct user_id from answers WHERE form_id = " . $fid )->fetchAll("assoc");
                    $additionalspeakers = $connection->execute("SELECT distinct user_id from additional_speakers WHERE answer_id IN (SELECT id FROM answers WHERE form_id = " . $fid . ")")->fetchAll('assoc');
                    break;
                case "to-accept":
                    $pspeakers = $connection->execute("SELECT distinct user_id FROM answers WHERE id IN (SELECT answer_id FROM accepts WHERE vote = 1 AND event_id = " . $event
                            . " ) AND form_id = " . $fid)->fetchAll("assoc");
                    $additionalspeakers = $connection->execute("SELECT distinct user_id FROM additional_speakers WHERE answer_id IN "
                            . "(SELECT id FROM answers WHERE id IN (SELECT answer_id FROM accepts WHERE vote = 1 AND event_id = " . $event . " ) AND form_id = " . $fid . ")")->fetchAll("assoc");
                    break;
                case "to-reject":
                    $pspeakers = $connection->execute("SELECT distinct user_id FROM answers WHERE id IN (SELECT answer_id FROM accepts WHERE vote = -1 AND event_id = " . $event
                            . " ) AND form_id = " . $fid . " AND user_id NOT IN (SELECT user_id FROM answers WHERE id IN (SELECT answer_id FROM accepts WHERE vote = 1 AND event_id = " . $event
                            . " ) AND form_id = " . $fid . ") AND user_id NOT IN (SELECT user_id FROM additional_speakers WHERE answer_id IN "
                            . "(SELECT id FROM answers WHERE id IN (SELECT answer_id FROM accepts WHERE vote = 1 AND event_id = " . $event . " ) AND form_id = " . $fid . "))")->fetchAll("assoc");
                    $additionalspeakers = $connection->execute("SELECT distinct user_id FROM additional_speakers WHERE answer_id IN "
                            . "(SELECT id FROM answers WHERE id IN (SELECT answer_id FROM accepts WHERE vote = -1 AND event_id = " . $event
                            . " ) AND form_id = " . $fid . ") AND user_id NOT IN (SELECT user_id FROM answers WHERE id IN (SELECT answer_id FROM accepts WHERE vote = 1 AND event_id = " . $event
                            . " ) AND form_id = " . $fid . ") AND user_id NOT IN (SELECT user_id FROM additional_speakers WHERE answer_id IN "
                            . "(SELECT id FROM answers WHERE id IN (SELECT answer_id FROM accepts WHERE vote = 1 AND event_id = " . $event . " ) AND form_id = " . $fid . "))")->fetchAll("assoc");
                    break;
                case "to-hotel-paid":
                    $pspeakers = $connection->execute("SELECT distinct user_id FROM hotel_booking WHERE event_id = " . $event . " AND vote = 1 AND informed = 0")->fetchAll("assoc");
                    $additionalspeakers = array();
                    break;
                case "to-hotel-unpaid":
                    $pspeakers = $connection->execute("SELECT distinct user_id FROM hotel_booking WHERE event_id = " . $event . " AND vote = 0 AND informed = 0")->fetchAll("assoc");
                    $additionalspeakers = array();
                    break;
                    
            }
        }

        $speakers = array_merge($pspeakers, $additionalspeakers);
        $mail = $connection->execute('SELECT * FROM cfp.emails WHERE event_id = ' . $event . ' and type = "' . $type . '" and form_type = "' . $form . '"')->fetchAll('assoc')[0];
        $smtp = $connection->execute('SELECT * FROM cfp.smtps WHERE event_id = ' . $event)->fetchAll('assoc')[0];
        $smtp['password'] = parent::decrypt($smtp['password']);
        $smtp['username'] = parent::decrypt($smtp['username']);
        $einfo = $connection->execute('SELECT name, year FROM cfp.events WHERE id = ' . $event)->fetchAll('assoc')[0];
        Email::configTransport("custom", ['className'=>"Smtp", "timeout"=>60, "tls"=>($smtp['tls']==1?true:false), 'host'=>$smtp['host'], 'port'=>$smtp['port'], 'username'=>$smtp['username'], 
        'password'=>$smtp['password'], 'context'=>['ssl'=>['allow_self_signed'=>true, 'verify_peer'=>false, 'verify_peer_name'=>false]]]);

        $informed = array();
        foreach($speakers as $speaker){
            $sinfo = $connection->execute('SELECT first_name, last_name, email FROM users WHERE id like ("' . $speaker['user_id'] . '")')->fetchAll('assoc');
            if(count($sinfo) > 0 && !in_array($speaker['user_id'], $informed)){
                $sinfo = $sinfo[0];
                $text = str_replace(parent::getKeywords('all'), array(urlencode($sinfo['first_name']), urlencode($sinfo['last_name']), urlencode($speaker['user_id']), 
                            $sinfo['first_name'], $sinfo['last_name'], $sinfo['email'], $einfo['name'], $einfo['year']), $mail['text']);
                $email = new Email();
                $email->dropTransport("Smtp");
                $email->emailFormat($mail['format']);
                $email->setFrom([$smtp['username'] => $smtp['nick']]);
                if($smtp['replyto'] != ''){
                    $email->setReplyTo($smtp['replyto']);
                }
                $email->setTo($sinfo['email']);
                $email->setSubject($mail['subject']);
                $email->send($text);
                $informed[] = $speaker['user_id'];
                if(strpos($type, "hotel") !== false){
                    $connection->execute("UPDATE hotel_booking SET informed = 1 WHERE event_id = " . $event . " AND user_id like '" . $speaker['user_id'] . "'");
                }
            }
        }
    }
}
