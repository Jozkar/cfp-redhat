<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Datasource\ConnectionManager;
/**
 * Static content controller
 *
 * This controller will render views from Template/HomePage/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class CollectController extends AppController
{

    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Network\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function show(...$path)
    {

        parent::printFlush($this->request->here());

        $this->set("active", "accept");
        $this->set("admin", parent::getAdmin() | parent::getSuperUser());
        $this->set("reviewer", parent::getReviewer());
        $this->set("program_manager", parent::getProgramManager());
        $connection = ConnectionManager::get('cfp');

        if($this->request->session()->read("first-name")){
        	$this->set('username', $_SESSION['first-name'] . " " . $_SESSION['last-name']);
        }

        if(count($path) > 1 && is_numeric($path[0]) && is_numeric($path[1])){
    	    //show form for collecting
        	$form = $connection->execute('SELECT id FROM cfp.forms WHERE event_id = ' . $path[0] . ' and type like ("collect")')->fetch('assoc');
        	$event = $connection->execute('SELECT name, year, id, redhat_event FROM events WHERE id = ' . $path[0])->fetch('assoc');

            if($event['redhat_event'] == 0){
                $_SESSION['errorMessage'][] = "This event isn't organized by Red Hat. You can't upload anything.";
                return $this->redirect("/acceptproposals");
            }
    
       		$questions = $connection->execute('SELECT * FROM cfp.questions WHERE form_id = ' . $form['id'] . ' ORDER BY question_order')->fetchAll('assoc');

        	$options = $connection->execute('SELECT * FROM cfp.question_options WHERE form_id = ' . $form['id'] . ' ORDER BY question_id, id')->fetchAll('assoc');

        	foreach($questions as $i=>$q){
    	    	$questions[$i]['options'] = array();
    		    foreach($options as $o){
    			    if($q['id'] == $o['question_id']){
    				    $questions[$i]['options'][] = $o;
       				}
        		}
        	}

            // CHECK IF ANSWER EXISTS -> IF SO, UPDATE FORM SHOULD BE PROCEEDED
            $this->set("values", null);
            $this->set("canswer_id", null);

            if($path[1] < 0){
                if(count($path) > 2 && is_numeric($path[2])){
                    $exist = $connection->execute("SELECT id FROM answers WHERE form_id = " . $form['id'] . " AND collect_answer_id = " . $path[2])->fetch("assoc");
                    if($exist){
                        foreach($questions as $q){
                            $v = $connection->execute("SELECT text FROM cfp.answer_parts WHERE question_id = " . $q['id'] . " AND answer_id = " . $exist['id'])->fetchAll("assoc");
                            if(count($v) > 0){
                                $values[] = $v[0]['text'];
                            } else {
                                $values[] = NULL;
                            }
                        }
                        $this->set("values", $values);
                        $this->set("canswer_id", $exist['id']);
                    }


            	    $proposal = $connection->execute("SELECT a.id, ap.text as title FROM answer_parts as ap, answers as a, accepts as c" .
            		    " WHERE c.answer_id = a.id AND c.vote = 1 AND a.confirmed = 1 AND a.event_id = " . $path[0] . " AND a.user_id like ('" . $_SESSION['loggedUser'] . "') and a.id = " . $path[2] . " and a.id = ap.answer_id" . 
                        " AND ap.as_title = 1")->fetch('assoc');

                    if(!isset($proposal['title']) || trim($proposal['title']) == ""){
                        $answer = $connection->execute("SELECT a.id, f.type FROM answers as a, forms as f, accepts as c" .
                            " WHERE c.answer_id = a.id AND c.vote = 1 AND a.confirmed = 1 AND f.id = a.form_id AND a.event_id = " . $path[0] . " AND a.user_id like ('" . $_SESSION['loggedUser'] . "') and a.id = " . $path[2])
                            ->fetch('assoc');
                        $answer['title'] = ucfirst($answer['type']). " proposal no. " . $answer['id'];
                        $proposal = $answer;
                    }
                    $this->set("answer_id", $proposal['id']);
                    $this->set("response_id", 0);
                } else {
                    $_SESSION['errorMessage'][] = "Unsufficient data. Your link is probably broken.";
                    return $this->redirect("/acceptproposals");
                }
            } else {
                $exist = $connection->execute("SELECT id FROM answers WHERE form_id = " . $form['id'] . " AND collect_response_id = " . $path[1])->fetch("assoc");
                if($exist){
                    foreach($questions as $q){
                        $v = $connection->execute("SELECT text FROM cfp.answer_parts WHERE question_id = " . $q['id'] . " AND answer_id = " . $exist['id'])->fetchAll("assoc");
                        if(count($v) > 0){
                            $values[] = $v[0]['text'];
                        } else {
                            $values[] = NULL;
                        }
                    }
                    $this->set("values", $values);
                    $this->set("canswer_id", $exist['id']);
                }

            	$proposal = $connection->execute("SELECT distinct r.id, r.title FROM responses as r, accepts as a, responses_to_topics as t" .
            		" WHERE a.response_to_topic_id = t.id AND a.vote = 1 AND r.confirmed = 1 AND a.event_id = " . $path[0] . " AND t.response_id = r.id AND r.user_id like ('" . $_SESSION['loggedUser'] . "')" .
                    " AND r.id = " . $path[1])->fetch('assoc');
                $this->set("response_id", $proposal['id']);
                $this->set("answer_id", 0);
            }
        	$this->set("proposal", $proposal);
        	$this->set("questions", $questions);
    	    $this->set("event", $event);
        	$this->set("form", $form);

        	try{
        		$this->render("collect");
    	    } catch (MissingTemplateException $exception){
    		    if (Configure::read('debug')) {
    			    throw $exception;
        		}
        		throw new NotFoundException();
    	    }

        } else if(count($path) > 1 and ($path[0] == "add" || $path[0] == "update") and is_numeric($path[1])) {
            $form = $connection->execute('SELECT * FROM forms WHERE id = ' . $path[1])->fetchAll('assoc');

            if(count($form) < 1){
                $_SESSION['errorMessage'][] = "Form not found. Your link is probably broken.";
                return $this->redirect("/acceptproposals");
            }

            $form = $form[0];

            $event = $connection->execute('SELECT id, year, name, redhat_event, GROUP_CONCAT(topic_id) AS topics, cfp_close FROM cfp.events, cfp.topics_to_events WHERE id = ' . $form['event_id'] .
                     ' AND event_id = id')->fetchAll('assoc')[0];

            if($event['redhat_event'] == 0){
                $_SESSION['errorMessage'][] = "This event isn't organized by Red Hat. You can't upload anything.";
                return $this->redirect("/acceptproposals");
            }

            $questions = $connection->execute('SELECT * FROM cfp.questions WHERE form_id = ' . $path[1] . ' ORDER BY question_order')->fetchAll('assoc');
            return $this->add($connection, $questions, $form);
        } else {
            $_SESSION['errorMessage'][] = "Invalid request.";
            return $this->redirect('/acceptproposals');
        }
    }

    public function add($connection, $questions, $form) {
        if( $_POST['collectAnswerId'] < 1 ){

            $answerId = -1;
            foreach($questions as $q){
                switch($q['type']){
                    case "text":
                    case "textarea":
                    case "radio":
                        if(strpos($q['take_as'], "self") !== false){
                            $answer = "'" . str_replace(array("'", "\""), array("\'", "\\\""), $_POST[$q['take_as']]) . "'";
                            try {
                                if($answerId < 0){
                                    $connection->execute("INSERT INTO answers (event_id, user_id, form_id, collect_answer_id, collect_response_id) VALUES" .
                                        " (" .$form['event_id']. ", '" .$_SESSION['loggedUser']. "', " .$form['id']. ", " . $_POST['answerId'] . ", " . $_POST['responseId'] . ")");
                                    $answerId = $connection->execute("SELECT LAST_INSERT_ID() as id")->fetchAll('assoc')[0]['id'];
                                }
                                // INSERT INTO answers
                                $connection->execute("INSERT INTO answer_parts (question_id, answer_id, text, as_title) VALUES (" . $q['id'] . ", " . $answerId . ", " . $answer . ", " . $q['as_title'] . ")");
                            }catch(\Exception $e){
                                $_SESSION['errorMessage'][] = $e->getMessage();
                                $connection->execute("DELETE FROM answer_parts WHERE answer_id = ". $answerId);
                                $connection->execute("DELETE FROM answers WHERE id = " . $answerId);
                                return $this->redirect("/collect/" . $form['id'] . "/" . $_POST['responseId'] . "/" . $_POST['answerId']);
                            }
                        }
                        break;
                    case "checkbox":
                        if(isset($_POST[$q['take_as']]) && count($_POST[$q['take_as']]) > 0){
                            $answers = array();
                            foreach($_POST[$q['take_as']] as $t){
                                $answers[] = str_replace(array("'", "\""), array("\'", "\\\""), $t);
                            }
                            try{
                                if($answerId < 0){;
                                    $connection->execute("INSERT INTO answers (event_id, user_id, form_id, collect_answer_id, collect_response_id) VALUES" .
                                        " (" .$form['event_id']. ", '" .$_SESSION['loggedUser']. "', " .$form['id']. ", " . $_POST['answerId'] . ", " . $_POST['responseId'] . ")");
                                    $answerId = $connection->execute("SELECT LAST_INSERT_ID() as id")->fetchAll('assoc')[0]['id'];
                                }
                                //INSERT INTO answers
                                $connection->execute("INSERT INTO answer_parts (question_id, answer_id, text) VALUES (" . $q['id'] . ", " . $answerId . ", '" . implode(",", $answers) . "')");
                            }catch(\Exception $e){
                                $_SESSION['errorMessage'][] = $e->getMessage();
                                $connection->execute("DELETE FROM answer_parts WHERE answer_id = ". $answerId);
                                $connection->execute("DELETE FROM answers WHERE id = " . $answerId);
                                return $this->redirect("/collect/" . $form['id'] . "/" . $_POST['responseId'] . "/" . $_POST['answerId']);
                            }
                        } else {
                            if($q['required']){
                                $_SESSION['errorMessage'][] = "You didn't specified required fields. Your proposal wasn't accepted.";
                                if($answerId > 0){
                                    $connection->execute("DELETE FROM answer_parts WHERE answer_id = ". $answerId);
                                    $connection->execute("DELETE FROM answers WHERE id = " . $answerId);
                                }
                                return $this->redirect("/collect/" . $form['id'] . "/" . $_POST['responseId'] . "/" . $_POST['answerId']);
                            }
                        }
                        break;
                    default: 
                        break;
                }
            }

        } else {
            foreach($questions as $q){
                switch($q['type']){
                    case "text":
                    case "textarea":
                    case "radio":
                        if(strpos($q['take_as'], "self") !== false){
                            $answer = "'" . str_replace(array("'", "\""), array("\'", "\\\""), $_POST[$q['take_as']]) . "'";
                            try {
                                $row = $connection->execute("SELECT 1 FROM answer_parts WHERE question_id = " . $q['id'] . " AND answer_id = " . $_POST['collectAnswerId'])->rowCount();
                                if($row > 0){
                                    $connection->execute("UPDATE answer_parts SET text=" . $answer . " WHERE question_id = " . $q['id'] . " AND answer_id = " . $_POST['collectAnswerId']);
                                } else {
                                    $connection->execute("INSERT INTO answer_parts (question_id, answer_id, text, as_title) VALUES (" . $q['id'] . ", " . $_POST['collectAnswerId'] . ", " . $answer . ", " . $q['as_title'] . ")");
                                }
                            }catch(\Exception $e){
                                $_SESSION['errorMessage'][] = $e->getMessage();
                                return $this->redirect("/collect/" . $form['id'] . "/" . $_POST['responseId'] . "/" . $_POST['answerId']);
                            }
                        }
                        break;
                    case "checkbox":
                        if(isset($_POST[$q['take_as']]) && count($_POST[$q['take_as']]) > 0){
                            $answers = array();
                            foreach($_POST[$q['take_as']] as $t){
                                $answers[] = str_replace(array("'", "\""), array("\'", "\\\""), $t);
                            }
                            try{
                                $row = $connection->execute("SELECT 1 FROM answer_parts WHERE question_id = " . $q['id'] . " AND answer_id = " . $_POST['collectAnswerId'])->rowCount();
                                if($row > 0){
                                    $connection->execute("UPDATE answer_parts SET text='" . implode(",", $answers) . "' WHERE question_id = " . $q['id'] . " AND answer_id = " . $_POST['collectAnswerId']);
                                } else {
                                    $connection->execute("INSERT INTO answer_parts (question_id, answer_id, text) VALUES (" . $q['id'] . ", " . $_POST['collectAnswerId'] . ", '" . implode(",", $answers) . "')");
                                }
                            }catch(\Exception $e){
                                $_SESSION['errorMessage'][] = $e->getMessage();
                                return $this->redirect("/collect/" . $form['id'] . "/" . $_POST['responseId'] . "/" . $_POST['answerId']);
                            }
                        } else {
                            if($q['required']){
                                $_SESSION['errorMessage'][] = "You didn't specified required fields. Your answer was partially updated.";
                                return $this->redirect("/acceptedproposals/" . $form['event_id']);
                            }
                        }
                        break;
                    default: 
                        continue;
                        break;
                }
            }
        }

        $_SESSION['successMessage'][] = "Your answer has been accepted.";

        return $this->redirect("/acceptedproposals/" . $form['event_id']);
    }
}

