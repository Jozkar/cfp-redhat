<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Datasource\ConnectionManager;
/**
 * Static content controller
 *
 * This controller will render views from Template/HomePage/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class HotelBookingController extends AppController
{

    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Network\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function show(...$path)
    {
    
        parent::printFlush($this->request->here());

        $this->set("active", "accept");
        $this->set("admin", parent::getAdmin() | parent::getSuperUser());
        $this->set("reviewer", parent::getReviewer());
        $this->set("program_manager", parent::getProgramManager());

		$connection = ConnectionManager::get('cfp');

        if($this->request->session()->read("first-name")){
        	$this->set('username', $_SESSION['first-name'] . " " . $_SESSION['last-name']);
        }
        
        if(count($path) < 1 or !is_numeric($path[0])){
			$_SESSION['errorMessage'][] = "Unsufficient data. Your link is probably broken.";
			return $this->redirect("/myproposals");
		}
        
	    $booking = $connection->execute("SELECT user_id, vote FROM hotel_booking WHERE event_id = " . $path[0] . " AND user_id like ('" . $_SESSION['loggedUser'] . "')")->fetchAll('assoc');

	   	$event = $connection->execute('SELECT name, year, id, cfp_close FROM events WHERE id = ' . $path[0])->fetch('assoc');
	    
	    if(count((isset($booking)?$booking:array())) > 0 && $booking[0]['vote'] > 0){
	    	try{
				$form = $connection->execute('SELECT id, cfp_close FROM cfp.forms WHERE event_id = ' . $path[0] . ' and type like ("covered accommodation")')->fetch('assoc');
				$exists = $connection->execute("SELECT 1 FROM cfp.answers WHERE user_id like '" . $_SESSION['loggedUser'] . "' AND form_id = " . $form['id'] . " AND event_id = " . $path[0])->rowCount();
				$hotels = $connection->execute("SELECT id, name, description, web FROM hotels WHERE event_id = " . $path[0] . " AND use_for_paid_form = 1")->fetchAll('assoc');
				
				foreach($hotels as $i=>$h){
					$rooms = $connection->execute("SELECT id, name, description, covered_price as price FROM hotel_rooms WHERE hotel_id = " . $h['id'] . " AND amount > 0")->fetchAll("assoc");
					$hotels[$i]['rooms'] = $rooms;
				}
		
				if(!$exists){
					$questions = $connection->execute('SELECT * FROM cfp.questions WHERE form_id = ' . $form['id'] . ' ORDER BY question_order')->fetchAll('assoc');
					$options = $connection->execute('SELECT * FROM cfp.question_options WHERE form_id = ' . $form['id'] . ' ORDER BY question_id, id')->fetchAll('assoc');

					foreach($questions as $i=>$q){
						$questions[$i]['options'] = array();
						foreach($options as $o){
							if($q['id'] == $o['question_id']){
								$questions[$i]['options'][] = $o;
							}
						}
					}
				} else {
					$hotels = $questions = $options = null;
				}
			}catch(\Exception $e){
				$hotels = $questions = $options = null;
			}
	    } else {
	    	try{
	    		$form = $connection->execute('SELECT id, cfp_close FROM cfp.forms WHERE event_id = ' . $path[0] . ' and type like ("accommodation")')->fetch('assoc');
				$exists = $connection->execute("SELECT 1 FROM cfp.answers WHERE user_id like '" . $_SESSION['loggedUser'] . "' AND form_id = " . $form['id'] . " AND event_id = " . $path[0])->rowCount();
				$hotels = $connection->execute("SELECT id, name, description, web FROM hotels WHERE event_id = " . $path[0])->fetchAll('assoc');
		
				foreach($hotels as $i=>$h){
					$rooms = $connection->execute("SELECT id, name, description, full_price as price FROM hotel_rooms WHERE hotel_id = " . $h['id'] . " AND amount > 0")->fetchAll("assoc");
					$hotels[$i]['rooms'] = $rooms;
				}
				
				if(!$exists){
					$questions = $connection->execute('SELECT * FROM cfp.questions WHERE form_id = ' . $form['id'] . ' ORDER BY question_order')->fetchAll('assoc');
					$options = $connection->execute('SELECT * FROM cfp.question_options WHERE form_id = ' . $form['id'] . ' ORDER BY question_id, id')->fetchAll('assoc');

					foreach($questions as $i=>$q){
						$questions[$i]['options'] = array();
						foreach($options as $o){
							if($q['id'] == $o['question_id']){
								$questions[$i]['options'][] = $o;
							}
						}
					}
				} else {
					$hotels = $questions = $options = null;
				}
			}catch(\Exception $e){
				$hotels = $question = $options = null;
			}
	    }
	    
	    if(strtotime($form['cfp_close']) < time() && ($form['cfp_close'] == "" || strtotime($form['cfp_close']) < time())){
	    	$hotels = $questions = $options = null;
	    }

    	$this->set("hotels", $hotels);
    	$this->set("questions", $questions);
	    $this->set("event", $event);
    	$this->set("form", $form);

    	try{
    		$this->render("booking");
	    } catch (MissingTemplateException $exception){
		    if (Configure::read('debug')) {
			    throw $exception;
    		}
    		throw new NotFoundException();
	    }
    }
}
