<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Datasource\ConnectionManager;
/**
 * Static content controller
 *
 * This controller will render views from Template/HomePage/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class ScheduleController extends AppController
{

    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Network\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function show(...$action)
    {
        if(count($action) < 1 or !is_numeric($action[0])){
			$_SESSION['errorMessage'][] = "Unsufficient data. Your link is probably broken.";
			return $this->redirect("/");
		}
		
		$connection = ConnectionManager::get('cfp');
        
        $event = $connection->execute("SELECT name, id, year, schedule, open, close, timezone, schedule_version FROM events WHERE id = " . $action[0])->fetch("assoc");
        		
        parent::printFlush($this->request->here());
        $this->set("admin", parent::getAdmin() | parent::getSuperUser());
        $this->set("reviewer", parent::getReviewer());
        $this->set("program_manager", parent::getProgramManager());

        $rooms = $connection->execute('SELECT * FROM cfp.rooms WHERE event_id = ' . $action[0])->fetchAll('assoc');
        foreach($rooms as $i=>$r){
            $blocks = $connection->execute('SELECT * FROM cfp.room_availability WHERE room_id = ' . $r['id'])->fetchAll('assoc');
            
            $rooms[$i]['available'] = $blocks;
        }

        $eversion = $connection->execute("SELECT schedule_version FROM events WHERE id = " . $action[0])->fetch("assoc");
        if(isset($action[1]) && is_numeric($action[1])){
            $eversion['schedule_version']++;
        }
        $placedResp = $connection->execute("SELECT s.id, s.room_id, s.date, s.start, s.end, r.id as response_id, r.title, r.duration, r.abstract as description, CONCAT(u.first_name,' ',u.last_name) as speaker,"
            . " u.avatar, u.organization, u.bio, t.name as topic, t.color, t.text_color FROM"
            . " responses as r, users as u, topics as t, responses_to_topics as rt, schedule as s WHERE"
            . " r.id IN (SELECT response_id FROM schedule WHERE event_id = " . $action[0] . " and room_id is NOT NULL and version = " . $eversion['schedule_version'] . ") AND r.event_id = " . $action[0]
            . " AND u.id = r.user_id AND rt.response_id = r.id AND rt.topic_id = t.id AND r.id = s.response_id and s.version = " . $eversion['schedule_version'])->fetchAll("assoc");

        $placedAnsw = $connection->execute("SELECT s.id, s.room_id, s.date, s.start, s.end, a.id as answer_id, CONCAT(u.first_name,' ',u.last_name) as speaker, u.avatar, u.organization, u.bio, f.type as topic, f.color, f.text_color"
            . " FROM answers as a, users as u, schedule as s, forms as f WHERE"
            . " a.id IN (SELECT answer_id FROM schedule WHERE event_id = " . $action[0] . " and room_id is NOT NULL and version = " . $eversion['schedule_version'] . ") AND a.event_id = " . $action[0]
            . " AND u.id = a.user_id AND a.form_id = f.id AND a.id = s.answer_id and s.version = " . $eversion['schedule_version'])->fetchAll("assoc");

        foreach($placedAnsw as $k=>$ua){
            $title = $connection->execute("SELECT IFNULL(text,'') as text FROM answer_parts WHERE answer_id = " . $ua['answer_id'] . " AND as_title = 1")->fetch("assoc");
            $placedAnsw[$k]['title'] = $title['text'];

            $duration = $connection->execute("SELECT IFNULL(text,5) as text FROM answer_parts WHERE answer_id = " . $ua['answer_id'] . " AND as_duration = 1")->fetch("assoc");
            $placedAnsw[$k]['duration'] = intval($duration['text']);

            $description = $connection->execute("SELECT IFNULL(text,'') as text FROM answer_parts WHERE answer_id = " . $ua['answer_id'] . " AND as_description = 1")->fetch("assoc");
            $placedAnsw[$k]['description'] = $description['text'];
        }

        $breaks = $connection->execute("SELECT id, title, description, room_id, date, start, end FROM schedule WHERE event_id = " . $action[0] . " AND answer_id IS NULL and response_id IS NULL and version = "
            . $eversion['schedule_version'])->fetchAll("assoc");

        $this->set("Responses", $placedResp);
        $this->set("Answers", $placedAnsw);
        $this->set("breaks", $breaks);

        $this->set("event", $event);
        $this->set("rooms", $rooms);
        $this->set("active", "manager");
        $token = $this->request->getParam('_csrfToken');

        $_SESSION['token'] = $token;
        $this->set("token", $token);
        $this->set('username', $_SESSION['first-name']." ".$_SESSION['last-name']);

        try {
            $this->render('schedule');
        } catch (MissingTemplateException $exception) {
            if (Configure::read('debug')) {
                throw $exception;
            }
            throw new NotFoundException();
        }
    }
}
