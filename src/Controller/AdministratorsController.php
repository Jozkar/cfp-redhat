<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Datasource\ConnectionManager;
/**
 * Static content controller
 *
 * This controller will render views from Template/HomePage/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class AdministratorsController extends AppController
{

    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Network\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function show(...$action)
    {

        if(!parent::getAdmin() && !parent::getSuperUser()){
            return $this->redirect("/");
        }

        if(count($action) < 1 || !is_numeric($action[0])){
            $_SESSION['errorMessage'][] = "Unsufficient data. Your link is probably broken.";
            return $this->redirect("/admin");
        }

        if(!parent::getAdminForEvent($action[0]) && !parent::getSuperUser()){
            $_SESSION['errorMessage'][] = "This action can't be performet - invalid token.";
            return $this->redirect("/admin");
        }

        $connection = ConnectionManager::get('cfp');
        
        if(isset($_POST['save'])){
            if(isset($_POST['_Token']) && $_POST['_Token'] == $_SESSION['token']){
                if(isset($action[1]) && $action[1] == "add"){
                    // get user id
                    $_POST['user'] = "'" . str_replace(array("'", "\""), array("\'", "\\\""), $_POST['user']) . "'";
                    $newAdmin = $connection->execute("SELECT id FROM users WHERE email like (" . $_POST['user'] . ")")->fetchAll("assoc");
                    // set admin column in db to 1
                    return $this->modify($connection,$action[0],$newAdmin[0]["id"],1);
                } else {
                    $_SESSION['errorMessage'][] = "You are not allowed to do this opperation";
                    $this->redirect("/administrators/" . $action[0]);
                }
            } else {
                $_SESSION['errorMessage'][] = "You are not allowed to do this opperation";
                $this->redirect("/administrators/" . $action[0]);
            }
        }

        parent::printFlush($this->request->here());
        $this->set("eventId", $action[0]);
        $this->set("admin", parent::getAdmin() | parent::getSuperUser());
        $this->set("reviewer", parent::getReviewer());
        $this->set("program_manager", parent::getProgramManager());

        $formAction = "add";            

        // check, if are passed any arguments
        if(count($action) > 1){
            // supported are arguments /.../action/userid/token
            if( count($action) <= 3 ){
                //not enough arguments error message  + redirect
                $_SESSION['errorMessage'][] = "Unsufficient data. Your link is probably broken.";
                return $this->redirect("/administrators/" . $action[0]); 
            }
            //for delete action
            if ($action[1] == "delete") {
                if($_SESSION['token'] == $action[3]){
                    // set admin column in db to 0
                    return $this->modify($connection,$action[0], $action[2],0);
                } else {
                    $_SESSION['errorMessage'][] = "This action can't be performet - invalid token.";
                    return $this->redirect("/administrators/" . $action[0]);
                }
            }
        }

        $allUsers = $connection->execute("SELECT first_name, last_name, id, email, avatar FROM users WHERE superuser = 0 AND id NOT IN (SELECT user_id FROM admins WHERE event_id = " . $action[0] . ")")->fetchAll("assoc");
        $adminUsers = $connection->execute("SELECT first_name, last_name, id, email, avatar FROM users WHERE admin = 1 AND id in (SELECT user_id FROM admins WHERE event_id = " . $action[0] . ")")->fetchAll("assoc");

        $this->set("admins", $adminUsers);
        $this->set("users", $allUsers);
        $this->set("action", $formAction);
        $this->set("active", "admin");
        $token = $this->request->getParam('_csrfToken');

        $_SESSION['token'] = $token;
        $this->set("token", $token);
        $this->set('username', $_SESSION['first-name']." ".$_SESSION['last-name']);
    
        try {
            $this->render('list');
        } catch (MissingTemplateException $exception) {
            if (Configure::read('debug')) {
                throw $exception;
            }
            throw new NotFoundException();
        }
    }
    
    public function modify($connect, $eventId, $key, $val){
        $key = "'" . str_replace(array("'", "\""), array("\'", "\\\""), $key) . "'";
        
        try{
            if($val){
                $res = $connect->execute("UPDATE cfp.users SET `admin`=" . $val . " WHERE `id` like (" . $key . ")");
                $res = $connect->execute("INSERT INTO cfp.admins (event_id, user_id) VALUES (" . $eventId . ", " . $key . ")");
                $_SESSION['successMessage'][] = "Admin has been successfully created.";
            } else {
                $res = $connect->execute("DELETE FROM cfp.admins WHERE `event_id`=" . $eventId . " AND `user_id` like (" . $key . ")");
                $test = $connect->execute("SELECT * FROM cfp.admins WHERE user_id like(" . $key . ")");
                if(count($test) < 1) {
                    $connect->execute("UPDATE cfp.users SET `admin`= 0 WHERE `id` like (" . $key . ")");
                }

                $_SESSION['successMessage'][] = "Admin has been successfully removed.";
            }
        }catch(\Exception $e){
            if($val){
                $_SESSION['errorMessage'][] = "Admin can't be created.";
            } else {
                $_SESSION['errorMessage'][] = "Admin can't be removed.";
            }
        }    
        $this->redirect("/administrators/" . $eventId);
    }
}
