<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Datasource\ConnectionManager;
use Cake\Filesystem\File;
/**
 * Static content controller
 *
 * This controller will render views from Template/HomePage/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class EventController extends AppAdminController
{

    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Network\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function show(...$action)
    {

        if(!parent::getAdmin() && !parent::getSuperUser()){
            return $this->redirect("/");
        }
        $connection = ConnectionManager::get('cfp');
        
        if(isset($_POST['save'])){
            if(isset($_POST['_Token']) && $_POST['_Token'] == $_SESSION['token']){
                if(isset($action[0]) && $action[0] == "add"){
                   return $this->add($connection);
                } else {
                   return $this->update($connection,$action[1]);
                }
            } else {
                $_SESSION['errorMessage'][] = "You are not allowed to do this opperation";
                return $this->redirect("/admin");
            }
        }

        parent::printFlush($this->request->here());
        $this->set("admin", parent::getAdmin() | parent::getSuperUser());
        $this->set("reviewer", parent::getReviewer());
        $this->set("program_manager", parent::getProgramManager());

        $formAction = "add";            

        // check, if are passed any arguments
        if(!empty($action)){
            // supported are arguments /.../action/event-id
            if((count($action) <= 1 && $action[0] != "add") || (count($action) >= 2 && !is_numeric($action[1]))){
                //not enough arguments error message  + redirect
                $_SESSION['errorMessage'][] = "Unsufficient data. Your link is probably broken.";
                return $this->redirect("/admin"); 
            }
            if(!parent::getAdminForEvent($action[1])){
                $_SESSION['errorMessage'][] = "You are not allowed to do this opperation.";
                return $this->redirect("/admin");
            }
            //for edit action
            if($action[0] == "edit"){
                $results = $connection->execute('SELECT * FROM cfp.events WHERE id = ' . $action[1])->fetchAll('assoc');
                $event_topics = array();
                $evTo = $connection->execute('SELECT topic_id FROM cfp.topics_to_events WHERE event_id = ' . $action[1])->fetchAll('assoc');
                foreach($evTo as $et){
                    $event_topics [] = $et['topic_id'];
                }
                $formAction = "update";

            //for delete action
            } else if ($action[0] == "delete") {
                if(count($action) > 2){
                    if($_SESSION['token'] == $action[2]){
                        return $this->delete($connection,$action[1]);
                    } else {
                        $_SESSION['errorMessage'][] = "This action can't be performet - invalid token.";
                        return $this->redirect("/admin");
                    }
                } else {
                    $_SESSION['errorMessage'][] = "This action can't be performet - unsufficient data. Your link is probably broken.";
                    return $this->redirect("/admin");
                }
            } else if($action[0] == "duplicate") {
                if(count($action) > 2){
                    if($_SESSION['token'] == $action[2]){
                        return $this->duplicate($connection,$action[1]);
                    } else {
                        $_SESSION['errorMessage'][] = "This action can't be performet - invalid token.";
                        return $this->redirect("/admin");
                    }
                } else {
                    $_SESSION['errorMessage'][] = "This action can't be performet - unsufficient data. Your link is probably broken.";
                    return $this->redirect("/admin");
                }
            }
        }

        $topics = $connection->execute('SELECT * FROM cfp.topics')->fetchAll('assoc');
        if(isset($results)){
            $results[0]['topics'] = $event_topics;
            $this->set("event", $results[0]);
        }else{
            $this->set("event", "");
        }
        $this->set("action", $formAction);
        $this->set("topics", $topics);
        $this->set("active", "admin");
        $token = $this->request->getParam('_csrfToken');

        $_SESSION['token'] = $token;
        $this->set("token", $token);
        $this->set('username', $_SESSION['first-name']." ".$_SESSION['last-name']);

        try {
            $this->render('event');
        } catch (MissingTemplateException $exception) {
            if (Configure::read('debug')) {
                throw $exception;
            }
            throw new NotFoundException();
        }
    }

    public function add($connect){
        foreach($_POST as $key=>$val){
            if(!is_numeric($_POST[$key]) && !is_array($_POST[$key])){
                $_POST[$key] = "'" . $this->sanity($val) . "'";
            }
        }
        if($_FILES['logo']['name'] != ""){
            $name = "/webroot/img/logos/" . str_replace(array("'","/"),"",$_POST['name']) . "_" . $_POST['year'] . "_" . $_FILES['logo']['name'];
            move_uploaded_file($_FILES['logo']['tmp_name'], WWW_ROOT . "img/logos/" . str_replace(array("'", "/"),"", $_POST['name']) . "_" . $_POST['year'] . "_" . $_FILES['logo']['name']);
        } else {
            $name = "";
        }
        try{
            $res = $connect->execute("INSERT INTO cfp.events (name, logo, cfp_open, cfp_close, open, close, year, url, redhat_event, for_redhat, blind_review, code_to, collect_data,"
                . " accommodation, schedule, confirmation_info, timezone)"
                . " VALUES (" . $_POST['name']. ", '". $name . "', " . $_POST['cfp_open'] . ", " . $_POST['cfp_close'] . ", " . $_POST['open'] . ", " . $_POST['close'] . ", " . $_POST['year'] . ", ". $_POST['url'] . ", "
                        . (isset($_POST['redhat_event'])?1:0) . ", " . (isset($_POST['for_redhat'])?1:0) . ", " . (isset($_POST['blind_review'])?1:0) 
                . ", " . (isset($_POST['code_to'])?1:0) . ", " . (isset($_POST['collect_data'])?1:0) . ", " . (isset($_POST['accommodation'])?1:0) . ", " . (isset($_POST['schedule'])?1:0) . ", " . $_POST['confirmation_info']
                . ", " . $_POST['timezone'] . ")");
            $eventId = $connect->execute("SELECT LAST_INSERT_ID() as id")->fetchAll('assoc')[0]['id'];
            if(isset($_POST['topic'])){
                $topicsSQL = "INSERT INTO cfp.topics_to_events (event_id, topic_id) VALUES ";
                $topicsValues = array();
                foreach($_POST['topic'] as $t){
                    $topicsValues[] = "(" . $eventId . ", " . $t . ")";
                }
                if(sizeof($topicsValues) > 0){
                    $connect->execute($topicsSQL . implode(", ", $topicsValues));
                }
            }
            $connect->execute("INSERT INTO cfp.smtps (event_id) VALUES (" . $eventId . ")");
            $connect->execute("INSERT INTO cfp.emails (event_id, type, form_type) VALUES (" . $eventId . ", 'confirmedSpeakers', 'event'), (" . $eventId . ", 'confirmation', 'event')");
            if(!parent::getSuperUser()){
                $connect->execute("INSERT INTO cfp.admins (event_id, user_id) VALUES (" . $eventId . ", '" . $_SESSION['loggedUser'] . "')");
            }
            $_SESSION['successMessage'][] = "Event has been successfully created.";
        }catch(\Exception $e){
            $_SESSION['errorMessage'][] = "Event can't be created.";
        }    
        return $this->redirect("/admin");
    }

    public function update($connect, $id){
        if(parent::getAdminForEvent($id)){

			$accomm = $connect->execute("SELECT accommodation FROM events WHERE id = " . $id)->fetch("assoc");
            foreach($_POST as $key=>$val){
                if(!is_numeric($_POST[$key]) && !is_array($_POST[$key])){
                    $_POST[$key] = "'" . $this->sanity($val) . "'";
                }
            }
            if($_FILES['logo']['name'] != ""){
                $name = "/webroot/img/logos/" . str_replace(array("'","/"),"",$_POST['name']) . "_" . $_POST['year'] . "_" . $_FILES['logo']['name'];
                move_uploaded_file($_FILES['logo']['tmp_name'], WWW_ROOT . "img/logos/" . str_replace(array("'","/"),"", $_POST['name']) . "_" . $_POST['year'] . "_" . $_FILES['logo']['name']);
            } else {
                $name = "";
            }
            try{
                $res = $connect->execute("UPDATE cfp.events SET `name`=" . $_POST['name']. ", `cfp_open`=" . $_POST['cfp_open'] . ", `cfp_close`=" . $_POST['cfp_close']
                    . ", `open`=" . $_POST['open'] . ", `close`=" . $_POST['close'] . ", `year`=". $_POST['year'] . ", `url`=" . $_POST['url'] . ", `redhat_event` = " 
                    . (isset($_POST['redhat_event'])?1:0) . ", `confirmation_info` = " . $_POST['confirmation_info'] . ", `timezone`=" . $_POST['timezone']
                    . ", `for_redhat` = " . (isset($_POST['for_redhat'])?1:0) . ", `blind_review` = " . (isset($_POST['blind_review'])?1:0) . ", `collect_data` = " . (isset($_POST['collect_data'])?1:0)
                    . ", `accommodation` = " . (isset($_POST['accommodation'])?1:0) .  ", `schedule` = " . (isset($_POST['schedule'])?1:0)
                    . ", `code_to` = " . (isset($_POST['code_to'])?1:0) . ($name != ''?", logo='".$name."'":'') . " WHERE `id` = " . $id);

				if(!isset($_POST['accommodation']) && $accomm['accommodation'] == 1){
					$connect->execute("DELETE FROM cfp.emails WHERE event_id = " . $id . " AND form_type LIKE ('accommodation')");
					$connect->execute("DELETE FROM cfp.hotels WHERE event_id = " . $id);
					$connect->execute("DELETE FROM cfp.hotel_rooms WHERE event_id = " . $id);
				}
				
				if(isset($_POST['topic'])){
                    $current = $connect->execute("SELECT GROUP_CONCAT(topic_id) as topics FROM cfp.topics_to_events WHERE event_id = " . $id)->fetch("assoc");
                    $ctopics = explode(",", $current['topics']);
                    if(sizeof(array_diff($_POST['topic'], $ctopics)) != 0 || sizeof(array_diff($ctopics, $_POST['topic'])) != 0){
                        $connect->execute("DELETE FROM cfp.topics_to_events WHERE event_id = " . $id);
                        $topicsSQL = "INSERT INTO cfp.topics_to_events (event_id, topic_id) VALUES ";
                        $topicsValues = array();
                        foreach($_POST['topic'] as $t){
                            $topicsValues[] = "(" . $id . ", " . $t . ")";
                        }
                        if(sizeof($topicsValues) > 0){
                            $connect->execute($topicsSQL . implode(", ", $topicsValues));
                            $_SESSION['warningMessage'][] = "Time slots per track has been set to 0.";
                        }
                    }
                }
                $_SESSION['successMessage'][] = "Event has been successfully modified.";
            }catch(\Exception $e){
                $_SESSION['errorMessage'][] = "Event can't be updated. " . $e->getMessage();
            }
            return $this->redirect("/admin");    
        } else {
            $_SESSION['errorMessage'][] = "You are not allowed to update this event.";
            return $this->redirect("/admin");
        }
    }

    public function delete($connect, $key){
        if(parent::getAdminForEvent($key)){
            try{
                $file = $connect->execute("SELECT logo FROM cfp.events WHERE id = " . $key)->fetchAll('assoc');
                $f = new File(str_replace("webroot", $file[0]['logo'], WWW_ROOT));
                if( $f->exists() ){
                    $f->delete();
                }
                $res = $connect->execute("DELETE FROM cfp.events WHERE id = " . $key);
                $connect->execute("DELETE FROM cfp.smtps WHERE event_id = " . $key);
                $connect->execute("DELETE FROM cfp.emails WHERE event_id = " . $key);
                $connect->execute("DELETE FROM cfp.accepts WHERE event_id = " . $key);
                $connect->execute("DELETE FROM cfp.managers WHERE event_id = " . $key);
                $connect->execute("DELETE FROM cfp.responses_to_topics WHERE event_id = " . $key);
                $connect->execute("DELETE FROM cfp.reviewers WHERE event_id = " . $key);
                $connect->execute("DELETE FROM cfp.votes WHERE event_id = " . $key);
                $connect->execute("DELETE FROM cfp.hotel_booking WHERE event_id = " . $key);
                $connect->execute("DELETE FROM cfp.codes WHERE event_id = " . $key);
                $connect->execute("DELETE FROM cfp.topics_to_events WHERE event_id = " . $key);

                $responses = $connect->execute("SELECT id FROM responses WHERE event_id = " . $key)->fetchAll("assoc");
                $answers = $connect->execute("SELECT id FROM answers WHERE event_id = " . $key)->fetchAll("assoc");
                $forms = $connect->execute("SELECT id FROM forms WHERE event_id = " . $key)->fetchAll("assoc");

                foreach ($responses as $r){
                    $connect->execute("DELETE FROM cfp.comments WHERE response_id = " . $r['id']);
                    $connect->execute("DELETE FROM cfp.additional_speakers WHERE response_id = " . $r['id']);
                }
                $connect->execute("DELETE FROM cfp.responses WHERE event_id = " . $key);

                foreach ($answers as $a){
                    $connect->execute("DELETE FROM cfp.answer_parts WHERE answer_id = " . $a['id']);
                    $connect->execute("DELETE FROM cfp.additional_speakers WHERE answer_id = " . $a['id']);
                }
                $connect->execute("DELETE FROM cfp.answers WHERE event_id = " . $key);

                foreach ($forms as $f){
                    $connect->execute("DELETE FROM cfp.questions WHERE form_id = " . $f['id']);
                    $connect->execute("DELETE FROM cfp.question_options WHERE form_id = " . $f['id']);
                }
                $connect->execute("DELETE FROM cfp.forms WHERE event_id = " . $key);
                
                $connect->execute("DELETE FROM cfp.hotels WHERE event_id = " . $key);
				$connect->execute("DELETE FROM cfp.hotel_rooms WHERE event_id = " . $key);

                $connect->execute("UPDATE cfp.users SET reviewer = 0 WHERE reviewer = 1 AND id NOT IN (SELECT user_id FROM cfp.reviewers)");
                $connect->execute("UPDATE cfp.users SET program_manager = 0 WHERE program_manager = 1 AND id NOT IN (SELECT user_id FROM cfp.managers)");

                $_SESSION['successMessage'][] = "Event has been successfully removed.";
            }catch(\Exception $e){
                $_SESSION['errorMessage'][] = "Event can't be removed.";
            }
            return $this->redirect("/admin");
        } else {
            $_SESSION['successMessage'][] = "You are not allowed to delete this event.";
            return $this->redirect("/admin");
        }
    }

    public function sanity($string){
        return str_replace(array("'", "\""), array("\'", "\\\""), $string);
    }

    public function duplicate($connect, $key){
        try{
            $event = $connect->execute("SELECT name, open, close, schedule, cfp_open, cfp_close, year, url, redhat_event, for_redhat, blind_review, code_to, topics, collect_data, confirmation_info, accommodation"
                . " FROM cfp.events WHERE id = " . $key)->fetchAll("assoc")[0];
            $connect->execute("INSERT INTO events (name, open, close, cfp_open, cfp_close, year, url, redhat_event, for_redhat, blind_review, code_to, topics, collect_data, confirmation_info, accommodation, schedule) VALUES "
                . "('" . $this->sanity($event['name']) . " (copy)', '" . $this->sanity($event['open']) . "', '" . $this->sanity($event['close']) . "', '"
                . $this->sanity($event['cfp_open']) . "', '" . $this->sanity($event['cfp_close']) . "', ". $event['year'] . ", '" . $this->sanity($event['url']) . "', " . $event['redhat_event'] . ", "
                . $event['for_redhat'] . ", " . $event['blind_review'] . ", ". $event['code_to'] . ", '" . $event['topics'] . "', " . $event['collect_data'] . ", '" . $event['confirmation_info'] . "', "
                . $event['accommodation'] . "," . $event['schedule'] . ")");
            $newId = $connect->execute("SELECT LAST_INSERT_ID() as id")->fetchAll('assoc')[0]['id'];
            $smtp = $connect->execute("SELECT * FROM cfp.smtps WHERE event_id = " . $key)->fetchAll("assoc");
            $mail = $connect->execute("SELECT * FROM cfp.emails WHERE event_id = " . $key)->fetchAll("assoc");
            $forms = $connect->execute("SELECT * FROM cfp.forms WHERE event_id = " . $key)->fetchAll("assoc");
            $rooms = $connect->execute("SELECT * FROM cfp.rooms WHERE event_id = " . $key)->fetchAll("assoc");

            foreach($smtp as $s){
                $connect->execute("INSERT INTO cfp.smtps (event_id, host, port, tls, username, password, nick) VALUES ".
                    "(" . $newId . ", '" . $s['host'] . "', " . ( $s['port'] !=null ? $s['port'] : 0) . ", " . $s['tls'] . ", '" . $s['username'] . "', '" . $s['password'] . "', '" . $this->sanity($s['nick']) . "')");
            }

            foreach($mail as $m){
                $connect->execute("INSERT INTO cfp.emails (event_id, type, text, subject, format, sumarize, form_type) VALUES ".
                    "(" . $newId. ", '" . $m['type'] . "', '" . $this->sanity($m['text']) . "', '" . $this->sanity($m['subject']) . "', '" . $m['format'] . "', " . $m['sumarize'] .", '" . $m['form_type'] ."')");
            }

            foreach($forms as $f){
                $connect->execute("INSERT INTO cfp.forms (event_id, type, one_response, speaker_limit, placeholder, form_order, color, text_color, schedule) VALUES ".
                    "(" . $newId . ", '" . $f['type'] . "', " . $f['one_response'] . ", " . $f['speaker_limit'] . ", '" . $f['placeholder'] . "', " . $f['form_order'] . ", '".
                    $f['color'] . "', '" . $f['text_color'] . "', " . $f['schedule'] . ")");    
                $newFormId = $connect->execute("SELECT LAST_INSERT_ID() as id")->fetch('assoc')['id'];
                $questions = $connect->execute("SELECT * FROM cfp.questions WHERE form_id = " . $f['id'] . " ORDER BY id")->fetchAll("assoc");

                foreach($questions as $q){
                    $connect->execute("INSERT INTO cfp.questions (form_id, id, question, type, help_text, size, take_as, question_order, required) VALUES ".
                        "(" . $newFormId . ", " . $q['id'] . ", '" . $this->sanity($q['question']) . "', '" . $q['type'] . "', '" . $this->sanity($q['help_text']) . "', " . 
                        ($q['size'] == NULL?"NULL":$q['size']) . ", '" . ($q['take_as'] == NULL?"NULL":$q['take_as']) . "', " . $q['question_order'] . ", " . $q['required'] . ")");

                    $qoptions = $connect->execute("SELECT * FROM cfp.question_options WHERE question_id = " . $q['id'] . " AND form_id = " . $f['id'] . " ORDER BY id")->fetchAll("assoc");
                    foreach($qoptions as $qo){
                        $connect->execute("INSERT INTO question_options (question_id, value, label, form_id, id) VALUES ".
                            "(" . $qo['question_id'] . ", '" . $this->sanity($qo['value']) . "', '" . $this->sanity($qo['label']) . "', " . $newFormId . ", " . $qo['id'] . ")");    
                    }
                }
            }

            foreach($rooms as $r){
                $connect->execute("INSERT INTO cfp.rooms (event_id, name, description, capacity, position, speaker_info) VALUES " .
                    "(" . $newId . ", '" . $r['name'] . "', '" . $r['description'] . "', " . $r['capacity'] . ", " . ($r['position'] ==""?"NULL":$r['position']) . ", '" . $r['speaker_info'] . "')");
            }

            if(!parent::getSuperUser()){
                $connect->execute("INSERT INTO cfp.admins (event_id, user_id) VALUES (" . $newId . ", '" . $_SESSION['loggedUser'] . "')");
            }
        }catch(\Exception $e){
            $_SESSION['errorMessage'][] = "There was an issue during duplication process. Some parts of origin event might not be correctly migrated.";
            $_SESSION['errorMessage'][] = $e->getMessage();
            return $this->redirect("/admin");
        }
        $_SESSION['successMessage'][] = "Event has been successfully duplicated.";
        return $this->redirect("/event/edit/" . $newId);
    }
}
