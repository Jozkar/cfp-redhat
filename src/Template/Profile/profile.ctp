<div class="container-fluid container-cards-pf container-pf-nav-pf-vertical">
    <ol class="breadcrumb" role="navigation" aria-label="breadcrumbs">
        <li><a href="/"><span class="fa fa-home"></span></a></li>
        <li class="active">Profile<span></span></li>
    </ol>
	<div class="row row-cards-pf">
		<div class="col-xs-12 ">
			<div class="card-pf card-pf-view">
				<h1 class="card-pf-title">My Profile</h1>
				<div class="card-pf-body">
					<form class="profile" action="/profile/<?= $action ?>" method="post">
						<input type="hidden" name="_Token" value="<?= $token ?>">
						<input type="hidden" name="_method" value="post">
						<div class="form-group">
							<label class="control-label" for="avatar">Avatar URL</label>
							<input type="text" name="avatar" class="form-control" value="<?= ($user['avatar'][0] == "/"?"":htmlspecialchars($user['avatar'])) ?>">
							<span class="help-block">Enter full URL path to your avatar image.</span>
							<img src="<?= $user['avatar'] ?>" style="width: 12%">
							<span class="help-block">Your current avatar.</span>
						</div>
						<div class="form-group">
							<label class="control-label" for="first-name"><span class='required-pf'>*</span> First Name</label>
							<input type="text" maxlength="512" class="form-control" value="<?= htmlspecialchars($user['first_name']) ?>" name="first_name" required>
							<span class="help-block">Enter your first name.</span>
						</div>
						<div class="form-group">
							<label class="control-label" for="last-name"><span class='required-pf'>*</span> Last Name</label>
							<input type="text" maxlength="512" class="form-control" value="<?= htmlspecialchars($user['last_name']) ?>" name="last_name" required>
							<span class="help-block">Enter your last name.</span>
						</div>
						<div class="form-group">
							<label class="control-label" for="email"><span class='required-pf'>*</span> Email</label>
							<input type="text" class="form-control" value="<?= $user['email'] ?>" name="email" required disabled>
							<span class="help-block">Your email address can't be modified.</span>
						</div>
						<div class="form-group">
							<label class="control-label" for="organization"><span class='required-pf'>*</span> Organization</label>
							<input type="text" maxlength="512" class="form-control" value="<?= htmlspecialchars($user['organization']) ?>" name="organization" required>
							<span class="help-block">Enter name of your organization.</span>
						</div>
						<div class="form-group">
							<label class="control-label" for="position"><span class='required-pf'>*</span> Position</label>
							<input type="text" maxlength="512" class="form-control" value="<?= htmlspecialchars($user['position']) ?>" name="position" required>
							<span class="help-block">Enter your work position.</span>
						</div>
						<div class="form-group">
							<label class="control-label" for="country">Country</label>
							<input type="text" maxlength="50" class="form-control" value="<?= htmlspecialchars($user['country']) ?>" name="country">
 							<span class="help-block">Enter name of your country in English (50 characters max).</span>
						</div>
						<div class="form-group">
							<label class="control-label" for="Bio"><span class='required-pf'>*</span> Bio</label>
							<textarea maxlength="1000" class="form-control" name="bio" required><?= htmlspecialchars($user['bio']) ?></textarea>
							<span class="help-block">Enter your bio.</span>
						</div>
						<p>Questions marked with <span class='required-pf'>*</span> are <strong>required</strong>.</p>
						<div class="form-group">
							<span>
								<input type="submit" class="btn btn-primary" name="save" value="Save">
							</span>
							<span>
								<a href="/" class="btn btn-default">Cancel</a>
							</span>
							<?php if($action == "update"){ ?>
							<span>
								<a href="#" class="btn btn-danger openModal">Delete profile</a>
							</span>
							<?php } ?>

						</div>
					</form> <!-- end form -->	

				</div> <!-- end card body -->
			</div> <!-- end card -->
        </div> <!-- cols -->
    </div> <!-- row -->
</div> <!-- container -->
<div class="modal fade" id="confirmations" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
	<div class="modal-dialog">
	        <div class="modal-content">
	                <div class="modal-header">
	                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" aria-label="Close">
 	                               <span class="pficon pficon-close"></span>
                                </button>
                                <h4 class="modal-title" id="myModalLabel">Confirm profile deletion</h4>
                        </div> <!-- modal header -->
                        <div class="modal-body">
                                <p>Do you really want to delete your profile? Keep in mind, that this action will remove also all your proposals, roles, comments and votes.</p>
                        </div>
                        <div class="modal-footer">
	                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                <a href="/profile/delete/<?= $token ?>" id="modalDelete" class="btn btn-danger">Delete</a>
                        </div> <!-- modal footer -->
                 </div> <!-- modal content -->
         </div> <!-- modal dialog -->
</div> <!-- modal -->

<script>
  $(document).ready(function() {
    // matchHeight the contents of each .card-pf and then the .card-pf itself
    $(".row-cards-pf > [class*='col'] > .card-pf .card-pf-title").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf > .card-pf-body").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf > .card-pf-footer").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf").matchHeight();

    // Initialize the vertical navigation
    $().setupVerticalNavigation(true);

	// autohide toasts
	$('.toast-pf').mouseenter(function () {
		$(this).attr('mouseenter','true');
	});

	$('.toast-pf').mouseleave(function () {
		$(this).removeAttr('mouseenter');
		if($(this).attr('remove') != null){
			$(this).remove();
		}		
	});

	function hideToasts() {
		var toasts = $('.toast-pf'), i;

		for(i = 0; i < toasts.length; i++){
			if($(toasts[i]).attr('mouseenter') != null){
				$(toasts[i]).attr('remove','true');
			} else {
				$(toasts[i]).hide('slow', function(){ $(toasts[i]).remove(); });
			}
		}
	}

	$(".openModal").click(function (){
                $("#confirmations").modal('show');
	});


	setTimeout(hideToasts, 8000);

  });
</script>
</body>
</html>
 
