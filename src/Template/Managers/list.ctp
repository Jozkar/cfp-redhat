<div class="container-fluid container-cards-pf container-pf-nav-pf-vertical nav-pf-persistent-secondary">
    <ol class="breadcrumb" role="navigation" aria-label="breadcrumbs">
        <li><a href="/"><span class="fa fa-home"></span></a></li>
        <li><a href="/admin">Administration</a></li>
        <li class="active"><span>Program Managers</span></li>
    </ol>
    <form method="post" autocomplete="off" action="/managers/<?= $eventId ?>/add">
  		<div class="form-group"><label>Add program manager for <?= $eventInfo['name'] . " (" . $eventInfo['year'] . ")" ?></label>
        <select class="combobox form-control" name="user">
            <option value="" selected="selected">Choose user (start typing name or email)</option>
            <?php foreach($users as $u) { ?>
                <option value="<?= $u['email'] ?>"><?= $u['first_name'] . " " . $u['last_name'] . " (" . $u['email'] . ")" ?></option>
            <?php } ?>
        </select>
		</div>
		<div class="form-group">
		    <input type="hidden" name="_Token" value="<?= $token ?>">
		    <input type="hidden" name="_method" value="post">        
		    <input class="btn btn-primary" type="submit" name="save" value="Add user">
		</div>
    </form>
    <div class="list-group list-view-pf list-view-pf-view">
	<?php foreach($managers as $a){ ?>
	<div class="list-group-item">
            <div class="list-view-pf-actions">
                <a class="btn btn-danger btn-lg" href="/managers/<?= $eventId ?>/delete/<?= $a['id'] . "/" . $token ?>" data-toggle="tooltip" data-placement="top" title="Remove program manager">
		    Remove program manager
		</a>
            </div>
			<div class="list-view-pf-main-info">
				<div class="list-view-pf-left">
					<span class="fa list-view-pf-icon-sm" style="border: none !important">
							<img src="<?= $a['avatar'] ?>" style="width: 100%; object-fit: contain">
					</span>
				</div>
                <div class="list-view-pf-body">
                    <div class="list-view-pf-description">
                        <div class="list-group-item-text">
                			<strong><?= $a['first_name'] . " " . $a['last_name'] ?></strong> (<?= $a['email'] ?>)
                        </div>
                    </div> <!-- description -->
                </div>
			</div> 
	        </div> <!-- cols -->
	<?php } ?>
    </div> <!-- row -->
</div> <!-- container -->
<script>
  $(document).ready(function() {
    // Initialize the vertical navigation
    $().setupVerticalNavigation(true);

    // Initialize Boostrap-Combobox
    $('.combobox').combobox();

    $('#NaN').attr('autocomplete','off');

	// autohide toasts
	$('.toast-pf').mouseenter(function () {
		$(this).attr('mouseenter','true');
	});

	$('.toast-pf').mouseleave(function () {
		$(this).removeAttr('mouseenter');
		if($(this).attr('remove') != null){
			$(this).remove();
		}		
	});

	function hideToasts() {
		var toasts = $('.toast-pf'), i;

		for(i = 0; i < toasts.length; i++){
			if($(toasts[i]).attr('mouseenter') != null){
				$(toasts[i]).attr('remove','true');
			} else {
				$(toasts[i]).hide('slow', function(){ $(toasts[i]).remove(); });
			}
		}
	}

	setTimeout(hideToasts, 8000);
  
  });
</script>
</body>
</html>
  
