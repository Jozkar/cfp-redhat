<div class="container-fluid container-cards-pf container-pf-nav-pf-vertical">
    <ol class="breadcrumb" role="navigation" aria-label="breadcrumbs">
        <li><a href="/"><span class="fa fa-home"></span></a></li>
        <li class="active"><span>Submit proposal</span></li>
    </ol>
    <h1>Submit proposals for <?= $event['name'] . " (" . $event['year'] . ")" ?></h1>
	<?php if(count($forms) < 1) { ?>
	<div class="blank-slate-pf">
        <div class="blank-slate-pf-icon">
            <span class="pficon pficon-info"></span>
        </div>
        <h1>There is no form available yet.</h1>

	<?php }else{ ?>
    <div id="pf-list-standard" class="row row-cards-pf">
    <div class="row row-cards-pf">
    <?php foreach($forms as $form) { ?>
	<div class="col-xs-12 col-sm-6 col-md-3">
             <div class="card-pf card-pf-view card-pf-aggregate-status">
                  <div class="card-pf-body">
                        <h2 class="card-pf-title text-center"><?= ($form['placeholder'] != ""? $inflector->pluralize(ucfirst($form['placeholder'])) : $inflector->pluralize(ucfirst($form['type']))) ?></h2>
                        <div class="card-pf-items text-center">
                             <div class="card-pf-item">
                                  <a href="/proposal/<?= $form['id'] ?>" data-toggle="tooltip" data-placement="top" title="<?= ($form['type'] != 'quiz'?"Create a proposal":"Answer questions") ?>">
                                      <span class="pficon pficon-add-circle-o"></span> <?= ($form['type'] != 'quiz'?"Create a proposal":"Answer questions") ?>
                                  </a>
                             </div>
                        </div>
                   </div>
             </div>
         </div> <!-- cols -->
	 <?php } ?>
    </div>
    </div>
    <?php } ?>
</div> <!-- container -->
<script>
  $(document).ready(function() {
    // matchHeight the contents of each .card-pf and then the .card-pf itself
    $(".row-cards-pf > [class*='col'] > .card-pf .card-pf-title").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf > .card-pf-body").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf > .card-pf-footer").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf").matchHeight();

    // Initialize the vertical navigation
    $().setupVerticalNavigation(true);

    // autohide toasts
    $('.toast-pf').mouseenter(function () {
	$(this).attr('mouseenter','true');
    });

    $('.toast-pf').mouseleave(function () {
	$(this).removeAttr('mouseenter');
	if($(this).attr('remove') != null){
		$(this).remove();
	}		
    });

    function hideToasts() {
	var toasts = $('.toast-pf'), i;

	for(i = 0; i < toasts.length; i++){
		if($(toasts[i]).attr('mouseenter') != null){
			$(toasts[i]).attr('remove','true');
		} else {
			$(toasts[i]).hide('slow', function(){ $(toasts[i]).remove(); });
		}
	}
    }

    setTimeout(hideToasts, 8000);

  });
</script>
</body>
</html>  
