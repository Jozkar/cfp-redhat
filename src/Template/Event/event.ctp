<div class="container-fluid container-cards-pf container-pf-nav-pf-vertical">
    <ol class="breadcrumb" role="navigation" aria-label="breadcrumbs">
        <li><a href="/"><span class="fa fa-home"></span></a></li>
        <li><a href="/admin">Administration</a></li>
        <li class="active"><span>Event</span></li>
    </ol>
	<div class="row row-cards-pf">
		<div class="col-xs-12 ">
			<div class="card-pf card-pf-view">
				<?php if ($action == "add"){ ?>
				<h1 class="card-pf-title">Create new event</h1>
				<?php } else { ?>
				<h1 class="card-pf-title">Modify event</h1>
				<?php } ?>
				<div class="card-pf-body">
					<form class="profile" enctype="multipart/form-data" action="/event/<?= ($action != "add"?$action . "/" . $event['id']:$action) ?>" method="post">
						<input type="hidden" name="_Token" value="<?= $token ?>">
						<input type="hidden" name="_method" value="post">
						<div class="form-group">
							<label class="control-label" for="name">Name</label>
							<input type="text" maxlength="100" class="form-control" value="<?= (!empty($event)?$event['name']:'') ?>" name="name" required>
							<span class="help-block">Enter name of event.</span>
						</div>
						<div class="form-group">
							<label class="control-label" for="logo">Logo</label>
							<input type="file" name="logo">
						</div>
						<div class="form-group">
							<label class="control-label" for="url">URL</label>
							<input type="url" maxlength="1024" class="form-control" value="<?= (!empty($event)?$event['url']:'') ?>" name="url" required>
							<span class="help-block">Enter URL of event.</span>
						</div>
						<div class="form-group">
                            <label class="control-label" for="open">Event start date</label>
                            <input type="text" id="dateOpen" class="form-control bootstrap-datepicker" value="<?= (!empty($event)?$event['open']:'') ?>" name="open" required>
                            <span class="help-block">Enter date the event starts.</span>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="close">Event end date</label>
                            <input type="text" id="dateClose" class="form-control bootstrap-datepicker" value="<?= (!empty($event)?$event['close']:'') ?>" name="close" required>
                            <span class="help-block">Enter date when event ends.</span>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Choose event's time zone</label>
                            <select class="chosen-select form-control" data-placeholder="Choose time zone" name="timezone">
                                <option value=""></option>
                                <?php foreach(timezone_identifiers_list() as $t) { ?>
                                <option value="<?= $t ?>" <?= (!empty($event) && $t == $event['timezone']?"selected ":"") ?>><?= $t ?></option>
                                <?php } ?>
                            </select>
                            <span class="help-block">Set time zone as Europe/Rome etc. If empty, UTC is used as default.</span>
                        </div>
                        <div class="form-group">
							<label class="control-label" for="cfp_open">CfP open</label>
							<input type="text" id="cfpDateOpen" class="form-control bootstrap-datepicker" value="<?= (!empty($event)?$event['cfp_open']:'') ?>" name="cfp_open" required>
							<span class="help-block">Enter date since CfP form should be available to users.</span>
						</div>
						<div class="form-group">
							<label class="control-label" for="cfp_close">CfP close</label>
							<input type="text" id="cfpDateClose" class="form-control bootstrap-datepicker" value="<?= (!empty($event)?$event['cfp_close']:'') ?>" name="cfp_close" required>
							<span class="help-block">Enter date when CfP form should be closed to users.</span>
						</div>
						<div class="form-group">
							<label class="control-label" for="year">Year</label>
							<input type="number" maxlength="4" class="form-control" value="<?= (!empty($event)?$event['year']:'') ?>" name="year" required>
 							<span class="help-block">Enter year of the event. (YYYY)</span>
						</div>
						<div class="form-group">
							<label class="control-label" for="redhat_event">Organized by Red Hat?</label>
							<input class="form-control bootstrap-switch" type="checkbox" value="1" <?= (!empty($event) && $event['redhat_event']?'checked ':'') ?>name="redhat_event">
							<span class="help-block">Will the event use this website for CfP?</span>
						</div>
						<div class="form-group">
							<label class="control-label" for="for_redhat">Red Hat internal event?</label>
							<input class="form-control bootstrap-switch" type="checkbox" value="1" <?= (!empty($event) && $event['for_redhat']?'checked ':'') ?>name="for_redhat">
							<span class="help-block">Should be this event available to Red Hatters only?</span>
						</div>
						<div class="form-group">
							<label class="control-label" for="blind_review">Allow blind review for reviewers?</label>
							<input class="form-control bootstrap-switch" type="checkbox" value="1" <?= (!empty($event) && $event['blind_review']?'checked ':'') ?>name="blind_review">
							<span class="help-block">Used when Organized by Red Hat set true.</span>
						</div>
                        <div class="form-group">
                            <label class="control-label" for="blind_review">Allow data collection after submission confirmed?</label>
                            <input class="form-control bootstrap-switch" type="checkbox" value="1" <?= (!empty($event) && $event['collect_data']?'checked ':'') ?>name="collect_data">
                            <span class="help-block">After submission acceptance and confirmation, user will be able to fill 'collect data' form. Used when Organized by Red Hat set true.</span>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="confirmation_info">Information for users when confirming proposals</label>
                            <textarea maxlength="65530" class="form-control" name="confirmation_info"><?= (!empty($event) && $event['confirmation_info']?htmlspecialchars($event['confirmation_info']):'') ?></textarea>
                            <span class="help-block">This text is shown to speakers on page with list of their confirmed proposals.</span>
                        </div>
						<div class="form-group">
							<label class="control-label" for="code_to">Assign generated code to secondary speakers too?</label>
							<input class="form-control bootstrap-switch" type="checkbox" value="1" <?= (!empty($event) && $event['code_to']?'checked ':'') ?>name="code_to">
							<span class="help-block">Used when Organized by Red Hat set true.</span>
						</div>
						<div class="form-group">
							<label class="control-label" for="accommodation">Manage accommodation via CfP service?</label>
							<input class="form-control bootstrap-switch" type="checkbox" value="1" <?= (!empty($event) && $event['accommodation']?'checked ':'') ?>name="accommodation">
							<span class="help-block">Used when Organized by Red Hat set true. By enabling/disabling new options and email templates will become available/will be removed.</span>
						</div>
                        <div class="form-group">
                            <label class="control-label" for="schedule">Use CfP service for scheduling?</label>
                            <input class="form-control bootstrap-switch" type="checkbox" value="1" <?= (!empty($event) && $event['schedule']?'checked ':'') ?>name="schedule">
                            <span class="help-block">Used when Organized by Red Hat set true. By enabling/disabling new options will become available/will be removed.</span>
                        </div>
						<div class="form-group">
                            <label class="control-label">Choose topics for this event</label>
                            <select class="chosen-select form-control" multiple data-placeholder="Choose topics" name="topic[]">
                                <option value=""></option>
                                <?php foreach($topics as $t) { ?>
                                <option value="<?= $t['id'] ?>" <?= (!empty($event) && in_array($t['id'], $event['topics'])?"selected ":"") ?>><?= $t['name'] ?></option>
                                <?php } ?>
                            </select>
							<span class="help-block">Applicable only for events organized by Red Hat.</span>
						</div>
						<div class="form-group">
							<span>
								<input type="submit" class="btn btn-primary" name="save" value="Save">
							</span>

						</div>
					</form> <!-- end form -->	

				</div> <!-- end card body -->
			</div> <!-- end card -->
        </div> <!-- cols -->
    </div> <!-- row -->
</div> <!-- container -->
<script>
  $(document).ready(function() {
    // matchHeight the contents of each .card-pf and then the .card-pf itself
    $(".row-cards-pf > [class*='col'] > .card-pf .card-pf-title").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf > .card-pf-body").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf > .card-pf-footer").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf").matchHeight();

	// Initialize switch buttons
	$(".bootstrap-switch").bootstrapSwitch();

    $(".chosen-select").chosen();

	var startDate = $("#dateOpen");
	var cfpStartDate = $("#cfpDateOpen");
	var endDate = $("#dateClose");
	var cfpEndDate = $("#cfpDateClose");

	startDate.datepicker({
		format: 'yyyy-mm-dd',
		todayBtn: true,
		todayHighlight: true
	});

    cfpStartDate.datepicker({
        format: 'yyyy-mm-dd',
        todayBtn: true,
        todayHighlight: true
    });

	endDate.datepicker({
        format: 'yyyy-mm-dd',
        todayBtn: true,
        todayHighlight: true
    });

    cfpEndDate.datepicker({
        format: 'yyyy-mm-dd',
        todayBtn: true,
        todayHighlight: true
    });

    // Initialize the vertical navigation
    $().setupVerticalNavigation(true);

    var editor = new Jodit($("textarea[name=confirmation_info]")[0], {
                         "disablePlugins": "symbols,stat,media,imageproperties,imageprocessor,fullsize,draganddropelement,draganddrop,pastestorage,autofocus,addnewline",
                         "buttonsXS": "bold,strikethrough,underline,italic,|,ul,ol,outdent,indent,|,fontsize,link,|,undo,redo,cut,copy,paste,|",
                         "buttonsMD": "bold,strikethrough,underline,italic,|,ul,ol,outdent,indent,|,fontsize,link,|,undo,redo,cut,copy,paste,|",
                         "buttonsSM": "bold,strikethrough,underline,italic,|,ul,ol,outdent,indent,|,fontsize,link,|,undo,redo,cut,copy,paste,|",
                         "buttons": "bold,strikethrough,underline,italic,|,ul,ol,outdent,indent,|,fontsize,link,|,undo,redo,cut,copy,paste,|",
            });
  });
</script>
</body>
</html>
 
