<!DOCTYPE html>
<html class="layout-pf layout-pf-fixed transitions" lang="en">
<head>
<title>CfP Portal</title>
<!-- PatternFly Styles -->
<!-- Note: No other CSS files are needed regardless of what other JS packages located in patternfly/components that you decide to pull in -->
<link rel="icon" type="image/x-icon" href="/favicon.ico">
<link rel="stylesheet" href="/webroot/node_modules/patternfly/dist/css/patternfly.min.css">
<link rel="stylesheet" href="/webroot/node_modules/patternfly/dist/css/patternfly-additions.min.css">
<link rel="stylesheet" href="/webroot/node_modules/jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" href="/webroot/node_modules/chosen-js/chosen.css">
<link rel="stylesheet" href="/webroot/node_modules/jodit/build/jodit.min.css">
<link rel="stylesheet" href="/webroot/css/dad.css">

<!-- jQuery -->
<script src="/webroot/node_modules/jquery/dist/jquery.min.js"></script>

<!-- jQuery UI -->
<script src="/webroot/node_modules/jquery-ui/jquery-ui.min.js"></script>

<!-- Bootstrap JS -->
<script src="/webroot/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- Datatables, jQuery Grid Component -->
<!-- Note: jquery.dataTables.js must occur in the html source before patternfly*.js.-->
<script src="/webroot/node_modules/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="/webroot/node_modules/drmonty-datatables-colvis/js/dataTables.colVis.js"></script>
<script src="/webroot/node_modules/datatables.net-colreorder/js/dataTables.colReorder.js"></script>

<!-- PatternFly Custom Componets -  Sidebar, Popovers and Datatables Customizations -->
<!-- Note: jquery.dataTables.js must occur in the html source before patternfly*.js.-->
<script src="/webroot/node_modules/patternfly/dist/js/patternfly.min.js"></script>

<!-- Bootstrap Combobox -->
<script src="/webroot/node_modules/patternfly-bootstrap-combobox/js/bootstrap-combobox.js"></script>

<!-- Chosen Multiple Selector -->
<script src="/webroot/node_modules/chosen-js/chosen.jquery.js"></script>

<!-- Jodit editor -->
<script src="/webroot/node_modules/jodit/build/jodit.min.js"></script>

<!-- Bootstrap Date Picker -->
<script src="/webroot/node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>

<!-- Moment - required by Bootstrap Date Time Picker -->
<script src="/webroot/node_modules/moment/min/moment.min.js"></script>

<!-- Bootstrap Date Time Picker - requires Moment -->
<script src="/webroot/node_modules/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

<!-- Bootstrap Select -->
<script src="/webroot/node_modules/bootstrap-select/dist/js/bootstrap-select.min.js"></script>

<!-- Bootstrap Switch -->
<script src="/webroot/node_modules/bootstrap-switch/dist/js/bootstrap-switch.min.js"></script>

<!-- Bootstrap Touchspin -->
<script src="/webroot/node_modules/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js"></script>

<!-- Google Code Prettify - Syntax highlighting of code snippets -->
<script src="/webroot/node_modules/google-code-prettify/bin/prettify.min.js"></script>

<!-- MatchHeight - Used to make sure dashboard cards are the same height -->
<script src="/webroot/node_modules/jquery-match-height/jquery.matchHeight.js"></script>
</head>
<body class="cards-pf">
<nav class="navbar navbar-pf-vertical">
  <div class="navbar-header">
    <button type="button" class="navbar-toggle">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a href="/" class="navbar-brand">
      <img class="navbar-brand-icon" src="" alt=""/><img class="navbar-brand-name" src="/webroot/img/logo.png" alt="Call for Paper Web Site" />
    </a>
    
  </div>
  <nav class="collapse navbar-collapse">
    <ul class="nav navbar-nav navbar-right navbar-iconic navbar-utility">
      <li class="dropdown">
        <button class="btn btn-link dropdown-toggle nav-item-iconic" id="dropdownMenuHelp" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
          <span title="Help" class="fa pficon-help"></span>
        </button>
        <ul class="dropdown-menu" aria-labelledby="dropdownMenuHelp">
          <li><a href="/faq">Frequently Asked Questions</a></li>
	  <li><a href="mailto:jridky@redhat.com">Contact author</a></li>
        </ul>
      </li> 
      <li class="dropdown">
        <button class="btn btn-link dropdown-toggle nav-item-iconic" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
          <span title="Username" class="fa pficon-user"></span>
          <span class="dropdown-title">
            <?= $username ?> <span class="caret"></span>
          </span>
        </button>
        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
          <li><a href="/profile">User profile</a></li>
          <li><a href="/logout">Logout</a></li>
        </ul>
      </li>
    </ul>
  </nav>
  <!-- Menu -->
  <div class="nav-pf-vertical nav-pf-vertical-with-sub-menus hide-nav-pf">
  <ul class="list-group">
    <li class="list-group-item<?= ($active=="home"?" active":"") ?>">
      <a href="/">
        <span class="fa pficon-catalog" data-toggle="tooltip" title="CfP catalog"></span>
        <span class="list-group-item-value">CfP catalog</span>
      </a>
    </li>
    <li class="list-group-item<?= ($active=="proposal"?" active":"") ?>">
      <a href="/myproposals">
        <span class="fa pficon-applications" data-toggle="tooltip" title="My proposals"></span>
        <span class="list-group-item-value">My proposals</span>
      </a>
    </li>
    <li class="list-group-item<?= ($active=="accept"?" active":"") ?>">
      <a href="/acceptproposals">
        <span class="fa fa-check" data-toggle="tooltip" title="My accepted proposals"></span>
        <span class="list-group-item-value">My accepted</span>
      </a>
    </li>
    <?php if ($reviewer) { ?>
    <li class="list-group-item<?= ($active=="reviewer"?" active":"") ?>">
      <a href="/reviewer">
        <span class="fa pficon-rebalance" data-toggle="tooltip" title="Review tasks"></span>
        <span class="list-group-item-value">Review tasks</span>
      </a>
    </li>
    <?php } if ($program_manager) { ?>
    <li class="list-group-item<?= ($active=="manager"?" active":"") ?>">
      <a href="/manager">
        <span class="fa pficon-build" data-toggle="tooltip" title="PM Review"></span>
        <span class="list-group-item-value">PM Review</span>
      </a>
    </li>
    <?php } if ($admin) { ?>
    <li class="list-group-item<?= ($active=="admin"?" active":"") ?>">
      <a href="/admin">
        <span class="fa fa-tachometer" data-toggle="tooltip" title="Administration"></span>
        <span class="list-group-item-value">Administration</span>
      </a>
    </li>
    <?php } ?>
     
    
    <li class="list-group-item secondary-nav-item-pf mobile-nav-item-pf visible-xs-block">
      <a href="#0">
        <span class="pficon pficon-user" data-toggle="tooltip" title="" data-original-title="User"></span>
        <span class="list-group-item-value dropdown-title"><?= $username ?></span>
      </a>
      <div id="user-secondary" class="nav-pf-secondary-nav">
        <div class="nav-item-pf-header">
          <a href="#0" class="secondary-collapse-toggle-pf" data-toggle="collapse-secondary-nav"></a>
          <span><?= $username ?></span>
        </div>

        <ul class="list-group">
          <li class="list-group-item">
            <a href="/profile">
              <span class="list-group-item-value">User profile</span>
            </a>
          </li>

          <li class="list-group-item">
            <a href="/logout">
              <span class="list-group-item-value">Logout</span>
            </a>
          </li>
        </ul>
      </div>
    </li>
  </ul>
</div>
</nav>
<div class="toast-notifications-list-pf">
    <?= $this->Flash->render(); ?>
</div>
<!-- end Menu -->
<?= $this->fetch('content') ?>
