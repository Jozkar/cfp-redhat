<!DOCTYPE html>
<html class="layout-pf layout-pf-fixed transitions" lang="en">
<head>
<title>CfP Portal</title>
<!-- PatternFly Styles -->
<!-- Note: No other CSS files are needed regardless of what other JS packages located in patternfly/components that you decide to pull in -->
<link rel="icon" type="image/x-icon" href="/favicon.ico">
<link rel="stylesheet" href="/webroot/node_modules/patternfly/dist/css/patternfly.min.css">
<link rel="stylesheet" href="/webroot/node_modules/patternfly/dist/css/patternfly-additions.min.css">

<!-- Google Sign In -->
<script src="https://apis.google.com/js/platform.js" async defer></script>

<!-- jQuery -->
 <script src="/webroot/node_modules/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap JS -->
<script src="/webroot/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- Datatables, jQuery Grid Component -->
<!-- Note: jquery.dataTables.js must occur in the html source before patternfly*.js.-->
<script src="/webroot/node_modules/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="/webroot/node_modules/drmonty-datatables-colvis/js/dataTables.colVis.js"></script>
<script src="/webroot/node_modules/datatables.net-colreorder/js/dataTables.colReorder.js"></script>

<!-- PatternFly Custom Componets -  Sidebar, Popovers and Datatables Customizations -->
<!-- Note: jquery.dataTables.js must occur in the html source before patternfly*.js.-->
<script src="/webroot/node_modules/patternfly/dist/js/patternfly.min.js"></script>

<!-- Bootstrap Combobox -->
<script src="/webroot/node_modules/patternfly-bootstrap-combobox/js/bootstrap-combobox.js"></script>

<!-- Bootstrap Date Picker -->
<script src="/webroot/node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>

<!-- Moment - required by Bootstrap Date Time Picker -->
<script src="/webroot/node_modules/moment/min/moment.min.js"></script>

<!-- Bootstrap Date Time Picker - requires Moment -->
<script src="/webroot/node_modules/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

<!-- Bootstrap Select -->
<script src="/webroot/node_modules/bootstrap-select/dist/js/bootstrap-select.min.js"></script>

<!-- Bootstrap Switch -->
<script src="/webroot/node_modules/bootstrap-switch/dist/js/bootstrap-switch.min.js"></script>

<!-- Bootstrap Touchspin -->
<script src="/webroot/node_modules/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js"></script>

<!-- Google Code Prettify - Syntax highlighting of code snippets -->
<script src="/webroot/node_modules/google-code-prettify/bin/prettify.min.js"></script>

<!-- MatchHeight - Used to make sure dashboard cards are the same height -->
<script src="/webroot/node_modules/jquery-match-height/jquery.matchHeight.js"></script>
</head>
<body class="cards-pf">
<nav class="navbar navbar-pf-vertical">
  <div class="navbar-header">
    <button type="button" class="navbar-toggle">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a href="/" class="navbar-brand">
      <img class="navbar-brand-icon" src="" alt=""/><img class="navbar-brand-name" src="/webroot/img/logo.png" alt="Call for Paper Web Site" />
    </a>
    
  </div>
  <!-- Menu -->
  <div class="nav-pf-vertical nav-pf-vertical-with-sub-menus">
  <ul class="list-group">
    <li class="list-group-item">
      <a href="/">
        <span class="fa pficon-catalog" data-toggle="tooltip" title="CfP catalog"></span>
        <span class="list-group-item-value">CfP catalog</span>
      </a>
    </li>
  </ul>
</div>
</nav>
<div class="toast-notifications-list-pf">
    <?= $this->Flash->render(); ?>
</div>
<!-- end Menu -->
<?= $this->fetch('content') ?>
