<!DOCTYPE html>
<html class="login-pf">
<head>
<title>Login - CfP Portal</title>

<!-- PatternFly Styles -->
<!-- Note: No other CSS files are needed regardless of what other JS packages located in patternfly/components that you decide to pull in -->
<link rel="icon" type="image/x-icon" href="/favicon.ico">
<link rel="stylesheet" href="webroot/node_modules/patternfly/dist/css/patternfly.min.css">
<link rel="stylesheet" href="webroot/node_modules/patternfly/dist/css/patternfly-additions.min.css">
<link rel="stylesheet" href="webroot/css/loginbuttons.css">

<!-- jQuery -->
 <script src="webroot/node_modules/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap JS -->
<script src="webroot/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- PatternFly Custom Componets -  Sidebar, Popovers and Datatables Customizations -->
<!-- Note: jquery.dataTables.js must occur in the html source before patternfly*.js.-->
<script src="webroot/node_modules/patternfly/dist/js/patternfly.min.js"></script>

</head>
<body>
<div class="toast-notifications-list-pf">
    <?= $this->Flash->render(); ?>
</div>
<?= $this->fetch('content') ?>
</body>
</html>
