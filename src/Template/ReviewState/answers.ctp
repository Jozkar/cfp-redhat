<div class="container-fluid container-cards-pf container-pf-nav-pf-vertical">
    <ol class="breadcrumb" role="navigation" aria-label="breadcrumbs">
        <li><a href="/"><span class="fa fa-home"></span></a></li>
     	<li><a href="/reviewer">Review tasks</a></li>
     	<li><a href="/capreview/<?= $event . "/" . $form['type'] ?>">Captain review - <?= ($form['placeholder'] != ""? $inflector->pluralize(ucfirst($form['type'])) . " (" . $inflector->pluralize(ucfirst($form['placeholder'])) . ")" : $inflector->pluralize(ucfirst($form['type']))) ?></a></li>
        <li class="active"><span>Review status</span></li>
    </ol>
	<h1>Review status of <?= ($form['placeholder'] != ""? $form['type'] . " (" . $form['placeholder'] . ")" : $form['type']) ?> proposals for <?= $eventInfo['name'] . " (" . $eventInfo['year'] . ")" ?></h1>
	<?php if(count($reviewers) < 1) { ?>
	<div class="blank-slate-pf">
        <div class="blank-slate-pf-icon">
            <span class="pficon pficon-info"></span>
        </div>
        <h1>No reviewers have been set to review this form yet.</h1>
        <p>Assign some body as reviewer for your topics to see the review status on this page.</p>

	<?php }else{ ?>
    <div class="tab-content">	
        <div class="list-group list-view-pf list-view-pf-equalized-column">
            <div class="list-group-item">
                 <div class="list-view-pf-main-info">
                    <div class="list-view-pf-body">
                        <div class="list-view-pf-description">
                             <div class="list-group-item-heading">
                                 Summary
                              </div>
                              <div class="list-group-item-text">
                                  <div class="list-view-pf-additional-info">
                                      <div class="list-view-pf-additional-info-item">
                                          <span class="pficon pficon-users"></span>
                                          <strong><?= count($reviewers) ?></strong> Reviewers
                                      </div>
                                      <div class="list-view-pf-additional-info-item">
                                          <span class="pficon pficon-<?= (count($reviewers) - $proposals['done'] == 0?"ok":"error-circle-o") ?>"></span>
                                          <strong><?= $proposals['done'] ?> / <?= count($reviewers) ?></strong> reviews done
                                      </div>
                                   </div>
                                </div>
                             </div>
                          </div>
                      </div>
                   </div>
                </div>
               	<table class="table table-striped table-bordered table-hover" style="background: white; height: auto !important">
            		<thead>
            			<tr>
    						<th>Reviewer</th>
       	    				<th>Accepted</th>
       	    				<th>Rejected</th>
       	    				<th>Skipped</th>
       	    				<th>Unreviewed</th>
       	    			</tr>
       	    		</thead>
               		<tbody>
		        		<?php foreach($reviewers as $r) {
                            ?>	
		        			<tr class="<?=($r['unreviewed'] == 0?"success":"danger")?>">
							<td><?= $r['name'] ?></td>
							<td><?= $r['accepted'] ?></td>
							<td><?= $r['rejected'] ?></td>
							<td><?= $r['skipped'] ?></td>
							<td><?= $r['unreviewed'] ?></td>
						</tr>
		        		<?php } ?>
	          		</tbody>
	       		</table>	      
	    	</div>
        </div>
        <?php } ?>
    </div> <!-- tab content -->
</div> <!-- container -->
<script>
  $(document).ready(function() {
    // matchHeight the contents of each .card-pf and then the .card-pf itself
    $(".row-cards-pf > [class*='col'] > .card-pf .card-pf-title").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf > .card-pf-body").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf > .card-pf-footer").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf").matchHeight();

    // Initialize the vertical navigation
    $().setupVerticalNavigation(true);
    
  });
</script>
</body>
</html>  
