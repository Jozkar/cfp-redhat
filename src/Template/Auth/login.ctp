<div class="login-pf-page">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 col-lg-6 col-lg-offset-3">
        <header class="login-pf-page-header">
          <img class="login-pf-brand" src="webroot/node_modules/patternfly/dist/img/redhat_reverse.png" alt=" logo" />
          <p>
            Welcome to the Red Hat Call for Proposals website.
          </p>
        </header>
        <div class="row">
          <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
            <div class="card-pf">
                <header class="login-pf-header">
                    <h1>Log In with your Red Hat account</h1>
                </header>
    	<?php if($services != null){
    		foreach($services as $s){ ?>
    		<div align="center" style="margin: 5px 0" onclick="window.location='/auth/<?= $s['service'] ?>'">
    			<div style="height:36px;width:180px;" class="abcRioButton abcRioButtonLightBlue">
    				<div class="abcRioButtonContentWrapper">
    					<div class="abcRioButtonIcon" style="padding:8px">
    						<div style="width:18px;height:18px;" class="abcRioButtonSvgImageWithFallback abcRioButtonIconImage abcRioButtonIconImage18">
    							 <img src="webroot/node_modules/patternfly/dist/img/<?= $s['service'] ?>-logo.svg" style="height: 18px" class="abcRioButtonSvg">
    						</div>
    					</div>
    					<span style="font-size:13px;line-height:34px;" class="abcRioButtonContents"><span>Sign in with <?= $sName[$s['service']] ?></span></span>
    				</div>
    			</div>
    		</div>
    	<?php   }
    		}else{?>
    		<div class="alert alert-danger">
    			<span class="pficon pficon-error-circle-o"></span> No Sign In options for this domain.
    		</div>
    		<?php } ?>


            </div><!-- card -->
        <footer class="login-pf-page-footer"></footer>
          </div><!-- col -->
        </div><!-- row -->
      </div><!-- col -->
    </div><!-- row -->
  </div><!-- container -->
</div><!-- login-pf-page -->
<script>
  $(document).ready(function() {
        // autohide toasts
        $('.toast-pf').mouseenter(function () {
                $(this).attr('mouseenter','true');
        });

        $('.toast-pf').mouseleave(function () {
                $(this).removeAttr('mouseenter');
                if($(this).attr('remove') != null){
                        $(this).remove();
                }               
        });

        function hideToasts() {
                var toasts = $('.toast-pf'), i;

                for(i = 0; i < toasts.length; i++){
                        if($(toasts[i]).attr('mouseenter') != null){
                                $(toasts[i]).attr('remove','true');
                        } else {
                                $(toasts[i]).hide('slow', function(){ $(toasts[i]).remove(); });
                        }
                }
        }

        setTimeout(hideToasts, 8000);

  });
</script>
