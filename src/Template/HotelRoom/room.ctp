<div class="container-fluid container-cards-pf container-pf-nav-pf-vertical">
    <ol class="breadcrumb" role="navigation" aria-label="breadcrumbs">
        <li><a href="/"><span class="fa fa-home"></span></a></li>
        <li><a href="/manager">PM Review</a></li>
        <li><a href="/hotels/<?= $event['id'] ?>"><?= $event['name'] . " (" . $event['year'] . ")" ?> - Hotels</a></li>
        <li><a href="/hotelrooms/<?= $hotel['id'] ?>"><?= $hotel['name'] ?></a></li>
        <li class="active"><span><?= ($action == "add"?"Add new hotel room":"Modify hotel room") ?></span></li>
    </ol>
	<div class="row row-cards-pf">
		<div class="col-xs-12 ">
			<div class="card-pf card-pf-view">
				<?php if ($action == "add"){ ?>
				<h1 class="card-pf-title">Add new hotel room</h1>
				<?php } else { ?>
				<h1 class="card-pf-title">Modify hotel room</h1>
				<?php } ?>
				<div class="card-pf-body">
					<form class="profile" enctype="multipart/form-data" action="/hotelroom/<?= $hotel['id'] . "/" . ($action != "add"?$action . "/" . $room['id']:$action) ?>" method="post">
						<input type="hidden" name="_Token" value="<?= $token ?>">
						<input type="hidden" name="_method" value="post">
						<div class="form-group">
							<label class="control-label" for="name">Name</label>
							<input type="text" maxlength="512" class="form-control" value="<?= (!empty($room)?$room['name']:'') ?>" name="name" required>
							<span class="help-block">Enter name for the hotel room.</span>
						</div>
						<div class="form-group">
                            <label class="control-label" for="description">Room description</label>
                            <textarea maxlength="65530" class="form-control" name="description"><?= (!empty($room) && $room['description']?htmlspecialchars($room['description']):'') ?></textarea>
                            <span class="help-block">Add hotel room description or offerrings for guests.</span>
                        </div>
                        <div class="form-group">
							<label class="control-label" for="amount">Room amount</label>
							<input type="number" min="1" class="form-control" value="<?= (!empty($room)?$room['amount']:'') ?>" name="amount" required>
 							<span class="help-block">Set amount of available rooms in the hotel.</span>
						</div>
                        <div class="form-group">
							<label class="control-label" for="price">Full price</label>
							<input type="text" maxlength="512" class="form-control" value="<?= (!empty($room)?$room['full_price']:'') ?>" name="full_price" required>
							<span class="help-block">Enter full price for the hotel room including currency symbol. This price will be used for public booking form only.</span>
						</div>
						<div class="form-group">
							<label class="control-label" for="name">Sponsored price</label>
							<input type="text" maxlength="512" class="form-control" value="<?= (!empty($room)?$room['covered_price']:'') ?>" name="covered_price" required>
							<span class="help-block">Enter sponsored price for the hotel room including currency symbol. This price will be used for speakers (sponsored) booking form only.</span>
						</div>
						<div class="form-group">
							<span>
								<input type="submit" class="btn btn-primary" name="save" value="Save">
							</span>

						</div>
					</form> <!-- end form -->	

				</div> <!-- end card body -->
			</div> <!-- end card -->
        </div> <!-- cols -->
    </div> <!-- row -->
</div> <!-- container -->
<script>
  $(document).ready(function() {
    // matchHeight the contents of each .card-pf and then the .card-pf itself
    $(".row-cards-pf > [class*='col'] > .card-pf .card-pf-title").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf > .card-pf-body").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf > .card-pf-footer").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf").matchHeight();

    // Initialize the vertical navigation
    $().setupVerticalNavigation(true);

    var editor = new Jodit($("textarea[name=confirmation_info]")[0], {
                         "disablePlugins": "symbols,stat,media,imageproperties,imageprocessor,fullsize,draganddropelement,draganddrop,pastestorage,autofocus,addnewline",
                         "buttonsXS": "bold,strikethrough,underline,italic,|,ul,ol,outdent,indent,|,fontsize,link,|,undo,redo,cut,copy,paste,|",
                         "buttonsMD": "bold,strikethrough,underline,italic,|,ul,ol,outdent,indent,|,fontsize,link,|,undo,redo,cut,copy,paste,|",
                         "buttonsSM": "bold,strikethrough,underline,italic,|,ul,ol,outdent,indent,|,fontsize,link,|,undo,redo,cut,copy,paste,|",
                         "buttons": "bold,strikethrough,underline,italic,|,ul,ol,outdent,indent,|,fontsize,link,|,undo,redo,cut,copy,paste,|",
            });
  });
</script>
</body>
</html>
 
