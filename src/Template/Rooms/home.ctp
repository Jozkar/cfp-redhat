<div class="container-fluid container-cards-pf container-pf-nav-pf-vertical">
    <ol class="breadcrumb" role="navigation" aria-label="breadcrumbs">
        <li><a href="/"><span class="fa fa-home"></span></a></li>
        <li><a href="/manager">PM Review</a></li>
        <li class="active"><span><?= $event['name'] . " (" . $event['year'] . ")" ?> - Rooms</span></li>
    </ol>
    <a href="/room/<?= $event['id'] ?>" class="btn btn-success">Add new Room</a>
	<?php if(count($rooms) < 1) { ?>
	<div class="blank-slate-pf">
        <div class="blank-slate-pf-icon">
            <span class="pficon pficon-info"></span>
        </div>
        <h1>There is no room available yet.</h1>
        <p>Click on Add new to create one.</p>

	<?php }else{ ?>
    <div id="pf-list-standard" class="list-group list-view-pf list-view-pf-view">
    <div id="room-container" class="list-group list-view-pf list-view-pf-view">
    <?php foreach($rooms as $r) { ?>
	<div class="room-group-item col-xs-12 col-sm-4 col-md-3" id="room<?= $r['id']?>">
             <div class="card-pf card-pf-view card-pf-aggregate-status">
                  <div class="card-pf-body">
                        <span class="pf-grab-landscape ui-sortable-handle"></span>
                        <h2 class="card-pf-title text-center"><?= $r['name'] ?></h2>
                        <?php if($r['description'] != "") { ?>
						<h4 class="text-center">(<?= $r['description'] ?>)</h4>
						<?php } ?>
                        <div class="card-pf-items text-center">
                             <div class="card-pf-item">
							  	  <span class="pficon pficon-users" title="Capacity"></span> <?= $r['capacity'] ?>
							 </div>
						</div>
                        
                        <div class="card-pf-items text-center">
                             <div class="card-pf-item">
                                  <a href="/room/<?= $event['id'] ?>/edit/<?= $r['id'] ?>" title="Edit room">
                                      <span class="pficon pficon-edit"></span> Edit
                                  </a>
                             </div>
							 <div class="card-pf-item">
							  <a class="openModal" action="delete" href="#" data-toggle="tooltip" data-placement="top" event-id="/room/<?= $event['id'] ?>/delete/<?= $r['id'] . "/" . $token ?>" title="Remove hotel room">
								  <span class="pficon pficon-error-circle-o"></span> Remove
							  </a>
							 </div>
                        </div>
                   </div>
             </div>
         </div> <!-- cols -->
	 <?php }

         } ?>

    </div> <!-- group list -->
    
    <div class="modal fade" id="confirmations" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
   						<button type="button" class="close" data-dismiss="modal" aria-hidden="true" aria-label="Close">
					        <span class="pficon pficon-close"></span>
				        </button>
	        			<h4 class="modal-title" id="myModalLabel">Confirm room deletion</h4>
				</div> <!-- modal header -->
				<div class="modal-body">
					<p>Do you really want to delete this room? Keep in mind, that this action will <strong>remove the room and all associated talks</strong> and can't be undone.</p>
				</div>
			 	<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				        <a href="" id="modalDelete" class="btn btn-danger">Delete</a>
				</div> <!-- modal footer -->
			</div> <!-- modal content -->
		</div> <!-- modal dialog -->
	</div> <!-- modal -->        
		
</div> <!-- container -->
<script>
  $(document).ready(function() {
    // matchHeight the contents of each .card-pf and then the .card-pf itself
    function even(){
		$(".list-view-pf-view > [class*='col'] > .card-pf .card-pf-title").matchHeight();
		$(".list-view-pf-view > [class*='col'] > .card-pf > .card-pf-body").matchHeight();
		$(".list-view-pf-view > [class*='col'] > .card-pf > .card-pf-footer").matchHeight();
		$(".list-view-pf-view > [class*='col'] > .card-pf").matchHeight();
    }

    $( "#room-container" ).sortable({
        handle: ".pf-grab-landscape",
        stop: function (event, ui) {
            var order = [];
            $.each($("#room-container .room-group-item"), function(i,form){
                order.push(parseInt(form.getAttribute('id').replace("room","")));
            });
            console.log(order);
            updateOrder(order);
          }
    });

	var eventId = <?= $event["id"] ?>, token = '<?= $token ?>';
	
    even();
    
    // Initialize the vertical navigation
    $().setupVerticalNavigation(true);

	$(".openModal").click(function (){
    	$("#modalDelete").attr("href", $(this).attr("event-id"));
    	$("#confirmations").modal('show');
    });

    // autohide toasts
    $('.toast-pf').mouseenter(function () {
 		$(this).attr('mouseenter','true');
    });

    $('.toast-pf').mouseleave(function () {
		$(this).removeAttr('mouseenter');
		if($(this).attr('remove') != null){
			$(this).remove();
		}		
    });

    function hideToasts() {
		var toasts = $('.toast-pf'), i;

		for(i = 0; i < toasts.length; i++){
			if($(toasts[i]).attr('mouseenter') != null){
				$(toasts[i]).attr('remove','true');
			} else {
				$(toasts[i]).hide('slow', function(){ $(toasts[i]).remove(); });
			}
		}
    }

    function showToast(message, type){
        var toast = $(".toast-notifications-list-pf").html()+
            '<div class="toast-pf alert alert-' + (type == "error"?"danger":"success") + ' alert-dismissable">'+
            '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">'+
            '<span class="pficon pficon-close"></span>'+
            '</button>'+
            '<span class="pficon ' + (type == "error"?'pficon-error-circle-o':'pficon-ok') + '"></span>'+
            message+
            '</div>';
        $(".toast-notifications-list-pf").html(toast);
    }

    function updateOrder(order){
        
        var xhr = new XMLHttpRequest();
        xhr.open('POST', '/rooms/order/');
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.onload = function() {
            try{
                var data = JSON.parse(xhr.responseText);
            } catch (e) {
                showToast(xhr.responseText);
                return;
            }
            console.log(data);
            if(data.result != 'success'){
                showToast(data.reason, "error");
            }else{
                showToast("Room order has been successfully updated.", "success");    
                setTimeout(hideToasts, 8000);               
            }
        };
        xhr.send(jQuery.param({'_Token': token, '_method': "post", 'token': token, 'eventID': eventId, 'order': order}));
    
    }

    setTimeout(hideToasts, 8000);
  });
</script>
</body>
</html>  
