<div class="container-fluid container-cards-pf container-pf-nav-pf-vertical nav-pf-persistent-secondary">
    <ol class="breadcrumb" role="navigation" aria-label="breadcrumbs">
        <li><a href="/"><span class="fa fa-home"></span></a></li>
    	<li><a href="/reviewer">Review tasks</a></li>
        <li class="active"><span>Reviewers</span></li>
    </ol>
    <form method="post" autocomplete="off" action="/capreviewers/<?= $eventId ?>/add">
        <div class="form-group">
    		<label>Add reviewer for <?= $eventInfo['name'] . " (" . $eventInfo['year'] . ")" ?></label>
            <select class="combobox form-control" autocomplete="off" name="user">
                <option value="" selected="selected">Choose user (start typing name or email)</option>
                <?php foreach($users as $u) { ?>
                    <option value="<?= $u['email'] ?>"><?= $u['first_name'] . " " . $u['last_name'] . " (" . $u['email'] . ")" ?></option>
                <?php } ?>
            </select>
    	</div>
    	<?php if(count($allowedTopics) > 0){ ?>
    	<div class="form-group">
    		<label>Set session's topic to review</label>
    		<div>
    		<?php foreach($allowedTopics as $t) { ?>
    			<label class="checkbox-inline">
    				<input type="checkbox" name="topic[]" value="<?= $t['id'] ?>"> <?= $t['name'] ?>
    			</label>
    		<?php } ?>
    		<span class="help-block">Applicable only for 'Sessions' group of proposals.</span>
    		</div>
    	</div>
    	<?php }else{ ?>
    	<input type="hidden" name="topic[]" value="">
    	<?php }
    	if(count($forms) > 1) { ?>
    	<div class="form-group">
    		<label>Choose group of proposals</label>
    		<div>
    		<?php foreach($forms as $f) { ?>
    			<label class="radio-inline">
    				<input type="radio" required name="formType" value="<?= $f['type'] ?>"> <?= ($f['placeholder'] != ""? $inflector->pluralize(ucfirst($f['type'])) . " (" . $inflector->pluralize(ucfirst($f['placeholder'])) . ")" : $inflector->pluralize(ucfirst($f['type']))) ?>
    			</label>
    		<?php } ?>
    		</div>
    	</div>
    	<?php } else { ?>
    		<input type="hidden" name="formType" value="<?= $forms[0]['type'] ?>">
    		<span class="help-block">Reviewer's group of proposals set to '<?= ($forms[0]['placeholder'] != ""? $inflector->pluralize(ucfirst($forms[0]['type'])) . " (" . $inflector->pluralize(ucfirst($forms[0]['placeholder'])) . ")" : $inflector->pluralize(ucfirst($forms[0]['type']))) ?>'.</span>
    	<?php } ?>
    	
        <input type="hidden" name="_Token" value="<?= $token ?>">
        <input type="hidden" name="_method" value="post">       
    	<div class="form-group">
    		<?php if(count($allowedTopics) > 0){ ?>
    		<span>
    		    <a href="#" class="btn btn-default" id="checkAll">Select all topics</a>
    		</span>
    		<?php } ?>
    		<span>
    		    <input class="btn btn-primary" type="submit" name="save" value="Add reviewer">
    		</span>
    	</div>
    </form>
    <div class="list-group list-view-pf list-view-pf-view">
    <?php foreach($reviewers as $a){ ?>
        <div class="list-group-item">
           		<div class="list-view-pf-main-info">
    			<div class="list-view-pf-left">
    				<span class="fa list-view-pf-icon-sm" style="border: none !important">
    						<img src="<?= $a['avatar'] ?>" style="width: 100%; object-fit: contain">
    				</span>
    			</div>
                <div class="list-view-pf-body">
                    <div class="list-view-pf-description">
                        <div class="list-group-item-text">
                    		<strong><?= $a['first_name'] . " " . $a['last_name'] ?></strong> (<?= $a['email'] ?>)
                        </div>
                    </div> <!-- description -->
                    <div class="list-view-pf-additional-info">
                        <div class="list-view-pf-additional-info-item">
                            <span class='label label-success'><?= ucfirst($a['form_type']) ?>s</span>
                        </div>
                        <?php foreach(explode(",", $a['topics']) as $top){
                                if($top != ""){?>
                                    <div class="list-view-pf-additional-info-item">
                                        <?= $topics[$top]['name'] ?>
                                    </div>
                        <?php   }
                              } ?>
                    </div>
                </div>
    		</div> 
            </div> <!-- cols -->
    <?php } ?>
    </div> <!-- row -->
</div> <!-- container -->
<script>
  $(document).ready(function() {
    // Initialize the vertical navigation
    $().setupVerticalNavigation(true);

    // Initialize Boostrap-Combobox
    $('.combobox').combobox();

    $('#NaN').attr('autocomplete','off');

    $('#checkAll').click(function (){
        $('input[type=checkbox]').attr('checked', 'checked');
    });
    
    // autohide toasts
    $('.toast-pf').mouseenter(function () {
    	$(this).attr('mouseenter','true');
    });

    $('.toast-pf').mouseleave(function () {
    	$(this).removeAttr('mouseenter');
    	if($(this).attr('remove') != null){
    		$(this).remove();
    	}		
    });

    function hideToasts() {
    	var toasts = $('.toast-pf'), i;

    	for(i = 0; i < toasts.length; i++){
    		if($(toasts[i]).attr('mouseenter') != null){
    			$(toasts[i]).attr('remove','true');
    		} else {
    			$(toasts[i]).hide('slow', function(){ $(toasts[i]).remove(); });
    		}
    	}
    }

    setTimeout(hideToasts, 8000);

  });
</script>
</body>
</html>
  
