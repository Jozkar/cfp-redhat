<div class="container-fluid container-cards-pf container-pf-nav-pf-vertical nav-pf-persistent-secondary">
        <ol class="breadcrumb" role="navigation" aria-label="breadcrumbs">
            <li><a href="/"><span class="fa fa-home"></span></a></li>
            <li class="active">My accepted<span></span></li>
        </ol>
    <?php if(count($events) < 1) { ?>
    <div class="blank-slate-pf">
        <div class="blank-slate-pf-icon">
            <span class="pficon pficon-info"></span>
        </div>
        <h1>None of your proposal has been accepted yet.</h1>
        <p>Check out your email for review results. Previously accepted and confirmed proposals are listed below.</p>
    </div>
    <?php } else { ?>
    <div class="list-group list-view-pf">
    	<div class="list-group-item list-view-pf-stacked">
    		<div class="list-view-pf-main-info">
    			<div class="list-view-pf-body">
    				<div>
    					<div class="h1"><strong>Congratulation!</strong></div>
     					<div>
    						Some of your proposals has been accepted to following events. 
    						Please, confirm or reject your participation at these events as soon as possible. 
    						Keep in mind your <strong>decission is final and can't be undone.</strong>
    					</div>
    				</div>
    			</div>
    		</div>
    	</div>
    </div>
    <div class="row row-cards-pf">
    <?php foreach($events as $e){ ?>
            <div class="col-xs-12 col-sm-6 col-md-3">
    		<div class="card-pf card-pf-view card-pf-aggregate-status">
    			<div class="card-pf-body">
    				<div class="card-pf-top-element">
    					<span class="card-pf-icon-circle" style="border: none !important; width: 100%;">
    						<img src="<?= ($e['logo']!=""?$e['logo']:"/webroot/img/logos/default.png") ?>" style="width: 100%; object-fit: contain; height: inherit">
    					</span>
    				</div>
    				<h2 class="card-pf-title text-center"><?= $e['name'] . "<br>(" . $e['year'] .")" ?></h2>
    				<div class="card-pf-items text-center">
    					<div class="card-pf-item">
    						<a href="/acceptproposals/<?= $e['id'] ?>" class="btn btn-success" title="Confirm results">Fill confirmation form</a>
    						<a href="#" reject="/confirmproposals/reject/<?= $e['id'] ?>" class="btn btn-danger openModal" title="Reject results">Reject all</a>
    					</div>
    				</div>
    			</div>
    		</div> 
            </div> <!-- cols -->
    <?php } ?>
    </div> <!-- row -->
    <?php } 
    if(count($confirmedEvents) > 0){ ?>
    <h2>My confirmed events</h2>
    <div class="row row-cards-pf">
    <?php foreach($confirmedEvents as $e){ ?>
    	<div class="col-xs-12 col-sm-6 col-md-3">
    		<div class="card-pf card-pf-view card-pf-aggregate-status">
    			<div class="card-pf-body">
    				<div class="card-pf-top-element">
    					<span class="card-pf-icon-circle" style="border: none !important; width: 100%;">
    						<img src="<?= ($e['logo']!=""?$e['logo']:"/webroot/img/logos/default.png") ?>" style="width: 100%; object-fit: contain; height: inherit">
    					</span>
    				</div>
    				<h2 class="card-pf-title text-center"><?= $e['name'] . "<br>(" . $e['year'] .")" ?></h2>
    				<div class="card-pf-items text-center">
    					<div class="card-pf-item">
    						<a href="/acceptedproposals/<?= $e['id'] ?>" class="btn btn-default" title="See confirmed proposals">See confirmed proposals</a>
    					</div>
    				 <?php if ($e['booking']){ ?>
                        <div class="card-pf-item">
                            <a href="/booking/<?= $e['id'] ?>" class="btn btn-default" title="Book a hotel">Book a hotel</a>
                        </div>
                    <?php } ?>
                    </div>
    			</div>
    		</div>
    	</div>
    <?php } ?>
    </div>
    <?php } ?>

</div> <!-- container -->
<div class="modal fade" id="confirmations" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
     <div class="modal-dialog">
         <div class="modal-content">
             <div class="modal-header">
                 <button type="button" class="close" data-dismiss="modal" aria-hidden="true" aria-label="Close">
                     <span class="pficon pficon-close"></span>
                 </button>
                 <h4 class="modal-title" id="myModalLabel">Confirm review result rejection</h4>
             </div> <!-- modal header -->
             <div class="modal-body">
                 <p>Do you really want to reject all your accepted proposals? Keep in mind, that this action can't be undone.</p>
             </div>
             <div class="modal-footer">
                 <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                 <a href="" id="modalDelete" class="btn btn-danger">Reject all</a>
             </div> <!-- modal footer -->
         </div> <!-- modal content -->
     </div> <!-- modal dialog -->
</div> <!-- modal -->

<script>
  $(document).ready(function() {
    // matchHeight the contents of each .card-pf and then the .card-pf itself
    $(".row-cards-pf > [class*='col'] > .card-pf .card-pf-title").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf > .card-pf-body").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf > .card-pf-footer").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf").matchHeight();

    // Initialize the vertical navigation
    $().setupVerticalNavigation(true);

    // autohide toasts
    $('.toast-pf').mouseenter(function () {
    	$(this).attr('mouseenter','true');
    });

    $('.toast-pf').mouseleave(function () {
    	$(this).removeAttr('mouseenter');
    	if($(this).attr('remove') != null){
    		$(this).remove();
    	}		
    });

    function hideToasts() {
    	var toasts = $('.toast-pf'), i;

    	for(i = 0; i < toasts.length; i++){
    		if($(toasts[i]).attr('mouseenter') != null){
    			$(toasts[i]).attr('remove','true');
    		} else {
    			$(toasts[i]).hide('slow', function(){ $(toasts[i]).remove(); });
    		}
    	}
    }

    $(".openModal").click(function (){
        	$("#modalDelete").attr("href", $(this).attr("reject"));
            $("#confirmations").modal('show');
    });

    setTimeout(hideToasts, 8000);

  });
</script>
</body>
</html>
  
