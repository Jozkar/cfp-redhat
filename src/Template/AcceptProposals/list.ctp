<div class="container-fluid container-cards-pf container-pf-nav-pf-vertical nav-pf-persistent-secondary">
    <ol class="breadcrumb" role="navigation" aria-label="breadcrumbs">
        <li><a href="/"><span class="fa fa-home"></span></a></li>
       	<li><a href="/acceptproposals">My accepted</a></li>
        <li class="active"><span>Confirmed proposals</span></li>
    </ol>
    <?php if(count($responses)+count($answers)+count($additionalResponses)+count($additionalAnswers) < 1) { ?>
    <div class="blank-slate-pf">
        <div class="blank-slate-pf-icon">
            <span class="pficon pficon-info"></span>
        </div>
        <h1>You haven't confirm any accepted proposal yet.</h1>
        <p>Check out your email for review results.</p>
    </div>
    <?php } else { ?>
    <div class="list-group list-view-pf">
    	<div class="list-group-item list-view-pf-stacked">
    		<div class="list-view-pf-main-info">
    			<div class="list-view-pf-body">
    				<div>
    					<div class="h1"><strong>Confirmed proposals</strong></div>
     					<div>
    						You have confirmed following proposals.
    					</div>
                        <div>
                            <h4><?= $event['confirmation_info'] ?></h4>
                        </div>
    				</div>
    			</div>
    		</div>
    	</div>
    </div>
    <div class="row row-cards-pf">
    <?php foreach($responses as $r){ ?>
            <div class="col-xs-12 col-sm-6 col-md-3">
    		<div class="card-pf card-pf-view card-pf-aggregate-status">
    			<div class="card-pf-body">
    				<div class="card-pf-top-element">
    					<span class="card-pf-icon-circle" style="border: none !important">
    						<img src="<?= ($event['logo']!=""?$event['logo']:"/webroot/img/logos/default.png") ?>" style="width: 100%; object-fit: contain; height: inherit">
    					</span>
    				</div>
    				<h2 class="card-pf-title text-center"><?= $r['title'] ?></h2>
    				<div class="card-pf-items text-center">
    					<div class="card-pf-item">
    						<a href="/proposals/<?= $event['id'] . "/" . $r['id'] ?>" class="btn btn-default" title="See proposal">See proposal</a>
    					</div>
                    <?php if ($event['collect_data']){ ?>
                        <div class="card-pf-item">
                            <a href="/collect/<?= $event['id'] . "/" . $r['id'] ?>" class="btn btn-default" title="Upload data">Upload data</a>
                        </div>
                    <?php } ?>
    				</div>
    			</div>
    		</div> 
            </div> <!-- cols -->
    <?php } ?>
    <?php foreach($answers as $r){ ?>
            <div class="col-xs-12 col-sm-6 col-md-3">
    		<div class="card-pf card-pf-view card-pf-aggregate-status">
    			<div class="card-pf-body">
    				<div class="card-pf-top-element">
    					<span class="card-pf-icon-circle" style="border: none !important">
    						<img src="<?= ($event['logo']!=""?$event['logo']:"/webroot/img/logos/default.png") ?>" style="width: 100%; object-fit: contain; height: inherit">
    					</span>
    				</div>
    				<h2 class="card-pf-title text-center"><?php if(isset($r['Title'])) { ?>
                        <?= $r['Title'] ?> (<?= ucfirst($r['type']) . " proposal" ?>)
                    <?php }else{ ?>
    				    <?= ucfirst($r['type']) . " proposal no. " . $r['id'] ?>
                    <?php } ?></h2>
    				<div class="card-pf-items text-center">
    					<div class="card-pf-item">
    						<a href="/answer/<?= $r['id'] ?>" class="btn btn-default" title="See proposal">See proposal</a>
    					</div>
                    <?php if ($event['collect_data']){ ?>
                        <div class="card-pf-item">
                            <a href="/collect/<?= $event['id'] . "/-1/" . $r['id'] ?>" class="btn btn-default" title="Upload data">Upload data</a>
                        </div>
                    <?php } ?>
    				</div>
    			</div>
    		</div> 
            </div> <!-- cols -->
    <?php } ?>
    <?php foreach($additionalResponses as $r){ ?>
            <div class="col-xs-12 col-sm-6 col-md-3">
    		<div class="card-pf card-pf-view card-pf-aggregate-status">
    			<div class="card-pf-body">
    				<div class="card-pf-top-element">
    					<span class="card-pf-icon-circle" style="border: none !important">
    						<img src="<?= ($event['logo']!=""?$event['logo']:"/webroot/img/logos/default.png") ?>" style="width: 100%; object-fit: contain; height: inherit">
    					</span>
    				</div>
    				<h2 class="card-pf-title text-center"><?= $r['title'] . "<br>(you're additional speaker)" ?></h2>
    				<div class="card-pf-items text-center">
    					<div class="card-pf-item">
    						<a href="/proposals/<?= $event['id'] . "/" . $r['id'] ?>" class="btn btn-default" title="See proposal">See proposal</a>
    					</div>
    				</div>
    			</div>
    		</div> 
            </div> <!-- cols -->
    <?php } ?>
    <?php foreach($additionalAnswers as $r){ ?>
            <div class="col-xs-12 col-sm-6 col-md-3">
    		<div class="card-pf card-pf-view card-pf-aggregate-status">
    			<div class="card-pf-body">
    				<div class="card-pf-top-element">
    					<span class="card-pf-icon-circle" style="border: none !important">
    						<img src="<?= ($event['logo']!=""?$event['logo']:"/webroot/img/logos/default.png") ?>" style="width: 100%; object-fit: contain; height: inherit">
    					</span>
    				</div>
    				<h2 class="card-pf-title text-center"><?php if(isset($r['Title'])) { ?>
                        <?= $r['Title'] ?> (<?= ucfirst($r['type']) . " proposal" ?>)
                    <?php }else{ ?>
    				    <?= ucfirst($r['type']) . " proposal no. " . $r['id'] ?>
                    <?php } ?><br>(you're additional speaker)</h2>
    				<div class="card-pf-items text-center">
    					<div class="card-pf-item">
    						<a href="/answer/<?= $r['id'] ?>" class="btn btn-default" title="See proposal">See proposal</a>
    					</div>
    				</div>
    			</div>
    		</div> 
            </div> <!-- cols -->
    <?php } ?>
    </div> <!-- row -->
    <?php } ?>
</div> <!-- container -->
<script>
  $(document).ready(function() {
    // matchHeight the contents of each .card-pf and then the .card-pf itself
    $(".row-cards-pf > [class*='col'] > .card-pf .card-pf-title").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf > .card-pf-body").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf > .card-pf-footer").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf").matchHeight();

    // Initialize the vertical navigation
    $().setupVerticalNavigation(true);

    // autohide toasts
    $('.toast-pf').mouseenter(function () {
    	$(this).attr('mouseenter','true');
    });

    $('.toast-pf').mouseleave(function () {
    	$(this).removeAttr('mouseenter');
    	if($(this).attr('remove') != null){
    		$(this).remove();
    	}		
    });

    function hideToasts() {
    	var toasts = $('.toast-pf'), i;

    	for(i = 0; i < toasts.length; i++){
    		if($(toasts[i]).attr('mouseenter') != null){
    			$(toasts[i]).attr('remove','true');
    		} else {
    			$(toasts[i]).hide('slow', function(){ $(toasts[i]).remove(); });
    		}
    	}
    }

    setTimeout(hideToasts, 8000);

  });
</script>
</body>
</html>
  
