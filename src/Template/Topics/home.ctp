<div class="container-fluid container-cards-pf container-pf-nav-pf-vertical">
    <ol class="breadcrumb" role="navigation" aria-label="breadcrumbs">
        <li><a href="/"><span class="fa fa-home"></span></a></li>
        <li><a href="/admin">Administration</a></li>
        <li class="active"><span>Topics</span></li>
    </ol>
    <a href="#" action="add" class="openModal btn btn-primary">Add new topic</a>
	<?php if(count($topics) < 1) { ?>
	<div class="blank-slate-pf">
        <div class="blank-slate-pf-icon">
            <span class="pficon pficon-info"></span>
        </div>
        <h1>There is no topic created yet.</h1>
        <p>Click on Add new topic to create one.</p>

	<?php }else{ ?>
    <div id="pf-list-standard" class="list-group list-view-pf list-view-pf-view">
    <?php foreach($topics as $topic) { ?>
	<div class="col-xs-12 col-sm-3 col-md-2">
             <div class="card-pf card-pf-view card-pf-view-xs" style="border-top: 5px solid <?= ($topic['color'] != ""?$topic['color']:"#000000") ?>">
                  <div class="card-pf-body">
                        <h2 class="card-pf-title text-center"><?= $topic['name'] ?></h2>
                        <div class="card-pf-items text-center">
                             <div class="card-pf-item">
                                  <a href="#" id="<?= $topic['id'] ?>" name="<?= str_replace('"', '\"', $topic['name']) ?>" color="<?= $topic['color'] ?>" class="openModal" data-toggle="tooltip" data-placement="top" title="Edit topic">
                                      <span class="pficon pficon-edit"></span> Edit title
                                  </a>
                             </div>
                        </div>
                   </div>
             </div>
         </div> <!-- cols -->
	 <?php }

         } ?>

	<div class="modal fade" id="form" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true" aria-label="Close">
						<span class="pficon pficon-close"></span>
					</button>
					<h4 class="modal-title" id="myModalLabel">Manage topic</h4>
				</div> <!-- modal header -->
				<div class="modal-body">
					<div class="form-group">
						<label class="control-label" for="name">Set topic name</label>
						<input type="text" maxlength="100" class="form-control" id="name" name="name" required>
					</div>
                    <div class="form-group">
                        <label class="control-label" for="color">Set topic color</label>
                        <input type="color" class="form-control" id="color" name="color">
                    </div>
				</div>
				<div class="modal-footer">
					<button type="button" id="save" class="btn btn-primary" data-dismiss="modal">Save</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				</div> <!-- modal footer -->
			</div> <!-- modal content -->
		</div> <!-- modal dialog -->
	</div> <!-- modal -->
    </div> <!-- group list -->
</div> <!-- container -->
<script>
  $(document).ready(function() {
    // matchHeight the contents of each .card-pf and then the .card-pf itself
    function even(){
	$(".list-view-pf-view > [class*='col'] > .card-pf .card-pf-title").matchHeight();
	$(".list-view-pf-view > [class*='col'] > .card-pf > .card-pf-body").matchHeight();
	$(".list-view-pf-view > [class*='col'] > .card-pf > .card-pf-footer").matchHeight();
	$(".list-view-pf-view > [class*='col'] > .card-pf").matchHeight();
    }

    even();
    manageModal();
    // Initialize the vertical navigation
    $().setupVerticalNavigation(true);

    var token = "<?= $token ?>";

    function manageModal(){
	$(".openModal").off("click");
	$(".openModal").on("click", function (){
		if($(this).attr("id") != undefined){
			$("#name").attr("topicId", $(this).attr("id"));
			$("#name").val($(this).attr("name"));
			$("#color").val($(this).attr("color"));
 		}else{
			$("#name").removeAttr("topicId");
			$("#name").val("");
            $("#color").val("");
		}
		$("#form").modal('show');
	});
    }

    $("#save").click(function (){
	var name = $("#name").val(), id = $("#name").attr("topicId"), color = $("#color").val();
	
	if(name != ""){
		sendUpdate(name,id,color);	
	}else{
		showErrorToast("You have to specify topic name.");
	}

    });

    function showErrorToast(message){
        var toast = $(".toast-notifications-list-pf").html()+
		    '<div class="toast-pf alert alert-danger alert-dismissable">'+
		    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">'+
		    '<span class="pficon pficon-close"></span>'+
		    '</button>'+
		    '<span class="pficon pficon-error-circle-o"></span>'+
		    message+
		    '</div>';
	$(".toast-notifications-list-pf").html(toast);
    }

    function sendUpdate(name, id, color){
	if(id == undefined){ 
		id = -2;
	}
	var xhr = new XMLHttpRequest();
	xhr.open('POST', '/topicupdate/');
	xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	xhr.onload = function() {
 	    try{
                var data = JSON.parse(xhr.responseText);
            } catch (e) {
                showErrorToast(xhr.responseText);
                return;
            }
            if(data.result != 'success'){
                showErrorToast(data.reason);
            }else{
		addTopic(name, id, color);
		even();
		manageModal();
	    }
        };

        xhr.send(jQuery.param({'token': token, 'id': id, 'name': name, 'color': color}));


    }

    function addTopic(name, id, color){
	if(id > 0 || id == -1){
		$("#"+id).parent().parent().prev().html(name);
		$("#"+id).parent().parent().parent().parent().css("border-top", "5px solid " + color);
		$("#"+id).attr("name",name.replace('"', '\"'));
		$("#"+id).attr("color", color);
	} else {
		id = $("h2").length+1; 
		var content = '<div class="col-xs-12 col-sm-3 col-md-2">'+
			      '<div class="card-pf card-pf-view card-pf-view-xs" style="border-top: 5px solid ' + color + '">'+
			      '<div class="card-pf-body">'+
			      '<h2 class="card-pf-title text-center">' + name + '</h2>'+
			      '<div class="card-pf-items text-center">'+
			      '<div class="card-pf-item">'+
                              '<a href="#" id="'+ id + '" name="' + name.replace('"', '\"') + '" color="' + color + '" class="openModal" data-toggle="tooltip" data-placement="top" title="Edit title">'+
                              '<span class="pficon pficon-edit"></span> Edit title'+
                              '</a>'+
                              '</div>'+
                              '</div>'+
	                      '</div>'+
		              '</div>'+
			      '</div> <!-- cols -->';
		$("#pf-list-standard").append(content);

	}
    }

    // autohide toasts
    $('.toast-pf').mouseenter(function () {
 	$(this).attr('mouseenter','true');
    });

    $('.toast-pf').mouseleave(function () {
	$(this).removeAttr('mouseenter');
	if($(this).attr('remove') != null){
		$(this).remove();
	}		
    });

    function hideToasts() {
	var toasts = $('.toast-pf'), i;

	for(i = 0; i < toasts.length; i++){
		if($(toasts[i]).attr('mouseenter') != null){
			$(toasts[i]).attr('remove','true');
		} else {
			$(toasts[i]).hide('slow', function(){ $(toasts[i]).remove(); });
		}
	}
    }

    setTimeout(hideToasts, 8000);

  });
</script>
</body>
</html>  
