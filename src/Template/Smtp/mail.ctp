<div class="container-fluid container-cards-pf container-pf-nav-pf-vertical">
    <ol class="breadcrumb" role="navigation" aria-label="breadcrumbs">
        <li><a href="/"><span class="fa fa-home"></span></a></li>
        <li><a href="/admin">Administration</a></li>
        <li class="active"><span>SMTP settings</span></li>
    </ol>
	<div class="row row-cards-pf">
		<div class="col-xs-12 ">
			<div class="card-pf card-pf-view">
				<h1 class="card-pf-title">Update SMTP credentials</h1>
				<div class="card-pf-body">
					<form class="profile" action="/smtp/<?= $event ?>" method="post">
						<input type="hidden" name="_Token" value="<?= $token ?>">
						<input type="hidden" name="_method" value="post">
						<div class="form-group">
							<label class="control-label" for="host">Host</label>
							<input type="text" maxlength="100" class="form-control" value="<?= (!empty($smtp)?$smtp['host']:'') ?>" name="host" required>
							<span class="help-block">Enter address of host.</span>
						</div>
						<div class="form-group">
							<label class="control-label" for="port">Port</label>
							<input type="text" maxlength="5" class="form-control" value="<?= (!empty($smtp)?$smtp['port']:'') ?>" name="port" required>
							<span class="help-block">Enter SMTP port.</span>
						</div>
						<div class="form-group">
							<label class="control-label" for="user">User name</label>
							<div class="input-group">
								<input type="password" maxlength="100" class="form-control" value="<?= (!empty($smtp)?$smtp['username']:'') ?>" name="username">
								<span class="input-group-addon" for="username"><span class="glyphicon glyphicon-eye-open"></span></span>
							</div>
							<span class="help-block">Enter SMTP user name.</span>
						</div>
						<div class="form-group">
							<label class="control-label" for="password">Password</label>
							<div class="input-group">
								<input type="password" maxlength="100" class="input-group form-control" value="<?= (!empty($smtp)?$smtp['password']:'') ?>" name="password">
								<span class="input-group-addon" for="password"><span class="glyphicon glyphicon-eye-open"></span></span>
							</div>
							<span class="help-block">Enter SMTP password.</span>
						</div>	
						<div class="form-group">
							<label class="control-label" for="nick">Email alias</label>
							<input type="text" maxlength="100" class="form-control" value="<?= (!empty($smtp)?$smtp['nick']:'') ?>" name="nick" required>
							<span class="help-block">Enter email alias, that should be used as FROM field in email.</span>
						</div>						
                        <div class="form-group">
                            <label class="control-label" for="replyto">Reply-To email address</label>
                            <input type="email" maxlength="100" class="form-control" value="<?= (!empty($smtp)?$smtp['replyto']:'') ?>" name="replyto">
                            <span class="help-block">Enter one email address, that should receive replies from users (Set as Reply-To header).</span>
                        </div>
						<div class="form-group">
							<label class="control-label" for="tls">Use TLS?</label>
							<input class="form-control bootstrap-switch" type="checkbox" value="true" <?= (!empty($smtp) && $smtp['tls']?'checked ':'') ?>name="tls">
						</div>
						<div class="form-group">
							<span>
								<input type="submit" class="btn btn-primary" name="save" value="Save">
							</span>

						</div>
					</form> <!-- end form -->	

				</div> <!-- end card body -->
			</div> <!-- end card -->
        </div> <!-- cols -->
    </div> <!-- row -->
</div> <!-- container -->
<script>
  $(document).ready(function() {
    // matchHeight the contents of each .card-pf and then the .card-pf itself
    $(".row-cards-pf > [class*='col'] > .card-pf .card-pf-title").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf > .card-pf-body").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf > .card-pf-footer").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf").matchHeight();

    // Initialize switch buttons
    $(".bootstrap-switch").bootstrapSwitch();

    $(".input-group-addon").click(function(){
	var field = $(this).attr("for");
	var state = $("input[name="+field+"]").attr("type");
	if(state == "text"){
		$("input[name="+field+"]").attr("type","password");
	}else{
		$("input[name="+field+"]").attr("type","text");
	}
    });
    // Initialize the vertical navigation
    $().setupVerticalNavigation(true);
  });
</script>
</body>
</html>
 
