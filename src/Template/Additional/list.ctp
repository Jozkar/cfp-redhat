<div class="container-fluid container-cards-pf container-pf-nav-pf-vertical nav-pf-persistent-secondary">
    <ol class="breadcrumb" role="navigation" aria-label="breadcrumbs">
        <li><a href="/"><span class="fa fa-home"></span></a></li>
       	<li><a href="/myproposals">My proposals</a></li>
        <li class="active"><span>Additional speakers</span></li>
    </ol>
    <?php if ($form['speaker_limit'] >= 0 && count($speakers) >= $form['speaker_limit']){ ?>
    <h1>You have reached maximum number of allowed additional speakers.</h1>
    <?php } else { ?>
    <form method="post" autocomplete="off" action="/additional/add/<?= $form['id'] . "/" . $response . "/" . $answer ?>">
        <div class="form-group">
    	<label>Add user to your proposal:</label>
    	<select class="combobox form-control allow-free-form" name="user">
    		<option value="" selected="selected">Choose user (start typing name or email)</option>
                    <?php foreach($users as $u) { ?>
                    <option value="<?= $u['email'] ?>"><?= $u['first_name'] . " " . $u['last_name'] . " (" . $u['email'] . ")" ?></option>
                    <?php } ?>
            </select>
    	<span class="help-block">If not present, fill speaker's email.</span>
    </div>
           <input type="hidden" name="_Token" value="<?= $token ?>">
        <input type="hidden" name="_method" value="post">       
    <div class="form-group">
    	<span>
    	    <input class="btn btn-primary" type="submit" name="save" value="Add speaker">
    	</span>
    </div>
    </form>
    <?php } ?>
    <div class="row row-cards-pf">
    <?php foreach($speakers as $a){ ?>

            <div class="col-xs-12 col-sm-6 col-md-3">
    		<div class="card-pf card-pf-view card-pf-aggregate-status">
    			<div class="card-pf-body">
    				<div class="card-pf-top-element">
    					<span class="card-pf-icon-circle" style="border: none !important">
    						<img src="<?= $a['avatar'] ?>" style="width: 100%; object-fit: contain; height: inherit">
    					</span>
    				</div>
    				<h2 class="card-pf-title text-center"><?= $a['first_name'] . " " . $a['last_name'] . "<br>(" . $a['user_id'] .")" ?></h2>
    				<p class="card-pf-info text-center">
    					<form method="post" action="/additional/delete/<?= $form['id'] . "/" . $response . "/" . $answer ?>">
    						<input type="hidden" name="_Token" value="<?= $token ?>">
    						<input type="hidden" name="_method" value="post">
    						<input type="hidden" name="user" value="<?= $a['user_id'] ?>">
    						<input type="submit" class="btn btn-danger btn-lg" name="save" value="Remove speaker">
    					</form>
    				</p>
    			</div>
    		</div> 
            </div> <!-- cols -->
    <?php } ?>
    </div> <!-- row -->
</div> <!-- container -->
<script>
  $(document).ready(function() {
    // matchHeight the contents of each .card-pf and then the .card-pf itself
    $(".row-cards-pf > [class*='col'] > .card-pf .card-pf-title").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf > .card-pf-body").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf > .card-pf-footer").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf").matchHeight();

    // Initialize the vertical navigation
    $().setupVerticalNavigation(true);

    // Initialize Boostrap-Combobox
    $('.combobox').combobox({renderLimit: "10"});

    // autohide toasts
    $('.toast-pf').mouseenter(function () {
    $(this).attr('mouseenter','true');
    });

    $('.toast-pf').mouseleave(function () {
    $(this).removeAttr('mouseenter');
    if($(this).attr('remove') != null){
    	$(this).remove();
    }		
    });

    function hideToasts() {
    var toasts = $('.toast-pf'), i;

    for(i = 0; i < toasts.length; i++){
    	if($(toasts[i]).attr('mouseenter') != null){
    		$(toasts[i]).attr('remove','true');
    	} else {
    		$(toasts[i]).hide('slow', function(){ $(toasts[i]).remove(); });
    	}
    }
    }

    setTimeout(hideToasts, 8000);

  });
</script>
</body>
</html>
  
