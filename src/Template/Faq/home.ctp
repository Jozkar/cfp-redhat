<div class="container-fluid container-cards-pf container-pf-nav-pf-vertical">
    <ol class="breadcrumb" role="navigation" aria-label="breadcrumbs">
        <li><a href="/"><span class="fa fa-home"></span></a></li>
        <li class="active">FAQ<span></span></li>
    </ol>
    <div id="pf-list-simple-expansion" class="list-group list-view-pf list-view-pf-view">
    <?php foreach($questions as $q) { ?>
        <div class="list-group-item">
            <div class="list-group-item-header">
                <div class="list-view-pf-expand">
                    <span class="fa fa-angle-right"></span>
                </div>
                <div class="list-view-pf-main-info">
                    <div class="list-view-pf-body">
                        <div class="list-view-pf-description">
                            <div class="list-group-item-heading">
                                <?= $q['question'] ?>
                            </div>
                        </div> <!-- body description -->
                    </div> <!-- list view body -->
                 </div> <!-- main info -->
            </div><!-- item header -->
            <div class="list-group-item-container container-fluid hidden">
                <div class="close">
                    <span class="pficon pficon-close"></span>
                </div>
                <div class="row">
		    <div class="col-md-12">
                        <p><?= $q['answer'] ?></p>
      		    </div>
                </div>
            </div><!-- container fluid -->
        </div> <!-- group item -->
        <?php } ?>
    </div> <!-- group list -->
</div> <!-- container -->
<script>
  $(document).ready(function() {
    // matchHeight the contents of each .card-pf and then the .card-pf itself
    $(".row-cards-pf > [class*='col'] > .card-pf .card-pf-title").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf > .card-pf-body").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf > .card-pf-footer").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf").matchHeight();

    // Initialize the vertical navigation
    $().setupVerticalNavigation(true);

	// autohide toasts
	$('.toast-pf').mouseenter(function () {
		$(this).attr('mouseenter','true');
	});

	$('.toast-pf').mouseleave(function () {
		$(this).removeAttr('mouseenter');
		if($(this).attr('remove') != null){
			$(this).remove();
		}		
	});

	function hideToasts() {
		var toasts = $('.toast-pf'), i;

		for(i = 0; i < toasts.length; i++){
			if($(toasts[i]).attr('mouseenter') != null){
				$(toasts[i]).attr('remove','true');
			} else {
				$(toasts[i]).hide('slow', function(){ $(toasts[i]).remove(); });
			}
		}
	}

    // click the list-view heading then expand a row
    $("#pf-list-simple-expansion .list-group-item-header").click(function(event){
      if(!$(event.target).is("button, a, input, .fa-ellipsis-v")){
        $(this).find(".fa-angle-right").toggleClass("fa-angle-down")
          .end().parent().toggleClass("list-view-pf-expand-active")
            .find(".list-group-item-container").toggleClass("hidden");
      } else {
      }
    })

    // click the close button, hide the expand row and remove the active status
    $("#pf-list-simple-expansion .list-group-item-container .close").on("click", function (){
      $(this).parent().addClass("hidden")
        .parent().removeClass("list-view-pf-expand-active")
          .find(".fa-angle-right").removeClass("fa-angle-down");
    })

    setTimeout(hideToasts, 8000);

  });
</script>
</body>
</html>  
