<div class="container-fluid container-cards-pf container-pf-nav-pf-vertical">
    <ol class="breadcrumb" role="navigation" aria-label="breadcrumbs">
        <li><a href="/"><span class="fa fa-home"></span></a></li>
        <li><a href="/manager">PM Review</a></li>
        <li><a href="/hotels/<?= $event['id'] ?>"><?= $event['name'] . " (" . $event['year'] . ")" ?> - Hotels</a></li>
        <li class="active"><span><?= ($action == "add"?"Add new hotel":"Modify hotel") ?></span></li>
    </ol>
	<div class="row row-cards-pf">
		<div class="col-xs-12 ">
			<div class="card-pf card-pf-view">
				<?php if ($action == "add"){ ?>
				<h1 class="card-pf-title">Add new hotel</h1>
				<?php } else { ?>
				<h1 class="card-pf-title">Modify hotel</h1>
				<?php } ?>
				<div class="card-pf-body">
					<form class="profile" enctype="multipart/form-data" action="/hotel/<?= $event['id'] . "/" . ($action != "add"?$action . "/" . $hotel['id']:$action) ?>" method="post">
						<input type="hidden" name="_Token" value="<?= $token ?>">
						<input type="hidden" name="_method" value="post">
						<div class="form-group">
							<label class="control-label" for="name">Name</label>
							<input type="text" maxlength="512" class="form-control" value="<?= (!empty($hotel)?$hotel['name']:'') ?>" name="name" required>
							<span class="help-block">Enter name of the hotel.</span>
                        </div>
						<div class="form-group">
                            <label class="control-label" for="email">Email</label>
                            <input type="email" maxlength="512" class="form-control" value="<?= (!empty($hotel)?$hotel['email']:'') ?>" name="email" required>
                            <span class="help-block">Enter email address, where room booking mails should be delivered.</span>
                        </div>
						<div class="form-group">
							<label class="control-label" for="web">Web page</label>
							<input type="text" maxlength="512" class="form-control" value="<?= (!empty($hotel)?$hotel['web']:'') ?>" name="web">
							<span class="help-block">Enter public web page with hotel informaiton.</span>
						</div>
                        
						<div class="form-group">
                            <label class="control-label" for="description">Hotel description</label>
                            <textarea maxlength="65530" class="form-control" name="description"><?= (!empty($hotel) && $hotel['description']?htmlspecialchars($hotel['description']):'') ?></textarea>
                            <span class="help-block">Add hotel description or offerrings for guests.</span>
                        </div>
    					<div class="form-group">
							<label class="control-label" for="use_for_paid_form">This hotel should be available to speakers too.</label>
							<input class="form-control bootstrap-switch" type="checkbox" value="1" <?= (!empty($hotel) && $hotel['use_for_paid_form']?'checked ':'') ?>name="use_for_paid_form">
							<span class="help-block">If true, this hotel will be part of booking form for speakers. Prices from "covered price" field will be used.</span>
						</div>
						<div class="form-group">
							<span>
								<input type="submit" class="btn btn-primary" name="save" value="Save">
							</span>

						</div>
					</form> <!-- end form -->	

				</div> <!-- end card body -->
			</div> <!-- end card -->
        </div> <!-- cols -->
    </div> <!-- row -->
</div> <!-- container -->
<script>
  $(document).ready(function() {
    // matchHeight the contents of each .card-pf and then the .card-pf itself
    $(".row-cards-pf > [class*='col'] > .card-pf .card-pf-title").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf > .card-pf-body").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf > .card-pf-footer").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf").matchHeight();

	// Initialize switch buttons
	$(".bootstrap-switch").bootstrapSwitch();

    $(".chosen-select").chosen();

    // Initialize the vertical navigation
    $().setupVerticalNavigation(true);

    var editor = new Jodit($("textarea[name=confirmation_info]")[0], {
                         "disablePlugins": "symbols,stat,media,imageproperties,imageprocessor,fullsize,draganddropelement,draganddrop,pastestorage,autofocus,addnewline",
                         "buttonsXS": "bold,strikethrough,underline,italic,|,ul,ol,outdent,indent,|,fontsize,link,|,undo,redo,cut,copy,paste,|",
                         "buttonsMD": "bold,strikethrough,underline,italic,|,ul,ol,outdent,indent,|,fontsize,link,|,undo,redo,cut,copy,paste,|",
                         "buttonsSM": "bold,strikethrough,underline,italic,|,ul,ol,outdent,indent,|,fontsize,link,|,undo,redo,cut,copy,paste,|",
                         "buttons": "bold,strikethrough,underline,italic,|,ul,ol,outdent,indent,|,fontsize,link,|,undo,redo,cut,copy,paste,|",
            });
  });
</script>
</body>
</html>
 
