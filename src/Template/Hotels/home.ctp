<div class="container-fluid container-cards-pf container-pf-nav-pf-vertical">
    <ol class="breadcrumb" role="navigation" aria-label="breadcrumbs">
        <li><a href="/"><span class="fa fa-home"></span></a></li>
        <li><a href="/manager">PM Review</a></li>
        <li class="active"><span><?= $event['name'] . " (" . $event['year'] . ")" ?> - Hotels</span></li>
    </ol>
    <a href="/hotel/<?= $event['id'] ?>" class="btn btn-success">Add new Hotel</a>
	<?php if(count($hotels) < 1) { ?>
	<div class="blank-slate-pf">
        <div class="blank-slate-pf-icon">
            <span class="pficon pficon-info"></span>
        </div>
        <h1>There is no hotel available yet.</h1>
        <p>Click on Add new to create one.</p>

	<?php }else{ ?>
    <div id="pf-list-standard" class="list-group list-view-pf list-view-pf-view">
    <?php foreach($hotels as $h) { ?>
	<div class="col-xs-12 col-sm-4 col-md-3">
             <div class="card-pf card-pf-view card-pf-view-xs">
                  <div class="card-pf-body">
                        <h2 class="card-pf-title text-center"><?= $h['name'] ?><?= ($h['use_for_paid_form']?" <span class='pficon pficon-warning-triangle-o' title='We are covering room prices'></span>":"") ?></h2>
						<h4 class="text-center">(<?= $h['email'] . (isset($h['web']) && $h['web'] != ""?" | <a href='" . $h['web'] . "' target='_blank'>WWW</a>":"") ?>)</h4>
                        <div class="card-pf-items text-center">
                             <div class="card-pf-item">
                                  <a href="/hotel/<?= $event['id'] ?>/edit/<?= $h['id'] ?>" title="Edit hotel">
                                      <span class="pficon pficon-edit"></span> Edit
                                  </a>
                             </div>
                             <div class="card-pf-item">
                                  <a href="/hotelrooms/<?= $h['id'] ?>/" title="Edit hotel rooms">
                                      <span class="pficon pficon-key"></span> Rooms
                                  </a>
                             </div>
							 <div class="card-pf-item">
							  <a class="openModal" action="delete" href="#" data-toggle="tooltip" data-placement="top" event-id="/hotel/<?= $event['id'] ?>/delete/<?= $h['id'] . "/" . $token ?>" title="Remove hotel">
								  <span class="pficon pficon-error-circle-o"></span> Remove
							  </a>
							 </div>
                        </div>
                   </div>
             </div>
         </div> <!-- cols -->
	 <?php }

         } ?>

    </div> <!-- group list -->
    
    <div class="modal fade" id="confirmations" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
       						<button type="button" class="close" data-dismiss="modal" aria-hidden="true" aria-label="Close">
						        <span class="pficon pficon-close"></span>
					        </button>
		        			<h4 class="modal-title" id="myModalLabel">Confirm hotel deletion</h4>
					</div> <!-- modal header -->
					<div class="modal-body">
						<p>Do you really want to delete this hotel? Keep in mind, that this action will <strong>remove all hotel rooms and reservations assigned to it</strong> and can't be undone.</p>
					</div>
				 	<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					        <a href="" id="modalDelete" class="btn btn-danger">Delete</a>
					</div> <!-- modal footer -->
				</div> <!-- modal content -->
			</div> <!-- modal dialog -->
		</div> <!-- modal -->        
		
</div> <!-- container -->
<script>
  $(document).ready(function() {
    // matchHeight the contents of each .card-pf and then the .card-pf itself
    function even(){
		$(".list-view-pf-view > [class*='col'] > .card-pf .card-pf-title").matchHeight();
		$(".list-view-pf-view > [class*='col'] > .card-pf > .card-pf-body").matchHeight();
		$(".list-view-pf-view > [class*='col'] > .card-pf > .card-pf-footer").matchHeight();
		$(".list-view-pf-view > [class*='col'] > .card-pf").matchHeight();
    }

	var eventId = <?= $event["id"] ?>, token = '<?= $token ?>';
	
    even();
    
    // Initialize the vertical navigation
    $().setupVerticalNavigation(true);

	$(".openModal").click(function (){
    	$("#modalDelete").attr("href", $(this).attr("event-id"));
    	$("#confirmations").modal('show');
    });

    // autohide toasts
    $('.toast-pf').mouseenter(function () {
 		$(this).attr('mouseenter','true');
    });

    $('.toast-pf').mouseleave(function () {
		$(this).removeAttr('mouseenter');
		if($(this).attr('remove') != null){
			$(this).remove();
		}		
    });

    function hideToasts() {
		var toasts = $('.toast-pf'), i;

		for(i = 0; i < toasts.length; i++){
			if($(toasts[i]).attr('mouseenter') != null){
				$(toasts[i]).attr('remove','true');
			} else {
				$(toasts[i]).hide('slow', function(){ $(toasts[i]).remove(); });
			}
		}
    }

    setTimeout(hideToasts, 8000);
  });
</script>
</body>
</html>  
