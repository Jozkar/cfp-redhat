<div class="container-fluid container-cards-pf container-pf-nav-pf-vertical">
    <ol class="breadcrumb" role="navigation" aria-label="breadcrumbs">
        <li><a href="/"><span class="fa fa-home"></span></a></li>
        <li class="active"><span>PM Review</span></li>
    </ol>
    <?php if(count($events) < 1) { ?>
    <div class="blank-slate-pf">
       <div class="blank-slate-pf-icon">
           <span class="pficon pficon-info"></span>
       </div>
       <h1>No event is currently assigned to you to manage.</h1>
      <p>Get in touch with event organizers, if you think, you should see there somethig.</p>

    <?php }else{ ?>
    <div id="pf-list-standard" class="list-group list-view-pf list-view-pf-view">
    <?php foreach($events as $event) {	?>
        <div class="list-group-item">
            <div class="list-view-pf-actions">
		<div class="dropdown pull-left">
			<button type="button" class="btn btn-default dropdown-toggle" title="Export .csv file" id="ddE<?= $event['id'] ?>" data-toggle="dropdown">Export (as .csv)<span class="caret"></span></button>
			<ul class="dropdown-menu" aria-labelledby="ddE<?= $event['id'] ?>">
			<?php foreach($event['forms'] as $forms){ if($forms['type'] == 'confirmation' || $forms['type'] == 'collect' || $forms['type'] == 'accommodation' || $forms['type'] == 'covered accommodation') { continue; }?>
				<li><a href="/manager/export/<?= $event['id'] ?>/<?= $token ?>/<?= $forms['type'] ?>">Accepted <?= ($forms['placeholder'] != ""? $inflector->pluralize($forms['type']) . " (" . $inflector->pluralize($forms['placeholder']) . ")" : $inflector->pluralize($forms['type'])) ?></a></li>
			<?php } ?>
				<li class="divider"></li>
			<?php foreach($event['forms'] as $forms){ if($forms['type'] == 'confirmation' || $forms['type'] == 'collect' || $forms['type'] == 'accommodation' || $forms['type'] == 'covered accommodation') { continue; }?>
				<li><a href="/manager/export/<?= $event['id'] ?>/<?= $token ?>/<?= $forms['type'] ?>/speakers">Accepted speakers for <?= ($forms['placeholder'] != ""? $inflector->pluralize($forms['type']) . " (" . $inflector->pluralize($forms['placeholder']) . ")" : $inflector->pluralize($forms['type'])) ?></a></li>
			<?php } ?>
				<li class="divider"></li>
			<?php foreach($event['forms'] as $forms){ if($forms['type'] == 'confirmation' || $forms['type'] == 'collect' || $forms['type'] == 'accommodation' || $forms['type'] == 'covered accommodation') { continue; }?>
				<li><a href="/manager/export/<?= $event['id'] ?>/<?= $token ?>/<?= $forms['type'] ?>/confirmed">Confirmed <?= ($forms['placeholder'] != ""? $inflector->pluralize($forms['type']) . " (" . $inflector->pluralize($forms['placeholder']) . ")" : $inflector->pluralize($forms['type'])) ?></a></li>
			<?php } ?>
				<li class="divider"></li>
            <?php foreach($event['forms'] as $forms){ if($forms['type'] == 'confirmation' || $forms['type'] == 'collect' || $forms['type'] == 'accommodation' || $forms['type'] == 'covered accommodation') { continue; }?>
				<li><a href="/manager/stats/<?= $event['id'] ?>/<?= $token ?>/<?= $forms['type'] ?>">Stats for <?= ($forms['placeholder'] != ""? $inflector->pluralize($forms['type']) . " (" . $inflector->pluralize($forms['placeholder']) . ")" : $inflector->pluralize($forms['type'])) ?></a></li>
            <?php } ?>
				<li class="divider"></li>
			<?php foreach($event['forms'] as $forms){ if($forms['type'] == 'collect' || strpos($forms['type'], "accommodation") !== false) { continue; }?>
				<li><a href="/manager/allproposals/<?= $event['id'] ?>/<?= $token ?>/<?= $forms['type'] ?>">Export all <?= ($forms['placeholder'] != ""? $forms['type'] . " (" . $forms['placeholder'] . ")" : $forms['type']) ?> proposals</a></li>
			<?php } ?>
			
			<?php if($event['schedule']) { ?>
			<li class="divider"></li>
		    <li><a href="/manager/schedule/<?= $event['id'] ?>/<?= $token ?>/1">Export published schedule</a></li>
			<?php } ?>
				
			</ul>
		</div>
		<div class="dropdown pull-left">
			<button type="button" class="btn btn-primary dropdown-toggle" id="ddM<?= $event['id'] ?>" data-toggle="dropdown">Results <span class="caret"></span></button>
			<ul class="dropdown-menu" aria-labelledby="ddM<?= $event['id'] ?>">
			<?php foreach($event['forms'] as $forms){ if($forms['type'] == 'confirmation' || $forms['type'] == 'collect' || $forms['type'] == 'accommodation' || $forms['type'] == 'covered accommodation') { continue; }?>
				<li><a href="/manage/<?= $event['id'] ?>/<?= $forms['type'] ?>"><?= ($forms['placeholder'] != ""? $inflector->pluralize(ucfirst($forms['type'])) . " (" . $inflector->pluralize(ucfirst($forms['placeholder'])) . ")" : $inflector->pluralize(ucfirst($forms['type']))) ?></a></li>
			<?php } ?>
				<li class="divider"></li>
			<?php foreach($event['forms'] as $forms){ 
				if($forms['type'] != "session"){ if(strpos($forms['type'], "accommodation") !== false) { continue; } ?>
				<li><a href="/answers/<?= $forms['id'] ?>">See all <?= ($forms['placeholder'] != ""? $inflector->pluralize($forms['type']) . " (" . $inflector->pluralize($forms['placeholder']) . ")" : $inflector->pluralize($forms['type'])) ?></a></li>
			<?php }else{ ?>
				<li><a href="/proposals/<?= $event['id'] ?>">See all <?= ($forms['placeholder'] != ""? $inflector->pluralize($forms['type']) . " (" . $inflector->pluralize($forms['placeholder']) . ")" : $inflector->pluralize($forms['type'])) ?></a></li>
			<?php } } ?>				
			</ul>
		</div>

        <div class="dropdown pull-left">
            <button type="button" class="btn btn-warning dropdown-toggle" id="ddT<?= $event['id'] ?>" data-toggle="dropdown">Topics<span class="caret"></span></button>
            <ul class="dropdown-menu" aria-labelledby="ddT<?= $event['id'] ?>">
                <li><a href="/topicfix/<?= $event['id'] ?>">Reduce or Change topic assignment</a></li>
                <li><a href="/slots/<?= $event['id'] ?>">Time allocation per track</a></li>
            </ul>
        </div>

        <?php if($event['accommodation']) { ?>
            <div class="dropdown pull-left">
                <button type="button" class="btn btn-info dropdown-toggle" id="ddH<?= $event['id'] ?>" data-toggle="dropdown">Hotels<span class="caret"></span></button>
                <ul class="dropdown-menu" aria-labelledby="ddH<?= $event['id'] ?>">
                    <li><a href="/hotels/<?= $event['id'] ?>">Manage hotels</a></li>
                    <li><a href="/managebooking/<?= $event['id'] ?>">Sponsorships</a></li>
                    <li class="divider"></li>
                    <?php foreach($event['forms'] as $forms){
                        if(strpos($forms['type'], "accommodation") !== false){ ?>
                    <li><a href="/answers/<?= $forms['id'] ?>">See all <?= ($forms['placeholder'] != ""? $inflector->pluralize($forms['type']) . " (" . $inflector->pluralize($forms['placeholder']) . ")" : $inflector->pluralize($forms['type'])) ?></a></li>
                    <?php } } ?>
                    <li class="divider"></li>
                    <?php foreach($event['forms'] as $forms){
                        if(strpos($forms['type'], "accommodation") !== false){ ?>
                    <li><a href="/manager/allproposals/<?= $event['id'] ?>/<?= $token ?>/<?= $forms['type'] ?>">Export all <?= ($forms['placeholder'] != ""? $forms['type'] . " (" . $forms['placeholder'] . ")" : $forms['type'] )?> proposals</a></li>
                    <?php } } ?>
                </ul>
            </div>

    
        <?php } ?>

        <?php if($event['schedule']) { ?>
            <div class="dropdown pull-left">
                <button type="button" class="btn btn-success dropdown-toggle" id="ddS<?= $event['id'] ?>" data-toggle="dropdown">Schedule<span class="caret"></span></button>
                <ul class="dropdown-menu" aria-labelledby="ddS<?= $event['id'] ?>">
                    <li><a href="/rooms/<?= $event['id'] ?>">Manage rooms</a></li>
                    <li><a href="/scheduler/<?= $event['id'] ?>">Scheduler</a></li>
                </ul>
            </div>


        <?php } ?>


		<a class="btn btn-danger" href="/emails/<?= $event['id'] ?>">Emails</a>
		
        <div class="dropdown pull-right dropdown-kebab-pf">
			<button class="btn btn-link dropdown-toggle" type="button" id="dropdown<?= $event['id'] ?>" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
				<span class="fa fa-ellipsis-v"></span>
			</button>
			<ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown<?= $event['id'] ?>">
                		<li><a href="/reviewers/<?= $event['id'] ?>">Reviewers</a></li>
                		<li><a href="/speakers/<?= $event['id'] ?>">Submitted speakers</a></li>
                		<li><a href="/codes/<?= $event['id'] ?>">Unique code management</a></li>
                		<li><a href="/submit/<?= $event['id'] ?>">Submit proposal for speaker</a></li>
			</ul>
		</div>
            </div> <!-- actions -->
            <div class="list-view-pf-main-info">
                <div class="list-view-pf-left">
                    <span class="fa list-view-pf-icon-sm" style="border: none !important">
			<img src="<?= ($event['logo']!=""?$event['logo']:"/webroot/img/logos/default.png") ?>" style="width: 100%; object-fit: contain">
                    </span>
                </div> 
                <div class="list-view-pf-body">
                    <div class="list-view-pf-description">
                        <div class="list-group-item-heading">
                            <a target="_blank" href="<?= $event['url'] ?>"><?= $event['name'] . " (" . $event['year'] . ")" ?></a>
                        </div>
                    </div> <!-- body description -->
                    <div class="list-view-pf-additional-info">
                        <div class="list-view-pf-additional-info-item">
                            <span class="pficon pficon-ok"></span>
                            CfP open <?= date("j/M/Y", strtotime($event['cfp_open'])) ?>
                        </div>
                        <div class="list-view-pf-additional-info-item">
                            <span class="pficon pficon-error-circle-o"></span>
                            CfP close <?= date("j/M/Y", strtotime($event['cfp_close'])) ?>
                        </div>
                        <?php if ($event['for_redhat']) { ?>
                            <div class="list-view-pf-additional-info-item">
                                <span class="pficon pficon-security"></span>
                                Internal event
                            </div>
                        <?php } else { ?> 
                            <div class="list-view-pf-additional-info-item">
                                <span class="pficon pficon-warning-triangle-o"></span>
                                Public event
                            </div>
			<?php } ?>

			        </div> <!-- additional info -->
                </div> <!-- list view body -->
            </div> <!-- main info -->
        </div> <!-- group item -->
        <?php }} ?>
    </div> <!-- group list -->
</div> <!-- container -->
<script>
  $(document).ready(function() {
    // matchHeight the contents of each .card-pf and then the .card-pf itself
    $(".row-cards-pf > [class*='col'] > .card-pf .card-pf-title").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf > .card-pf-body").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf > .card-pf-footer").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf").matchHeight();

    // Initialize the vertical navigation
    $().setupVerticalNavigation(true);

	// autohide toasts
	$('.toast-pf').mouseenter(function () {
		$(this).attr('mouseenter','true');
	});

	$('.toast-pf').mouseleave(function () {
		$(this).removeAttr('mouseenter');
		if($(this).attr('remove') != null){
			$(this).remove();
		}		
	});

	function hideToasts() {
		var toasts = $('.toast-pf'), i;

		for(i = 0; i < toasts.length; i++){
			if($(toasts[i]).attr('mouseenter') != null){
				$(toasts[i]).attr('remove','true');
			} else {
				$(toasts[i]).hide('slow', function(){ $(toasts[i]).remove(); });
			}
		}
	}

	setTimeout(hideToasts, 8000);

  });
</script>
</body>
</html>  
