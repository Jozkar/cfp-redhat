<?php
echo "id,speaker";
foreach($questions as $q){
        echo ',"' . str_replace('"', '""', $q['question']) . '"';
}
echo "\n";
if($form == "session"){
    foreach($data as $k=>$d){
        echo $d['id'] . ',"' . str_replace('"', '""', $d['speakers']) . '"';
        foreach($questions as $q){
            if(strpos($q['take_as'],"self") !== false){
                $found = false;
                foreach($d['answers'] as $a){
                    if($q['id'] == $a['question_id']){
                        $found = true;
                        echo ',"' . str_replace('"', '""', $a['text']) . '"';
                    }
                }
                if(!$found){
                    echo ',-';
                }
            } else {
                echo ',"' . str_replace('"', '""', $d[$q['take_as']]) . '"';
            }
        }
        echo "\n";
    }
} else {
    foreach($data as $k=>$d){
        echo $d['id'] . ',"' . str_replace('"', '""', $d['speakers']) . '"';
        foreach($d as $kw=>$v){
                if($kw != "answer_id" && $kw != "answers" && $kw != "id" && $kw != "speakers"){
                
                        echo ',"' . str_replace('"', '""', $v) . '"';
                }
                if($kw == "answers"){
                        foreach($questions as $q){
                                $found = false;
                                foreach($v as $a){
                                        if($q['id'] == $a['question_id']){
                                                $found = true;
                                                if($q['take_as'] == 'topics'){
                                                    echo ',"';
                                                    $tps = array();
                                                    foreach(explode(",", $a['text']) as $tID){
                                                        foreach($topics as $t){
                                                            if($t['topic_id'] == $tID){
                                                                $tps[] = $t['name'];
                                                            }
                                                        }
                                                    }
                                                    echo implode(",", $tps) . '"';
                                                } else {
                                                    echo ',"' . str_replace('"', '""', $a['text']) . '"';
                                                }
                                        }
                                }
                                if(!$found){
                                        echo ',-';
                                }
                        }
                }
        }
        echo "\n";
    }
}
?>
