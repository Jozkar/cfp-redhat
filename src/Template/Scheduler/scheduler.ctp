<div class="container-fluid container-cards-pf container-pf-nav-pf-vertical">
    <ol class="breadcrumb" role="navigation" aria-label="breadcrumbs">
        <li><a href="/"><span class="fa fa-home"></span></a></li>
        <li><a href="/manager">PM Review</a></li>
        <li class="active"><span>Schedule management</span></li>
    </ol>
	<div class="row row-cards-pf">
		<div class="col-xs-12 ">
			<div class="card-pf card-pf-view">
				<div class="card-pf-body">
					<form id="scheduleform" enctype="multipart/form-data" action="/scheduler/<?= $event['id'] ?>" method="post">
						<input type="hidden" name="_Token" value="<?= $token ?>">
						<input type="hidden" name="_method" value="post">
                        <div id='external-events' class='col-xs-2'>
                            <div class="form-group">
                                <span>
                                    <input type="submit" class="btn btn-primary" name="save" value="Save">
                                </span>
                                <span>
                                    <input type="submit" class="btn btn-warning" name="sync" value="Sync accepted talks">
                                </span>
                            </div>
                            <div class="form-group">
                                <span>
                                    <a href="/schedule/<?= $event['id'] . "/" . $event['version'] ?>" class="btn btn-info" target="_blank">Preview</a>
                                </span>
                                <span>
                                    <input type="submit" class="btn btn-success" name="publish" value="Publish schedule">
                                </span>
                            </div>

                            <p>
                                <strong>Accepted talks</strong>
                            </p>
                            <div id="event-container" style="overflow: auto">
                                <div id="break-block" class='fc-event panel-body table-bordered alert-info' keep="true" data-event='{"title":"Break","editable":true,"duration":"00:10","backgroundColor":"#ddd",
                                            "borderColor":"#ddd","textColor":"#333","extendedProps":{"break":true}}'>
                                    <div>Add break</div>
                                </div>
                                <?php foreach($uResponses as $r){ ?>
                                <div class='fc-event panel-body table-bordered' style="background: <?= $r['color'] ?>; color: <?= $r['text_color'] ?>"
                                     data-event='<?= json_encode(array("title" => $r['title'] . " - " . $r['speaker'], "textColor" => $r['text_color'], "duration" => date_time_set(new DateTime(), 0, $r['duration'])->format("H:i"),
                                        "durationEditable" => false, "startEditable" => true,"id" => $r['id'],"backgroundColor" => $r['color'], "borderColor" => $r['color'],
                                        "extendedProps" => array("response_id" => $r['response_id'], "speaker" => $r['speaker'],"topic" => $r['topic'],"duration" => $r['duration'] . " min",
                                        "durationInTime" => date_time_set(new DateTime(), 0, $r['duration'])->format("H:i"),"title" => $r['title'])), JSON_HEX_APOS|JSON_HEX_QUOT) ?>'>
                                    <div class='fc-event-main'><?= $r['title'] . "<br>" . $r['speaker'] ?><br>(<?= $r['topic'] ?>)<br><?= $r['duration'] ?> min</div>
                                </div>
                                <?php } ?>
                                <?php foreach($uAnswers as $a){ ?>
                                <div class='fc-event panel-body table-bordered' style="background: #fff; color: #333"
                                     data-event='<?= json_encode(array("title" => $a['title'] . " - " . $a['speaker'], "textColor" => $a['text_color'], "duration" => date_time_set(new DateTime(), 0, $a['duration'])->format("H:i"),
                                        "durationEditable" => false, "startEditable" => true, "id" => $a['id'], "backgroundColor" => $a['color'], "borderColor" => $a['color'], 
                                        "extendedProps" => array("answer_id" => $a['answer_id'], "speaker" => $a['speaker'], "topic" => $a['topic'], "duration" => $a['duration'] . " min",
                                        "durationInTime" => date_time_set(new DateTime(), 0, $a['duration'])->format("H:i"), "title" => $a['title'])), JSON_HEX_APOS|JSON_HEX_QUOT) ?>'>
                                    <div class='fc-event-main'><?= $a['title'] . "<br>" . $a['speaker'] ?><br>(<?= $a['topic'] ?>)<br><?= $a['duration'] ?> min</div>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="form-group col-xs-10">
                            <h1 class="text-center"><strong><?= $event['name'] . " " . $event['year'] ?></strong></h1>
                    <div style="margin: 0px auto">
                        <ul class="nav nav-tabs nav-tabs-pf" role="tablist" style="display: flex; justify-content: center; border-bottom: none">
                            <li class="nav active dateTab" date="<?= $event['open'] ?>" role="presentation"><a role="tab" data-toggle="tab" aria-expanded="true"><?= date_create($event['open'])->format('l, F j, Y') ?></a></li>
                        <?php   $start = date_create($event['open']);
                                $end = date_create($event['close']);
                                while($start != $end){
                                    $start->add(date_interval_create_from_date_string('1 day'));
                                    echo '<li class="nav dateTab" date="' . $start->format("Y-m-d") . '" role="presentation"><a role="tab" data-toggle="tab" aria-expanded="false">' . $start->format('l, F j, Y') . '</a></li>';
                                } ?>
                        </ul>
                    </div>
                    <label class="control-label" for="info" style="margin-top: 1ex">All sessions are in <?= $event['timezone'] ?> time zone</label>
                            <span class="help-block">Move accepted talks to available slots.</span>
                            <div id="calendar">
                            </div>
                        </div>
                        <div>&nbsp;</div>
					</form> <!-- end form -->	
				</div> <!-- end card body -->
			</div> <!-- end card -->
        </div> <!-- cols -->
    </div> <!-- row -->
</div> <!-- container -->
<script src='https://cdn.jsdelivr.net/npm/fullcalendar-scheduler@6.1.10/index.global.min.js'></script>
<script>
  $(document).ready(function() {
    // matchHeight the contents of each .card-pf and then the .card-pf itself
    $(".row-cards-pf > [class*='col'] > .card-pf .card-pf-title").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf > .card-pf-body").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf > .card-pf-footer").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf").matchHeight();

    // Initialize the vertical navigation
    $().setupVerticalNavigation(true);

  });

  document.addEventListener('DOMContentLoaded', function() {
      var calendarEl = document.getElementById('calendar');
      var containerEl = document.getElementById('event-container');

      function replace(key, val){
        if(key == "start" || key == "end"){
            return undefined;
        }
        return val;
      }

      new FullCalendar.Draggable(containerEl, {
        itemSelector: '.fc-event',
      });
      var calendar = new FullCalendar.Calendar(calendarEl, {
        schedulerLicenseKey: 'GPL-My-Project-Is-Open-Source',
        initialView: 'timeGridCustom',
        headerToolbar: false,
        timeZone: '<?= $event['timezone'] ?>',
        slotLabelFormat: {
            hour: 'numeric',
            minute: '2-digit',
            meridiem: false,
            hour12: false
        },
        titleFormat: {
            weekday: 'long',
            month: 'long',
            day: 'numeric',
            year: 'numeric'
        },
        views: {
            timeGridCustom: {
                type: 'resourceTimeGrid',
            }
        },
        buttonIcons: false,
        buttonText: {
            prev: 'prev day',
            next: 'next day'
        },
        initialDate: '<?= $event['open'] ?>',
        validRange: {
            start: '<?= $event['open'] ?>',
            end: '<?= ($event['open'] != $event['close']?date_create($event['close'])->add(date_interval_create_from_date_string('1 day'))->format('Y-m-d'):$event['close']) ?>'
        },
        <?php if($rooms != null && sizeof($rooms) > 0){ ?>
        resources: [
        <?php foreach($rooms as $r){ ?>
            {
                id: "<?= $r['id'] ?>",
                title: "<?= $r['name'] ?>",
                overlap: false
            },
        <?php } ?>
        ],
        eventConstraint: 1,
        events: [
        <?php foreach($rooms as $r){
                foreach($r['available'] as $b){ ?>
            {
                start: '<?= ($b['all_day'] == 1?$b['date']:$b['date'].'T'.$b['start']) ?>',
                end: '<?= ($b['all_day'] == 1?$b['date']:$b['date'].'T'.$b['end']) ?>',
                resourceId: '<?= $r['id'] ?>',
                display: 'background',
                groupId: 1
            },
            <?php } ?>
        <?php }
            foreach($Responses as $r){ ?>
            {
                title: "<?= str_replace("\"","\\\"", $r['title']) . " - " . $r['speaker'] ?>",
                textColor: "<?= $r['text_color'] ?>",
                durationEditable: false,
                startEditable: true,
                resourceId: '<?= $r['room_id'] ?>',
                start: '<?= $r['date'] . "T" . $r['start'] ?>',
                end: '<?= $r['date'] . "T" . $r['end'] ?>',
                id: <?= $r['id'] ?>,
                backgroundColor: "<?= $r['color'] ?>",
                borderColor: "<?= $r['color'] ?>",
                extendedProps: {
                    "response_id":"<?= $r['response_id'] ?>",
                    "speaker":"<?= $r['speaker'] ?>",
                    "topic":"<?= $r['topic'] ?>",
                    "duration":"<?= $r['duration'] ?> min",
                    "durationInTime":"<?= date_time_set(new DateTime(), 0, $r['duration'])->format("H:i") ?>",
                    "title":"<?= str_replace("\"","\\\"", $r['title']) ?>"
                }
            },
        <?php } 
            foreach($Answers as $a) { ?>
            {
                title: "<?= str_replace("\"","\\\"", $a['title']) . " - " . $a['speaker'] ?>",
                textColor: "<?= $a['text_color'] ?>",
                durationEditable: false,
                startEditable: true,
                resourceId: '<?= $a['room_id'] ?>',
                start: '<?= $a['date'] . "T" . $a['start'] ?>',
                end: '<?= $a['date'] . "T" . $a['end'] ?>',
                id: <?= $a['id'] ?>,
                backgroundColor: "<?= $a['color'] ?>",
                borderColor: "<?= $a['color'] ?>",
                extendedProps: {
                    "answer_id": "<?= $a['answer_id'] ?>",
                    "speaker": "<?= $a['speaker'] ?>",
                    "topic": "<?= $a['topic'] ?>",
                    "duration": "<?= $a['duration'] ?> min",
                    "durationInTime": "<?= date_time_set(new DateTime(), 0, $a['duration'])->format("H:i") ?>",
                    "title": "<?= str_replace("\"","\\\"", $a['title']) ?>"
                }
            },
        <?php } 
            foreach($breaks as $b) {?>
            {
                title: "<?= $b['title'] ?>",
                editable: true,
                start: '<?= $b['date'] . "T" . $b['start'] ?>',
                end: '<?= $b['date'] . "T" . $b['end'] ?>',
                id: '<?= $b['id'] ?>',
                resourceId: '<?= $b['room_id'] ?>',
                backgroundColor: "#ddd",
                borderColor: "#ddd",
                textColor: "#333",
                extendedProps: {
                    "break":true
                }
            },
            <?php } ?>
        ],
        <?php } ?>
        eventOverlap: true,
        eventClick: function(info){
            if(info.el.classList.contains("btn-danger")){
                info.event.remove();
                if(info.event.extendedProps.break == undefined){
                    var obj = JSON.stringify(info.event, replace);
                    var nobj = JSON.parse(obj);
                    nobj['duration'] = info.event.extendedProps.durationInTime;
                    nobj['durationEditable'] = false;
                    var el = "<div class='fc-event panel-body table-bordered' style='background: " + info.event.backgroundColor + "; color: " + info.event.textColor + "'" +
                         " data-event='" + JSON.stringify(nobj) + "'>" +
                         "<div class='fc-event-main'>" + info.event.extendedProps.title + "<br>" + info.event.extendedProps.speaker + "<br>(" + info.event.extendedProps.topic + ")<br>" + info.event.extendedProps.duration + "</div>" +
                    "</div>";
                    $(el).insertAfter("#break-block");
                    $("<input />").attr("type", "hidden")
                        .attr("name", "unassign[]")
                        .attr("value", info.event.id)
                        .appendTo("#scheduleform");

                } else {
                    if(info.event.id != ""){
                        $("<input />").attr("type", "hidden")
                        .attr("name", "remove[]")
                        .attr("value", info.event.id)
                        .appendTo("#scheduleform");
                    }
                }
            } else {
                info.el.classList.add("btn-danger");
                $(info.el).find(".fc-event-title").html("Click to remove");
            }
        },
        select: function(){
            $(".fc-event.btn-danger").each(function(e){
                console.log($(this));
                $(this).removeClass("btn-danger");
                $(this).find(".fc-event-title").html("");
                calendar.render();
            });
        },
        editable: true,
        unselectAuto: false,
        droppable: true,
        selectable: true,
        selectHelper: false,
        slotDuration: '00:05:00',
        drop: function(info) {
            if(info.draggedEl.getAttribute("keep")==undefined){
                // if so, remove the element from the "Draggable Events" list
                console.log(info.draggedEl);
                if(info.draggedEl.parentNode != null){
                    info.draggedEl.parentNode.removeChild(info.draggedEl);
                }
            }
        },
        viewDidMount: function(isLoading){
                $("#event-container").css("max-height",$("#calendar").height());
        }
      });
      calendar.render();

      $(".dateTab").on("click", function(element){
            calendar.gotoDate($(this).attr('date'));
      });

      $("#scheduleform").submit(function(event){
        console.log(event);
        if(event.originalEvent.submitter.value != "Save"){
            if(event.originalEvent.submitter.value == "Publish schedule"){
                return confirm("Confirm your intention to publish new version of the schedule.");
            } else {
                return true;
            }
        } else {
            calendar.getEvents().forEach(function(e, i){
                if(e.display != "background"){
                    if(e.extendedProps.break){
                        if(e.id != ""){
                            $("<input />").attr("type", "hidden")
                                .attr("name", "slot[" + e.id + "][title]")
                                .attr("value", e.title)
                                .appendTo("#scheduleform");
 
                            $("<input />").attr("type", "hidden")
                                .attr("name", "slot[" + e.id + "][start]")
                                .attr("value", e.startStr)
                                .appendTo("#scheduleform");

                            $("<input />").attr("type", "hidden")
                                .attr("name", "slot[" + e.id + "][end]")
                                .attr("value", e.endStr)
                                .appendTo("#scheduleform");

                            $("<input />").attr("type", "hidden")
                                .attr("name", "slot[" + e.id + "][room]")
                                .attr("value", e._def.resourceIds[0])
                                .appendTo("#scheduleform");
                        } else {
                            $("<input />").attr("type", "hidden")
                                .attr("name", "slot[b" + i + "][title]")
                                .attr("value", e.title)
                                .appendTo("#scheduleform");
 
                            $("<input />").attr("type", "hidden")
                                .attr("name", "slot[b" + i + "][start]")
                                .attr("value", e.startStr)
                                .appendTo("#scheduleform");

                            $("<input />").attr("type", "hidden")
                                .attr("name", "slot[b" + i + "][end]")
                                .attr("value", e.endStr)
                                .appendTo("#scheduleform");

                            $("<input />").attr("type", "hidden")
                                .attr("name", "slot[b" + i + "][room]")
                                .attr("value", e._def.resourceIds[0])
                                .appendTo("#scheduleform");
                        }
                    } else {
                        $("<input />").attr("type", "hidden")
                          .attr("name", "slot[" + e.id + "][start]")
                          .attr("value", e.startStr)
                          .appendTo("#scheduleform");

                        $("<input />").attr("type", "hidden")
                          .attr("name", "slot[" + e.id + "][end]")
                          .attr("value", e.endStr)
                          .appendTo("#scheduleform");

                        $("<input />").attr("type", "hidden")
                          .attr("name", "slot[" + e.id + "][room]")
                          .attr("value", e._def.resourceIds[0])
                          .appendTo("#scheduleform");
                    }
                }
            });
            return true;
        }
      });
  });
   
</script>
</body>
</html>
 
