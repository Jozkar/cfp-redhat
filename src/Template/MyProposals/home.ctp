<div class="container-fluid container-cards-pf container-pf-nav-pf-vertical nav-pf-persistent-secondary">
        <ol class="breadcrumb" role="navigation" aria-label="breadcrumbs">
            <li><a href="/"><span class="fa fa-home"></span></a></li>
            <li class="active"><span>My proposals</span></li>
        </ol>
    <?php if(count($responses)+count($answers) < 1) { ?>
    <div class="blank-slate-pf">
        <div class="blank-slate-pf-icon">
            <span class="pficon pficon-info"></span>
        </div>
        <h1>You haven't filled any proposal yet.</h1>
        <p>Check out <a href="/">CfP catalog</a> page and submit your first proposal.</p>
    </div>
    <?php } else { ?>
	<a href="/" class="btn btn-success">Create new proposal</a>
	<?php foreach($responses as $id => $r){ 
		if($id == 0){?>
		<div class="row row-cards-pf"><h2>Session proposals</h2>
		<?php } ?>
        	<div class="col-xs-12 col-sm-6 col-md-3">
			<div class="card-pf card-pf-view card-pf-aggregate-status">
				<div class="card-pf-body">
					<div class="card-pf-top-element">
						<span class="card-pf-icon-circle" style="border: none !important; width: 100%;">
							<img src="<?= ($r['logo']!=""?$r['logo']:"/webroot/img/logos/default.png") ?>" <?= (strtotime(($r['form_close'] != "" && $r['form_close'] != $r['cfp_close']?$r['form_close']:$r['cfp_close'])) < strtotime(date("Y-m-d", time()))?'style="width: 100%; object-fit: contain; filter: grayscale(100%); height: inherit"':'style="width: 100%; object-fit: contain; height: inherit"') ?>>
						</span>
					</div>
					<h2 class="card-pf-title text-center"><?= $r['title'] . "<br>(" . $r['name'] . " " . $r['year'] .")" ?></h2>
					<?php if(strtotime(($r['form_close'] != "" && $r['form_close'] != $r['cfp_close']?$r['form_close']:$r['cfp_close'])) >= strtotime(date("Y-m-d", time()))){ ?>
					<div class="card-pf-items text-center">
						<div class="card-pf-item">
							<a href="/proposal/<?= $r['form_id'] . "/" . $r['id'] ?>/<?= ($r['answer_id'] != NULL?$r['answer_id']:"-1") ?>" 
							   data-toggle="tooltip" data-placement="top" title="Edit proposal">
								<span class="pficon pficon-edit"></span> Edit
							</a>
						</div>
						<div class="card-pf-item">
							<a href="/additional/<?= $r['form_id'] . "/" . $r['id'] ?>/-1" data-toggle="tooltip" data-placement="top" title="Additional speakers">
								<span class="pficon pficon-users"></span> Additional speakers
							</a>
						</div>
					</div>
					<?php } ?>
					<div class="card-pf-items text-center">
						<div class="card-pf-item">
							<a href="/proposals/<?= $r['event_id'] . "/" . $r['id'] ?>" data-toggle="tooltip" data-placement="top" title="See proposal">
								<span class="pficon pficon-orders"></span> See proposal
							</a>
						</div>
						<?php if(strtotime(($r['form_close'] != "" && $r['form_close'] != $r['cfp_close']?$r['form_close']:$r['cfp_close'])) >= strtotime(date("Y-m-d", time())) && $r['user_id'] == $_SESSION['loggedUser']){ ?>
						<div class="card-pf-item">
							<a href="#" delete="/proposal/<?= $r['form_id'] ?>/delete/<?= $r['id'] ?>/-1" class="openModal" data-toggle="tooltip" data-placement="top" title="See proposal">
								<span class="pficon pficon-delete"></span> Delete proposal
							</a>
						</div>
						<?php } ?>
					</div>
					<?php if(strtotime(($r['form_close'] != "" && $r['form_close'] != $r['cfp_close']?$r['form_close']:$r['cfp_close'])) >= strtotime(date("Y-m-d", time()))){ ?>
					<p class="card-pf-info text-center"><strong>You can update until</strong> <?= date("d/M/Y", strtotime(($r['form_close'] != "" && $r['form_close'] != $r['cfp_close']?$r['form_close']:$r['cfp_close']))) ?></p>
					<?php } ?>
				</div>
			</div> 
	        </div> <!-- cols -->
	<?php } 
	if(count($responses) > 0){?>
	</div>
	
	<?php }
		$last = ""; foreach($answers as $i=>$a){ 
		if($a['type'] == "session"){
			continue;
		}
		if($last != $a['type']){
			if($i>0){
				echo "</div><div class='row row-cards-pf'><h2>" . ucfirst($a['type']) . " proposals</h2>";
			} else {
				echo "<div class='row row-cards-pf'><h2>" . ucfirst($a['type']) . " proposals</h2>";
			}
			$last = $a['type'];
		}
	?>
                <div class="col-xs-12 col-sm-6 col-md-3">
                        <div class="card-pf card-pf-view card-pf-aggregate-status">
                                <div class="card-pf-body">
                                        <div class="card-pf-top-element">
                                                <span class="card-pf-icon-circle" style="border: none !important; width: 100%;">
                                                        <img src="<?= ($a['logo']!=""?$a['logo']:"/webroot/img/logos/default.png") ?>" <?= (strtotime(($a['form_close'] != "" && $a['form_close'] != $a['cfp_close']?$a['form_close']:$a['cfp_close'])) < strtotime(date("Y-m-d", time()))?'style="width: 100%; object-fit: contain; filter: grayscale(100%); height: inherit"':'style="width: 100%; object-fit: contain; height: inherit"') ?>>
                                                </span>
                                        </div>
                                        <h2 class="card-pf-title text-center"><?= (isset($a['Title'])?$a['Title']:"Proposal no. " .  $a['id']) ?><br>(<?= $a['name'] . " " . $a['year'] .")" ?></h2>
					<?php if(strtotime(($a['form_close'] != "" && $a['form_close'] != $a['cfp_close']?$a['form_close']:$a['cfp_close'])) >= strtotime(date("Y-m-d", time()))){ ?>
					<div class="card-pf-items text-center">
						<div class="card-pf-item">
							<a href="/proposal/<?= $a['form_id'] . "/-1/" .$a['id']?>" data-toggle="tooltip" data-placement="top" title="Edit proposal">
								<span class="pficon pficon-edit"></span> Edit
							</a>
						</div>
                        <?php if($a['type'] != 'quiz'){ ?>
						<div class="card-pf-item">
							<a href="/additional/<?= $a['form_id'] . "/-1/" .$a['id'] ?>" data-toggle="tooltip" data-placement="top" title="Additional speakers">
								<span class="pficon pficon-users"></span> Additional speakers
							</a>
						</div>
                        <?php } ?>
					</div>
					<?php } ?>
					<div class="card-pf-items text-center">
                                                <div class="card-pf-item">
                                                        <a href="/answer/<?= $a['id'] ?>" data-toggle="tooltip" data-placement="top" title="See proposal">
                                                                <span class="pficon pficon-orders"></span> See proposal
                                                        </a>
                                                </div>
						<?php if(strtotime(($a['form_close'] != "" && $a['form_close'] != $a['cfp_close']?$a['form_close']:$a['cfp_close'])) >= strtotime(date("Y-m-d", time())) && $a['user_id'] == $_SESSION['loggedUser']){ ?>
                                                <div class="card-pf-item">
                                                        <a href="#" delete="/proposal/<?= $a['form_id'] ?>/delete/-1/<?= $a['id'] ?>" class="openModal" data-toggle="tooltip" data-placement="top" title="See proposal">
                                                                <span class="pficon pficon-delete"></span> Delete proposal
                                                        </a>
                                                </div>
                                                <?php } ?>
                                        </div>
					<?php if(strtotime(($a['form_close'] != "" && $a['form_close'] != $a['cfp_close']?$a['form_close']:$a['cfp_close'])) >= strtotime(date("Y-m-d", time())) ){ ?>
                                        <p class="card-pf-info text-center"><strong>You can update until</strong> <?= date("d/M/Y", strtotime(($a['form_close'] != "" && $a['form_close'] != $a['cfp_close']?$a['form_close']:$a['cfp_close']))) ?></p>
					<?php } ?>
                                </div>
                        </div>
                </div> <!-- cols -->
	 <?php   } 
	if(count($answers) > 0){ ?>
    </div> <!-- row -->
    <?php }
	} ?>
</div> <!-- container -->
<div class="modal fade" id="confirmations" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
     <div class="modal-dialog">
         <div class="modal-content">
             <div class="modal-header">
                 <button type="button" class="close" data-dismiss="modal" aria-hidden="true" aria-label="Close">
                     <span class="pficon pficon-close"></span>
                 </button>
                 <h4 class="modal-title" id="myModalLabel">Confirm proposal deletion</h4>
             </div> <!-- modal header -->
             <div class="modal-body">
                 <p>Do you really want to delete this proposal? Keep in mind, that this action can't be undone.</p>
             </div>
             <div class="modal-footer">
                 <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                 <a href="" id="modalDelete" class="btn btn-danger">Delete</a>
             </div> <!-- modal footer -->
         </div> <!-- modal content -->
     </div> <!-- modal dialog -->
</div> <!-- modal -->

<script>
  $(document).ready(function() {
    // matchHeight the contents of each .card-pf and then the .card-pf itself
    $(".row-cards-pf > [class*='col'] > .card-pf .card-pf-title").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf > .card-pf-body").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf > .card-pf-footer").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf").matchHeight();

    // Initialize the vertical navigation
    $().setupVerticalNavigation(true);

	// autohide toasts
	$('.toast-pf').mouseenter(function () {
		$(this).attr('mouseenter','true');
	});

	$('.toast-pf').mouseleave(function () {
		$(this).removeAttr('mouseenter');
		if($(this).attr('remove') != null){
			$(this).remove();
		}		
	});

	function hideToasts() {
		var toasts = $('.toast-pf'), i;

		for(i = 0; i < toasts.length; i++){
			if($(toasts[i]).attr('mouseenter') != null){
				$(toasts[i]).attr('remove','true');
			} else {
				$(toasts[i]).hide('slow', function(){ $(toasts[i]).remove(); });
			}
		}
	}

	$(".openModal").click(function (){
	    	$("#modalDelete").attr("href", $(this).attr("delete"));
        	$("#confirmations").modal('show');
	});

	setTimeout(hideToasts, 8000);

  });
</script>
</body>
</html>
  
