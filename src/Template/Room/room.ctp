<div class="container-fluid container-cards-pf container-pf-nav-pf-vertical">
    <ol class="breadcrumb" role="navigation" aria-label="breadcrumbs">
        <li><a href="/"><span class="fa fa-home"></span></a></li>
        <li><a href="/manager">PM Review</a></li>
        <li><a href="/rooms/<?= $event['id'] ?>"><?= $event['name'] . " (" . $event['year'] . ")" ?> - Rooms</a></li>
        <li class="active"><span><?= ($action == "add"?"Add new room":"Modify room") ?></span></li>
    </ol>
	<div class="row row-cards-pf">
		<div class="col-xs-12 ">
			<div class="card-pf card-pf-view">
				<?php if ($action == "add"){ ?>
				<h1 class="card-pf-title">Add new room</h1>
				<?php } else { ?>
				<h1 class="card-pf-title">Modify room</h1>
				<?php } ?>
				<div class="card-pf-body">
					<form id="roomform" enctype="multipart/form-data" action="/room/<?= $event['id'] . "/" . ($action != "add"?$action . "/" . $room['id']:$action) ?>" method="post">
						<input type="hidden" name="_Token" value="<?= $token ?>">
						<input type="hidden" name="_method" value="post">
						<div class="form-group">
							<label class="control-label" for="name">Name</label>
							<input type="text" maxlength="512" class="form-control" value="<?= (!empty($room)?$room['name']:'') ?>" name="name" required>
							<span class="help-block">Enter name for the room.</span>
						</div>
						<div class="form-group">
                            <label class="control-label" for="description">Room description</label>
                            <input type="text" maxlength="1024" class="form-control" name="description"value="<?= (!empty($room)?$room['description']:'') ?>">
                            <span class="help-block">Add room description e.g. Keynote room, Main stage etc.</span>
                        </div>
                        <div class="form-group">
							<label class="control-label" for="capacity">Capacity</label>
							<input type="number" min="1" class="form-control" value="<?= (!empty($room)?$room['capacity']:'') ?>" name="capacity" required>
 							<span class="help-block">Set amount of available seats in the room.</span>
						</div>
                        <div class="form-group">
                            <label class="control-label" for="info">Speaker info</label>
                            <input type="text" maxlength="1024" class="form-control" value="<?= (!empty($room)?$room['info']:'') ?>" name="info">
                            <span class="help-block">Add some speaker related info e.g. HDMI is only available in the room etc..</span>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="info">Available slots in <?= $event['timezone'] ?> time zone</label>
                            <div id="calendar">
                            </div>
                            <span class="help-block">Set availability slots.</span>
                        </div>
						<div class="form-group">
							<span>
								<input type="submit" class="btn btn-primary" name="save" value="Save">
							</span>

						</div>
					</form> <!-- end form -->	

				</div> <!-- end card body -->
			</div> <!-- end card -->
        </div> <!-- cols -->
    </div> <!-- row -->
</div> <!-- container -->
<script src='https://cdn.jsdelivr.net/npm/fullcalendar-scheduler@6.1.10/index.global.min.js'></script>
<script>
  $(document).ready(function() {
    // matchHeight the contents of each .card-pf and then the .card-pf itself
    $(".row-cards-pf > [class*='col'] > .card-pf .card-pf-title").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf > .card-pf-body").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf > .card-pf-footer").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf").matchHeight();

    // Initialize the vertical navigation
    $().setupVerticalNavigation(true);

  });

  document.addEventListener('DOMContentLoaded', function() {
      var calendarEl = document.getElementById('calendar');
      var calendar = new FullCalendar.Calendar(calendarEl, {
        schedulerLicenseKey: 'GPL-My-Project-Is-Open-Source',
        initialView: 'timeGridCustom',
        headerToolbar: false,
        initialDate: '<?= $event['open'] ?>',
        timezone: '<?= $event['timezone'] ?>',
        slotLabelFormat: {
            hour: 'numeric',
            minute: '2-digit',
            meridiem: false,
            hour12: false
        },
        dayHeaderFormat: {
            weekday: 'short',
            month: 'short',
            day: 'numeric'
        },
        views: {
            timeGridCustom: {
                type: 'timeGrid',
                duration: { days: <?= (new DateTime($event['open']))->diff(new DateTime($event['close']))->days+1 ?>}
            }
        },
        <?php if($availability != null && sizeof($availability) > 0){ ?>
        events: [
        <?php foreach($availability as $a){ ?>
            {
                start: "<?= ($a['all_day'] == 1? $a['date'] : $a['date'] . " " . $a['start']) ?>",
                end: "<?= ($a['all_day'] == 1? $a['date'] : $a['date'] . " " . $a['end']) ?>",
                allDay: <?= ($a['all_day'] == 1? "true":"false") ?>,
                overlap: false
            },
        <?php } ?>
        ],
        <?php } ?>
        select: function(info){
            calendar.addEvent({
                start: info.startStr,
                end: info.endStr,
                overlap: false
            });
            $("#calendar").find(".btn-danger").find(".fc-event-title").html("");
            $("#calendar").find(".btn-danger").removeClass("btn-danger");
        },
        eventOverlap: false,
        eventClick: function(info){
            if(info.el.classList.contains("btn-danger")){
                info.event.remove();
            } else {
                info.el.classList.add("btn-danger");
                $(info.el).find(".fc-event-title").html("Click to remove");
            }
        },
        unselectAuto: false,
        editable: true,
        selectable: true,
        selectHelper: true,
        slotDuration: '00:05:00',
      });
      calendar.render();

      $("#roomform").submit(function(event){
        calendar.getEvents().forEach(function(e, i){
            console.log(i + " " + e.allDay);
            $("<input />").attr("type", "hidden")
              .attr("name", "roomblock[" + i + "][start]")
              .attr("value", e.startStr)
              .appendTo("#roomform");

            $("<input />").attr("type", "hidden")
              .attr("name", "roomblock[" + i + "][end]")
              .attr("value", e.endStr)
              .appendTo("#roomform");

            $("<input />").attr("type", "hidden")
              .attr("name", "roomblock[" + i + "][allday]")
              .attr("value", e.allDay)
              .appendTo("#roomform");
        });
        return true;
      });
  });
   
</script>
</body>
</html>
 
