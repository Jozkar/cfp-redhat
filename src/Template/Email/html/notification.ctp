<p>Hi,<br><br>
we would like to inform you, that you have been set as additional speaker for <?= $event['name'] . " (" . $event['year'] . ")" ?> proposal by <?= $speaker ?>.</p>
You can check this proposal here [1].</p>
<p>Regards<br>-- <?= $event['name'] ?> organizers --
<br><br>[1] <a href='http://<?= $domain ?>/myproposals'><?= $domain ?>/myproposals</a></p>
