<p>Hi,<br><br>
You have been set as additional speaker for <?= $event['name'] . " (" . $event['year'] . ")" ?> proposal by <?= $speaker ?>.</p>
<p>Before we can process the proposal, we need some additional profile information from you.<br><br>
Please head here [1], log-in with this email address (<?= $email ?>) and update your profile information.</p>
<p>Regards<br>-- <?= $event['name'] ?> organizers --
<br><br>[1] <a href='http://<?= $domain ?>'><?= $domain ?></a></p>
