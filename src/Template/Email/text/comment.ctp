Hi <?= $name ?>,

<?php if($talk == ""){ ?>we would like to inform you, that a new comment has been added to your proposal no. <?= $answer ?>.
<?php } else { ?>we would like to inform you, that a new comment has been added to your proposal 
'<?= $talk ?>'
<?php } ?>

From: <?= $from ?>


<?= $comment ?>


If you want to reply, please click on 'See proposal' link on 'My proposals' page at our CfP portal [1].

[1] <?= $link ?>

Your <?= $event ?> team.
