<div class="container-fluid container-cards-pf container-pf-nav-pf-vertical">
    <ol class="breadcrumb" role="navigation" aria-label="breadcrumbs">
        <li><a href="/"><span class="fa fa-home"></span></a></li>
    	<li><a href="/manager">PM Review</a></li>
    	<li><a href="/emails/<?= $event['id'] ?>">Email management</a></li>
        <li class="active"><span><?= ucfirst($email['type']) ?> for <?= $email['form_type'] ?></span></li>
    </ol>
	<div class="row row-cards-pf">
		<div class="col-xs-12 ">
			<div class="card-pf card-pf-view">
				<h1 class="card-pf-title">Modify '<?= $email['type'] ?>' email (for <?= $email['form_type'] ?>)</h1>
				<div class="card-pf-body">
					<form class="profile" enctype="multipart/form-data" action="/emails/<?= $event['id'] ?>" method="post">
						<input type="hidden" name="_Token" value="<?= $token ?>">
						<input type="hidden" name="_method" value="post">
						<input type="hidden" name="type" value="<?= $email['type'] ?>">
						<input type="hidden" name="form_type" value="<?= $email['form_type'] ?>">
						<input type="hidden" name="event" value="<?= $event['id'] ?>">
						<div class="form-group">
							<label class="control-label" for="name">Subject</label>
							<input type="text" maxlength="100" class="form-control" value="<?= $email['subject'] ?>" name="subject" required>
							<span class="help-block">Enter subject of email.</span>
						</div>
						<div class="form-group">
							<label class="control-label" for="html">Send as HTML formated message?</label>
							<input class="form-control bootstrap-switch" type="checkbox" value="html" <?= ($email['format']=='html'?'checked ':'') ?>name="format">
							<span class="help-block">HTML tags in email's content will be applied.</span>
						</div>
						<?php if(!in_array($email['type'], array('all','confirmation',"confirmedSpeakers","unconfirmedSpeakers", "to-accept", "to-reject"))){ ?>
						<div class="form-group">
							<label class="control-label" for="sumarize">Group proposal to one email?</label>
							<input class="form-control bootstrap-switch" type="checkbox" value="1" <?= ($email['sumarize']=='1'?'checked ':'') ?>name="sumarize">
							<span class="help-block">If set, speakers will receive only one message with list of all <?= $email['type'] ?>ed proposals.</span>
						</div>
						<?php } else { ?>
						 <input type="hidden" name="sumarize" value="0">
						<?php } ?>
						<div class="form-group">
                            <label class="control-label" for="Bio">Email content</label>
                            <textarea id="textarea" maxlength="65536" class="form-control" rows="15" name="text" required><?= $email['text'] ?></textarea>
                            <span class="help-block">Enter content of email.</span>
                            <span class="help-block">Supported substitues:
							<?php foreach($substitues[$email['type']] as $key => $val){ 
								  echo "<br>" . $key . " = " . $val; 
							      } ?>
                                                </div>

						<div class="form-group">
							<span>
								<input type="submit" class="btn btn-primary" name="save" value="Save">
							</span>

						</div>
					</form> <!-- end form -->	

				</div> <!-- end card body -->
			</div> <!-- end card -->
        </div> <!-- cols -->
    </div> <!-- row -->
</div> <!-- container -->
<script>
  $(document).ready(function() {
    // matchHeight the contents of each .card-pf and then the .card-pf itself
    $(".row-cards-pf > [class*='col'] > .card-pf .card-pf-title").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf > .card-pf-body").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf > .card-pf-footer").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf").matchHeight();

    var editVal = $("#textarea").html();
    var editor = new Jodit("#textarea", {
        "disablePlugins": "symbols,stat,media,imageproperties,imageprocessor,fullsize,draganddropelement,draganddrop,pastestorage,autofocus,addnewline",
        "buttons": "source,|,bold,strikethrough,underline,italic,|,ul,ol,outdent,indent,|,fontsize,paragraph,align,|,link,|,undo,redo,cut,copy,paste,|",
        "buttonsXS": "source,|,bold,strikethrough,underline,italic,|,ul,ol,outdent,indent,|,fontsize,paragraph,align,|,link,|,undo,redo,cut,copy,paste,|",
        "buttonsSM": "source,|,bold,strikethrough,underline,italic,|,ul,ol,outdent,indent,|,fontsize,paragraph,align,|,link,|,undo,redo,cut,copy,paste,|",
        "buttonsMD": "source,|,bold,strikethrough,underline,italic,|,ul,ol,outdent,indent,|,fontsize,paragraph,align,|,link,|,undo,redo,cut,copy,paste,|",
        "events":{ "afterInit": function (event){
                                     setTimeout(function(){
                                     <?= ($email['format']=='html'?"":"event.jodit.toggleMode();") ?>
                                     }, 200);
                                }
        }
    });
    // Initialize switch buttons
    $(".bootstrap-switch").bootstrapSwitch();
    $(".bootstrap-switch[name=format]").on('switchChange.bootstrapSwitch', function(event,state){
        if(state){
            if(editor.getMode() != 1){
                editor.toggleMode();
            }
        } else {
            if(editor.getMode() != 2){
                editor.toggleMode();
            }
        }
    });

    // Initialize the vertical navigation
    $().setupVerticalNavigation(true);
  });
</script>
</body>
</html>
 
