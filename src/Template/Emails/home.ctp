<div class="container-fluid container-cards-pf container-pf-nav-pf-vertical">
    <ol class="breadcrumb" role="navigation" aria-label="breadcrumbs">
        <li><a href="/"><span class="fa fa-home"></span></a></li>
    	<li><a href="/manager">PM Review</a></li>
        <li class="active"><span>Email management</span></li>
    </ol>
    <div class="tab-content">
        <ul class="nav nav-tabs" role="tablist">
            <?php foreach($forms as $i=>$f) { ?>
            <li role="presentation"<?= ($i==0?' class="active"':'') ?>><a href="#<?= str_replace(" ", "_", $f['form_type']) ?>" aria-controlls="<?= str_replace(" ", "_", $f['form_type']) ?>" data-toggle="tab"><?= ucfirst($f['form_type']) ?></a></li>
            <?php } ?>
        </ul>

    <?php foreach($forms as $i=>$f){ ?>
    <div role="tabpanel" style="border: 1px solid #ddd" class="tab-pane<?= ($i==0?" active":"") ?>" id="<?= str_replace(" ", "_", $f['form_type']) ?>">
        <div id="pf-list-simple-expansion" class="list-group list-view-pf list-view-pf-view">
        <?php foreach($emails[$f['form_type']] as $email) { ?>
            <div class="list-group-item">
                <div class="list-group-item-header">
                    <div class="list-view-pf-expand">
                        <span class="fa fa-angle-right"></span>
                    </div>
                    <div class="list-view-pf-actions">
                        <a href="/emails/<?= $event['id'] ?>/edit/<?= $email['type'] ?>/<?= str_replace(" ", "_", $email['form_type']) ?>" class="btn btn-primary">Edit</a>
		                <a href="/emails/<?= $event['id'] ?>/test/<?= $email['type'] ?>/<?= str_replace(" ", "_", $email['form_type']) ?>" class="btn btn-warning">Send me test mail</a>
                        <?php if($f['form_type'] != "event" && $email['type'] != "hotel-paid" && $email['type'] != "hotel-unpaid") { ?>
                        <a href="#" form="<?= $email['form_type'] ?>" action="<?= $email['type'] ?>" event="<?= $event['id'] ?>" data-toggle="modal" data-target="#confirmaiton" 
                            class="btn btn-danger <?=($event['hasSMTP']?"openModal":"openModal\" title=\"No SMTP credentials available. Contact event administrator.")?>">Send to speakers</a>
                        <?php } ?>
                    </div> <!-- actions -->
                    <div class="list-view-pf-main-info">
                        <div class="list-view-pf-left">
                            <span class="pficon list-view-pf-icon-md <?= $types[$email['type']]['logo'] ?>"></span>
                        </div> 
                        <div class="list-view-pf-body">
                            <div class="list-view-pf-description">
                                <div class="list-group-item-heading">
                                    <?= $event['name'] . " (" . $event['year'] . ") - " . $types[$email['type']]['title'] . ($f['form_type'] == 'accommodation' || $f['form_type'] == 'covered accommodation'?"":" (for " . $email['form_type'] . ")") ?>
                                </div>
                            </div> <!-- body description -->
                        </div> <!-- list view body -->
                     </div> <!-- main info -->
                </div><!-- item header -->
                <div class="list-group-item-container container-fluid hidden">
                    <div class="close">
                        <span class="pficon pficon-close"></span>
                    </div>
                    <div class="row">
        	    	    <div class="col-md-12">
                            <p><strong>Format:</strong> <?= $email['format'] ?><br>
                                <strong>Subject: </strong> <?= $email['subject'] ?>
                            </p>
                            <p><strong>Content</strong></p><p><?= ($email['format'] == "text"?nl2br(htmlspecialchars($email['text'])):$email['text']) ?></p>
              		    </div>
                    </div>
                </div><!-- container fluid -->
            </div> <!-- group item -->
            <?php } ?>
        </div> <!-- group list -->
    </div>
    <?php } ?>
    </div>
</div> <!-- container -->

<div class="modal fade" id="confirmations" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
    			<button type="button" class="close" data-dismiss="modal" aria-hidden="true" aria-label="Close">
				        <span class="pficon pficon-close"></span>
		        </button>
       			<h4 class="modal-title" id="myModalLabel">Confirm informing speakers</h4>
			</div> <!-- modal header -->
			<div class="modal-body">
				<p>You are going to send email message to large group of speakers. Are you sure, you have completed your program manager review and you have all sumbissions in Accepted, Waitlist or Rejected group? This action can't be undone!</p>
			</div>
		 	<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
		        <button type="button" id="modalSend" class="btn btn-danger" data-dismiss="modal">I am sure, send it</button>
			</div> <!-- modal footer -->
			</div> <!-- modal content -->
	</div> <!-- modal dialog -->
</div> <!-- modal -->

<script>
  $(document).ready(function() {
    // matchHeight the contents of each .card-pf and then the .card-pf itself
    $(".row-cards-pf > [class*='col'] > .card-pf .card-pf-title").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf > .card-pf-body").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf > .card-pf-footer").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf").matchHeight();

    // Initialize the vertical navigation
    $().setupVerticalNavigation(true);

    $(".openModal").click(function (){
		$("#modalSend").attr("event", $(this).attr("event"));
		$("#modalSend").attr("action", $(this).attr("action"));
		$("#modalSend").attr("form", $(this).attr("form"));
		$("#confirmations").modal('show');
    });

    $("#modalSend").click(function (){
        var xhr = new XMLHttpRequest(),
            event_id = $(this).attr("event"),
		    action = $(this).attr("action"),
		    form = $(this).attr("form");

        xhr.open('POST', '/emailpi/');
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.onreadystatechange = function() {
            if (this.readyState == 3) {
              showSuccessToast("Your request has been scheduled on server.");
            }
        };
        xhr.onload = function() {
            try{
                var data = JSON.parse(xhr.responseText);
            } catch (e) {
                $("#confirmations").modal('hide');
                showErrorToast(xhr.responseText);
                return;
            }
            if(data.result != 'success'){
                $("#confirmations").modal('hide');
                showErrorToast(data.reason);
            } else {
                showSuccessToast("Your request is done.");
            }
        };

        xhr.send(jQuery.param({'token': '<?= $token ?>', 'event': event_id, 'action': action, 'form': form}));    

    });

     function showSuccessToast(message){
        var toast = $(".toast-notifications-list-pf").html()+
                    '<div class="toast-pf alert alert-success alert-dismissable">'+
                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">'+
                    '<span class="pficon pficon-close"></span>'+
                    '</button>'+
                    '<span class="pficon pficon-ok"></span>'+
                    message+
                    '</div>';
         $(".toast-notifications-list-pf").html(toast);        
    }

    function showErrorToast(message){
        var toast = $(".toast-notifications-list-pf").html()+
                    '<div class="toast-pf alert alert-danger alert-dismissable">'+
                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">'+
                    '<span class="pficon pficon-close"></span>'+
                    '</button>'+
                    '<span class="pficon pficon-error-circle-o"></span>'+
                    message+
                    '</div>';
         $(".toast-notifications-list-pf").html(toast);        
    }

	// autohide toasts
	$('.toast-pf').mouseenter(function () {
		$(this).attr('mouseenter','true');
	});

	$('.toast-pf').mouseleave(function () {
		$(this).removeAttr('mouseenter');
		if($(this).attr('remove') != null){
			$(this).remove();
		}		
	});

	function hideToasts() {
		var toasts = $('.toast-pf'), i;

		for(i = 0; i < toasts.length; i++){
			if($(toasts[i]).attr('mouseenter') != null){
				$(toasts[i]).attr('remove','true');
			} else {
				$(toasts[i]).hide('slow', function(){ $(toasts[i]).remove(); });
			}
		}
	}

    // click the list-view heading then expand a row
    $("#pf-list-simple-expansion .list-group-item-header").click(function(event){
      if(!$(event.target).is("button, a, input, .fa-ellipsis-v")){
        $(this).find(".fa-angle-right").toggleClass("fa-angle-down")
          .end().parent().toggleClass("list-view-pf-expand-active")
            .find(".list-group-item-container").toggleClass("hidden");
      } else {
      }
    })

    // click the close button, hide the expand row and remove the active status
    $("#pf-list-simple-expansion .list-group-item-container .close").on("click", function (){
      $(this).parent().addClass("hidden")
        .parent().removeClass("list-view-pf-expand-active")
          .find(".fa-angle-right").removeClass("fa-angle-down");
    })

    setTimeout(hideToasts, 8000);

  });
</script>
</body>
</html>  
