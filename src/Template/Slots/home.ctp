<div class="container-fluid container-cards-pf container-pf-nav-pf-vertical">
    <ol class="breadcrumb" role="navigation" aria-label="breadcrumbs">
        <li><a href="/"><span class="fa fa-home"></span></a></li>
	<li><a href="/manager">PM Review</a></li>
        <li class="active"><span>Timeslots</span></li>
    </ol>
<div class="row row-cards-pf">
		<div class="col-xs-12 ">
			<div class="card-pf card-pf-view">
				<h1 class="card-pf-title">Time allocation per track</h1>
                <h5>Fill total number of minutes allocated for each track.</h5>            
				<div class="card-pf-body">
					<form class="form-horizontal" enctype="multipart/form-data" action="/slots/<?= $event['id'] ?> /" method="post" role="form">
						<input type="hidden" name="_Token" value="<?= $token ?>">
                        <input type="hidden" name="event" value="<?= $event['id'] ?>">
						<input type="hidden" name="_method" value="post">
                        <?php foreach($topics as $t){ ?>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="<?= $t['id'] ?>"><?= $t['name'] ?></label>
                            <div class="col-sm-10">
    							<input type="number" maxlength="4" class="form-control col-sm-10" value="<?= (!empty($t['timeAllocation'])?$t['timeAllocation']:'0') ?>" name="<?= $t['id'] ?>" required>
                            </div>
						</div>
                        <?php } ?>
						<div class="form-group">
							<span class="col-sm-2 control-label">
								<input type="submit" class="btn btn-primary" name="save" value="Save">
							</span>

						</div>
					</form> <!-- end form -->	

				</div> <!-- end card body -->
			</div> <!-- end card -->
        </div> <!-- cols -->
    </div> <!-- row -->
</div> <!-- container -->
</body>
<script>
    $().setupVerticalNavigation(true);
</script>
</html>
 
