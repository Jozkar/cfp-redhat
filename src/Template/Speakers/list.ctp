<div class="container-fluid container-cards-pf container-pf-nav-pf-vertical nav-pf-persistent-secondary">
    <ol class="breadcrumb" role="navigation" aria-label="breadcrumbs">
        <li><a href="/"><span class="fa fa-home"></span></a></li>
    	<li><a href="/manager">PM Review</a></li>
        <li class="active"><span>Speakers</span></li>
    </ol>
    <div class="list-group list-view-pf">
	<div class="list-group-item list-view-pf-stacked">
		<div class="list-view-pf-main-info">
			<div class="list-view-pf-left">
				<span class="fa list-view-pf-icon-md" style="background: none !important">
					<img src="<?= ($eventInfo['logo']!=""?$eventInfo['logo']:"/webroot/img/logos/default.png") ?>" style="width: 100%; object-fit: contain">
				</span>
			</div>
			<div class="list-view-pf-body">
				<div class="list-view-pf-description">
					<div class="list-group-item-heading">
						<?= $eventInfo['name'] . " (" . $eventInfo['year'] . ")" ?>
					</div>
					<div class="list-group-item-text">
						Number of speakers: <?= count($users) ?>
					</div>
				</div>
			</div>
			<div class="list-view-pf-right">
				<a href="/speakers/<?= $eventInfo['id'] ?>/export" class="btn btn-primary">Export email addresses (.csv)</a>
				<a href="/speakers/<?= $eventInfo['id'] ?>/exportAll" class="btn btn-success">Export speaker's info (.csv)</a>
			</div>
		</div>
	</div>
    </div>
    <div class="row row-cards-pf">
	<?php foreach($users as $a){ ?>
        	<div class="col-xs-12 col-sm-6 col-md-3">
			<div class="card-pf card-pf-view card-pf-aggregate-status">
				<div class="card-pf-body">
					<div class="card-pf-top-element">
						<span class="card-pf-icon-circle" style="border: none !important">
							<img src="<?= $a['avatar'] ?>" style="width: 100%; object-fit: contain">
						</span>
					</div>
					<h2 class="card-pf-title text-center"><?= $a['first_name'] . " " . $a['last_name'] . "<br>(" . $a['email'] .")" ?></h2>
				</div>
			</div> 
	        </div> <!-- cols -->
	<?php } ?>
    </div> <!-- row -->
</div> <!-- container -->
<script>
  $(document).ready(function() {
    // matchHeight the contents of each .card-pf and then the .card-pf itself
    $(".row-cards-pf > [class*='col'] > .card-pf .card-pf-title").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf > .card-pf-body").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf > .card-pf-footer").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf").matchHeight();

    // Initialize the vertical navigation
    $().setupVerticalNavigation(true);

    // autohide toasts
    $('.toast-pf').mouseenter(function () {
	$(this).attr('mouseenter','true');
    });

    $('.toast-pf').mouseleave(function () {
	$(this).removeAttr('mouseenter');
	if($(this).attr('remove') != null){
		$(this).remove();
	}		
    });

    function hideToasts() {
	var toasts = $('.toast-pf'), i;

	for(i = 0; i < toasts.length; i++){
		if($(toasts[i]).attr('mouseenter') != null){
			$(toasts[i]).attr('remove','true');
		} else {
			$(toasts[i]).hide('slow', function(){ $(toasts[i]).remove(); });
		}
	}
    }

    setTimeout(hideToasts, 8000);

  });
</script>
</body>
</html>
  
