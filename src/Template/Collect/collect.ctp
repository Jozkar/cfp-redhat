<div class="container-fluid container-cards-pf container-pf-nav-pf-vertical">
    <ol class="breadcrumb" role="navigation" aria-label="breadcrumbs">
        <li><a href="/"><span class="fa fa-home"></span></a></li>
       	<li><a href="/acceptproposals">My accepted</a></li>
       	<li><a href="/acceptedproposals/<?= $event['id'] ?>">Confirmed proposals</a></li>
        <li class="active"><span>Proposal data upload</span></li>
    </ol>
    <div class="row row-cards-pf">
    <div class="col-xs-12">
        <div class="card-pf card-pf-view">
    	<h1>Proposal data upload for <?= $event['name'] . " (" . $event['year'] . ")" ?></h1>
    	<p>Questions marked with <span class='required-pf'>*</span> are <strong>required</strong>.</p>
        <div class="card-pf-body">
            <h3>You're uploading data for <strong><?= $proposal['title'] ?></strong>.</h3>
    		<form action="/collect/<?= ($canswer_id != null?"update":"add") ?>/<?= $form['id'] ?>" onsubmit="checkRequired(event)" method="post">
    			<input type="hidden" name="_method" value="post">
                <input type="hidden" name="answerId" value="<?= $answer_id ?>">
                <input type="hidden" name="collectAnswerId" value="<?= ($canswer_id==null?0:$canswer_id) ?>">
                <input type="hidden" name="responseId" value="<?= $response_id ?>">
    		<?php

    			$required = array();
    			foreach ($questions as $i=>$q){
    				if($q['required']){
    					$required[]=array("type"=>$q['type'], "name"=>$q['take_as']);
    				}
    			?>
    			<div class="form-group">
    		<?php
    			switch ($q['type']){
    				case "text": ?>
    					<label class="control-label" for="<?= $q['take_as'] ?>"><?= ($q['required']?"<span class='required-pf'>*</span> ":"") ?><?= $q['question'] ?></label>
    					<?php if ($q['help_text'] != "") {?>
    					<span class="help-block"><?= $q['help_text'] ?></span>
    					<?php } ?>
    					<input type="text" class="form-control" name="<?= $q['take_as'] ?>"
                            <?php if($values != NULL && $values[$i] != NULL){
                                echo ' value="' . htmlspecialchars($values[$i]) . '"';
                            }?>
    						<?= ($q['size'] > 0?' maxlength="' . $q['size'] . '"':'') ?><?= ($q['required']?" required":"") ?>>
    					<?php 
                            if($q['size'] > 0){?>
                        <span class="pull-right chars-remaining-pf">
                            <span id="cRCF<?= $q['take_as'] ?>"></span> characters remaining
                        </span>
                        <script>
                            (function($) {
                                $('input[name=<?= $q['take_as'] ?>]').countRemainingChars( {
                                    countFld: 'cRCF<?= $q['take_as'] ?>',
                                    charsMaxLimit: <?= $q['size'] ?>,
                                    charsWarnRemaining: <?= ($q['size']-5 >= 0?5:0) ?>,
                                    blockInputAtMaxLimit: false} );

                            })(jQuery);
                        </script>
                        <?php }
                            break;

    				case "textarea": ?>
    					<label class="control-label" for="<?= $q['take_as'] ?>"><?= ($q['required']?"<span class='required-pf'>*</span> ":"") ?><?= $q['question'] ?></label>
    					<?php if ($q['help_text'] != "") {?>
    					<span class="help-block"><?= $q['help_text'] ?></span>
    					<?php } ?>
    					<textarea class="form-control" name="<?= $q['take_as'] ?>"<?= ($q['size'] > 0?' maxlength="' . $q['size'] . '"':'') ?><?= ($q['required']?" required":"") ?>
    					><?php if($values != NULL && $values[$i] != NULL) {
                            echo htmlspecialchars($values[$i]);
                        } ?></textarea>
    					<?php 
                            if($q['size'] > 0){?>
                        <span class="pull-right chars-remaining-pf">
                            <span id="cRCF<?= $q['take_as'] ?>"></span> characters remaining
                        </span> 
                        <script>
                            (function($) {
                                $('textarea[name=<?= $q['take_as'] ?>]').countRemainingChars( {
                                    countFld: 'cRCF<?= $q['take_as'] ?>',
                                    charsMaxLimit: <?= $q['size'] ?>,
                                    charsWarnRemaining: <?= ($q['size']-5 >= 0?5:0) ?>,
                                    blockInputAtMaxLimit: false} );

                            })(jQuery);
                        </script>
                        <?php }
    					break;
    				case "radio":
    				case "checkbox":?>
    					<label class="control-label" for="<?= $q['take_as'] ?>"><?= ($q['required']?"<span class='required-pf'>*</span> ":"") ?><?= $q['question'] ?></label><br>
    					<?php if ($q['help_text'] != "") {?>
    					<span class="help-block"><?= $q['help_text'] ?></span>
    					<?php } ?>
    					<ul>
    					<?php
    					if($q['take_as'] == "topics"){
    						foreach($topics as $t){?>
    							<li><label class="<?= $q['type'] ?>"><input name='<?= $q["take_as"] ?><?= ($q['type'] == "checkbox"?"[]":"") ?>' value='<?= $t['id'] ?>' type='<?= $q['type'] ?>'
                                <?= (in_array($t['id'], $vas)?" checked": "") ?>>
    							<?= $t['name'] ?></label></li>
    						<?php }
    					} else {
    						foreach($q['options'] as $o){ ?>
    							<li><label class="<?= $q['type'] ?>"><input name='<?= $q["take_as"] ?><?= ($q['type'] == "checkbox"?"[]":"") ?>' value='<?= $o['value'] ?>' 
    								type='<?= $q['type'] ?>'<?= (count($q['options']) == 1 && $q['required']==1?" required":"") ?>
                                    <?= (in_array($o['value'], $vas)?" checked": "") ?>>
    								<?= $o['label'] ?></label></li>
    							<?php 
    						}								
    					} ?>
    					</ul>
    					<?php	
    					break;
    				case "description":?>
    					<?php if ($q['question'] != ""){ ?><h2><?= $q['question'] ?></h2><?php } ?>
    					<?php if ($q['help_text'] != ""){ ?><p><?= $q['help_text'] ?></p><?php } 
    					break;
    			}?>
    			</div>
    		<?php } ?>
    			<div class="form-group">
    				<span>
    					<input type="submit" class="btn btn-success" name="save" value="Upload data">
    				</span><span class="spinner spinner-sm spinner-inline hidden" style="vertical-align: middle"></span>
    			</div>
    		</form>
                  </div>
            </div>
        </div>
    </div>
</div> <!-- container -->
<script>
  var required = [<?php  
    foreach($required as $r){
    	echo "{'name': '" . $r['name'] . "', 'type': '" . $r['type'] . "'},";
    }
  ?>];

  $(document).ready(function() {
    // matchHeight the contents of each .card-pf and then the .card-pf itself
    $(".row-cards-pf > [class*='col'] > .card-pf .card-pf-title").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf > .card-pf-body").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf > .card-pf-footer").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf").matchHeight();

    // Initialize the vertical navigation
    $().setupVerticalNavigation(true);

    // Initialize Boostrap-Combobox
    $('.combobox').combobox();

    $('#NaN').attr('autocomplete','off');

    // autohide toasts
    $('.toast-pf').mouseenter(function () {
    $(this).attr('mouseenter','true');
    });

    $('.toast-pf').mouseleave(function () {
    $(this).removeAttr('mouseenter');
    if($(this).attr('remove') != null){
    	$(this).remove();
    }		
    });

    function hideToasts() {
    var toasts = $('.toast-pf'), i;

    for(i = 0; i < toasts.length; i++){
    	if($(toasts[i]).attr('mouseenter') != null){
    		$(toasts[i]).attr('remove','true');
    	} else {
    		$(toasts[i]).hide('slow', function(){ $(toasts[i]).remove(); });
    	}
    }
    }

    setTimeout(hideToasts, 8000);

  });

  function checkRequired(e){
    $("input[type=submit]").addClass("disabled");
    $(".spinner").removeClass("hidden");
    var notify = false;
    $.each(required, function(key, val){
    	if(val.type == "checkbox" || val.type == "radio"){
    		var name = "*[name=" + val.name + (val.type == "checkbox"?"\\[\\]":"") + "]";
    		if(!($(name).is(":checked"))){
    			$(name).closest(".form-group").addClass("has-error");
    			notify = true;
    		}
    	}else{
    		var name = "*[name=" + val.name + "]";
    		if($.trim($(name).val()) == ""){
    			$(name).closest(".form-group").addClass("has-error");
    			notify = true;
    		}
    	}
    });

    if(notify){
    	e.preventDefault();
    	$("input[type=submit]").removeClass("disabled");
    	$(".spinner").addClass("hidden");
    	showToast("There are some mistakes in the form. Please, check and fill all required fields and submit the form again.");
    }
  }

  function showToast(message){
        var toast = $(".toast-notifications-list-pf").html()+
                    '<div class="toast-pf alert alert-danger alert-dismissable">'+
                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">'+
                    '<span class="pficon pficon-close"></span>'+
                    '</button>'+
                    '<span class="pficon pficon-error-circle-o"></span>'+
                    message+
                    '</div>';
    $(".toast-notifications-list-pf").html(toast);        
  }

</script>
</body>
</html>  
