<?php
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
<div class="toast-pf alert alert-danger alert-dismissable">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
    <span class="pficon pficon-close"></span>
  </button>
  <span class="pficon pficon-error-circle-o"></span>
  <?= $message ?>
</div>
