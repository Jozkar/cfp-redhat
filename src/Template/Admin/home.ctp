<div class="container-fluid container-cards-pf container-pf-nav-pf-vertical">
    <ol class="breadcrumb" role="navigation" aria-label="breadcrumbs">
        <li><a href="/"><span class="fa fa-home"></span></a></li>
        <li class="active"><span>Administration</span></li>
    </ol>
    <a href="/event" class="btn btn-primary">Create new event</a>
    <a href="/topics" class="btn btn-warning">Manage topics</a>
    <?php if ($superuser){ ?>
    <a href="/domains" class="btn btn-danger">Manage domains</a>
    <?php } ?>
    <?php if(count($events) < 1) { ?>
    <div class="blank-slate-pf">
        <div class="blank-slate-pf-icon">
            <span class="pficon pficon-info"></span>
        </div>
        <h1>There is no event scheduled yet.</h1>
        <p>Click on Create new event to create one.</p>

    <?php }else{ ?>
    <div id="pf-list-standard" class="list-group list-view-pf list-view-pf-view">
    <?php foreach($events as $event) { ?>
        <div class="list-group-item">
            <div class="list-view-pf-actions">
                <a href="/event/edit/<?= $event['id'] ?>" class="btn btn-primary">Edit</a>
                <a href="/administrators/<?= $event['id'] ?>" class="btn btn-success">Event administrators</a>
                <?php if ($event['redhat_event']) { ?>
                <a href="/managers/<?= $event['id'] ?>" class="btn btn-default">Program managers</a>
                <a href="/forms/<?= $event['id'] ?>" class="btn btn-warning">Manage forms</a>
                <div class="dropdown pull-right dropdown-kebab-pf">
                    <button class="btn btn-link dropdown-toggle" type="button" id="dropdownKebabRight9" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        <span class="fa fa-ellipsis-v"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownKebabRight9">
                        <li><a href="/smtp/<?= $event['id'] ?>">SMTP email settings</a></li>
                		<li><a href="/emails/<?= $event['id'] ?>">Manage email's content</a></li>
                        <li><a href="/reviewers/<?= $event['id'] ?>">Reviewers</a></li>
                        <li><a href="/speakers/<?= $event['id'] ?>">Speakers</a></li>
    		            <li><a href="/submit/<?= $event['id'] ?>">Submit proposal for speaker</a></li>
                        <li role="separator" class="divider"></li>
                		<li><a href="/event/duplicate/<?= $event['id'] . "/" . $token ?>">Duplicate event</a></li>
                        <li><a href="#" class="openModal" data-toggle="modal" data-target="#confirmation" event-id="/event/delete/<?= $event['id'] . "/" . $token ?>">Delete</a></li>
                    </ul>
                </div> <!-- drop down -->
                <?php } else { ?>
                <a href="#" data-toggle="modal" data-target="#confirmation" event-id="/event/delete/<?= $event['id'] . "/" . $token ?>" class="openModal btn btn-danger">Delete</a>
                <?php } ?>
            </div> <!-- actions -->
            <div class="list-view-pf-main-info">
                <div class="list-view-pf-left">
                    <span class="fa list-view-pf-icon-sm" style="border: none !important">
    		<img src="<?= ($event['logo']!=""?$event['logo']:"/webroot/img/logos/default.png") ?>" style="width: 100%; object-fit: contain">
                    </span>
                </div> 
                <div class="list-view-pf-body">
                    <div class="list-view-pf-description">
                        <div class="list-group-item-heading">
                            <a href="<?= $event['url'] ?>" target="_blank"><?= $event['name'] . " (" . $event['year'] . ")" ?></a>
                            <?php if ($event['for_redhat']) { ?>
                                <br><div class="list-view-pf-additional-info-item">
                                <span class="pficon pficon-warning-triangle-o"></span>
                                Internal event
                            </div>
                        <?php } ?>

                        </div>
                    </div> <!-- body description -->
                    <div class="list-view-pf-additional-info">
                        <div class="list-view-pf-additional-info-item">
                            <span class="pficon pficon-ok"></span>
                            CfP open <?= date("j/M/Y", strtotime($event['cfp_open'])) ?>
                        </div>
                        <div class="list-view-pf-additional-info-item">
                            <span class="pficon pficon-error-circle-o"></span>
                            CfP close <?= date("j/M/Y", strtotime($event['cfp_close'])) ?>
                        </div>
                    </div> <!-- additional info -->
                </div> <!-- list view body -->
            </div> <!-- main info -->
        </div> <!-- group item -->
        <?php }} ?>
    </div> <!-- group list -->
</div> <!-- container -->

<div class="modal fade" id="confirmations" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog">
    	<div class="modal-content">
    		<div class="modal-header">
    				<button type="button" class="close" data-dismiss="modal" aria-hidden="true" aria-label="Close">
    			        <span class="pficon pficon-close"></span>
    		        </button>
            		<h4 class="modal-title" id="myModalLabel">Confirm event deletion</h4>
    		</div> <!-- modal header -->
    		<div class="modal-body">
    			<p>Do you really want to delete this event? Keep in mind, that this action will remove all proposals assigned to this event and can't be undone.</p>
    		</div>
    	 	<div class="modal-footer">
    			<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
    		        <a href="" id="modalDelete" class="btn btn-danger">Delete</a>
    		</div> <!-- modal footer -->
    	</div> <!-- modal content -->
    </div> <!-- modal dialog -->
</div> <!-- modal -->

<script>
  $(document).ready(function() {
    // matchHeight the contents of each .card-pf and then the .card-pf itself
    $(".row-cards-pf > [class*='col'] > .card-pf .card-pf-title").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf > .card-pf-body").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf > .card-pf-footer").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf").matchHeight();

    // Initialize the vertical navigation
    $().setupVerticalNavigation(true);

    $(".openModal").click(function (){
    	$("#modalDelete").attr("href", $(this).attr("event-id"));
    	$("#confirmations").modal('show');
    });

    // autohide toasts
    $('.toast-pf').mouseenter(function () {
    	$(this).attr('mouseenter','true');
    });

    $('.toast-pf').mouseleave(function () {
    	$(this).removeAttr('mouseenter');
    	if($(this).attr('remove') != null){
    		$(this).remove();
    	}		
    });

    function hideToasts() {
    	var toasts = $('.toast-pf'), i;

    	for(i = 0; i < toasts.length; i++){
    		if($(toasts[i]).attr('mouseenter') != null){
    			$(toasts[i]).attr('remove','true');
    		} else {
    			$(toasts[i]).hide('slow', function(){ $(toasts[i]).remove(); });
    		}
    	}
    }

    setTimeout(hideToasts, 8000);

  });
</script>
</body>
</html>  
