<div class="container-fluid container-cards-pf container-pf-nav-pf-vertical">
    <ol class="breadcrumb" role="navigation" aria-label="breadcrumbs">
        <li><a href="/"><span class="fa fa-home"></span></a></li>
        <li class="active"><span>Review tasks</span></li>
    </ol>
	<?php if(count($events) < 1) { ?>
	<div class="blank-slate-pf">
        <div class="blank-slate-pf-icon">
            <span class="pficon pficon-info"></span>
        </div>
        <h1>No event is currently assigned to you to review.</h1>
        <p>Get in touch with event organizers, if you think, you should see there somethig.</p>

	<?php }else{ ?>
    <?php foreach($events as $i=>$event) { 
		if($event['form_type'] != $lastType){
			$lastType = $event['form_type'];
			if($i > 0){
	 ?>
	</div>
	<?php 		} ?>
	<h1><?= $inflector->pluralize(ucfirst($event['form_type'])) ?></h1>
	<div id="pf-list-standard" class="list-group list-view-pf list-view-pf-view" style="margin-top: 0px">
	<?php } ?>
        <div class="list-group-item">
            <div class="list-view-pf-actions">
                <?php if ($event['captain']) { ?>
                <a href="/capreview/<?= $event['id']."/".$event['form_type'] ?>" class="btn btn-warning">Captain review</a>
                	<?php if($event['count'] > 1) { ?>
			<a href="/review/<?= $event['id']."/".$event['form_type'] ?>" class="btn btn-default">Review proposals</a>
			<?php } ?>
		<a href="/capreviewers/<?= $event['id'] ?>" class="btn btn-primary">Set reviewers</a>
                <?php } else { ?>
                <a href="/review/<?= $event['id']."/".$event['form_type'] ?>" class="btn btn-default">Review proposals</a>
		<?php } ?>
            </div> <!-- actions -->
            <div class="list-view-pf-main-info">
                <div class="list-view-pf-left">
                    <span class="fa list-view-pf-icon-sm" style="border: none !important">
			<img src="<?= ($event['logo']!=""?$event['logo']:"/webroot/img/logos/default.png") ?>" style="width: 100%; object-fit: contain">
                    </span>
                </div> 
                <div class="list-view-pf-body">
                    <div class="list-view-pf-description">
                        <div class="list-group-item-heading">
                            <?= $event['name'] . " (" . $event['year'] . ")" ?>
                        </div>
                    </div> <!-- body description -->
                    <div class="list-view-pf-additional-info">
                        <div class="list-view-pf-additional-info-item">
                            <span class="pficon pficon-ok"></span>
                            CfP open <?= date("j/M/Y", strtotime($event['cfp_open'])) ?>
                        </div>
                        <div class="list-view-pf-additional-info-item">
                            <span class="pficon pficon-error-circle-o"></span>
                            CfP close <?= date("j/M/Y", strtotime($event['cfp_close'])) ?>
                        </div>
                        <?php if ($event['for_redhat']) { ?>
                            <div class="list-view-pf-additional-info-item">
                                <span class="pficon pficon-security"></span>
                                Internal event
                            </div>
                        <?php } else { ?> 
                            <div class="list-view-pf-additional-info-item">
                                <span class="pficon pficon-warning-triangle-o"></span>
                                Public event
                            </div>
			<?php } ?>
                    </div> <!-- additional info -->
                </div> <!-- list view body -->
            </div> <!-- main info -->
        </div> <!-- group item -->
        <?php }} ?>
    </div> <!-- group list -->
</div> <!-- container -->
<script>
  $(document).ready(function() {
    // matchHeight the contents of each .card-pf and then the .card-pf itself
    $(".row-cards-pf > [class*='col'] > .card-pf .card-pf-title").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf > .card-pf-body").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf > .card-pf-footer").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf").matchHeight();

    // Initialize the vertical navigation
    $().setupVerticalNavigation(true);

	// autohide toasts
	$('.toast-pf').mouseenter(function () {
		$(this).attr('mouseenter','true');
	});

	$('.toast-pf').mouseleave(function () {
		$(this).removeAttr('mouseenter');
		if($(this).attr('remove') != null){
			$(this).remove();
		}		
	});

	function hideToasts() {
		var toasts = $('.toast-pf'), i;

		for(i = 0; i < toasts.length; i++){
			if($(toasts[i]).attr('mouseenter') != null){
				$(toasts[i]).attr('remove','true');
			} else {
				$(toasts[i]).hide('slow', function(){ $(toasts[i]).remove(); });
			}
		}
	}

	setTimeout(hideToasts, 8000);

  });
</script>
</body>
</html>  
