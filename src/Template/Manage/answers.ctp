<div class="container-fluid container-cards-pf container-pf-nav-pf-vertical">
    <ol class="breadcrumb" role="navigation" aria-label="breadcrumbs">
        <li><a href="/"><span class="fa fa-home"></span></a></li>
	<li><a href="/manager">PM Review</a></li>
        <li class="active"><span><?= $eventInfo['name'] . " (" . $eventInfo['year'] . ") - " . ($form['placeholder'] != ""? $inflector->pluralize(ucfirst($form['type'])) . " (" . $inflector->pluralize(ucfirst($form['placeholder'])) . ")" : $inflector->pluralize(ucfirst($form['type']))) ?></span></li>
    </ol>
	<h1>Manage <?= ($form['placeholder'] != ""? $form['type'] . " (" . $form['placeholder'] . ")" : $form['type']) ?> proposals for <?= $eventInfo['name'] . " (" . $eventInfo['year'] . ")" ?></h1>
	<?php if((count($unreviewed) + count($accepted) + count($rejected) + count($waitlist)) < 1) { ?>
	<div class="blank-slate-pf">
        <div class="blank-slate-pf-icon">
            <span class="pficon pficon-info"></span>
        </div>
        <h1>No responses are ready to manage.</h1>
        <p>You have to wait for them. Meanwhile you can help with promotion of this event.</p>

	<?php }else{ ?>
			<div class="modal fade" id="responseDetail" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header btn-success" style="background-color: #3f9c35;">
        						<button type="button" class="close close-white" data-dismiss="modal" aria-hidden="true" aria-label="Close">
							        <span class="pficon pficon-close"></span>
						        </button>
			        			<h4 class="modal-title">PM Review: Response</h4>
						</div> <!-- modal header -->
						<div class="modal-body">
							<div class="h2 text-center" id="modalTitle"></div>
							<div class="tab-content">
								<ul class="nav nav-tabs" role="tablist">
									<li role="presentation" id="tabActive" class="active"><a href="#modalBody" aria-controls="modalBody" role="tab" data-toggle="tab">Proposal</a></li>
									<li role="presentation"><a href="#comments" aria-controls="comments" role="tab" data-toggle="tab">Comments <span class="badge" id="comments-count"></span></a></li>
								</ul>
								<div class="tab-pane active" id="modalBody"></div>
								<div class="tab-pane" id="comments">
									<div class="form-group">
										<label class="control-label">Add comment</label>
										<textarea id="comment-input" class="form-control"></textarea>
									</div>
									<div class="form-group">
										<span><a href="#" class="btn btn-primary add-comment" pub='0' style="float: right">Add</a></span>
										<span><a href="#" class="btn btn-danger add-comment" pub='1'>Send comment to speaker</a></span>
									</div>
									<div id="comments-list"></div>
								</div>
							</div>
						</div>
					 	<div class="modal-footer">
							<a id="accept" class="btn btn-success">Accept</a>
							<a id="prev" class="btn btn-default">Prev</a>
                                                        <a id="wait" class="btn btn-warning">Waitlist</a>
							<a id="skip" class="btn btn-default">Next</a>
							<a id="reject" class="btn btn-danger">Reject</a>
						</div> <!-- modal footer -->
 					</div> <!-- modal content -->
				</div> <!-- modal dialog -->
			</div> <!-- modal -->
            	<div class="tab-content"><ul class="nav nav-tabs nav-tabs-pf" style="margin-bottom: 8px" role="tablist">
            		<li role="presentation" class="active"><a href="#unreviewed" aria-controls="unreviewed" role="tab" data-toggle="tab">Unreviewed <span class="label label-default" id="unreviewed-count"><?= count($unreviewed) ?></span></a></li>
            		<li role="presentation"><a href="#accepted" aria-controls="accepted" role="tab" data-toggle="tab">Accepted <span class="label label-success" id="accepted-count"><?= count($accepted) ?></span></a></li>
            		<li role="presentation"><a href="#waitlist" aria-controls="waitlist" role="tab" data-toggle="tab">Waitlist <span class="label label-warning" id="waitlist-count"><?= count($waitlist) ?></span></a></li>
            		<li role="presentation"><a href="#rejected" aria-controls="rejected" role="tab" data-toggle="tab">Rejected <span class="label label-danger" id="rejected-count"><?= count($rejected) ?></span></a></li>
            	</ul>
		        <div role="tabpanel" class="tab-pane active" id="unreviewed">
                	<table class="table table-striped table-bordered table-hover" id="table-unreviewed" style="background: white; height: auto !important">
        	    		<thead>
        	    			<tr>
						<th>#</th>
						<th>Title</th>
   	    				<th>Speaker</th>
                        <th>Duration</th>
   	    				<th>No. Comments</th>
						<th>Score</th>
						<th>Captain's vote</th>
						<th>Status</th>
						<th>External link</th>
						<th>Edit</th>
						<th>Addit Speakers</th>
        	    			</tr>
        	    		</thead>
                		<tbody>
		        		<?php	foreach($unreviewed as $r) { ?>	
		        			<tr style="cursor: pointer" answer_id="<?= $r['answer_id'] ?>">
							<td><?= $r['answer_id'] ?></td>
							<td><?php if(isset($r['Title'])) { ?>
                                <?= $r['Title'] ?>
                            <?php }else{ ?>
                                <?= ucfirst($form['type']) . " proposal no. " . $r['answer_id'] ?>
                            <?php } ?></td>
							<td><?= $r['speaker'] ?></td>
                            <td class='duration'>
						<?php if(isset($r['Duration'])) { 
						    echo $r['Duration'];
						    } else {
						        echo 0;
						    } ?>
						</td>
							<td><?= count($r['comments']) ?></td>
							<td class='score'><?=($r['score']!=""?$r['score']:"0") ?></td>
							<td><?=($r['captainscore']!=""?$r['captainscore']:"-empty-") ?></td>
							<td class=<?=($r['confirmed']==0?'"warning">Undecided':($r['confirmed']>0?'"success">Confirmed':'"danger">Rejected')) ?></td>
							<td><a href="/answer/<?= $r['answer_id'] ?>" target="_blank" class="btn btn-default">Open in new panel</a></td>
							<td><a target="_blank" href="/proposal/<?= $r['form_id'] ?>/-1/<?= $r['answer_id'] ?>" class="btn btn-default">Edit</a></td>
							<td><a target="_blank" href="/additional/<?= $r['form_id'] ?>/-1/<?= $r['answer_id'] ?>" class="btn btn-default">Manage</a></td>
						</tr>
		        		<?php } ?>
	            		</tbody>
	        		</table>	      
    	    	</div>
	        	<div role="tabpanel" class="tab-pane" id="accepted">
            	<table class="table table-striped table-bordered table-hover" id="table-accepted" style="background: white; height: auto !important">
		        	<thead>
		        		<tr>
						<th>#</th>
	        			<th>Title</th>
    					<th>Speaker</th>
                        <th>Duration</th>
    					<th>No. Comments</th>
						<th>Score</th>
						<th>Captain's vote</th>
						<th>Status</th>
						<th>External link</th>
						<th>Edit</th>
						<th>Addit Speakers</th>
					</tr>
		        	</thead>
		        	<tbody>
		    		<?php 	foreach($accepted as $r) { ?>	
		    			<tr style="cursor: pointer" answer_id="<?= $r['answer_id'] ?>">
						<td><?= $r['answer_id'] ?></td>
						<td><?php if(isset($r['Title'])) { ?>
                            <?= $r['Title'] ?>
                        <?php }else{ ?>
                            <?= ucfirst($form['type']) . " proposal no. " . $r['answer_id'] ?>
                        <?php } ?></td>
						<td><?= $r['speaker'] ?></td>
                        <td class='duration'>
						<?php if(isset($r['Duration'])) { 
						    echo $r['Duration'];
						    } else {
						        echo 0;
						    } ?>
						</td>
						<td><?= count($r['comments']) ?></td>
						<td class='score'><?= ($r['score']!=""?$r['score']:"0") ?></td>
						<td><?=($r['captainscore']!=""?$r['captainscore']:"-empty-") ?></td>
						<td class=<?=($r['confirmed']==0?'"warning">Undecided':($r['confirmed']>0?'"success">Confirmed':'"danger">Rejected')) ?></td>
						<td><a href="/answer/<?= $r['answer_id'] ?>" target="_blank" class="btn btn-default">Open in new panel</a></td>
						<td><a target="_blank" href="/proposal/<?= $r['form_id'] ?>/-1/<?= $r['answer_id'] ?>" class="btn btn-default">Edit</a></td>
						<td><a target="_blank" href="/additional/<?= $r['form_id'] ?>/-1/<?= $r['answer_id'] ?>" class="btn btn-default">Manage</a></td>
					</tr>
		    		<?php } ?>
        			</tbody>
		    	</table>	      
		    </div>
		    <div role="tabpanel" class="tab-pane" id="rejected">
            	<table class="table table-striped table-bordered table-hover" id="table-rejected" style="background: white; height: auto !important">
	        		<thead>
	        			<tr>
						<th>#</th>
	       				<th>Title</th>
        				<th>Speaker</th>
                        <th>Duration</th>
   	   					<th>No. Comments</th>
    					<th>Score</th>
						<th>Captain's vote</th>
						<th>Status</th>
	       				<th>External link</th>
						<th>Edit</th>
						<th>Addit Speakers</th>
					</tr>
	        		</thead>
	        		<tbody>
	    			<?php	foreach($rejected as $r) { ?>	
    					<tr style="cursor: pointer" answer_id="<?= $r['answer_id'] ?>">
						<td><?= $r['answer_id'] ?></td>
						<td><?php if(isset($r['Title'])) { ?>
                            <?= $r['Title'] ?>
                        <?php }else{ ?>
                            <?= ucfirst($form['type']) . " proposal no. " . $r['answer_id'] ?>
                        <?php } ?></td>
						<td><?= $r['speaker'] ?></td>
                        <td class='duration'>
						<?php if(isset($r['Duration'])) { 
						    echo $r['Duration'];
						    } else {
						        echo 0;
						    } ?>
						</td>
						<td><?= count($r['comments']) ?></td>
						<td class='score'><?= ($r['score']!=""?$r['score']:"0") ?></td>
						<td><?=($r['captainscore']!=""?$r['captainscore']:"-empty-") ?></td>
						<td class=<?=($r['confirmed']==0?'"warning">Undecided':($r['confirmed']>0?'"success">Confirmed':'"danger">Rejected')) ?></td>
						<td><a href="/answer/<?= $r['answer_id'] ?>" target="_blank" class="btn btn-default">Open in new panel</a></td>
						<td><a target="_blank" href="/proposal/<?= $r['form_id'] ?>/-1/<?= $r['answer_id'] ?>" class="btn btn-default">Edit</a></td>
						<td><a target="_blank" href="/additional/<?= $r['form_id'] ?>/-1/<?= $r['answer_id'] ?>" class="btn btn-default">Manage</a></td>
					</tr>
	    			<?php } ?>
	        		</tbody>
	    		</table>	      
	    	</div>
                <div role="tabpanel" class="tab-pane" id="waitlist">
            	<table class="table table-striped table-bordered table-hover" id="table-waitlist" style="background: white; height: auto !important">
		        	<thead>
		        		<tr>
						<th>#</th>
	        			<th>Title</th>
    					<th>Speaker</th>
                        <th>Duration</th>
    					<th>No. Comments</th>
						<th>Score</th>
						<th>Captain's vote</th>
						<th>Status</th>
						<th>External link</th>
						<th>Edit</th>
						<th>Addit Speakers</th>
					</tr>
		        	</thead>
		        	<tbody>
		    		<?php 	foreach($waitlist as $r) { ?>	
		    			<tr style="cursor: pointer" answer_id="<?= $r['answer_id'] ?>">
						<td><?= $r['answer_id'] ?></td>
						<td><?php if(isset($r['Title'])) { ?>
                            <?= $r['Title'] ?>
                        <?php }else{ ?>
                            <?= ucfirst($form['type']) . " proposal no. " . $r['answer_id'] ?>
                        <?php } ?></td>
						<td><?= $r['speaker'] ?></td>
                        <td class='duration'>
						<?php if(isset($r['Duration'])) { 
						    echo $r['Duration'];
						    } else {
						        echo 0;
						    } ?>
						</td>
						<td><?= count($r['comments']) ?></td>
						<td class='score'><?= ($r['score']!=""?$r['score']:"0") ?></td>
						<td><?=($r['captainscore']!=""?$r['captainscore']:"-empty-") ?></td>
						<td class=<?=($r['confirmed']==0?'"warning">Undecided':($r['confirmed']>0?'"success">Confirmed':'"danger">Rejected')) ?></td>
						<td><a href="/answer/<?= $r['answer_id'] ?>" target="_blank" class="btn btn-default">Open in new panel</a></td>
						<td><a target="_blank" href="/proposal/<?= $r['form_id'] ?>/-1/<?= $r['answer_id'] ?>" class="btn btn-default">Edit</a></td>
						<td><a target="_blank" href="/additional/<?= $r['form_id'] ?>/-1/<?= $r['answer_id'] ?>" class="btn btn-default">Manage</a></td>
					</tr>
		    		<?php } ?>
        			</tbody>
		    	</table>	      
		    </div></div>
        </div>
	<?php   
	} ?>
    </div> <!-- tab content -->
</div> <!-- container -->
<script>
  $(document).ready(function() {
    // matchHeight the contents of each .card-pf and then the .card-pf itself
    $(".row-cards-pf > [class*='col'] > .card-pf .card-pf-title").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf > .card-pf-body").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf > .card-pf-footer").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf").matchHeight();
    
    var event_id = "<?= $event ?>", token = "<?= $token ?>", fullReviewer = "<?= $username ?>", modalState = false;
    var database = {
	    <?php foreach($unreviewed as $val){ ?> 
		    "<?= $val['answer_id'] ?>": {
			    <?php foreach($val as $key=>$v){
				    if($key != "id" && $key != "score" && $key != "captainscore" && $key != "form_id" && $key != "confirmed") {
						if($key == "comments"){ ?>
							"<?= $key ?>": [
							<?php foreach($v as $cmt){ ?>
										{"author": "<?= $cmt['author'] ?>",
										 "time": <?= $cmt['time'] ?>,
										 "public": <?= $cmt['public'] ?>,
										 "text": <?= json_encode(preg_replace('/\s+/', ' ', nl2br($cmt['comment'], false))) ?> },
							<?php } ?>
							],
						<?php } else { ?>
					    <?= json_encode(preg_replace('/\s+/', ' ', nl2br($key, false))) ?>: <?= json_encode(preg_replace('/\s+/', ' ', nl2br($v, false))) ?>,
				    <?php 	  } 
					}
                		}     
				echo '"vote": undefined';?>
		    },
	    <?php } 
		 foreach($accepted as $val){ ?>
		    
		    "<?= $val['answer_id'] ?>": {
			    <?php foreach($val as $key=>$v){
				    if($key != "id" && $key != "score" && $key != "captainscore" && $key != "form_id"  && $key != "confirmed") {
						if($key == "comments"){ ?>
							"<?= $key ?>": [
							<?php foreach($v as $cmt){ ?>
										{"author": "<?= $cmt['author'] ?>",
										 "time": <?= $cmt['time'] ?>,
										 "public": <?= $cmt['public'] ?>,
									 "text": <?= json_encode(preg_replace('/\s+/', ' ', nl2br($cmt['comment'], false))) ?> },
							<?php } ?>
							],
						<?php } else { ?>
					    <?= json_encode(preg_replace('/\s+/', ' ', nl2br($key, false))) ?>: <?= json_encode(preg_replace('/\s+/', ' ', nl2br($v, false))) ?>,
				    <?php 	  } 
					}
		                }
				echo '"vote": "1"';?>
		    },
	    <?php } 
		 foreach($rejected as $val){ ?>
		    
		    "<?= $val['answer_id'] ?>": {
			    <?php foreach($val as $key=>$v){
				    if($key != "id" && $key != "score" && $key != "captainscore" && $key != "form_id" && $key != "confirmed") {
						if($key == "comments"){ ?>
							"<?= $key ?>": [
							<?php foreach($v as $cmt){ ?>
										{"author": "<?= $cmt['author'] ?>",
										 "time": <?= $cmt['time'] ?>,
										 "public": <?= $cmt['public'] ?>,
										 "text": <?= json_encode(preg_replace('/\s+/', ' ', nl2br($cmt['comment'], false))) ?> },
							<?php } ?>
							],
						<?php } else { ?>
					    <?= json_encode(preg_replace('/\s+/', ' ', nl2br($key, false))) ?>: <?= json_encode(preg_replace('/\s+/', ' ', nl2br($v, false))) ?>,
				    <?php 	  } 
					}
                		}     
               echo '"vote": "-1"';?>		
            },
	    <?php } 
		foreach($waitlist as $val){ ?>
		    
		    "<?= $val['answer_id'] ?>": {
			    <?php foreach($val as $key=>$v){
				    if($key != "id" && $key != "score" && $key != "captainscore" && $key != "form_id" && $key != "confirmed") {
						if($key == "comments"){ ?>
							"<?= $key ?>": [
							<?php foreach($v as $cmt){ ?>
										{"author": "<?= $cmt['author'] ?>",
										 "time": <?= $cmt['time'] ?>,
										 "public": <?= $cmt['public'] ?>,
									 "text": <?= json_encode(preg_replace('/\s+/', ' ', nl2br($cmt['comment'], false))) ?> },
							<?php } ?>
							],
						<?php } else { ?>
					    <?= json_encode(preg_replace('/\s+/', ' ', nl2br($key, false))) ?>: <?= json_encode(preg_replace('/\s+/', ' ', nl2br($v, false))) ?>,
				    <?php 	  } 
					}
		                }
				echo '"vote": "0"';?>
		    },
	    <?php }?>
    };
    // Initialize the vertical navigation
    $().setupVerticalNavigation(true);

    $('#responseDetail').on('hidden.bs.modal', function (e) {
	  modalState = false;
    });

    function init(){
	$("tbody").off('click', 'tr');
	$("tbody").on('click', "tr",function (){
		var response = $(this).attr("answer_id");
		var record = database[response];
		fillModal(response);
		$("#responseDetail").modal('show');
		modalState = true;
	});
    }

    function nextKey() {
        var response = $("#modalTitle").attr("response");
        var vote = $("#modalTitle").attr("voteGroup");

	var key = $("#table-" + (vote == undefined?"unreviewed":(vote>0?"accepted":(vote<0?"rejected":"waitlist"))) + " tr[answer_id=" + response +"]").next().attr("answer_id");
	if($.isNumeric(key)){
		return key;
	}
        return null;
    }

    function prevKey() {
	var response = $("#modalTitle").attr("response");
	var vote = $("#modalTitle").attr("voteGroup");

	var key = $("#table-" + (vote == undefined?"unreviewed":(vote>0?"accepted":(vote<0?"rejected":"waitlist"))) + " tr[answer_id=" + response +"]").prev().attr("answer_id");
	if($.isNumeric(key)){
		return key;
	}
	return null;

    }

    function showErrorToast(message){
        var toast = $(".toast-notifications-list-pf").html()+
                    '<div class="toast-pf alert alert-danger alert-dismissable">'+
                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">'+
                    '<span class="pficon pficon-close"></span>'+
                    '</button>'+
                    '<span class="pficon pficon-error-circle-o"></span>'+
                    message+
                    '</div>';
         $(".toast-notifications-list-pf").html(toast);        
    }

    function showSuccessToast(message){
        var toast = $(".toast-notifications-list-pf").html()+
                    '<div class="toast-pf alert alert-success alert-dismissable">'+
                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">'+
                    '<span class="pficon pficon-close"></span>'+
                    '</button>'+
                    '<span class="pficon pficon-ok"></span>'+
                    message+
                    '</div>';
         $(".toast-notifications-list-pf").html(toast);        
    }

    function moveRow(response, vote){
        var oldVote = database[response].vote;
        var move = (vote>0?"accepted":(vote<0?"rejected":"waitlist"));
		database[response].vote = vote;

        var row = $("tr[answer_id='" + response + "']");
        var oldTable = $("tr[answer_id='" + response + "']").parent().parent();
        var order = oldTable.DataTable().order();
        oldTable.DataTable().destroy();
        $("tr[answer_id='" + response + "']").remove();

        $("#table-" + move + " tbody tr td.dataTables_empty").parent().remove();
	$("#table-" + move).DataTable().destroy();
        if(oldVote == undefined){
            var count = parseInt($("#unreviewed-count").html());
            $("#unreviewed-count").html(count-1);

            if(vote > 0){
                count = parseInt($("#accepted-count").html());
                $("#accepted-count").html(count+1);
            } else if (vote < 0) {
                count = parseInt($("#rejected-count").html());
                $("#rejected-count").html(count+1);
            } else {
                count = parseInt($("#waitlist-count").html());
                $("#waitlist-count").html(count+1);
            }
        } else {
            if(oldVote > 0){
                var count = parseInt($("#accepted-count").html());
                $("#accepted-count").html(count-1);
                count = parseInt($("#"+move+"-count").html());
                $("#"+move+"-count").html(count+1);
            } else if(oldVote < 0){
                var count = parseInt($("#"+move+"-count").html());
                $("#"+move+"-count").html(count+1);
                count = parseInt($("#rejected-count").html());
                $("#rejected-count").html(count-1);
            } else {
                var count = parseInt($("#"+move+"-count").html());
                $("#"+move+"-count").html(count+1);
                count = parseInt($("#waitlist-count").html());
                $("#waitlist-count").html(count-1);
            }
        }
        $("#table-" + move + " tbody").append(row);
	$(".table").DataTable({paging: false});
	oldTable.DataTable({"order": order});
	init();   
    }


    function sendVote(vote, response) {
        var voted = vote;

        var xhr = new XMLHttpRequest();
        xhr.open('POST', '/accept/');
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.onload = function() {
            try{
                var data = JSON.parse(xhr.responseText);
            } catch (e) {
                $("#responseDetail").modal('hide');
                showErrorToast(xhr.responseText);
                return;
            }
            if(data.result != 'success'){
                $("#responseDetail").modal('hide');
                showErrorToast(data.reason);
            }else{
                moveRow(response, vote);
            }
        };

        xhr.send(jQuery.param({'token': token, 'event': event_id, 'answer': response, 'vote': voted}));
    }

    function addComment(from, when, what, record, pub){
	var content = $("#comments-list").html();
	var ncom = {"author": from, "time": when, "text": what, "public": parseInt(pub)};
	if(pub != "1"){
		$("#comments-list").html("<div class='comment alert alert-danger'><span class='pficon pficon-private' title='Private comment'></span><h4>" + from + "</h4><p><i><small>" + new Date(when).toString() + "</small></i><br>" + what + "</p></div>" + content);
	}else{
		$("#comments-list").html("<div class='comment'><h4>" + from + "</h4><p><i><small>" + new Date(when).toString() + "</small></i><br>" + what + "</p></div>" + content);
	}
	$.each(database, function (key, val){
		if(key == record.answer_id){
			val.comments.unshift(ncom);
		}
	});
	$("#comments-count").html(record.comments.length);
    }

    $(".add-comment").click(function (){
	var response = $("#modalTitle").attr("response");
	var publicly = $(this).attr("pub");
	var record = database[response];

	var from = fullReviewer + " (<?= ($form['placeholder'] != ""? $form['placeholder'] : $form['type']) ?> program manager)";
	var what = $("#comment-input").val();
	if(what == ""){
		showErrorToast("You have to add some text first.");
		return;
	}
	
	from = encodeURIComponent(from);
	what = encodeURIComponent(what);

	var when = new Date().getTime();

	var xhr = new XMLHttpRequest();
        xhr.open('POST', '/comment/');
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.onload = function() {
            try{
                var data = JSON.parse(xhr.responseText);
            } catch (e) {
                $("#responseDetail").modal('hide');
                showErrorToast(xhr.responseText);
                return;
            }
            if(data.result != 'success'){
                $("#responseDetail").modal('hide');
                showErrorToast(data.reason);
            }else{
		if(data.message != undefined){
			showSuccessToast(data.message);
		}
                addComment(decodeURIComponent(from), when, decodeURIComponent(what), record, publicly);
            }
        };

	if(publicly != "1"){
	        xhr.send(jQuery.param({'token': token, 'answer': response, 'from': from, 'what': what , 'when': when}));
	} else {
	        xhr.send(jQuery.param({'token': token, 'answer': response, 'from': from, 'what': what , 'when': when, 'public': 1}));
	}
		$("#comment-input").val("");
	});

    function fillModal(keys){
        if($("#comment-input").val() != ""){
            alert("You haven't submit your comment for this proposal. Please, submit your comment or remove text from comment input field.");
            return false;
        } else {
	    if(keys != null){
            	var record = database[keys];
            	if('Title' in record){
            	    $("#modalTitle").html("<b>" + record.Title + "</b>");
            	} else {
            	    $("#modalTitle").html("<?= ucfirst($form['type']) ?> proposal no. <b>" + keys + "</b>");
            	}
    		var html = "", comments="";
	    	$.each(record, function (key, val){
	       		if(key != "answer_id" && key != "vote" && key != "comments"){
	    			html += "<p><label>" + key + "</label><br>"+ val+"</p>";
	    		}
	    	});
		$.each(record.comments, function (key, val){
			if(val.public != 1){
				comments += "<div class='comment alert alert-danger'><span class='pficon pficon-private' title='Private comment'></span><h4>" + 
						val.author +"</h4><p><i><small>"+new Date(val.time).toString()+"</small></i><br>"+val.text+"</p></div>";
			}else{
				comments += "<div class='comment'><h4>" + val.author +"</h4><p><i><small>"+new Date(val.time).toString()+"</small></i><br>"+val.text+"</p></div>";
			}
		});
	    	$("#modalTitle").attr("voteGroup", record.vote);
	    	$("#modalTitle").attr("response", keys);
		$(".modal .active").removeClass("active");
		$("#tabActive").addClass("active");
		$("#modalBody").addClass("active");
	    	$("#modalBody").html(html);
		$("#comments-list").html(comments);
		$("#comments-count").html(record.comments.length);
		$("#responseDetail").animate({scrollTop: 0}, 100);
            } else {
                $("#responseDetail").modal('hide');
            }
	    return true;
	}
    }

    $("#skip").click(function (){
        var nextKeys = nextKey();
	fillModal(nextKeys);        
    });

    $("#prev").click(function (){
	var prevKeys = prevKey();
	fillModal(prevKeys);
    });

    $("#comment-input").keydown(function(event){ 
	event.stopPropagation(); 
    });

    $(window).keydown(function (event){
	if(modalState){
		if(event.keyCode == 39){ // ArrowRight
			var nextKeys = nextKey();
			fillModal(nextKeys);
		}

		if(event.keyCode == 37){ // ArrowLeft
			var prevKeys = prevKey();
			fillModal(prevKeys);
		}
	}
    });

    $("#accept").click(function (){
	var nextKeys = nextKey(),
            response = $("#modalTitle").attr("response");

	if(fillModal(nextKeys)){
		sendVote(1, response);
	}
    });

    $("#reject").click(function (){
	var nextKeys = nextKey(),
            response = $("#modalTitle").attr("response");

	if(fillModal(nextKeys)){
		sendVote(-1, response);
	}
    });

    $("#wait").click(function (){
	var nextKeys = nextKey(),
            response = $("#modalTitle").attr("response");

	if(fillModal(nextKeys)){
		sendVote(0, response);
	}
    });


    init(); 
    $(".table").DataTable({paging:false});
  });
</script>
</body>
</html>  
