<div class="container-fluid container-cards-pf container-pf-nav-pf-vertical">
    <ol class="breadcrumb" role="navigation" aria-label="breadcrumbs">
        <li><a href="/"><span class="fa fa-home"></span></a></li>
       	<li><a href="/acceptproposals">My accepted</a></li>
        <li class="active"><span>Hotel booking for <?= $event['name'] . " (" . $event['year'] . ")" ?></span></li>
    </ol>
    <?php if($questions == null){ ?>
    <div class="blank-slate-pf">
        <div class="blank-slate-pf-icon">
            <span class="pficon pficon-info"></span>
        </div>
        
        <h1>No booking options are available.</h1>
        <p>Either you have booked a room already or booking process is not ready yet.</p>
    <?php } else { ?>
    <div class="row row-cards-pf">
    <div class="col-xs-12">
        <div class="card-pf card-pf-view">
    	<h1>Hotel booking for <?= $event['name'] . " (" . $event['year'] . ")" ?></h1>
    	
    	<p>Questions marked with <span class='required-pf'>*</span> are <strong>required</strong>.</p>
        <div class="card-pf-body">
    		<form action="/sendreservation/<?= $form['id'] ?>" onsubmit="checkRequired(event)" method="post">
    			<input type="hidden" name="_method" value="post">
    		<?php

    			$required = array();
    			foreach ($questions as $i=>$q){
    				if($q['required']){
    					$required[]=array("type"=>$q['type'], "name"=>$q['take_as']);
    				}
    			?>
    			<div class="form-group">
    		<?php
    			switch ($q['type']){
    				case "text": ?>
    					<label class="control-label" for="<?= $q['take_as'] ?>"><?= ($q['required']?"<span class='required-pf'>*</span> ":"") ?><?= $q['question'] ?></label>
    					<?php if ($q['help_text'] != "") {?>
    					<span class="help-block"><?= $q['help_text'] ?></span>
    					<?php } ?>
    					<input type="text" class="form-control" name="<?= $q['take_as'] ?>"
    						<?= ($q['size'] > 0?' maxlength="' . $q['size'] . '"':'') ?><?= ($q['required']?" required":"") ?>>
    					<?php 
                            if($q['size'] > 0){?>
                        <span class="pull-right chars-remaining-pf">
                            <span id="cRCF<?= $q['take_as'] ?>"></span> characters remaining
                        </span>
                        <script>
                            (function($) {
                                $('input[name=<?= $q['take_as'] ?>]').countRemainingChars( {
                                    countFld: 'cRCF<?= $q['take_as'] ?>',
                                    charsMaxLimit: <?= $q['size'] ?>,
                                    charsWarnRemaining: <?= ($q['size']-5 >= 0?5:0) ?>,
                                    blockInputAtMaxLimit: false} );

                            })(jQuery);
                        </script>
                        <?php }
                            break;

    				case "textarea": ?>
    					<label class="control-label" for="<?= $q['take_as'] ?>"><?= ($q['required']?"<span class='required-pf'>*</span> ":"") ?><?= $q['question'] ?></label>
    					<?php if ($q['help_text'] != "") {?>
    					<span class="help-block"><?= $q['help_text'] ?></span>
    					<?php } ?>
    					<textarea class="form-control" name="<?= $q['take_as'] ?>"<?= ($q['size'] > 0?' maxlength="' . $q['size'] . '"':'') ?><?= ($q['required']?" required":"") ?>
    					></textarea>
    					<?php 
                            if($q['size'] > 0){?>
                        <span class="pull-right chars-remaining-pf">
                            <span id="cRCF<?= $q['take_as'] ?>"></span> characters remaining
                        </span> 
                        <script>
                            (function($) {
                                $('textarea[name=<?= $q['take_as'] ?>]').countRemainingChars( {
                                    countFld: 'cRCF<?= $q['take_as'] ?>',
                                    charsMaxLimit: <?= $q['size'] ?>,
                                    charsWarnRemaining: <?= ($q['size']-5 >= 0?5:0) ?>,
                                    blockInputAtMaxLimit: false} );

                            })(jQuery);
                        </script>
                        <?php }
    					break;
    				case "radio":
    				case "checkbox":
    					if($q['take_as'] == "accommodation"){ ?>
							<label class="control-label" for="<?= $q['take_as'] ?>"><?= ($q['required']?"<span class='required-pf'>*</span> ":"") ?><?= $q['question'] ?></label><br>
							<?php if ($q['help_text'] != "") {?>
							<span class="help-block"><?= $q['help_text'] ?></span>
							<?php } ?>
							<?php foreach($hotels as $h){ if(count($h['rooms']) < 1) { continue; } ?>
							<ul>
							<li><label class="control-label" for="<?= $q['take_as'] ?>>"><?= $h['name'] . ($h['web'] != ""?" (<a href='" . $h['web'] . "' target='_blank'>Web page</a>)":"")?></label>
							<?php if ($h['description'] != ""){ ?>
							<span class="help-block"><?= $h['description'] ?></span>
							<?php } ?>
							<ul>
								<?php foreach($h['rooms'] as $r){ ?>
									<li><label class="radio"><input name='<?= $q['take_as'] ?>' value='<?= $r['id'] ?>' type='radio'> <?= $r['name'] ?> | <?= $r['price'] ?>
									<?php if ($r['description'] != ""){ ?>
									<span class="help-block"><?= $r['description'] ?></span>
									<?php } ?>
									</label></li>
								<?php } ?>
							</ul>
							</li>
							</ul>
							<?php } ?>
    				<?php	} else {
    				?>
    					<label class="control-label" for="<?= $q['take_as'] ?>"><?= ($q['required']?"<span class='required-pf'>*</span> ":"") ?><?= $q['question'] ?></label><br>
    					<?php if ($q['help_text'] != "") {?>
    					<span class="help-block"><?= $q['help_text'] ?></span>
    					<?php } ?>
    					<ul>
    					<?php
    					foreach($q['options'] as $o){ ?>
							<li><label class="<?= $q['type'] ?>"><input name='<?= $q["take_as"] ?><?= ($q['type'] == "checkbox"?"[]":"") ?>' value='<?= $o['value'] ?>' 
								type='<?= $q['type'] ?>'<?= (count($q['options']) == 1 && $q['required']==1?" required":"") ?>>
								<?= $o['label'] ?></label></li>
							<?php 
						} ?>
    					</ul>
    					<?php	
    					}
    					break;
    				case "description":?>
    					<?php if ($q['question'] != ""){ ?><h2><?= $q['question'] ?></h2><?php } ?>
    					<?php if ($q['help_text'] != ""){ ?><p><?= $q['help_text'] ?></p><?php } 
    					break;
    			}?>
    			</div>
    		<?php } ?>
    			<div class="form-group">
    				<span>
    					<input type="submit" class="btn btn-success" name="save" value="Book a hotel room">
    				</span><span class="spinner spinner-sm spinner-inline hidden" style="vertical-align: middle"></span>
    			</div>
    		</form>
                  </div>
            </div>
        </div>
        <?php } ?>
    </div>
</div> <!-- container -->
<script>
  var required = [<?php  
    foreach($required as $r){
    	echo "{'name': '" . $r['name'] . "', 'type': '" . $r['type'] . "'},";
    }
  ?>];

  $(document).ready(function() {
    // matchHeight the contents of each .card-pf and then the .card-pf itself
    $(".row-cards-pf > [class*='col'] > .card-pf .card-pf-title").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf > .card-pf-body").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf > .card-pf-footer").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf").matchHeight();

    // Initialize the vertical navigation
    $().setupVerticalNavigation(true);

    // Initialize Boostrap-Combobox
    $('.combobox').combobox();

    $('#NaN').attr('autocomplete','off');

    // autohide toasts
    $('.toast-pf').mouseenter(function () {
    $(this).attr('mouseenter','true');
    });

    $('.toast-pf').mouseleave(function () {
    $(this).removeAttr('mouseenter');
    if($(this).attr('remove') != null){
    	$(this).remove();
    }		
    });

    function hideToasts() {
    var toasts = $('.toast-pf'), i;

    for(i = 0; i < toasts.length; i++){
    	if($(toasts[i]).attr('mouseenter') != null){
    		$(toasts[i]).attr('remove','true');
    	} else {
    		$(toasts[i]).hide('slow', function(){ $(toasts[i]).remove(); });
    	}
    }
    }

    setTimeout(hideToasts, 8000);

  });

  function checkRequired(e){
    $("input[type=submit]").addClass("disabled");
    $(".spinner").removeClass("hidden");
    var notify = false;
    $.each(required, function(key, val){
    	if(val.type == "checkbox" || val.type == "radio"){
    		var name = "*[name=" + val.name + (val.type == "checkbox"?"\\[\\]":"") + "]";
    		if(!($(name).is(":checked"))){
    			$(name).closest(".form-group").addClass("has-error");
    			notify = true;
    		}
    	}else{
    		var name = "*[name=" + val.name + "]";
    		if($.trim($(name).val()) == ""){
    			$(name).closest(".form-group").addClass("has-error");
    			notify = true;
    		}
    	}
    });

    if(notify){
    	e.preventDefault();
    	$("input[type=submit]").removeClass("disabled");
    	$(".spinner").addClass("hidden");
    	showToast("There are some mistakes in the form. Please, check and fill all required fields and submit the form again.");
    }
  }

  function showToast(message){
        var toast = $(".toast-notifications-list-pf").html()+
                    '<div class="toast-pf alert alert-danger alert-dismissable">'+
                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">'+
                    '<span class="pficon pficon-close"></span>'+
                    '</button>'+
                    '<span class="pficon pficon-error-circle-o"></span>'+
                    message+
                    '</div>';
    $(".toast-notifications-list-pf").html(toast);        
  }

</script>
</body>
</html>  
