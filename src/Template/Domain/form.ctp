<div class="container-fluid container-cards-pf container-pf-nav-pf-vertical">
    <ol class="breadcrumb" role="navigation" aria-label="breadcrumbs">
        <li><a href="/"><span class="fa fa-home"></span></a></li>
        <li><a href="/admin">Administration</a></li>
        <li><a href="/domains">Domains settings</a></li>
        <li class="active"><span>Domain</span></li>
    </ol>
	<div class="row row-cards-pf">
		<div class="col-xs-12 ">
			<div class="card-pf card-pf-view">
				<?php if($domain == null){ ?>
					<h1 class="card-pf-title">Add domain info</h1>
				<?php }else{ ?>
					<h1 class="card-pf-title">Update domain info</h1>
				<?php } ?>
				<div class="card-pf-body">
					<form class="profile" enctype="multipart/form-data" action="/domains/add" method="post">
						<input type="hidden" name="_Token" value="<?= $token ?>">
						<input type="hidden" name="action" value="<?= $action ?>">
						<input type="hidden" name="_method" value="post">
						<div class="form-group">
							<label class="control-label" for="domain">Domain</label>
							<input type="text" maxlength="256" class="form-control" value="<?= (!empty($domain)?$domain['domain'].'" disabled="disabled':'') ?>" name="domain" required>
							<span class="help-block">Enter domain (e.g. foo.example.com - without http/https at the beginning).</span>
						</div>
						<div class="form-group">
							<label class="control-label" for="name">Name</label>
							<input type="text" maxlength="1024" class="form-control" value="<?= (!empty($domain)?$domain['name']:'') ?>" name="name" required>
							<span class="help-block">Enter domain name (e.g. for exampledomain.org fill Example Domain).</span>
						</div>
						<div class="form-group">
							<label class="control-label" for="logo">Logo</label>
							<input type="file" name="logo">
						</div>					
						<div class="form-group">
							<span>
								<input type="submit" class="btn btn-primary" name="save" value="Save">
							</span>

						</div>
					</form> <!-- end form -->	

				</div> <!-- end card body -->
			</div> <!-- end card -->
        </div> <!-- cols -->
    </div> <!-- row -->
</div> <!-- container -->
<script>
  $(document).ready(function() {
    // matchHeight the contents of each .card-pf and then the .card-pf itself
    $(".row-cards-pf > [class*='col'] > .card-pf .card-pf-title").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf > .card-pf-body").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf > .card-pf-footer").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf").matchHeight();

    // Initialize switch buttons
    $(".bootstrap-switch").bootstrapSwitch();

    $(".input-group-addon").click(function(){
	var field = $(this).attr("for");
	var state = $("input[name="+field+"]").attr("type");
	if(state == "text"){
		$("input[name="+field+"]").attr("type","password");
	}else{
		$("input[name="+field+"]").attr("type","text");
	}
    });
    // Initialize the vertical navigation
    $().setupVerticalNavigation(true);
  });
</script>
</body>
</html>
 
