<div class="container-fluid container-cards-pf container-pf-nav-pf-vertical">
    <ol class="breadcrumb" role="navigation" aria-label="breadcrumbs">
        <li><a href="/"><span class="fa fa-home"></span></a></li>
        <li><a href="/admin">Administration</a></li>
        <li class="active"><span>Domains settings</span></li>
    </ol>
    <a href="/domains/add" action="add" class="btn btn-success">Add new domain</a>
	<?php if(count($domains) < 1) { ?>
	<div class="blank-slate-pf">
        <div class="blank-slate-pf-icon">
            <span class="pficon pficon-info"></span>
        </div>
        <h1>There is no domain created yet so the service uses the default one.</h1>
        <p>Click on Add new to create one.</p>

	<?php }else{ ?>
    <div id="pf-list-standard" class="list-group list-view-pf list-view-pf-view">
    <?php foreach($domains as $d) { ?>
	<div class="col-xs-12 col-sm-4">
             <div class="card-pf card-pf-view card-pf-view-xs">
                  <div class="card-pf-body">
                        <h2 class="card-pf-title text-center"><?= $d['name'] ?></h2>
            			<h4 class="text-center">(<?= $d['domain'] ?>)</h4>
                        <div class="card-pf-items text-center">
                             <div class="card-pf-item">
                                  <a href="/signins/<?= urlencode($d['domain']) ?>" title="Manage SignIn options">
                                      <span class="pficon pficon-key"></span> Sign in options
                                  </a>
                             </div>
                             <div class="card-pf-item">
                                  <a href="/domains/edit/<?= urlencode($d['domain']) ?>" title="Edit domain">
                                      <span class="pficon pficon-edit"></span> Edit
                                  </a>
                             </div>
            			     <div class="card-pf-item">
			                    <a href="/domains/remove/<?= urlencode($d['domain']) ?>" title="Remove domain">
                			        <span class="pficon pficon-error-circle-o"></span> Remove
                				</a>
                            </div>
                        </div>
                   </div>
             </div>
         </div> <!-- cols -->
	 <?php }

         } ?>

    </div> <!-- group list -->
</div> <!-- container -->
<script>
  $(document).ready(function() {
    // matchHeight the contents of each .card-pf and then the .card-pf itself
    function even(){
	$(".list-view-pf-view > [class*='col'] > .card-pf .card-pf-title").matchHeight();
	$(".list-view-pf-view > [class*='col'] > .card-pf > .card-pf-body").matchHeight();
	$(".list-view-pf-view > [class*='col'] > .card-pf > .card-pf-footer").matchHeight();
	$(".list-view-pf-view > [class*='col'] > .card-pf").matchHeight();
    }

    even();
    // Initialize the vertical navigation
    $().setupVerticalNavigation(true);


    // autohide toasts
    $('.toast-pf').mouseenter(function () {
 	$(this).attr('mouseenter','true');
    });

    $('.toast-pf').mouseleave(function () {
	$(this).removeAttr('mouseenter');
	if($(this).attr('remove') != null){
		$(this).remove();
	}		
    });

    function hideToasts() {
	var toasts = $('.toast-pf'), i;

	for(i = 0; i < toasts.length; i++){
		if($(toasts[i]).attr('mouseenter') != null){
			$(toasts[i]).attr('remove','true');
		} else {
			$(toasts[i]).hide('slow', function(){ $(toasts[i]).remove(); });
		}
	}
    }

    setTimeout(hideToasts, 8000);

  });
</script>
</body>
</html>  
