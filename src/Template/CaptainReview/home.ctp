<div class="container-fluid container-cards-pf container-pf-nav-pf-vertical">
    <ol class="breadcrumb" role="navigation" aria-label="breadcrumbs">
        <li><a href="/"><span class="fa fa-home"></span></a></li>
    	<li><a href="/reviewer">Review tasks</a></li>
        <li class="active"><span><?= $eventInfo['name'] . " (" . $eventInfo['year'] . ")" ?> - Captain review - <?= ($form['placeholder'] != ""? $inflector->pluralize(ucfirst($form['type'])) . " (" . $inflector->pluralize(ucfirst($form['placeholder'])) . ")" : $inflector->pluralize(ucfirst($form['type']))) ?></span></li>
    </ol>
    <h1>Review <?= ($form['placeholder'] != ""? $form['type'] . " (" . $form['placeholder'] . ")" : $form['type']) ?> proposals for <?= $eventInfo['name'] . " (" . $eventInfo['year'] . ")" ?> as captain</h1>
    <?php if((count($unreviewed) + count($accepted) + count($rejected)) < 1) { ?>
    <div class="blank-slate-pf">
        <div class="blank-slate-pf-icon">
            <span class="pficon pficon-info"></span>
        </div>
        <h1>No responses are ready to review.</h1>
        <p>You have to wait for them. Meanwhile you can help with promotion of this event.</p>

    <?php }else{ ?>
            <span style="display: inline-block; width: 100%"><a href="/reviewstate/<?= $event . '/' . $form['type'] ?>" class="btn btn-info" style="float: right">Review status</a></span>
    		<div class="modal fade" id="responseDetail" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    			<div class="modal-dialog">
    				<div class="modal-content">
    					<div class="modal-header btn-warning" style="background-color: #ec7a08;">
            					<button type="button" class="close close-white" data-dismiss="modal" aria-hidden="true" aria-label="Close">
    						        <span class="pficon pficon-close"></span>
    					        </button>
    		        			<h4 class="modal-title">Captain Review: Response</h4>
    					</div> <!-- modal header -->
    					<div class="modal-body">
    						<div class="h2 text-center" id="modalTitle"></div>
    						<div class="tab-content">
    							<ul class="nav nav-tabs" role="tablist">
    								<li role="presentation" id="tabActive" class="active"><a href="#modalBody" aria-controls="modalBody" role="tab" data-toggle="tab">Proposal</a></li>
    								<li role="presentation"><a href="#comments" aria-controls="comments" role="tab" data-toggle="tab">Comments <span class="badge" id="comments-count"></span></a></li>
    							</ul>
    							<div class="tab-pane active" id="modalBody"></div>
    							<div class="tab-pane" id="comments">
    								<div class="form-group">
    									<label class="control-label">Add comment</label>
    									<textarea id="comment-input" class="form-control"></textarea>
    								</div>
    								<div class="form-group">
    									<span><a href="#" class="btn btn-primary add-comment" pub='0' style="float: right">Add</a></span>
    									<span><a href="#" class="btn btn-danger add-comment" pub='1'>Send comment to speaker</a></span>
    								</div>
    								<div id="comments-list"></div>
    							</div>
    						</div>
    					</div>
    				 	<div class="modal-footer">
    						<a vote="-1" class="btn btn-danger reject">Reject</a>
    						<a id="prev" class="btn btn-default">Prev</a>
    						<a id="skip" class="btn btn-default">Next</a>
    						<a vote="1" class="btn btn-info accept">Backup</a>
    						<a vote="2" class="btn btn-warning accept">Waitlist</a>
    						<a vote="3" class="btn btn-success accept">Accept</a>
    					</div> <!-- modal footer -->
     				</div> <!-- modal content -->
    			</div> <!-- modal dialog -->
    		</div> <!-- modal -->
    		<div class="modal fade" id="otherResponseDetail" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    			<div class="modal-dialog">
    				<div class="modal-content">
    					<div class="modal-header btn-primary">
            					<button type="button" class="close close-white" data-dismiss="modal" aria-hidden="true" aria-label="Close">
    						        <span class="pficon pficon-close"></span>
    					        </button>
    		        			<h4 class="modal-title">Overview: Response</h4>
    					</div> <!-- modal header -->
    					<div class="modal-body">
    						<div class="h2 text-center" id="otherModalTitle"></div>
    						<div class="tab-content">
    							<ul class="nav nav-tabs" role="tablist">
    								<li role="presentation" id="otherTabActive" class="active"><a href="#otherModalBody" aria-controls="otherModalBody" role="tab" data-toggle="tab">Proposal</a></li>
    								<li role="presentation"><a href="#otherComments" aria-controls="otherComments" role="tab" data-toggle="tab">Comments <span class="badge" id="other-comments-count"></span></a></li>
    							</ul>
    							<div class="tab-pane active" id="otherModalBody"></div>
    							<div class="tab-pane" id="otherComments">
    								<div class="form-group">
    									<label class="control-label">Add comment</label>
    									<textarea id="other-comment-input" class="form-control"></textarea>
    								</div>
    								<div class="form-group">
    									<a href="#" class="btn btn-primary add-other-comment" style="float: right" pub='0'>Add</a>
    									<a href="#" class="btn btn-danger add-other-comment" pub='1'>Send comment to speaker</a>
    								</div>
    								<div id="other-comments-list"></div>
    							</div>
    						</div>
    					</div>
    				 	<div class="modal-footer">
    						<div class="form-group">
    							<?php foreach($allTopics as $t) { ?>
    								<label class="checkbox-inline"><input name='topic[]' value='<?= $t['id'] ?>' type='checkbox'><?= $t['name'] ?></label>
    							<?php } ?>
    						</div>
    						<div class="form-group">
    							<a id="addTopic" class="btn btn-primary">Add to selected topics</a>	
    					        <a id="next" class="btn btn-default">Next</a>
    						</div>
    					</div> <!-- modal footer -->
     				</div> <!-- modal content -->
    			</div> <!-- modal dialog -->
    		</div> <!-- modal -->
    <div class="tab-content">    
        <ul class="nav nav-tabs" style="margin-bottom: 15px" role="tablist">
        <?php $first = true; 
    		  foreach($topics as $t) { 
    			if($first){?>
    	            <li role="presentation" class="active"><a href="#topic-<?= $t['id'] ?>" aria-controls="topic-<?= $t['id'] ?>" role="tab" data-toggle="tab"><?= $t['name'] ?></a></li>
          <?php } else {?>
    	            <li role="presentation"><a href="#topic-<?= $t['id'] ?>" aria-controls="topic-<?= $t['id'] ?>" role="tab" data-toggle="tab"><?= $t['name'] ?></a></li>
    	  <?php } 
    			$first = false;
    		  }
    		if(count($rest) > 0){ ?>
                <li role="presentation" class="bg-info"><a href="#topic-others" aria-controls="topic-others" role="tab" data-toggle="tab">All</a></li>
      <?php }?>
        </ul>
        <?php $first = true;
    		  foreach($topics as $t) { 

    	?>
        	<div role="tabpanel" style="background: white !important" class="tab-pane <?= ($first?'active':'') ?>" id="topic-<?= $t['id'] ?>">   
                <div class="tab-content">
                  <div style="float: right; padding: 1em" title="This is estimation just for you. Only accepted talks are substracted from assigned amount.">
                    Remaining minutes in the schedule for this track: <span class="label label-warning" id="minutes-<?= $t['id'] ?>"><?= $t['timeAllocationResult'] ?></span>
                  </div>
                <ul class="nav nav-tabs nav-tabs-pf" style="margin-bottom: 8px" role="tablist">
                	<li role="presentation" <?= ($first?'class="active"':'') ?>><a href="#unreviewed-<?= $t['id'] ?>" aria-controls="unreviewed-<?= $t['id'] ?>" role="tab" data-toggle="tab">Unreviewed <span class="label label-default" id="unreviewed-<?= $t['id'] ?>-count"><?= (isset($unreviewed[$t['id']])?count($unreviewed[$t['id']]):0) ?></span></a></li>
                	<li role="presentation"><a href="#accepted-<?= $t['id'] ?>" aria-controls="accepted-<?= $t['id'] ?>" role="tab" data-toggle="tab">Accepted <span class="label label-success" id="accepted-<?= $t['id'] ?>-count"><?= (isset($accepted[$t['id']])?count($accepted[$t['id']]):0) ?></span></a></li>
                	<li role="presentation"><a href="#rejected-<?= $t['id'] ?>" aria-controls="rejected-<?= $t['id'] ?>" role="tab" data-toggle="tab">Rejected <span class="label label-danger" id="rejected-<?= $t['id'] ?>-count"><?= (isset($rejected[$t['id']])?count($rejected[$t['id']]):0) ?></span></a></li>
                </ul>
    	        <div role="tabpanel" class="tab-pane active" id="unreviewed-<?= $t['id'] ?>">
                    <table class="table table-striped table-bordered table-hover" id="table-<?= $t['id'] ?>-unreviewed" style="background: white; height: auto !important">
                		<thead>
                			<tr>
    					<th>#</th>
                				<th>Title</th>
                				<th>Type</th>
                				<th>Topic</th>
                				<th>Speaker</th>
                				<th>Duration</th>
                				<th>No. Comments</th>
    					<th>Score</th>
    					<th>Review status</th>
    					<th>Captain vote</th>
    					<th>Status</th>
    					<th>External link</th>
                			</tr>
                		</thead>
                    	<tbody>
    	        		<?php if(isset($unreviewed[$t['id']])){
    						foreach($unreviewed[$t['id']] as $r) { ?>	
    	        			<tr style="cursor: pointer" response_id="<?= $r['id'] ?>">
    						<td><?= $r['response_id'] ?></td>
    						<td><?= $r['title'] ?></td>
    						<td><?= $r['type'] ?></td>
    						<td><?= $r['topics'] ?></td>
    						<td><?= $r['speaker'] ?></td>
    						<td><?= $r['duration'] ?></td>
    						<td><?= count($r['comments']) ?></td>
    						<td class='score'><?=($r['score']!=""?$r['score']:"0") ?></td>
    						<td><?= $r['reviews'] ?></td>
    						<td class="vote">-empty-</td>
    						<td class=<?=($r['confirmed']==0?'"warning">Undecided':($r['confirmed']>0?'"success">Confirmed':'"danger">Rejected')) ?></td>
    						<td><a href="/proposals/<?= $event . "/". $r['response_id'] ?>" target="_blank" class="btn btn-default">Open in new panel</a></td>
    					</tr>
    	        		<?php }} ?>
                		</tbody>
            		</table>	      
            	</div>
            	<div role="tabpanel" class="tab-pane" id="accepted-<?= $t['id'] ?>">
                <table class="table table-striped table-bordered table-hover" id="table-<?= $t['id'] ?>-accepted" style="background: white; height: auto !important">
    	        	<thead>
    	        		<tr>
    					<th>#</th>
    	        			<th>Title</th>
    	        			<th>Type</th>
    	        			<th>Topic</th>
               					<th>Speaker</th>
    	        			<th>Duration</th>
               					<th>No. Comments</th>
    					<th>Score</th>
    					<th>Review status</th>
    					<th>Captain vote</th>
    					<th>Status</th>
    					<th>External link</th>
    				</tr>
    	        	</thead>
    	        	<tbody>
    	    		<?php if(isset($accepted[$t['id']])){
    				foreach($accepted[$t['id']] as $r) { ?>	
    	    			<tr style="cursor: pointer" response_id="<?= $r['id'] ?>">
    					<td><?= $r['response_id'] ?></td>
    					<td><?= $r['title'] ?></td>
    					<td><?= $r['type'] ?></td>
    					<td><?= $r['topics'] ?></td>
    					<td><?= $r['speaker'] ?></td>
    					<td><?= $r['duration'] ?></td>
    					<td><?= count($r['comments']) ?></td>
    					<td class='score'><?= ($r['score']!=""?$r['score']:"0") ?></td>
    					<td><?= $r['reviews'] ?></td>
    					<td class="vote"><?= ($r['vote']==3?"Accepted":($r['vote'] == 2?"Waitlist":"Backup")) ?></td>
    					<td class=<?=($r['confirmed']==0?'"warning">Undecided':($r['confirmed']>0?'"success">Confirmed':'"danger">Rejected')) ?></td>
    					<td><a href="/proposals/<?= $event . "/". $r['response_id'] ?>" target="_blank" class="btn btn-default">Open in new panel</a></td>
    				</tr>
    	    		<?php }} ?>
            		</tbody>
    	    	</table>	      
    	    </div>
    	    <div role="tabpanel" class="tab-pane" id="rejected-<?= $t['id'] ?>">
                <table class="table table-striped table-bordered table-hover" id="table-<?= $t['id'] ?>-rejected" style="background: white; height: auto !important">
            		<thead>
            			<tr>
    					<th>#</th>
            				<th>Title</th>
            				<th>Type</th>
            				<th>Topic</th>
           	    				<th>Speaker</th>
    	       				<th>Duration</th>
               					<th>No. Comments</th>
    					<th>Score</th>
    					<th>Review status</th>
    					<th>Captain vote</th>
    					<th>Status</th>
            				<th>External link</th>
    				</tr>
            		</thead>
            		<tbody>
        			<?php if(isset($rejected[$t['id']])){
    				foreach($rejected[$t['id']] as $r) { ?>	
        				<tr style="cursor: pointer" response_id="<?= $r['id'] ?>">
    					<td><?= $r['response_id'] ?></td>
    					<td><?= $r['title'] ?></td>
    					<td><?= $r['type'] ?></td>
    					<td><?= $r['topics'] ?></td>
    					<td><?= $r['speaker'] ?></td>
    					<td><?= $r['duration'] ?></td>
    					<td><?= count($r['comments']) ?></td>
    					<td class='score'><?= ($r['score']!=""?$r['score']:"0") ?></td>
    					<td><?= $r['reviews'] ?></td>
    					<td class='vote'>Rejected</td>
    					<td class=<?=($r['confirmed']==0?'"warning">Undecided':($r['confirmed']>0?'"success">Confirmed':'"danger">Rejected')) ?></td>
    					<td><a href="/proposals/<?= $event . "/". $r['response_id'] ?>" target="_blank" class="btn btn-default">Open in new panel</a></td>
    				</tr>
        			<?php }} ?>
            		</tbody>
        		</table>	      
        	</div></div>
        </div>
    <?php $first=false; } 

    if(count($rest) > 0) {  ?>
    	<div role="tabpanel" class="tab-pane" id="topic-others">   
                <div class="tab-content">
                    <table class="table table-striped table-bordered table-hover" id="table-others" style="background: white; height: auto !important">
                		<thead>
                			<tr>
                                <th>#</th>
                				<th>Title</th>
                				<th>Type</th>
                				<th>Topic</th>
                				<th>Speaker</th>
                				<th>Duration</th>
                				<th>No. Comments</th>
    					<th>External link</th>
                			</tr>
                		</thead>
                    	<tbody>
    	        		<?php foreach($rest as $r) { ?>	
    	        			<tr style="cursor: pointer" from="other" other_response_id="<?= $r['id'] ?>">
                            <td><?= $r['id'] ?></td>
    						<td><?= $r['title'] ?></td>
    						<td><?= $r['type'] ?></td>
    						<td other_topic="<?= $r['id'] ?>"><?= $r['topic'] ?></td>
    						<td><?= $r['speaker'] ?></td>
    						<td><?= $r['duration'] ?></td>
    						<td><?= count($r['comments']) ?></td>
    						<td><a href="/proposals/<?= $event . "/". $r['id'] ?>" target="_blank" class="btn btn-default">Open in new panel</a></td>
    					</tr>
    	        		<?php } ?>
                		</tbody>
            		</table>	      
    			</div>
        </div>
    <?php } 

    	} ?>
    </div> <!-- tab content -->
</div> <!-- container -->
<script>
  $(document).ready(function() {
    // matchHeight the contents of each .card-pf and then the .card-pf itself
    $(".row-cards-pf > [class*='col'] > .card-pf .card-pf-title").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf > .card-pf-body").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf > .card-pf-footer").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf").matchHeight();
    
    var event_id = "<?= $event ?>", token = "<?= $token ?>", reviewer = "<?= $reviewer ?>", fullReviewer = "<?= $username ?>", modalState = false;
    var allTopics = { <?php foreach($allTopics as $t) { ?>
    		<?= '"' . $t['id'] . '"' . ':"' . $t['name'] . '",'  ?>
    			<?php } ?>
    				};
    var database = {
    <?php foreach($topics as $t) { ?>
        <?php if(isset($unreviewed[$t['id']])){
    			foreach($unreviewed[$t['id']] as $val){ ?>
    	    
    	    "<?= $val['id'] ?>": {
    		    <?php foreach($val as $key=>$v){
    			    if($key != "id" && $key != "score" && $key != "answer_id" && $key != "reviews" && $key != 'confirmed') {
    					if($key == "comments"){ ?>
    						"<?= $key ?>": [
    						<?php foreach($v as $cmt){ ?>
    									{"author": "<?= $cmt['author'] ?>",
    									 "time": <?= $cmt['time'] ?>,
    									 "public": <?= $cmt['public'] ?>,
    									 "text": <?= json_encode(preg_replace('/\s+/', ' ', nl2br($cmt['comment'], false))) ?> },
    						<?php } ?>
    						],
    					<?php } else if($key == "answers"){ 
    						foreach($v as $answ){?>
    						<?= json_encode(preg_replace('/\s+/', ' ', nl2br($answ['question'], false)))?>:<?= json_encode(preg_replace('/\s+/', ' ', nl2br($answ['text'], false))) ?>,
    					<?php }
    					      } else {?>
    				    "<?= $key ?>": <?= json_encode(preg_replace('/\s+/', ' ', nl2br($v, false))) ?>,
    			    <?php 	  } 
    				}
                }     
    			echo '"vote": "0"';?>
    	    },
        <?php } }
    		if(isset($accepted[$t['id']])){
    		 foreach($accepted[$t['id']] as $val){ ?>
    	    
    	    "<?= $val['id'] ?>": {
    		    <?php foreach($val as $key=>$v){
    			    if($key != "id" && $key != "score" && $key != "answer_id" && $key != "reviews" && $key != 'confirmed') {
    					if($key == "comments"){ ?>
    						"<?= $key ?>": [
    						<?php foreach($v as $cmt){ ?>
    									{"author": "<?= $cmt['author'] ?>",
    									 "time": <?= $cmt['time'] ?>,
    									 "public": <?= $cmt['public'] ?>,
    								 "text": <?= json_encode(preg_replace('/\s+/', ' ', nl2br($cmt['comment'], false))) ?> },
    						<?php } ?>
    						],
    					<?php } else if($key == "answers"){
    						foreach($v as $answ){?>
    						<?= json_encode(preg_replace('/\s+/', ' ', nl2br($answ['question'], false)))?>: <?= json_encode(preg_replace('/\s+/', ' ', nl2br($answ['text'],false))) ?>,
    					<?php }
    					      } else { ?>
    				    "<?= $key ?>": <?= json_encode(preg_replace('/\s+/', ' ', nl2br($v, false))) ?>,
    			    <?php 	  } 
    				}
                }  ?>
    	    },
        <?php } }
    		if(isset($rejected[$t['id']])){
    		 foreach($rejected[$t['id']] as $val){ ?>
    	    
    	    "<?= $val['id'] ?>": {
    		    <?php foreach($val as $key=>$v){
    			    if($key != "id" && $key != "score" && $key != "answer_id" && $key != "reviews" && $key != 'confirmed') {
    					if($key == "comments"){ ?>
    						"<?= $key ?>": [
    						<?php foreach($v as $cmt){ ?>
    									{"author": "<?= $cmt['author'] ?>",
    									 "time": <?= $cmt['time'] ?>,
    									 "public": <?= $cmt['public'] ?>,
    									 "text": <?= json_encode(preg_replace('/\s+/', ' ', nl2br($cmt['comment'], false))) ?> },
    						<?php } ?>
    						],
    					<?php } else if($key == "answers"){
    						foreach($v as $answ){?>
    						<?= json_encode(preg_replace('/\s+/', ' ', nl2br($answ['question'],false)))?>: <?= json_encode(preg_replace('/\s+/', ' ', nl2br($answ['text'], false))) ?>,
    					<?php }
    					      } else { ?>
    				    "<?= $key ?>": <?= json_encode(preg_replace('/\s+/', ' ', nl2br($v, false))) ?>,
    			    <?php 	  } 
    				}
                }     ?>    	
            },
        <?php } } }?>
    };

    var other = {
    	<?php if(count($rest) > 0){
    			 foreach($rest as $val){ ?>
        
    	    "<?= $val['id'] ?>": {
    		    <?php foreach($val as $key=>$v){
    				if($key != "id" && $key != "score" && $key != "answer_id" && $key != "reviews" && $key != 'confirmed') {
    				   if($key == "comments"){ ?>
    						"<?= $key ?>": [
    						<?php foreach($v as $cmt){ ?>
    									{"author": "<?= $cmt['author'] ?>",
    									 "time": <?= $cmt['time'] ?>,
    									 "public": <?= $cmt['public'] ?>,
    									 "text": <?= json_encode(preg_replace('/\s+/', ' ', nl2br($cmt['comment'], false))) ?> },
    						<?php } ?>
    						],
    					<?php } else if($key == "answers"){
    						foreach($v as $answ){?>
    						<?= json_encode(preg_replace('/\s+/', ' ', nl2br($answ['question'], false)))?>: <?= json_encode(preg_replace('/\s+/', ' ', nl2br($answ['text'], false))) ?>,
    					<?php }
    					      } else { ?>
    				    "<?= $key ?>": <?= json_encode(preg_replace('/\s+/', ' ', nl2br($v, false))) ?>,
    			    <?php 	  } 
    				}
                }?>
    	    },
        <?php } } ?>
    };

    // Initialize the vertical navigation
    $().setupVerticalNavigation(true);

    $('#responseDetail').on('hidden.bs.modal', function (e) {
      modalState = false;
    });

    function init(){
    $("tbody").off('click', 'tr');
    $("tbody").on('click', "tr",function (){
    	var others = $(this).attr("from");
    	var response = $(this).attr("response_id");
    	var other_response = $(this).attr("other_response_id");

    	if(others != "other"){
    		fillModal(response);
    		$("#responseDetail").modal('show');
    		modalState = true;
    	} else {
    		fillOtherModal(other_response);
    		$("#otherResponseDetail").modal('show');
    	}
    });
    }

    function nextKey() {
        var response = $("#modalTitle").attr("response");
        var topic = $("#modalTitle").attr("topic");
        var vote = $("#modalTitle").attr("voteGroup");

    var key = $("#table-" + topic + "-" + (vote == 0?"unreviewed":(vote>0?"accepted":"rejected")) + " tr[response_id=" + response +"]").next().attr("response_id");
    if($.isNumeric(key)){
    	return key;
    }
        return null;
    }

    function prevKey() {
    var response = $("#modalTitle").attr("response");
    var topic = $("#modalTitle").attr("topic");
    var vote = $("#modalTitle").attr("voteGroup");

    var key = $("#table-" + topic + "-" + (vote == 0?"unreviewed":(vote>0?"accepted":"rejected")) + " tr[response_id=" + response +"]").prev().attr("response_id");
    if($.isNumeric(key)){
    	return key;
    }
    return null;

    }

    function showErrorToast(message){
        var toast = $(".toast-notifications-list-pf").html()+
                    '<div class="toast-pf alert alert-danger alert-dismissable">'+
                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">'+
                    '<span class="pficon pficon-close"></span>'+
                    '</button>'+
                    '<span class="pficon pficon-error-circle-o"></span>'+
                    message+
                    '</div>';
         $(".toast-notifications-list-pf").html(toast);        
    }

    function showSuccessToast(message){
        var toast = $(".toast-notifications-list-pf").html()+
                    '<div class="toast-pf alert alert-success alert-dismissable">'+
                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">'+
                    '<span class="pficon pficon-close"></span>'+
                    '</button>'+
                    '<span class="pficon pficon-ok"></span>'+
                    message+
                    '</div>';
         $(".toast-notifications-list-pf").html(toast);        
    }

    function moveRow(response, topic, vote, amount){
        var oldVote = parseInt(database[response].vote);
        var move = (vote?"accepted":"rejected");
    database[response].vote = amount;
    var textVote = "";

    switch(amount){
    	case 3: textVote = "Accepted";
    		break;
    	case 2: textVote = "Waitlist";
    		break;
    	case 1: textVote = "Backup";
    		break;
    	default: textVote = "Rejected";
    		 break;
    }
        if(oldVote == 3){
            if($.isNumeric(database[response].duration)){
                $("#minutes-" + topic).html(parseInt($("#minutes-" + topic).html()) + parseInt(database[response].duration));

            }
        }

        if(amount == 3){
            if($.isNumeric(database[response].duration)){
                $("#minutes-" + topic).html(parseInt($("#minutes-" + topic).html()) - parseInt(database[response].duration));
            }
        }
        var row = $("tr[response_id='" + response + "']");
    var oldTable = $("tr[response_id='" + response + "']").parent().parent();
        var order = oldTable.DataTable().order();
        oldTable.DataTable().destroy();
        $("tr[response_id='" + response + "'] td.vote").html(textVote);
        $("tr[response_id='" + response + "']").remove();

        $("#table-" + topic + "-" + move + " tbody tr td.dataTables_empty").parent().remove();
    $("#table-" + topic + "-" + move).DataTable().destroy();
        if(oldVote == 0){
            var count = parseInt($("#unreviewed-"+topic+"-count").html());
            $("#unreviewed-"+topic+"-count").html(count-1);

            if(vote){
                count = parseInt($("#accepted-"+topic+"-count").html());
                $("#accepted-"+topic+"-count").html(count+1);
            } else {
                count = parseInt($("#rejected-"+topic+"-count").html());
                $("#rejected-"+topic+"-count").html(count+1);
            }           
        } else {
            if(oldVote > 0){
                var count = parseInt($("#accepted-"+topic+"-count").html());
                $("#accepted-"+topic+"-count").html(count-1);
                count = parseInt($("#"+move+"-"+topic+"-count").html());
                $("#"+move+"-"+topic+"-count").html(count+1);
            } else {
                var count = parseInt($("#"+move+"-"+topic+"-count").html());
                $("#"+move+"-"+topic+"-count").html(count+1);
                count = parseInt($("#rejected-"+topic+"-count").html());
                $("#rejected-"+topic+"-count").html(count-1);
            }
        }
        $("#table-" + topic + "-" + move + " tbody").append(row);
    $(".table").DataTable({paging: false});
    oldTable.DataTable({"order": order});
    init();   
    }


    function nextOther() {
        var response = $("#otherModalTitle").attr("response");

    var key = $("#table-others tr[other_response_id=" + response +"]").next().attr("other_response_id");
        if($.isNumeric(key)){
                return key;
        }
        return null;
    }

    function sendTopics(response, toAdd, toConcat, toAddArray) {
    
        var xhr = new XMLHttpRequest();
        xhr.open('POST', '/vote/topics/');
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.onload = function() {
            try{
    		var data = JSON.parse(xhr.responseText);
            } catch (e) {
    		$("#otherResponseDetail").modal('hide');
    		showErrorToast(xhr.responseText);
            }
            if(data.result != 'success'){
          		$("#otherResponseDetail").modal('hide');
            	showErrorToast(data.reason);
            } else {
    		other[response].topic_id += toAdd;
    		other[response].topic += toConcat;
    		$("td[other_topic=" + response + "]").html(other[response].topic);
    	}
           };
           xhr.send(jQuery.param({'token': token, 'response': response, 'event': event_id, 'topic': toAddArray}));
    }

    $("#next").click(function (){
        var nextKeys = nextOther();

        fillOtherModal(nextKeys);
    });

    $("#addTopic").click(function (){
        var nextKeys = nextOther();
        var response = $("#otherModalTitle").attr("response");
    var checkboxes = $("input[type=checkbox]:checked");

    var toAdd = "", toConcat = "", toAddArray = [];
    $.each(checkboxes, function(key, val){ 
    	if($(val).attr("disabled") == undefined){
    		toAdd += "," + $(val).attr("value");
    		toAddArray.push($(val).attr("value"));
    		toConcat += ", " + allTopics[$(val).attr("value")];
    	}
    });
    
    if(toAdd == "") {
    	showErrorToast("You didn't specified any topic to add.");
    } else {
    	if(fillOtherModal(nextKeys)){
    		sendTopics(response, toAdd, toConcat, toAddArray);
    	}
    }
        
    });

    function sendVote(vote, amount, response, topic) {
        var xhr = new XMLHttpRequest();
        xhr.open('POST', '/vote/');
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.onload = function() {
            try{
                var data = JSON.parse(xhr.responseText);
            } catch (e) {
                $("#responseDetail").modal('hide');
                showErrorToast(xhr.responseText);
                return;
            }
            if(data.result != 'success'){
                $("#responseDetail").modal('hide');
                showErrorToast(data.reason);
            }else{
                moveRow(response, topic, vote, parseInt(amount));
            }
        };

        xhr.send(jQuery.param({'token': token, 'response': response, 'event': event_id, 'vote': amount , 'captain': 1}));
    }

    function addComment(from, when, what, record, pub){
    var content = $("#comments-list").html();
    var ncom = {"author": from, "time": when, "text": what, "public": parseInt(pub)};
    if(pub != "1"){
    	$("#comments-list").html("<div class='comment alert alert-danger'><span class='pficon pficon-private' title='Private comment'></span><h4>" + from + "</h4><p><i><small>" + new Date(when).toString() + "</small></i><br>" + what + "</p></div>" + content);
    }else{
    	$("#comments-list").html("<div class='comment'><h4>" + from + "</h4><p><i><small>" + new Date(when).toString() + "</small></i><br>" + what + "</p></div>" + content);
    }
    var ot = other[record.response_id];
    if(ot != undefined){
    	ot.comments.unshift(ncom);
    }
    $.each(database, function (key, val){
    	if(val.response_id == record.response_id){
    		val.comments.unshift(ncom);
    	}
    });
    $("#comments-count").html(record.comments.length);
    }

    function addOtherComment(from, when, what, record, pub){
    var content = $("#other-comments-list").html();
    var ncom = {"author": from, "time": when, "text": what, "public": parseInt(pub)};
    if(pub != "1"){
    	$("#other-comments-list").html("<div class='comment alert alert-danger'><span class='pficon pficon-private' title='Private comment'></span><h4>" + from + "</h4><p><i><small>" + new Date(when).toString() + "</small></i><br>" + what + "</p></div>" + content);
    }else{
    	$("#other-comments-list").html("<div class='comment'><h4>" + from + "</h4><p><i><small>" + new Date(when).toString() + "</small></i><br>" + what + "</p></div>" + content);
    }
    record.comments.unshift(ncom);
    $.each(database, function (key, val){
    	if(val.response_id == record.id){
    		val.comments.unshift(ncom);
    	}
    });
    $("#other-comments-count").html(record.comments.length);
    }

    $(".add-other-comment").click(function (){
    var response = $("#otherModalTitle").attr("response");
    var publicly = $(this).attr("pub");
    var record = other[response];

    var from = fullReviewer + " (captain reviewer)";
    var what = $("#other-comment-input").val();
    if(what == ""){
    	showErrorToast("You have to add some text first.");
    	return;
    }
    var when = new Date().getTime();

    from = encodeURIComponent(from);
    what = encodeURIComponent(what);

    var xhr = new XMLHttpRequest();
        xhr.open('POST', '/comment/');
           xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.onload = function() {
               try{
                   var data = JSON.parse(xhr.responseText);
            } catch (e) {
                   $("#otherResponseDetail").modal('hide');
                   showErrorToast(xhr.responseText);
                return;
               }
            if(data.result != 'success'){
                   $("#otherResponseDetail").modal('hide');
                   showErrorToast(data.reason);
            }else{
    	if(data.message != undefined){
    		showSuccessToast(data.message);
    	}
                   addOtherComment(decodeURIComponent(from), when, decodeURIComponent(what), record, publicly);
            }
           };

    if(publicly != "1"){
            xhr.send(jQuery.param({'token': token, 'response': response, 'from': from, 'what': what , 'when': when}));
    } else {
            xhr.send(jQuery.param({'token': token, 'response': response, 'from': from, 'what': what , 'when': when, 'public': 1}));
    }

    $("#other-comment-input").val("");
    });

    $(".add-comment").click(function (){
    var response = $("#modalTitle").attr("response");
    var publicly = $(this).attr("pub");
    var record = database[response];

    var from = fullReviewer + " (captain reviewer of " + record.topic + ")";
    var what = $("#comment-input").val();
    if(what == ""){
    	showErrorToast("You have to add some text first.");
    	return;
    }
    var when = new Date().getTime();
    
    from = encodeURIComponent(from);
    what = encodeURIComponent(what);

    var xhr = new XMLHttpRequest();
        xhr.open('POST', '/comment/');
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.onload = function() {
            try{
                var data = JSON.parse(xhr.responseText);
            } catch (e) {
                $("#responseDetail").modal('hide');
                showErrorToast(xhr.responseText);
                return;
            }
            if(data.result != 'success'){
                $("#responseDetail").modal('hide');
                showErrorToast(data.reason);
            }else{
    	if(data.message != undefined){
    		showSuccessToast(data.message);
    	}
                addComment(decodeURIComponent(from), when, decodeURIComponent(what), record, publicly);
            }
        };

    if(publicly != "1"){
            xhr.send(jQuery.param({'token': token, 'response': record['response_id'], 'from': from, 'what': what , 'when': when}));
    } else {
            xhr.send(jQuery.param({'token': token, 'response': record['response_id'], 'from': from, 'what': what , 'when': when, 'public': 1}));
    }
    $("#comment-input").val("");
    });

    function fillModal(keys){
    if($("#comment-input").val() != ""){
            alert("You haven't submit your comment for this proposal. Please, submit your comment or remove text from comment input field.");
            return false;
        } else {
        if(keys != null){
                var record = database[keys];
    	$("#modalTitle").html("<b>" + record.title + "</b><br>(Captain votes: " + record.vote + ")");
        	var html = "", comments="";
        	$.each(record, function (key, val){
           		if(key != "topic_id" && key != "response_id" && key != "title" && key != "vote" && key != "comments" && key != "topic"){
        			html += "<p><label>" + key + "</label><br>"+ val+"</p>";
        		}
        	});
    	$.each(record.comments, function (key, val){
    		if(val.public != 1){
    			comments += "<div class='comment alert alert-danger'><span class='pficon pficon-private' title='Private comment'></span><h4>" + 
    					val.author +"</h4><p><i><small>"+new Date(val.time).toString()+"</small></i><br>"+val.text+"</p></div>";
    		}else{
    			comments += "<div class='comment'><h4>" + val.author +"</h4><p><i><small>"+new Date(val.time).toString()+"</small></i><br>"+val.text+"</p></div>";
    		}
    	});
        	$("#modalTitle").attr("voteGroup", record.vote);
        	$("#modalTitle").attr("topic", record.topic_id);
        	$("#modalTitle").attr("response", keys);
    	$(".modal .active").removeClass("active");
    	$("#tabActive").addClass("active");
    	$("#modalBody").addClass("active");
        	$("#modalBody").html(html);
    	$("#comments-list").html(comments);
    	$("#comments-count").html(record.comments.length);
    	$("#responseDetail").animate({scrollTop: 0}, 100);
            } else {
            $("#responseDetail").modal('hide');
            }
            return true;
        }
    }

    function fillOtherModal(nextKeys){
        if($("#other-comment-input").val() != ""){
            alert("You haven't submit your comment for this proposal. Please, submit your comment or remove text from comment input field.");
            return false;
        } else {
    	if(nextKeys != null){
    	$('input[type=checkbox]').removeAttr('checked');
    	$('input[type=checkbox]').removeAttr('disabled');
    	$('input[type=checkbox]').prop('checked', false);

            var record = other[nextKeys];
        	$("#otherModalTitle").html("<b>" + record.title + "</b>");
        	var html = "", comments = "";
        	$.each(record, function (key, val){
           		if(key != "topic_id" && key != "comments" && key != "title" && key != "vote"){
        			html += "<p><label>" + key + "</label><br>"+ val+"</p>";
        		}
    		if(key == "topic_id") {
    			var tid = val.split(",");
    			$.each(tid, function( tkey, tval ){
    				$("input[type=checkbox][value="+tval+"]").prop('checked', true);
    				$("input[type=checkbox][value="+tval+"]").attr("checked", "checked");
    				$("input[type=checkbox][value="+tval+"]").attr("disabled", "disabled");
    			});
    		}
        	});
    	$.each(record.comments, function (key, val){
    		comments += "<div><h4>" + val.author +"</h4><p><i><small>"+new Date(val.time).toString()+"</small></i><br>"+val.text+"</p></div>";
    	});
        	$("#otherModalTitle").attr("response", nextKeys);
    	$(".modal .active").removeClass("active");
    	$("#otherTabActive").addClass("active");
    	$("#otherModalBody").addClass("active");
        	$("#otherModalBody").html(html);
        	$("#other-comments-list").html(comments);
    	$("#other-comments-count").html(record.comments.length);
    	$("#otherResponseDetail").animate({scrollTop: 0}, 100);
        } else {
            $("#otherResponseDetail").modal('hide');
        }
    return true;
    }
    }

    $("#skip").click(function (){
        var nextKeys = nextKey();
    fillModal(nextKeys);        
    });

    $("#prev").click(function (){
    var prevKeys = prevKey();
    fillModal(prevKeys);
    });

    $("#comment-input, #other-comment-input").keydown(function(event){ 
    event.stopPropagation(); 
    });

    $(window).keydown(function (event){
    if(modalState){
    	if(event.keyCode == 39){ // ArrowRight
    		var nextKeys = nextKey();
    		fillModal(nextKeys);
    	}

    	if(event.keyCode == 37){ // ArrowLeft
    		var prevKeys = prevKey();
    		fillModal(prevKeys);
    	}
    }
    });

    $(".accept, .reject").click(function (){
        var nextKeys = nextKey(),
            response = $("#modalTitle").attr("response"),
            topic = $("#modalTitle").attr("topic");

    if(fillModal(nextKeys)){
            sendVote(($(this).attr('vote')>0), $(this).attr('vote'), response, topic);
    }
    });

    init(); 
    $(".table").DataTable({paging:false});
  });
</script>
</body>
</html>  
