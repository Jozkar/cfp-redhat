<div class="container-fluid container-cards-pf container-pf-nav-pf-vertical">
    <ol class="breadcrumb" role="navigation" aria-label="breadcrumbs">
        <li><a href="/"><span class="fa fa-home"></span></a></li>
    	<li><a href="/reviewer">Review tasks</a></li>
        <li class="active"><span><?= $eventInfo['name'] . " (" . $eventInfo['year'] . ")" ?> - Sessions</span></li>
    </ol>
	<h1>Review <?= $formType ?> proposals for <?= $eventInfo['name'] . " (" . $eventInfo['year'] . ")" ?></h1>
	<?php if((count($unreviewed) + count($skipped) + count($accepted) + count($rejected)) < 1) { ?>
	<div class="blank-slate-pf">
        <div class="blank-slate-pf-icon">
            <span class="pficon pficon-info"></span>
        </div>
        <h1>No responses are ready to review.</h1>
        <p>You have to wait for them. Meanwhile you can help with promotion of this event.</p>

	<?php }else{ ?>
			<div class="modal fade" id="responseDetail" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
        						<button type="button" class="close" data-dismiss="modal" aria-hidden="true" aria-label="Close">
							        <span class="pficon pficon-close"></span>
						        </button>
			        			<h4 class="modal-title">Response</h4>
						</div> <!-- modal header -->
						<div class="modal-body">
							<div class="h2 text-center" id="modalTitle"></div>
							<div class="tab-content">
								<ul class="nav nav-tabs" role="tablist">
									<li role="presentation" id="tabActive" class="active"><a href="#modalBody" aria-controls="modalBody" role="tab" data-toggle="tab">Proposal</a></li>
									<li role="presentation"><a href="#comments" aria-controls="comments" role="tab" data-toggle="tab">Comments <span class="badge" id="comments-count"></span></a></li>
								</ul>
								<div class="tab-pane active" id="modalBody"></div>
								<div class="tab-pane" id="comments">
									<div class="form-group">
										<label class="control-label">Add comment</label>
										<textarea id="comment-input" class="form-control"></textarea>
									</div>
									<div class="form-group" align="right">
										<a href="#" class="btn btn-primary" id="add-comment">Add</a>
									</div>
									<div id="comments-list"></div>
								</div>
							</div>
						</div>
					 	<div class="modal-footer">
							<a vote="-3" class="btn btn-danger reject" title="It's nonsense and it doesn't belong here">-3</a>
							<a vote="-2" class="btn btn-danger reject" title="It doesn't bring anything new/interesting for the audience">-2</a>
							<a vote="-1" class="btn btn-danger reject" title="It's 'harmless', but I think there are better proposals">-1</a>
							<a id="prev" class="btn btn-default" title="Previous proposal">Prev</a>
                            <a vote="0" class="btn btn-info skip" title="Can't vote for this talk, due of I am one of the speakers">Skip</a>
							<a id="skip" class="btn btn-default" title="Next proposal">Next</a>
							<a vote="1" class="btn btn-success accept" title="Can be accepted, but it's not necessary">+1</a>
							<a vote="2" class="btn btn-success accept" title="Should be accepted if there is enough free spots">+2</a>
							<a vote="3" class="btn btn-success accept" title="It must be at the conference">+3</a>
						</div> <!-- modal footer -->
 					</div> <!-- modal content -->
				</div> <!-- modal dialog -->
			</div> <!-- modal -->
    <div class="tab-content">	
        <ul class="nav nav-tabs" style="margin-bottom: 15px" role="tablist">
        <?php $first = true; 
			  foreach($topics as $t) { 
				if($first){?>
		            <li role="presentation" class="active"><a href="#topic-<?= $t['id'] ?>" aria-controls="topic-<?= $t['id'] ?>" role="tab" data-toggle="tab"><?= $t['name'] ?></a></li>
	      <?php } else {?>
		            <li role="presentation"><a href="#topic-<?= $t['id'] ?>" aria-controls="topic-<?= $t['id'] ?>" role="tab" data-toggle="tab"><?= $t['name'] ?></a></li>
		  <?php } 
				$first = false;
			  } ?>
        </ul>
        <?php $first = true;
			  foreach($topics as $t) { 
		?>
    		<div role="tabpanel" style="background: white !important" class="tab-pane <?= ($first?'active':'') ?>" id="topic-<?= $t['id'] ?>">   
            	<div class="tab-content"><ul class="nav nav-tabs nav-tabs-pf" style="margin-bottom: 8px" role="tablist">
            		<li role="presentation" <?= ($first?'class="active"':'') ?>><a href="#unreviewed-<?= $t['id'] ?>" aria-controls="unreviewed-<?= $t['id'] ?>" role="tab" data-toggle="tab">Unreviewed <span class="label label-default" id="unreviewed-<?= $t['id'] ?>-count"><?= (isset($unreviewed[$t['id']])?count($unreviewed[$t['id']]):0) ?></span></a></li>
            		<li role="presentation"><a href="#skipped-<?= $t['id'] ?>" aria-controls="skipped-<?= $t['id'] ?>" role="tab" data-toggle="tab">Skipped <span class="label label-info" id="skipped-<?= $t['id'] ?>-count"><?= (isset($skipped[$t['id']])?count($skipped[$t['id']]):0) ?></span></a></li>
            		<li role="presentation"><a href="#accepted-<?= $t['id'] ?>" aria-controls="accepted-<?= $t['id'] ?>" role="tab" data-toggle="tab">Accepted <span class="label label-success" id="accepted-<?= $t['id'] ?>-count"><?= (isset($accepted[$t['id']])?count($accepted[$t['id']]):0) ?></span></a></li>
            		<li role="presentation"><a href="#rejected-<?= $t['id'] ?>" aria-controls="rejected-<?= $t['id'] ?>" role="tab" data-toggle="tab">Rejected <span class="label label-danger" id="rejected-<?= $t['id'] ?>-count"><?= (isset($rejected[$t['id']])?count($rejected[$t['id']]):0) ?></span></a></li>
            	</ul>
		        <div role="tabpanel" class="tab-pane active" id="unreviewed-<?= $t['id'] ?>">
                	<table class="table table-striped table-bordered table-hover" id="table-<?= $t['id'] ?>-unreviewed" style="background: white; height: auto !important">
        	    		<thead>
        	    			<tr>
						<th>#</th>
        	    				<th>Title</th>
        	    				<th>Type</th>
						<?php if(!$blindRev) { ?>
						<th>Speaker</th>
						<?php } ?>
        	    				<th>Topic</th>
        	    				<th>Duration</th>
                                <th>No. Comments</th>
						<th>My vote</th>
						<?php if(!$blindRev) { ?>
						<th>External link</th>
						<?php } ?>
					</tr>
        	    		</thead>
                		<tbody>
		        		<?php if(isset($unreviewed[$t['id']])){
					foreach($unreviewed[$t['id']] as $r) { ?>	
		        			<tr style="cursor: pointer" response_id="<?= $r['id'] ?>">
							<td><?= $r['response_id'] ?></td>
							<td><?= $r['title'] ?></td>
							<td><?= $r['type'] ?></td>
							<?= (!$blindRev?"<td>".$r['speaker']."</td>":"") ?>
							<td><?= $r['topics'] ?></td>
							<td><?= $r['duration'] ?></td>
							<td><?= count($r['comments']) ?></td>
							<td class='vote'>-empty-</td>
							<?php if(!$blindRev) { ?>
							<td><a href="/proposals/<?= $event . "/". $r['response_id'] ?>" target="_blank" class="btn btn-default">Open in new panel</a></td>
							<?php } ?>
						</tr>
		        		<?php }} ?>
	            		</tbody>
	        		</table>	      
    	    	</div>
                <div role="tabpanel" class="tab-pane" id="skipped-<?= $t['id'] ?>">
                    <table class="table table-striped table-bordered table-hover" id="table-<?= $t['id'] ?>-skipped" style="background: white; height: auto !important">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Title</th>
                                <th>Type</th>
                            <?php if(!$blindRev) { ?>
                                <th>Speaker</th>
                            <?php } ?>
                                <th>Topic</th>
                                <th>Duration</th>
                                <th>No. Comments</th>
                                <th>My vote</th>
                            <?php if(!$blindRev) { ?>
                                <th>External link</th>
                            <?php } ?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(isset($skipped[$t['id']])){
                            foreach($skipped[$t['id']] as $r) { ?>   
                                <tr style="cursor: pointer" response_id="<?= $r['id'] ?>">
                                <td><?= $r['response_id'] ?></td>
                                <td><?= $r['title'] ?></td>
                                <td><?= $r['type'] ?></td>
                                <?= (!$blindRev?"<td>".$r['speaker']."</td>":"") ?>
                                <td><?= $r['topics'] ?></td>
                                <td><?= $r['duration'] ?></td>
                                <td><?= count($r['comments']) ?></td>
                                <td class='vote'>0</td>
                                <?php if(!$blindRev) { ?>
                                <td><a href="/proposals/<?= $event . "/". $r['response_id'] ?>" target="_blank" class="btn btn-default">Open in new panel</a></td>
                                <?php } ?>
                            </tr>
                            <?php }} ?>
                        </tbody>
                    </table>
                </div>
	        	<div role="tabpanel" class="tab-pane" id="accepted-<?= $t['id'] ?>">
            	<table class="table table-striped table-bordered table-hover" id="table-<?= $t['id'] ?>-accepted" style="background: white; height: auto !important">
		        	<thead>
		        		<tr>
    						<th>#</th>
   		        			<th>Title</th>
		        			<th>Type</th>
						<?php if(!$blindRev) { ?>
	    					<th>Speaker</th>
						<?php } ?>
		        			<th>Topic</th>
		        			<th>Duration</th>
                            <th>No. Comments</th>
		    				<th>My vote</th>
						<?php if(!$blindRev) { ?>
			    			<th>External link</th>
						<?php } ?>
		        		</tr>
		        	</thead>
		        	<tbody>
		    		<?php if(isset($accepted[$t['id']])){
					foreach($accepted[$t['id']] as $r) { ?>	
		    			<tr style="cursor: pointer" response_id="<?= $r['id'] ?>">
				    		<td><?= $r['response_id'] ?></td>
    						<td><?= $r['title'] ?></td>
    						<td><?= $r['type'] ?></td>
	    					<?= (!$blindRev?"<td>".$r['speaker']."</td>":"") ?>
		    				<td><?= $r['topics'] ?></td>
			    			<td><?= $r['duration'] ?></td>
                            <td><?= count($r['comments']) ?></td>
				    		<td class='vote'><?= $r['vote'] ?></td>
					    	<?php if(!$blindRev) { ?>
						    <td><a href="/proposals/<?= $event . "/". $r['response_id'] ?>" target="_blank" class="btn btn-default">Open in new panel</a></td>
    						<?php } ?>
	    				</tr>
		    		<?php }} ?>
        			</tbody>
		    	</table>	      
		    </div>
		    <div role="tabpanel" class="tab-pane" id="rejected-<?= $t['id'] ?>">
            	<table class="table table-striped table-bordered table-hover" id="table-<?= $t['id'] ?>-rejected" style="background: white; height: auto !important">
	        		<thead>
	        			<tr>
		    				<th>#</th>
	        				<th>Title</th>
	        				<th>Type</th>
						<?php if(!$blindRev) { ?>
			    			<th>Speaker</th>
						<?php } ?>
	        				<th>Topic</th>
	        				<th>Duration</th>
                            <th>No. Comments</th>
				    		<th>My vote</th>
						<?php if(!$blindRev) { ?>
					    	<th>External link</th>
						<?php } ?>
	        			</tr>
	        		</thead>
	        		<tbody>
	    			<?php if(isset($rejected[$t['id']])){
					foreach($rejected[$t['id']] as $r) { ?>	
    					<tr style="cursor: pointer" response_id="<?= $r['id'] ?>">
    						<td><?= $r['response_id'] ?></td>
	    					<td><?= $r['title'] ?></td>
		    				<td><?= $r['type'] ?></td>
			    			<?= (!$blindRev?"<td>".$r['speaker']."</td>":"") ?>
				    		<td><?= $r['topics'] ?></td>
					    	<td><?= $r['duration'] ?></td>
                            <td><?= count($r['comments']) ?></td>
						    <td class='vote'><?= $r['vote'] ?></td>
    						<?php if(!$blindRev) { ?>
	    					<td><a href="/proposals/<?= $event . "/". $r['response_id'] ?>" target="_blank" class="btn btn-default">Open in new panel</a></td>
						<?php } ?>
		    			</tr>
	    			<?php }} ?>
	        		</tbody>
	    		</table>	      
	    	</div></div>
        </div>
	<?php $first=false; } } ?>
    </div> <!-- tab content -->
</div> <!-- container -->
<script>
  $(document).ready(function() {
    // matchHeight the contents of each .card-pf and then the .card-pf itself
    $(".row-cards-pf > [class*='col'] > .card-pf .card-pf-title").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf > .card-pf-body").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf > .card-pf-footer").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf").matchHeight();
    
    var event_id = "<?= $event ?>", token = "<?= $token ?>", reviewer = "<?= $reviewer ?>", fullReviewer = "<?= $username ?>", modalState = false;
    var database = {
    <?php foreach($topics as $t) { ?>
	    <?php if(isset($unreviewed[$t['id']])){
			foreach($unreviewed[$t['id']] as $val){ ?>
		    
		    "<?= $val['id'] ?>": {
			    <?php foreach($val as $key=>$v){
				    if($key != "id" && $key != "score" && $key != "answer_id" && (!$blindRev || $key != "speaker")) { 
						if($key == "comments"){ ?>
							"<?= $key ?>": [
							<?php foreach($v as $cmt){ ?>
										{"author": "<?= $cmt['author'] ?>",
										 "time": <?= $cmt['time'] ?>,
										 "public": <?= $cmt['public'] ?>,
										 "text": <?= json_encode(preg_replace('/\s+/', ' ', nl2br($cmt['comment'], false))) ?> },
							<?php } ?>
							],
						<?php } else if($key == "answers"){ 
							foreach($v as $answ){?>
							<?= json_encode(preg_replace('/\s+/', ' ', nl2br($answ['question'], false)))?>:<?= json_encode(preg_replace('/\s+/', ' ', nl2br($answ['text'], false))) ?>,
						<?php }
						      } else {?>
					    "<?= $key ?>": <?= json_encode(preg_replace('/\s+/', ' ', nl2br($v, false))) ?>,
				    <?php 	  } 
					}
                }     
				echo '"vote": "NaN"';?>
		    },

        <?php } }
        if(isset($skipped[$t['id']])){
             foreach($skipped[$t['id']] as $val){ ?>
            
            "<?= $val['id'] ?>": { 
                <?php foreach($val as $key=>$v){
                    if($key != "id" && $key != "score" && $key != "answer_id" && (!$blindRev || $key != "speaker")) {
                        if($key == "comments"){ ?>
                            "<?= $key ?>": [ 
                            <?php foreach($v as $cmt){ ?>
                                        {"author": "<?= $cmt['author'] ?>",
                                         "time": <?= $cmt['time'] ?>, 
                                         "public": <?= $cmt['public'] ?>,
                                         "text": <?= json_encode(preg_replace('/\s+/', ' ', nl2br($cmt['comment'], false))) ?> },
                            <?php } ?>
                            ],
                        <?php } else if($key == "answers"){
                            foreach($v as $answ){?>
                            <?= json_encode(preg_replace('/\s+/', ' ', nl2br($answ['question'], false)))?>: <?= json_encode(preg_replace('/\s+/', ' ', nl2br($answ['text'],false))) ?>,
                        <?php } 
                              } else { ?>
                        "<?= $key ?>": <?= json_encode(preg_replace('/\s+/', ' ', nl2br($v, false))) ?>,
                    <?php     }
                    }
                } ?>
            },
	    <?php } }
		if(isset($accepted[$t['id']])){
			 foreach($accepted[$t['id']] as $val){ ?>
		    
		    "<?= $val['id'] ?>": {
			    <?php foreach($val as $key=>$v){
				    if($key != "id" && $key != "score" && $key != "answer_id" && (!$blindRev || $key != "speaker")) { 
						if($key == "comments"){ ?>
							"<?= $key ?>": [
							<?php foreach($v as $cmt){ ?>
										{"author": "<?= $cmt['author'] ?>",
										 "time": <?= $cmt['time'] ?>,
										 "public": <?= $cmt['public'] ?>,
									 "text": <?= json_encode(preg_replace('/\s+/', ' ', nl2br($cmt['comment'], false))) ?> },
							<?php } ?>
							],
						<?php } else if($key == "answers"){
							foreach($v as $answ){?>
							<?= json_encode(preg_replace('/\s+/', ' ', nl2br($answ['question'], false)))?>: <?= json_encode(preg_replace('/\s+/', ' ', nl2br($answ['text'],false))) ?>,
						<?php }
						      } else { ?>
					    "<?= $key ?>": <?= json_encode(preg_replace('/\s+/', ' ', nl2br($v, false))) ?>,
				    <?php 	  } 
					}
                } ?>
		    },
	    <?php } }
		if(isset($rejected[$t['id']])){
			 foreach($rejected[$t['id']] as $val){ ?>
		    
		    "<?= $val['id'] ?>": {
			    <?php foreach($val as $key=>$v){
				    if($key != "id" && $key != "score" && $key != "answer_id" && (!$blindRev || $key != "speaker")) { 
						if($key == "comments"){ ?>
							"<?= $key ?>": [
							<?php foreach($v as $cmt){ ?>
										{"author": "<?= $cmt['author'] ?>",
										 "time": <?= $cmt['time'] ?>,
										 "public": <?= $cmt['public'] ?>,
										 "text": <?= json_encode(preg_replace('/\s+/', ' ', nl2br($cmt['comment'], false))) ?> },
							<?php } ?>
							],
						<?php } else if($key == "answers"){
							foreach($v as $answ){?>
							<?= json_encode(preg_replace('/\s+/', ' ', nl2br($answ['question'],false)))?>: <?= json_encode(preg_replace('/\s+/', ' ', nl2br($answ['text'], false))) ?>,
						<?php }
						      } else { ?>
					    "<?= $key ?>": <?= json_encode(preg_replace('/\s+/', ' ', nl2br($v, false))) ?>,
				    <?php 	  } 
					}
                } ?>		
            },
	    <?php } } }?>
    };

    // Initialize the vertical navigation
    $().setupVerticalNavigation(true);

    $('#responseDetail').on('hidden.bs.modal', function (e) {
        modalState = false;
    });

    function init(){
	    $("tbody").off('click', 'tr');
    	$("tbody").on('click', "tr",function (){
		    var response = $(this).attr("response_id");
    		fillModal(response);
	    	$("#responseDetail").modal('show');
		    modalState = true;
	    });
    }

    function nextKey() {
        var response = $("#modalTitle").attr("response");
        var topic = $("#modalTitle").attr("topic");
        var vote = $("#modalTitle").attr("voteGroup");

    	var key = $("#table-" + topic + "-" + (isNaN(vote)?"unreviewed":(vote>-1?(vote==0?"skipped":"accepted"):"rejected")) + " tr[response_id=" + response +"]").next().attr("response_id");
	    if($.isNumeric(key)){
		    return key;
    	}
        return null;
    }

    function prevKey() {
    	var response = $("#modalTitle").attr("response");
	    var topic = $("#modalTitle").attr("topic");
    	var vote = $("#modalTitle").attr("voteGroup");

	    var key = $("#table-" + topic + "-" + (isNaN(vote)?"unreviewed":(vote>-1?(vote==0?"skipped":"accepted"):"rejected")) + " tr[response_id=" + response +"]").prev().attr("response_id");
    	if($.isNumeric(key)){
	    	return key;
    	}
	    return null;
    }


    function showToast(message){
        var toast = $(".toast-notifications-list-pf").html()+
                    '<div class="toast-pf alert alert-danger alert-dismissable">'+
                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">'+
                    '<span class="pficon pficon-close"></span>'+
                    '</button>'+
                    '<span class="pficon pficon-error-circle-o"></span>'+
                    message+
                    '</div>';
        $(".toast-notifications-list-pf").html(toast);        
    }

    function moveRow(response, topic, amount){
        var oldVote = parseInt(database[response].vote);
        var move = (amount==0?"skipped":(amount>0?"accepted":"rejected"));
		database[response].vote = amount;

        var row = $("tr[response_id='" + response + "']");
    	var oldTable = $("tr[response_id='" + response + "']").parent().parent();
        var order = oldTable.DataTable().order();
        oldTable.DataTable().destroy();
        $("tr[response_id='" + response + "'] td.vote").html(amount);
        $("tr[response_id='" + response + "']").remove();

        $("#table-" + topic + "-" + move + " tbody tr td.dataTables_empty").parent().remove();
        $("#table-" + topic + "-" + move).DataTable().destroy();
        if(isNaN(oldVote)){
            var count = parseInt($("#unreviewed-"+topic+"-count").html());
            $("#unreviewed-"+topic+"-count").html(count-1);

            if(amount == 0){
                count = parseInt($("#skipped-"+topic+"-count").html());
                $("#skipped-"+topic+"-count").html(count+1);
            } else {
                if(amount > 0){
                    count = parseInt($("#accepted-"+topic+"-count").html());
                    $("#accepted-"+topic+"-count").html(count+1);
                } else {
                    count = parseInt($("#rejected-"+topic+"-count").html());
                    $("#rejected-"+topic+"-count").html(count+1);
                }
            }
        } else {
            var count = parseInt($("#"+move+"-"+topic+"-count").html());
            $("#"+move+"-"+topic+"-count").html(count+1);

            if(oldVote == 0){
                count = parseInt($("#skipped-"+topic+"-count").html());
                $("#skipped-"+topic+"-count").html(count-1);
            } else {
                if(oldVote > 0){
                    count = parseInt($("#accepted-"+topic+"-count").html());
                    $("#accepted-"+topic+"-count").html(count-1);
                } else { 
                    count = parseInt($("#rejected-"+topic+"-count").html());
                    $("#rejected-"+topic+"-count").html(count-1);
                }
            }
        }
        $("#table-" + topic + "-" + move + " tbody").append(row);

    	oldTable.DataTable({"order": order});
	    $(".table").DataTable({paging: false});
    	init();   
    }

    function sendVote(amount, response, topic) {
        var xhr = new XMLHttpRequest();
        xhr.open('POST', '/vote/');
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.onload = function() {
            try{
                var data = JSON.parse(xhr.responseText);
            } catch (e) {
                $("#responseDetail").modal('hide');
                showToast(xhr.responseText);
                return;
            }
            if(data.result != 'success'){
                $("#responseDetail").modal('hide');
                showToast(data.reason);
            }else{
                moveRow(response, topic, parseInt(amount));
            }
        };

        xhr.send(jQuery.param({'token': token, 'response': response, 'event': event_id, 'vote': amount , 'reviewer': reviewer}));
    }


    function addComment(from, when, what, record){
    	var content = $("#comments-list").html();
	    var ncom = {"author": from, "time": when, "text": what, "public": 0};
    	$("#comments-list").html("<div class='comment alert alert-danger'><span class='pficon pficon-private' title='Private comment'></span><h4>" + from + "</h4><p><i><small>" + new Date(when).toString() + "</small></i><br>" + what + "</p></div>" + content);
	    $.each(database, function (key, val){
		    if(val.response_id == record.response_id){
			    val.comments.unshift(ncom);
    		}
	    });
    	$("#comments-count").html(record.comments.length);
    }

    $("#add-comment").click(function (){
	    var response = $("#modalTitle").attr("response");
    	var record = database[response];

	    var from = fullReviewer + " (reviewer of " + record.topic + ")";
    	var what = $("#comment-input").val();
	    from = encodeURIComponent(from);
    	if(what == ""){
	    	showToast("You have to add some text first.");
		    return;
    	}
	    what = encodeURIComponent(what);
    	var when = new Date().getTime();

	    var xhr = new XMLHttpRequest();
    
        xhr.open('POST', '/comment/');
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.onload = function() {
            try{
                var data = JSON.parse(xhr.responseText);
            } catch (e) {
                $("#responseDetail").modal('hide');
                showToast(xhr.responseText);
                return;
            }
            if(data.result != 'success'){
                $("#responseDetail").modal('hide');
                showToast(data.reason);
            }else{
                addComment(decodeURIComponent(from), when, decodeURIComponent(what), record);
            }
        };

        xhr.send(jQuery.param({'token': token, 'response': record['response_id'], 'from': from, 'what': what , 'when': when}));

    	$("#comment-input").val("");
    });

    function fillModal(keys){
        if($("#comment-input").val() != ""){
            alert("You haven't submit your comment for this proposal. Please, submit your comment or remove text from comment input field.");
            return false;
        } else {
        	if(keys != null){
                var record = database[keys];
		        $("#modalTitle").html("<b>" + record.title + "</b><br>(My votes: " + record.vote + ")");
       	        var html = "", comments="";
                $.each(record, function (key, val){
                    if(key != "topic_id" && key != "response_id" && <?php if($blindRev){ ?>key != "speaker" &&<?php }?> key != "title" && key != "vote" && key != "comments" && key != "topic"){
                        html += "<p><label>" + key + "</label><br>"+ val+"</p>";
                    }
                });
	    	    $.each(record.comments, function (key, val){
		    	    if(val.public != 1){
			    	    comments += "<div class='comment alert alert-danger'><span class='pficon pficon-private' title='Private comment'></span><h4>" + 
				    	    	val.author +"</h4><p><i><small>"+new Date(val.time).toString()+"</small></i><br>"+val.text+"</p></div>";
        			}else{
	        			comments += "<div class='comment'><h4>" + val.author +"</h4><p><i><small>"+new Date(val.time).toString()+"</small></i><br>"+val.text+"</p></div>";
		        	}
    		    });
    	        $("#modalTitle").attr("voteGroup", record.vote);
	            $("#modalTitle").attr("topic", record.topic_id);
    	        $("#modalTitle").attr("response", keys);
	    	    $(".modal .active").removeClass("active");
    		    $("#tabActive").addClass("active");
        		$("#modalBody").addClass("active");
	            $("#modalBody").html(html);
		        $("#comments-list").html(comments);
        		$("#comments-count").html(record.comments.length);
	        	$("#responseDetail").animate({scrollTop: 0}, 100);
            } else {
	            $("#responseDetail").modal('hide');
            }
            return true;
        }
    }

    $("#skip").click(function (){
        var nextKeys = nextKey();
	    fillModal(nextKeys);        
    });

    $("#prev").click(function (){
    	var prevKeys = prevKey();
	    fillModal(prevKeys);
    });

    $("#comment-input").keydown(function(event){ 
    	event.stopPropagation(); 
    });

    $(window).keydown(function (event){
	if(modalState){
		if(event.keyCode == 39){ // ArrowRight
			var nextKeys = nextKey();
			fillModal(nextKeys);
		}

		if(event.keyCode == 37){ // ArrowLeft
			var prevKeys = prevKey();
			fillModal(prevKeys);
		}
	}
    });

    $(".accept, .reject, .skip").click(function (){
        var nextKeys = nextKey(),
            response = $("#modalTitle").attr("response"),
            topic = $("#modalTitle").attr("topic");

        if(fillModal(nextKeys)){
            sendVote($(this).attr('vote'), response, topic);
        }
    });

    init(); 
    $(".table").DataTable({paging:false});
  });
</script>
</body>
</html>  
