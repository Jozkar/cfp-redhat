<div class="container-fluid container-cards-pf container-pf-nav-pf-vertical">
    <ol class="breadcrumb" role="navigation" aria-label="breadcrumbs">
        <li><a href="/"><span class="fa fa-home"></span></a></li>
        <li class="active"><span>Schedule for <?= $event['name'] . " (" . $event['year'] . ")" ?></span></li>
    </ol>
    <?php if($event['schedule_version'] < 1) { ?>
    <div class="blank-slate-pf">
       <div class="blank-slate-pf-icon">
            <span class="pficon pficon-info"></span>
       </div>
        <h1>Schedule hasn't been published yet.</h1>
        <p>Check out your event channel to be aware of when schedule will be available.</p>
    </div>
    <?php } else { ?>
    <script src="/webroot/js/ics.js"></script>

	<div class="row row-cards-pf">
		<div class="col-xs-12 ">
			<div class="card-pf card-pf-view">
				<div class="card-pf-body">
                    <span id="icald" class="btn btn-primary">Download in .ical</span>
                    <h1 class="text-center"><strong><?= $event['name'] . " " . $event['year'] ?></strong></h1>
                    <div style="margin: 0px auto">
                        <ul class="nav nav-tabs nav-tabs-pf" role="tablist" style="display: flex; justify-content: center; border-bottom: none">
                            <li class="nav active dateTab" date="<?= $event['open'] ?>" role="presentation"><a role="tab" data-toggle="tab" aria-expanded="true"><?= date_create($event['open'])->format('l, F j, Y') ?></a></li>
                        <?php   $start = date_create($event['open']);
                                $end = date_create($event['close']);
                                while($start != $end){
                                    $start->add(date_interval_create_from_date_string('1 day'));
                                    echo '<li class="nav dateTab" date="' . $start->format("Y-m-d") . '" role="presentation"><a role="tab" data-toggle="tab" aria-expanded="false">' . $start->format('l, F j, Y') . '</a></li>';
                                } ?>
                        </ul>
                    </div>
                    <label class="control-label" for="info">All sessions are in <?= $event['timezone'] ?> time zone</label>

                    <div id="calendar">
                    </div>
				</div> <!-- end card body -->
			</div> <!-- end card -->
        </div> <!-- cols -->
    </div> <!-- row -->
    <div class="modal fade" id="eventDetail" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
   					<button type="button" class="close" data-dismiss="modal" aria-hidden="true" aria-label="Close" style="margin-top: 10px">
				        <span class="pficon pficon-close"></span>
			        </button>
                    <h4><strong>Session detail</strong></h4>
				</div> <!-- modal header -->
				<div class="modal-body">
                    <div>
                            <span class="fa fa-table" style="font-size: 2ex; margin-right: 1ex"></span><span id="modalDate"></span>
                            <span class="fa fa-clock-o" style="font-size: 2ex; margin: 0px 1ex"></span><span id="modalTime"></span>
                            <span class="fa fa-map-marker" style="font-size: 2ex; margin: 0px 1ex"></span><span id="modalRoom"></span><br>
                            <span class="label" style="margin: 1ex 1ex 0px 0px; display: inline-block" id="modalTopic"></span>           
                    </div>
					<div class="h2" id="modalTitle" style="margin-top: 1ex"></div>
                    <div id="modalDescription"></div>
                    <hr>
                    <span class="fa list-view-pf-icon-sm" style="border: 1px solid #333; border-radius: 10px; margin-right: 1ex">
            			<img src="" style="max-width: 40px; padding: 5px; border-radius: 10px;" id="modalAvatar">  
                    </span>
                    <span id="modalSpeaker" class="h5"></span><br>
                    <span id="modalBio" style="margin-left: calc(44px + 1ex)"></span>
                    
				</div>
			</div> <!-- modal content -->
		</div> <!-- modal dialog -->
	</div> <!-- modal -->
<?php } ?>
</div> <!-- container -->
<script src='https://cdn.jsdelivr.net/npm/fullcalendar-scheduler@6.1.10/index.global.min.js'></script>
<script src="https://cdn.jsdelivr.net/npm/luxon@3.4.4/build/global/luxon.min.js"></script>
<script>
  $(document).ready(function() {
    // matchHeight the contents of each .card-pf and then the .card-pf itself
    $(".row-cards-pf > [class*='col'] > .card-pf .card-pf-title").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf > .card-pf-body").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf > .card-pf-footer").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf").matchHeight();

    // Initialize the vertical navigation
    $().setupVerticalNavigation(true);

  });
<?php if($event['schedule_version'] > 0){ ?>
  document.addEventListener('DOMContentLoaded', function() {
      var calendarEl = document.getElementById('calendar');
      
      var calendar = new FullCalendar.Calendar(calendarEl, {
        schedulerLicenseKey: 'GPL-My-Project-Is-Open-Source',
        initialView: 'timeGridCustom',
        headerToolbar: {start:'', center:'', end:'timeGridCustom,timelineDayCustom,listDayCustom'},
        timeZone: '<?= $event['timezone'] ?>',
        resourceAreaColumns: [
            {
                field: 'title',
                headerContent: 'Room'
            }
        ],
        slotLabelFormat: {
            hour: 'numeric',
            minute: '2-digit',
            meridiem: false,
            hour12: false
        },
        titleFormat: {
            weekday: 'long',
            month: 'long',
            day: 'numeric',
            year: 'numeric'
        },
        views: {
            timeGridCustom: {
                type: 'resourceTimeGrid',
                buttonText: 'Grid'
            },
            timelineDayCustom: {
                type: 'resourceTimelineDay',
                buttonText: 'Timeline'
            },
            listDayCustom: {
                type: 'listDay',
                buttonText: 'Agenda'
            }
        },
        buttonIcons: false,
        initialDate: '<?= $event['open'] ?>',
        validRange: {
            start: '<?= $event['open'] ?>',
            end: '<?= ($event['open'] != $event['close']?date_create($event['close'])->add(date_interval_create_from_date_string('1 day'))->format('Y-m-d'):$event['close']) ?>'
        },
        <?php if($rooms != null && sizeof($rooms) > 0){ ?>
        resources: [
        <?php foreach($rooms as $r){ ?>
            {
                id: "<?= $r['id'] ?>",
                title: "<?= $r['name'] ?>",
                overlap: false
            },
        <?php } ?>
        ],
        events: [
         <?php   foreach($Responses as $r){ 
                echo json_encode(array("title" => $r['title'] . " - " . $r['speaker'],
                "textColor" => $r['text_color'],
                "durationEditable" => false,
                "startEditable" => false,
                "resourceId" => $r['room_id'],
                "start" => $r['date'] . "T" . $r['start'],
                "end" => $r['date'] . "T" . $r['end'],
                "id" => $r['id'],
                "backgroundColor" => $r['color'],
                "borderColor" => $r['color'],
                "extendedProps" => array(
                    "response_id" => $r['response_id'],
                    "speaker" => $r['speaker'],
                    "avatar" => $r['avatar'],
                    "employer" => $r['organization'],
                    "bio" => $r['bio'],
                    "topic" => $r['topic'],
                    "duration" => $r['duration'] . " min",
                    "durationInTime" => date_time_set(new DateTime(), 0, $r['duration'])->format("H:i"),
                    "title" => $r['title'],
                    "description" => json_encode(preg_replace('/\s+/', ' ', nl2br($r['description'], false)))))) . ",";
            } 
            foreach($Answers as $a) {
                echo json_encode(array(
                "title" => $a['title'] . " - " . $a['speaker'],
                "textColor" => $a['text_color'],
                "durationEditable" => false,
                "startEditable" => true,
                "resourceId" => $a['room_id'],
                "start" => $a['date'] . "T" . $a['start'],
                "end" => $a['date'] . "T" . $a['end'],
                "id" => $a['id'],
                "backgroundColor" => $a['color'],
                "borderColor" => $a['color'],
                "extendedProps" => array(
                    "answer_id" => $a['answer_id'],
                    "speaker" => $a['speaker'],
                    "employer" => $a['organization'],
                    "bio" => $a['bio'],
                    "avatar" => $a['avatar'],
                    "topic" => $a['topic'],
                    "duration" => $a['duration'] . " min",
                    "durationInTime" => date_time_set(new DateTime(), 0, $a['duration'])->format("H:i"),
                    "title" => $a['title'],
                    "description" => json_encode(preg_replace('/\s+/', ' ', nl2br($a['description'], false)))))) . ","; 
                
             } 
            foreach($breaks as $b) {?>
            {
                title: "<?= $b['title'] ?>",
                editable: false,
                start: '<?= $b['date'] . "T" . $b['start'] ?>',
                end: '<?= $b['date'] . "T" . $b['end'] ?>',
                id: '<?= $b['id'] ?>',
                resourceId: '<?= $b['room_id'] ?>',
                backgroundColor: "#ddd",
                borderColor: "#ddd",
                textColor: "#333",
                extendedProps: {
                    "break":true,
                }
            },
            <?php }} ?>
        ],
        eventClick: function(info){
            if(info.event.extendedProps.break == undefined){
                $("#modalTitle").html("<strong>" + info.event.extendedProps.title + "</strong>");
                $("#modalDescription").html(info.event.extendedProps.description);
                $("#modalDate").html(calendar.formatDate(info.event.start, {month: 'long', year: 'numeric', day: 'numeric'}));
                $("#modalTime").html(info.event.start.getUTCHours() + ":" + info.event.start.getUTCMinutes() + " - " + info.event.end.getUTCHours() + ":" + info.event.end.getUTCMinutes());
                $("#modalRoom").html(info.event.getResources()[0].title);
                $("#modalTopic").html(info.event.extendedProps.topic);
                $(".modal-header, #modalTopic").css({"background": info.event.backgroundColor, "color": info.event.textColor});
                $("#modalAvatar").attr("src", info.event.extendedProps.avatar);
                $("#modalSpeaker").html(info.event.extendedProps.speaker + " | " + info.event.extendedProps.employer);
                $("#modalBio").html(info.event.extendedProps.bio);
                $(".close").css({"color": info.event.textColor});
                $("#eventDetail").modal("show");
            }
        },
        editable: false,
        unselectAuto: false,
        filterResourcesWithEvents: true,
        slotDuration: '00:05:00',
      });
      calendar.render();

      $(".dateTab").on("click", function(element){
            calendar.gotoDate($(this).attr('date'));
      });

      var ical = ics();
      $.each(calendar.getEvents(),function(i,e){
          ical.addEvent("[<?= $event['name'] ?>] " + e.title, e.extendedProps.description, e.getResources()[0].title,
             luxon.DateTime.fromObject({year: e.start.getUTCFullYear(), month: e.start.getUTCMonth()+1, day: e.start.getUTCDate(), hour: e.start.getUTCHours(), minute: e.start.getUTCMinutes()}, {zone: '<?= $event['timezone'] ?>'}),
             luxon.DateTime.fromObject({year: e.end.getUTCFullYear(), month: e.end.getUTCMonth()+1, day: e.end.getUTCDate(), hour: e.end.getUTCHours(), minute: e.end.getUTCMinutes()}, {zone: '<?= $event['timezone'] ?>'}));
      });

      $("#icald").on('click',function(){
          ical.download('<?= $event['name'] ?>-schedule','.ics');
      });
});
<?php } ?>   
</script>
</body>
</html>
 
