<div class="container-fluid container-cards-pf container-pf-nav-pf-vertical">
	<div class="blank-slate-pf">
        <div class="blank-slate-pf-icon">
            <span class="pficon pficon-ok"></span>
        </div>
        <h1>Your proposal has been submitted successfully.</h1>
        <?php if($hasSMTP) { ?>
        <p>Thank you for your proposal for <?= $event['name'] . " (" . $event['year'] . ')' ?>. Confirmation with copy of your answer has been sent to your mail box.</p>
        <?php } else { ?>
        <p>Thank you for your proposal for <?= $event['name'] . " (" . $event['year'] . ')' ?>. You can see your proposal at <a href="/myproposals">My proposals</a> page.</p>
        <?php } ?>
	
	<div class="container-fluid container-cards-pf">
        	<div class="row row-cards-pf">
			<div class="col-xs-12 col-sm-12 col-md-12 col-log-12">
				<div class="card-pf card-pf-view">
					<div class="card-pf-body">
						<h2 class="card-pf-items text-center">
							What next?
						</h2>
						<div class="card-pf-items text-center">
							<div class="card-pf-item">
								<a href="/proposal/<?= $form['id'] . "/" . $response . "/" . $answer ?>">
									<span class="pficon pficon-edit"></span>
									Edit your proposal
								</a>
							</div>
							<div class="card-pf-item">
								<a href="/submit/<?= $event['id'] ?>">
									<span class="pficon pficon-add-circle-o"></span>
									Create new proposal
								</a>
							</div>
							<?php if ($form['speaker_limit'] != 0){ ?>
							<div class="card-pf-item">
								<a href="/additional/<?= $form['id'] . "/" . $response . "/" . $answer ?>">
									<span class="pficon pficon-users"></span>
									Manage additional speakers
								</a>
							</div>
							<?php } ?>
							<div class="card-pf-item">
								<a href="/myproposals">
									<span class="pficon pficon-applications"></span>
									See my proposals
								</a>
							</div>
							<div class="card-pf-item">
								<a href="/">
									<span class="pficon pficon-catalog"></span>
									Go to CfP catalog
								</a>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
    </div> <!-- tab content -->
</div> <!-- container -->
<script>
  $(document).ready(function() {
    // matchHeight the contents of each .card-pf and then the .card-pf itself
    $(".row-cards-pf > [class*='col'] > .card-pf .card-pf-title").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf > .card-pf-body").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf > .card-pf-footer").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf").matchHeight();
    
    // Initialize the vertical navigation
    $().setupVerticalNavigation(true);

  });
</script>
</body>
</html>  
