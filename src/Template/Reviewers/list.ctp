<div class="container-fluid container-cards-pf container-pf-nav-pf-vertical nav-pf-persistent-secondary">
    <ol class="breadcrumb" role="navigation" aria-label="breadcrumbs">
        <li><a href="/"><span class="fa fa-home"></span></a></li>
    	<li><a href="/manager">PM Review</a></li>
        <li class="active"><span>Reviewers</span></li>
    </ol>
    <form method="post" autocomplete="off" action="/reviewers/<?= $eventId ?>/add">
        <div class="form-group">
			<label>Add or update reviewer for <?= $eventInfo['name'] . " (" . $eventInfo['year'] . ")" ?></label>
    	    <select class="combobox form-control" name="user">
    	        <option value="" selected="selected">Choose user (start typing name or email)</option>
    	        <?php foreach($users as $u) { ?>
    	            <option value="<?= $u['email'] ?>"><?= $u['first_name'] . " " . $u['last_name'] . " (" . $u['email'] . ")" ?></option>
    	        <?php } ?>
    	    </select>
		</div>
		<div class="form-group">
			<label>Set session's topic to review</label>
			<div>
			<?php foreach($topics as $t) { ?>
				<label class="checkbox-inline">
					<input type="checkbox" act="tocheck" name="topic[]" value="<?= $t['id'] ?>"> <?= $t['name'] ?>
				</label>
			<?php } ?>
			<span class="help-block">Applicable only for 'Sessions' group of proposals.</span>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label">Set as captain for selected topics?</label>
			<input class="form-control bootstrap-switch" type="checkbox" value="1" name="captain">
		</div>
		<?php if(count($forms) > 1) { ?>
		<div class="form-group">
			<label>Choose group of proposals</label>
			<div>
			<?php foreach($forms as $f) { ?>
				<label class="radio-inline">
					<input type="radio" required name="formType" value="<?= $f['type'] ?>"> <?= ($f['placeholder'] != ""? $inflector->pluralize(ucfirst($f['type'])) . " (" . $inflector->pluralize(ucfirst($f['placeholder'])) . ")" : $inflector->pluralize(ucfirst($f['type']))) ?>
				</label>
			<?php } ?>
			</div>
		</div>
		<?php } else { ?>
		<input type="hidden" name="formType" value="session">
		<span class="help-block">Reviewer's group of proposals set to 'Sessions'.</span>
		<?php } ?>
		
        <input type="hidden" name="_Token" value="<?= $token ?>">
        <input type="hidden" name="_method" value="post">       
		<div class="form-group">
			<span>
			    <a href="#" class="btn btn-default" id="checkAll">Select all topics</a>
			</span>
			<span>
			    <input class="btn btn-primary" type="submit" name="save" value="Save reviewer">
			</span>
		</div>
    </form>
    <div class="list-group list-view-pf list-view-pf-view">
	<?php foreach($reviewers as $a){ ?>
        <div class="list-group-item">
        	<div class="list-view-pf-actions">
                <a class="btn btn-danger btn-lg" href="/reviewers/<?= $eventId ?>/delete/<?= $a['id'] . "/" . $a['captain'] . "/" . $token . "/" . $a['form_type'] ?>" data-toggle="tooltip" data-placement="top" title="Remove reviewer">
                    Remove reviewer
                </a>
            </div>
			<div class="list-view-pf-main-info">
				<div class="list-view-pf-left">
					<span class="fa list-view-pf-icon-sm" style="border: none !important">
							<img src="<?= $a['avatar'] ?>" style="width: 100%; object-fit: contain">
					</span>
				</div>
                <div class="list-view-pf-body">
                    <div class="list-view-pf-description">
                        <div class="list-group-item-text">
                			<strong><?= $a['first_name'] . " " . $a['last_name'] ?></strong> (<?= $a['email'] ?>)
                        </div>
                    </div> <!-- description -->
                    <div class="list-view-pf-additional-info">
                        <div class="list-view-pf-additional-info-item">
                            <span class='label label-success'><?= ($a['form_type'] == "quiz"?"Quizzes":($a['form_type'] == "activity"?"Activities":ucfirst($a['form_type'])."s")) ?></span> <?= ($a['captain']?"<span class='label label-warning'>Captain</span>":"") ?>
                        </div>
                        <?php foreach(explode(",", $a['topics']) as $top){
                                if($top != ""){?>
                                    <div class="list-view-pf-additional-info-item">
                                        <?= $topics[$top]['name'] ?>
                                    </div>
                        <?php   }
                              } ?>
                    </div>
                </div>
			</div> 
        </div>
	<?php } ?>
    </div> <!-- row -->
</div> <!-- container -->
<script>
  $(document).ready(function() {
    // Initialize the vertical navigation
    $().setupVerticalNavigation(true);

    // Initialize Boostrap-Combobox
    $('.combobox').combobox();

    $('#NaN').attr('autocomplete','off');

	// Initialize switch buttons
	$(".bootstrap-switch").bootstrapSwitch();

    $('#checkAll').click(function (){
        $('input[act=tocheck]').attr('checked', 'checked');
    });
    
	// autohide toasts
	$('.toast-pf').mouseenter(function () {
		$(this).attr('mouseenter','true');
	});

	$('.toast-pf').mouseleave(function () {
		$(this).removeAttr('mouseenter');
		if($(this).attr('remove') != null){
			$(this).remove();
		}		
	});

	function hideToasts() {
		var toasts = $('.toast-pf'), i;

		for(i = 0; i < toasts.length; i++){
			if($(toasts[i]).attr('mouseenter') != null){
				$(toasts[i]).attr('remove','true');
			} else {
				$(toasts[i]).hide('slow', function(){ $(toasts[i]).remove(); });
			}
		}
	}

	setTimeout(hideToasts, 8000);

  });
</script>
</body>
</html>
  
