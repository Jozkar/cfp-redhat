<div class="container-fluid container-cards-pf container-pf-nav-pf-vertical nav-pf-persistent-secondary">
    <ol class="breadcrumb" role="navigation" aria-label="breadcrumbs">
        <li><a href="/"><span class="fa fa-home"></span></a></li>
    	<li><a href="/manager">PM Review</a></li>
        <li class="active"><span>Unique codes</span></li>
    </ol>
    <div class="list-group list-view-pf">
    <div class="list-group-item list-view-pf-stacked">
    	<div class="list-view-pf-main-info">
    		<div class="list-view-pf-left">
    			<span class="fa list-view-pf-icon-md" style="background: none !important">
    				<img src="<?= ($eventInfo['logo']!=""?$eventInfo['logo']:"/webroot/img/logos/default.png") ?>" style="width: 100%; object-fit: contain">
    			</span>
    		</div>
    		<div class="list-view-pf-body">
    			<div class="list-view-pf-description">
    				<div class="list-group-item-heading">
    					<?= $eventInfo['name'] . " (" . $eventInfo['year'] . ")" ?>
    				</div>
    				<div class="list-group-item-text">
    					Number of codes: <?= count($codes) ?>
    				</div>
    			</div>
    		</div>
    		<div class="list-view-pf-right">
    			<a href="#" id="generate" class="btn btn-success openModal">Generate new codes</a>
    			<a href="/codes/<?= $eventInfo['id'] ?>/export" class="btn btn-primary">Export codes (.csv)</a>
    			<a href="/codes/<?= $eventInfo['id'] ?>/resend/1" class="btn btn-warning">Resend confirmaiton emails to primary speakers</a>
    			<a href="/codes/<?= $eventInfo['id'] ?>/resend/0" class="btn btn-default">Resend confirmaiton emails to secondary speakers</a>
    		</div>
    	</div>
    </div>
    </div>
    <table class="table table-striped table-bordered table-hover" id="table" style="background: white; height: auto !important">
    <thead>
    	<tr>
    		<th>Name</th>
    		<th>Email</th>
    		<th>Code</th>
    	</tr>
    </thead>
    <tbody>
    <?php foreach($codes as $a){ ?>
    	<tr>
    		<td><?= ($a['name']?$a['name']:"-empty-") ?></td>
    		<td><?= ($a['email']?$a['email']:"-empty-") ?></td>
    		<td><?= $a['code'] ?></td>
    	</tr> 
    <?php } ?>
    </tbody>
    </table> 
</div> <!-- container -->

<div class="modal fade" id="selection" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog">
    	<div class="modal-content">
    		<div class="modal-header">
    			<button type="button" class="close" data-dismiss="modal" aria-hidden="true" aria-label="Close">
    				<span class="pficon pficon-close"></span>
    			</button>
    			<h4 class="modal-title" id="myModalLabel">Generate codes</h4>
    		</div> <!-- modal header -->
    		<form action="/codes/<?= $eventInfo['id'] ?>/add" method="post">
    		<div class="modal-body">
    		</div>
    		<div class="modal-footer">
    			<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
    			<input type="submit" name="save" value="Save" class="btn btn-primary">
    		</div> <!-- modal footer -->
    		</form>
    	</div> <!-- modal content -->
    </div> <!-- modal dialog -->
</div> <!-- modal -->

<div id="modalContent" class="hidden">
    <input type="hidden" name="_Token" value="<?= $token ?>">
    <input type="hidden" name="_method" value="post">
    <div class="form-group">
    	<label class="control-label" for="prefix">Code prefix</label>
    	<input type="text" name="prefix" class="from-control" size="15">
    	<span class="help-block">Max. 15 characters</span>
    </div>
    <div class="form-group">
    	<label class="control-label" for="count">Count</label>
    	<input type="number" name="count" min="1" max="99999" value="1" class="form-control">
    	<span class="help-block">Set number of codes to generate (max. 99 999)</span>
    </div>
    <div class="form-group">
    	<label class="control-label" for="start">Start from</label>
    	<input type="number" name="start" min="0" max="99999" value="0" class="form-control">
    	<span class="help-block">Choose number, which should be used for first generated code (0 = detect it automatically)</span>
    </div>
</div>
<script>
  $(document).ready(function() {
    // matchHeight the contents of each .card-pf and then the .card-pf itself
    $(".row-cards-pf > [class*='col'] > .card-pf .card-pf-title").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf > .card-pf-body").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf > .card-pf-footer").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf").matchHeight();

    // Initialize the vertical navigation
    $().setupVerticalNavigation(true);

    // autohide toasts
    $('.toast-pf').mouseenter(function () {
    $(this).attr('mouseenter','true');
    });

    $('.toast-pf').mouseleave(function () {
    $(this).removeAttr('mouseenter');
    if($(this).attr('remove') != null){
    	$(this).remove();
    }		
    });

    $(".openModal").click(function (){
    if($(this).attr("action") == "delete"){
    	$("#modalDelete").attr("href", $(this).attr("event-id"));
    	$("#confirmations").modal('show');
    } else {
    	$("#selection .modal-body").html($("#modalContent").html());
    	$("#selection").modal('show');
    }
    });

    
    function hideToasts() {
    var toasts = $('.toast-pf'), i;

    for(i = 0; i < toasts.length; i++){
    	if($(toasts[i]).attr('mouseenter') != null){
    		$(toasts[i]).attr('remove','true');
    	} else {
    		$(toasts[i]).hide('slow', function(){ $(toasts[i]).remove(); });
    	}
    }
    }

    setTimeout(hideToasts, 8000);
    $("#table").DataTable({paging:false});

  });
</script>
</body>
</html>
  
