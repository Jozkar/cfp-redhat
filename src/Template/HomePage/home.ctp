<div class="container-fluid container-cards-pf container-pf-nav-pf-vertical nav-pf-persistent-secondary">
    <?php if(count($events)+count($upcommingEvents) < 1) { ?>
    <div class="blank-slate-pf">
        <div class="blank-slate-pf-icon">
            <span class="pficon pficon-info"></span>
        </div>
        <h1>No event is currently looking for proposals.</h1>
        <p>Check out your event channel to be aware of any CfP, that could be open in future.</p>
    </div>
    <?php } else { ?>
	<div class="row row-cards-pf">
	<?php foreach($events as $e){ ?>
        	<div class="col-xs-12 col-sm-6 col-md-3">
			<div class="card-pf card-pf-view card-pf-aggregate-status">
				<div class="card-pf-body">
					<div class="card-pf-top-element">
						<span class="card-pf-icon-circle" style="width: 100%; border: none !important">
							<img src="<?= ($e['logo']!=""?$e['logo']:"/webroot/img/logos/default.png") ?>" style="width: 100%; object-fit: contain; height: inherit">
						</span>
					</div>
					<h2 class="card-pf-title text-center"><?= $e['name'] . "<br>(" . $e['year'] .")" ?></h2>
					<div class="card-pf-items text-center">
						<?php if($e['redhat_event'] == 1){ ?>
						<div class="card-pf-item">
							<a href="/submit/<?= $e['id'] ?>" data-toggle="tooltip" data-placement="top" title="Submit">
								<span class="pficon pficon-edit"></span> Submit
							</a>
						</div>
						<?php } ?>
						<?php if($e['booking'] == true){ ?>
						<div class="card-pf-item">
							<a href="/booking/<?= $e['id'] ?>" data-toggle="tooltip" data-placement="top" title="Hotel booking">
								<span class="fa fa-home"></span> Hotel booking
							</a>
						</div>
						<?php } ?>
						<div class="card-pf-item">
							<a href="<?= $e['url'] ?>" data-toggle="tooltip" data-placement="top" target="_blank" title="Visit webpage">
								<span class="pficon pficon-connected"></span> Web
							</a>
						</div>
					</div>
					<p class="card-pf-info text-center"><strong>CfP closes on</strong> <?= date("d/M/Y \a\\t 23:59:59 e", strtotime(($e['form_close'] > $e['cfp_close']?$e['form_close']:$e['cfp_close']))) ?></p>
				</div>
			</div> 
	        </div> <!-- cols -->
	<?php } ?>
	<?php foreach($upcommingEvents as $e){ ?>
                <div class="col-xs-12 col-sm-6 col-md-3">
                        <div class="card-pf card-pf-view card-pf-aggregate-status">
                                <div class="card-pf-body">
                                        <div class="card-pf-top-element">
                                                <span class="card-pf-icon-circle" style="width: 100%; border: none !important">
                                                        <img src="<?= ($e['logo']!=""?$e['logo']:"/webroot/img/logos/default.png")  ?>" style="width: 100%; object-fit: contain; filter: blur(2px); height: inherit">
                                                </span>
                                        </div>
                                        <h2 class="card-pf-title text-center"><?= $e['name'] . "<br>(" . $e['year'] .")" ?></h2>
                                        <div class="card-pf-items text-center">
                                                <div class="card-pf-item">
                                                        <a href="<?= $e['url'] ?>" data-toggle="tooltip" data-placement="top" target="_blank" title="Visit webpage">
                                                                <span class="pficon pficon-connected"></span> Web
                                                        </a>
                                                </div>
                                        </div>
                                        <p class="card-pf-info text-center"><strong>CfP opens on</strong> <?= date("d/M/Y 0:00:00 e", strtotime($e['cfp_open'])) ?></p>
                                </div>
                        </div>
                </div> <!-- cols -->
        <?php } ?>
    </div> <!-- row -->
    <?php } ?>
    <?php if(count($oldEvents) >= 1) { ?>
	<div class="row row-cards-pf"><h2>Previous events</h2>
	<?php foreach($oldEvents as $e){ ?>
        	<div class="col-xs-12 col-sm-6 col-md-3">
    			<div class="card-pf card-pf-utilization">
	    			<div class="card-pf-body" style="padding: 0px">
		    			<div class="card-pf-utilization-details" style="border: none">
			    			<span class="card-pf-utilization-card-details-count card-pf-icon-circle" style="width: 25%; border: none !important">
				    			<img src="<?= ($e['logo']!=""?$e['logo']:"/webroot/img/logos/default.png")  ?>" style="width: 100%; object-fit: contain; filter: grayscale(100%); height: inherit">
					    	</span>
    					    <div class="card-pf-utilization-card-details-description" style="max-width: 70%">
                                <h3>
                                    <?= $e['name'] . " (" . $e['year'] .")" ?>
                                </h3>
                                <div class="card-pf-items text-center">
                                    <div class="card-pf-utilization-card-details-description" style="padding-right: 0.5em; margin-top: 0.5ex">
                                        CfP is closed
                                    </div>
                                    <?php if($e['schedule'] == true){ ?>
										<div class="card-pf-utilization-card-details-description" style="padding-left: 0.5em; padding-right: 0.5em; margin-top: 0.5ex; border-left: 1px solid #d1d1d1">
			        						<a href="/schedule/<?= $e['id'] ?>" data-toggle="tooltip" data-placement="top" title="Schedule">
				        						<span class="fa fa-table"></span> Schedule
					        				</a>
						        		</div>
									<?php } ?>
                                    <?php if($e['booking'] == true){ ?>
                                        <div class="card-pf-utilization-card-details-description" style="padding-left: 0.5em; padding-right: 0.5em; margin-top: 0.5ex; border-left: 1px solid #d1d1d1">
                                            <a href="/booking/<?= $e['id'] ?>" data-toggle="tooltip" data-placement="top" title="Hotel booking">
                                                <span class="fa fa-home"></span> Hotel booking
                                            </a>
                                        </div>
                                    <?php } ?>
            						<div class="card-pf-utilization-card-details-description" style="padding-left: 0.5em; margin-top: 0.5ex; border-left: 1px solid #d1d1d1">
	            						<a href="<?= $e['url'] ?>" data-toggle="tooltip" data-placement="top" target="_blank" title="Visit webpage">
		            						<span class="pficon pficon-connected"></span> Web
			            				</a>
				            		</div>
                                </div>
                            </div>
                        </div>
	    			</div>
		    	</div> 
	        </div> <!-- cols -->
	<?php } ?>
    </div> <!-- row -->
    <?php } ?>
</div> <!-- container -->
<script>
  $(document).ready(function() {
    // matchHeight the contents of each .card-pf and then the .card-pf itself
    $(".row-cards-pf > [class*='col'] > .card-pf .card-pf-title").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf > .card-pf-body").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf > .card-pf-footer").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf").matchHeight();

    // Initialize the vertical navigation
    $().setupVerticalNavigation(true);

	// autohide toasts
	$('.toast-pf').mouseenter(function () {
		$(this).attr('mouseenter','true');
	});

	$('.toast-pf').mouseleave(function () {
		$(this).removeAttr('mouseenter');
		if($(this).attr('remove') != null){
			$(this).remove();
		}		
	});

	function hideToasts() {
		var toasts = $('.toast-pf'), i;

		for(i = 0; i < toasts.length; i++){
			if($(toasts[i]).attr('mouseenter') != null){
				$(toasts[i]).attr('remove','true');
			} else {
				$(toasts[i]).hide('slow', function(){ $(toasts[i]).remove(); });
			}
		}
	}

	setTimeout(hideToasts, 8000);

  });
</script>
</body>
</html>
  
