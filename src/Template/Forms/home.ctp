<div class="container-fluid container-cards-pf container-pf-nav-pf-vertical">
    <ol class="breadcrumb" role="navigation" aria-label="breadcrumbs">
        <li><a href="/"><span class="fa fa-home"></span></a></li>
        <li><a href="/admin">Administration</a></li>
        <li class="active"><span>Forms</span></li>
    </ol>
    <h1>CfP forms for <?= $event['name'] . " (" . $event['year'] . ")" ?></h1>
    <a href="#" action="add" class="openModal btn btn-primary">Create new form</a>
	<?php if(count($forms) < 1) { ?>
	<div class="blank-slate-pf">
        <div class="blank-slate-pf-icon">
            <span class="pficon pficon-info"></span>
        </div>
        <h1>There is no form created yet.</h1>
        <p>Click on Create new form to create one.</p>
	</div>
	<?php }else{ ?>
    <div id="pf-list-standard" class="row row-cards-pf">
    <div id="form-container" class="row row-cards-pf">
    <?php foreach($forms as $form) { ?>
	<div class="form-group-item col-xs-12 col-sm-6 col-md-3" id="form<?= $form['id'] ?>">
             <div class="card-pf card-pf-view card-pf-aggregate-status" style="border-top: 5px solid <?= ($form['color'] != ""?$form['color']:"#000000") ?>">
                  <div class="card-pf-body">
                  <span class="pf-grab-landscape ui-sortable-handle"></span>
                        <h2 class="card-pf-title text-center" style="margin-top: 0px">CfP - <?= ($form['placeholder'] != ""?$inflector->pluralize(ucfirst($form['placeholder'])) : $inflector->pluralize(ucfirst($form['type']))) ?></h2>
                        <div class="card-pf-items text-center">
			     <div class="card-pf-item">
				<a href="#" class="openModal" action="update" type="<?= $form['type'] ?>" additional="<?= $form['speaker_limit'] ?>" multi="<?= $form['one_response'] ?>" sched="<?= $form['schedule'] ?>" cfpclose="<?= $form['cfp_close'] ?>" placehold="<?= $form['placeholder'] ?>" color="<?= $form['color'] ?>"
					data-toggle="tooltip" data-placement="top" title="Update form">
					<span class="pficon pficon-edit"></span> Edit
				</a>
			     </div>
                             <div class="card-pf-item">
                                  <a href="/form/edit/<?= $form['id'] ?>" data-toggle="tooltip" data-placement="top" title="Manage content">
                                      <span class="fa fa-list"></span> Content
                                  </a>
                             </div>
                             <div class="card-pf-item">
                                  <a class="openModal" action="delete" href="#" data-toggle="tooltip" data-placement="top" event-id="/form/delete/<?= $form['id'] . "/" . $token ?>" title="Delete form">
                                      <span class="pficon pficon-error-circle-o"></span> Delete
                                  </a>
                             </div>
                        </div>
                   </div>
             </div>
         </div> <!-- cols -->
	 <?php } ?>
    </div>
		<div class="modal fade" id="confirmations" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
       						<button type="button" class="close" data-dismiss="modal" aria-hidden="true" aria-label="Close">
						        <span class="pficon pficon-close"></span>
					        </button>
		        			<h4 class="modal-title" id="myModalLabel">Confirm form deletion</h4>
					</div> <!-- modal header -->
					<div class="modal-body">
						<p>Do you really want to delete this form? Keep in mind, that this action will <strong>remove all proposals assigned to this form</strong> and can't be undone.</p>
					</div>
				 	<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					        <a href="" id="modalDelete" class="btn btn-danger">Delete</a>
					</div> <!-- modal footer -->
				</div> <!-- modal content -->
			</div> <!-- modal dialog -->
		</div> <!-- modal -->
        <?php } ?>

	<div class="modal fade" id="selection" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true" aria-label="Close">
						<span class="pficon pficon-close"></span>
					</button>
					<h4 class="modal-title" id="myModalLabel">Create new form</h4>
				</div> <!-- modal header -->
				<div class="modal-body">
					<a href="/form/add/workshop/<?= $event["id"] . "/" . $token ?>" class="btn btn-primary">Call for Workshops</a>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					<button type="button" id="save" class="btn btn-primary">Save</a>
				</div> <!-- modal footer -->
			</div> <!-- modal content -->
		</div> <!-- modal dialog -->
	</div> <!-- modal -->
    </div> <!-- group list -->
    <div id="modalContent" class="hidden">
	<div class="form-group">
        	<label class="control-label" for="type">Select type of form you would like to create</label>
                	<div>
                        	<label class="radio-inline">
                                	<input type="radio" name="type" class="fromType" value="session"> Call for Sessions
                                </label>
                                <label class="radio-inline">
                                        <input type="radio" name="type" class="fromType" value="booth"> Call for Booths
                                </label>
                                <label class="radio-inline">
                                        <input type="radio" name="type" class="fromType" value="meetup"> Call for Meetups
                                </label>
                                <label class="radio-inline">
                                        <input type="radio" name="type" class="fromType" value="workshop"> Call for Workshops
                                </label>
                    </div>
                    <div>
                                <label class="radio-inline">
                                        <input type="radio" name="type" class="fromType" value="contest"> Call for Contests
                                </label>
                                <label class="radio-inline">
                                        <input type="radio" name="type" class="fromType" value="lightning talk"> Call for Lightning Talks
                                </label>
                                <label class="radio-inline">
                                        <input type="radio" name="type" class="fromType" value="activity"> Call for Activities
                                </label>
                    </div>
                    <div>
								<label class="radio-inline">
				                	<input type="radio" name="type" class="formType" value="confirmation"> Confirmation when accepted
                				</label>
                                <label class="radio-inline">
                                    <input type="radio" name="type" class="formType" value="collect"> Collect additional data when confirmed
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="type" class="formType" value="quiz"> Quiz form
                                </label>
                        </div>
                <?php if ($event['accommodation']){ ?>
                	<div>
								<label class="radio-inline">
                                    <input type="radio" name="type" class="formType" value="accommodation"> Booking accommodation (paid by attendee)
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="type" class="formType" value="covered accommodation"> Booking accommodation (covered by event)
                                </label>
                        </div>
                <?php } ?>
         </div>
         <div class="form-group">
                 <label class="checkbox-inline">
                        <input type="checkbox" name="multiple" value="checkbox"> Allow submit multiple proposals?
                 </label>
         </div>
        <?php if ($event['schedule']){ ?>
         <div class="form-group">
                 <label class="checkbox-inline">
                        <input type="checkbox" name="schedule" value="checkbox"> Should answers from this form be part of final schedule?
                 </label>
         </div>
        <?php } ?>
         <div class="form-group">
                 <label class="control-label" for="size">Additional speaker's limit</label>
                 <input type="number" name="additionals" min="-1" value="-1" class="form-control">
                 <span class="help-block">Set maximum number of allowed additionals speakers (-1 = without limit)</span>
         </div>
         <div class="form-group">
                <label class="control-label" for="cfpclose">Set custom closing date for this form</label>
                <input type="text" name="cfpclose" class="form-control bootstrap-datepicker">
                <span class="help-block">Keep empty to use default deadline set for event.</span>
         </div>
         <div class="form-group">
                <label class="control-label" for="placehold">Type custom name for this form</label>
                <input type="text" name="placehold" class="form-control">
                <span class="help-block">Use name in singular form. Keep empty to use default name for the form.</span>
         </div>
         <?php if ($event['schedule']){ ?>
    	 <div class="form-group">
                <label class="control-label" for="color">Set color for submission in schedule</label>
                <input type="color" class="form-control" id="color" name="color">
                <span class="help-block">This color will not be used for Session form type and has effect only when is allowed to have answers from this form as part of final schedule.</span>
         </div>
         <?php } ?>
    </div>

</div> <!-- container -->
<script>
  $(document).ready(function() {
    // matchHeight the contents of each .card-pf and then the .card-pf itself
    $(".row-cards-pf > [class*='col'] > .card-pf .card-pf-title").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf > .card-pf-body").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf > .card-pf-footer").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf").matchHeight();
    
    $( "#form-container" ).sortable({
	    handle: ".pf-grab-landscape",
	    stop: function (event, ui) {
		    var order = [];
		    $.each($("#form-container .form-group-item"), function(i,form){
			    order.push(parseInt(form.getAttribute('id').replace("form","")));
		    });
		    console.log(order);
		    updateOrder(order);
	      }
    });

    var eventId = <?= $event["id"] ?>, token = '<?= $token ?>';

    // Initialize the vertical navigation
    $().setupVerticalNavigation(true);

    $(".openModal").click(function (){
		if($(this).attr("action") == "delete"){
			$("#modalDelete").attr("href", $(this).attr("event-id"));
			$("#confirmations").modal('show');
		} else {
			$("#selection .modal-body").html($("#modalContent").html());
	        var $endDate = $("#selection input[name=cfpclose]");

        	$endDate.datepicker({
		        format: 'yyyy-mm-dd',
        		todayBtn: true,
		        todayHighlight: true
        	});
			if($(this).attr("action") == "add"){
				$("#selection #myModalLabel").text("Create new form");
				$("#selection").attr("action", "add");
				$("#save").addClass("disabled");
			}else{
				$("#selection #myModalLabel").text("Update " + $(this).attr("type") + " form");
				$("#selection input[name=additionals]").val($(this).attr('additional'));
				$("#selection input[name=cfpclose]").val($(this).attr('cfpclose'));
				$("#selection input[name=placehold]").val($(this).attr('placehold'));
				$("#selection input[name=color]").val($(this).attr('color'));
				$("#selection").attr("action", "update");
				$("#selection").attr("type", $(this).attr("type"));
				if($(this).attr("multi") == 1){			
					$("#selection input[name=multiple]").prop("checked", true);
				}
                if($(this).attr("sched") == 1){         
                    $("#selection input[name=schedule]").prop("checked", true);
                }
				$("#selection .modal-body .form-group:first").remove();
			}	
			$("#selection").modal('show');
			init();
		}
    });

    function init(){
	$("#selection input[name=type]").change(function(){
		$("#save").removeClass('disabled');
	});
    }

    $("#save").click(function(){
	    var multiple = $("#selection input[name=multiple]").is(":checked");
	    var schedule = <?php if ($event['schedule']){ ?> $("#selection input[name=schedule]").is(":checked");<?php } else { ?> false; <?php } ?>
	    var additionals = $("#selection input[name=additionals]").val();
	    var action = $("#selection").attr("action");
	    var type = $("#selection input[name=type]:checked").val();
        var close = $("#selection input[name=cfpclose]").val();
        var placehold = $("#selection input[name=placehold]").val();
        var color = <?php if ($event['schedule']){ ?> $("#selection input[name=color]").val(); <?php } else { ?> "#FFFFFF"; <?php } ?>

	    var xhr = new XMLHttpRequest();

	    if(action == "update"){
		    type = $("#selection").attr("type");	
	    }

	    xhr.open('POST', '/form/' + action);
	    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	    xhr.onload = function() {
		    try{
			    var data = JSON.parse(xhr.responseText);
		    }catch (e) {
			    showToast(xhr.responseText, "error");
			    return;
		    }
		    if(data.result != "success"){
			    showToast(data.reason, "error");
		    }else{
			    if(data.action == "add"){
				    window.location = data.href;
			    } else {
				    showToast("Form has been successfully updated.", "success");
				    $("#selection").modal('hide');
				    $(".openModal[type="+type+"]").attr("additional", additionals);
				    $(".openModal[type="+type+"]").attr("multi",(multiple?"1":"0"));
				    $(".openModal[type="+type+"]").attr("sched",(schedule?"1":"0"));
				    $(".openModal[type="+type+"]").attr("cfpclose", close);
			    }
		    }
	    };
        xhr.send(jQuery.param({'_Token': token, '_method': "post", 'token': token, 'eventID': eventId, 'multiple': multiple, 'color': color, 'schedule': schedule, 'additionals': additionals, 'close': close, 'placeholder': placehold, 'type': type}));
    });

    function showToast(message, type){
	    var toast = $(".toast-notifications-list-pf").html()+
		    '<div class="toast-pf alert alert-' + (type == "error"?"danger":"success") + ' alert-dismissable">'+
		    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">'+
		    '<span class="pficon pficon-close"></span>'+
		    '</button>'+
		    '<span class="pficon ' + (type == "error"?'pficon-error-circle-o':'pficon-ok') + '"></span>'+
		    message+
		    '</div>';
	    $(".toast-notifications-list-pf").html(toast);
    }

    // autohide toasts
    $('.toast-pf').mouseenter(function () {
 	$(this).attr('mouseenter','true');
    });

    $('.toast-pf').mouseleave(function () {
	$(this).removeAttr('mouseenter');
	if($(this).attr('remove') != null){
		$(this).remove();
	}		
    });

    function hideToasts() {
	var toasts = $('.toast-pf'), i;

	for(i = 0; i < toasts.length; i++){
		if($(toasts[i]).attr('mouseenter') != null){
			$(toasts[i]).attr('remove','true');
		} else {
			$(toasts[i]).hide('slow', function(){ $(toasts[i]).remove(); });
		}
	}
    }
    
    function updateOrder(order){
	    
	    var xhr = new XMLHttpRequest();
        xhr.open('POST', '/form/order/');
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.onload = function() {
            try{
                var data = JSON.parse(xhr.responseText);
            } catch (e) {
                showToast(xhr.responseText);
                return;
            }
            if(data.result != 'success'){
                showToast(data.reason, "error");
            }else{
    			showToast("Form has been successfully updated.", "success");	
    			setTimeout(hideToasts, 8000);			    
	    	}
        };
        xhr.send(jQuery.param({'_Token': token, '_method': "post", 'token': token, 'eventID': eventId, 'order': order}));
    
    }

    setTimeout(hideToasts, 8000);

  });
</script>
</body>
</html>  
