<div class="container-fluid container-cards-pf container-pf-nav-pf-vertical">
    <?php if(count($talk) < 1) { ?>
        <ol class="breadcrumb" role="navigation" aria-label="breadcrumbs">
            <li><a href="/"><span class="fa fa-home"></span></a></li>
        	<li><a href="/myproposals">My proposals</a></li>
            <li class="active"><span>Error 404</span></li>
        </ol>
         <div class="blank-slate-pf">
	 	<div class="blank-slate-pf-icon">
	            <span class="pficon pficon-info"></span>
        	</div>
	        <h1>Requested response doesn't exist.</h1>
        	<p>Your link is probably broken.</p>

   <?php }else{ ?>
    <ol class="breadcrumb" role="navigation" aria-label="breadcrumbs">
        <li><a href="/"><span class="fa fa-home"></span></a></li>
    	<li><a href="/myproposals">My proposals</a></li>
        <li class="active"><span><?= $talk['title'] ?></span></li>
    </ol>
    <div class="row row-cards-pf">
	<div class="col-xs-12">
	    <div class="card-pf card-pf-view">
		<h1><?= $talk['title'] ?></h1>
		<div class="tab-content">
			<ul class="nav nav-tabs" role="tablist">
				<li id="tabActive" class="active" role="presentation">
					<a href="#proposal" aria-controls="proposal" role="tab" data-toggle="tab">Proposal</a>
				</li>
				<li role="presentation">
					<a href="#comments" aria-controls="comments" role="tab" data-toggle="tab">Comments <span id="comments-count" class="badge"><?= count($talk['comments']) ?></span></a>
				</li>
			</ul>
			<div id="proposal" class="tab-pane active">
	                  	<div class="card-pf-body">
                    <?php if($permission) { ?>
					<div class="form-group">
						<label class="control-label" for="speaker">Speaker</label>
						<p name="speaker"><?= $talk['speaker'] ?></p>
				</div>
                    <?php } ?>
				<?php	foreach ($questions as $i=>$q){ 
						if($q['take_as'] == "title"){
							continue;
						}
					?>
					<div class="form-group">
				<?php
					switch ($q['type']){
						case "text": ?>
							<label class="control-label" for="<?= $q['take_as'] ?>"><?= $q['question'] ?></label>
							<p name="<?= $q['take_as'] ?>"><?php if($values != NULL && $values[$i] != NULL){ 
									echo  nl2br($values[$i]);
								}?>
								</p><?php
							break;

						case "textarea": ?>
							<label class="control-label" for="<?= $q['take_as'] ?>"><?= $q['question'] ?></label>
							<p name="<?= $q['take_as'] ?>">
							<?php if($values != NULL && $values[$i] != NULL) {
								echo nl2br($values[$i]);
							} ?></p>
							<?php 
							break;
						case "radio":
						case "checkbox":?>
							<label class="control-label" for="<?= $q['take_as'] ?>"><?= $q['question'] ?></label><br>
							<p name="<?= $q['take_as'] ?>"><?= $values[$i] ?></p>
							<?php 
							break;
						default:
							break;
					}?>
					</div>
			<?php } ?>
        	     		</div>
			</div>
			<div id="comments" class="tab-pane">
			<?php if($permission){ ?>
				<div class="form-group">
					<label class="control-label">Add comment</label>
					<textarea id="comment-input" class="form-control"></textarea>
				</div>
				<div class="form-group" align="right">
					<a href="#" class="btn btn-primary" id="add-comment">Add</a>
				</div>
			<?php } ?>
				<div id="comments-list">
					<?php foreach($talk['comments'] as $c){ 
						if($c['public'] != 1){?>
						<div class="comment alert alert-danger">
							<span class="pficon pficon-private" title="Private comment"></span>
							<h4><?= $c['author'] ?></h4>
							<p><i><small><?=  date("D M j Y H:i:s", intval(substr($c['time'], 0, -3))) ?></small></i><br>
							<?= $c['comment'] ?></p>
						</div>
					<?php }else{ ?>
						<div class="comment">
							<h4><?= $c['author'] ?></h4>
							<p><i><small><?=  date("D M j Y H:i:s", intval(substr($c['time'], 0, -3))) ?></small></i><br>
							<?= $c['comment'] ?></p>
						</div>
					<?php 	}
						} ?>
				</div>			
			</div>
	        </div>
	    </div>
        </div>
	<?php } ?>
    </div>
</div> <!-- container -->
<script>

  $(document).ready(function() {
    // matchHeight the contents of each .card-pf and then the .card-pf itself
    $(".row-cards-pf > [class*='col'] > .card-pf .card-pf-title").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf > .card-pf-body").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf > .card-pf-footer").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf").matchHeight();

    // Initialize the vertical navigation
    $().setupVerticalNavigation(true);

    // autohide toasts
    $('.toast-pf').mouseenter(function () {
	$(this).attr('mouseenter','true');
    });

    $('.toast-pf').mouseleave(function () {
	$(this).removeAttr('mouseenter');
	if($(this).attr('remove') != null){
		$(this).remove();
	}		
    });

    function hideToasts() {
	var toasts = $('.toast-pf'), i;

	for(i = 0; i < toasts.length; i++){
		if($(toasts[i]).attr('mouseenter') != null){
			$(toasts[i]).attr('remove','true');
		} else {
			$(toasts[i]).hide('slow', function(){ $(toasts[i]).remove(); });
		}
	}
    }

    setTimeout(hideToasts, 8000);

  function showToast(message){
        var toast = $(".toast-notifications-list-pf").html()+
                    '<div class="toast-pf alert alert-danger alert-dismissable">'+
                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">'+
                    '<span class="pficon pficon-close"></span>'+
                    '</button>'+
                    '<span class="pficon pficon-error-circle-o"></span>'+
                    message+
                    '</div>';
	$(".toast-notifications-list-pf").html(toast);        
  }

  <?php if($permission){?>

    var token = "<?= $token ?>", user = "<?= $username ?>";
    $("#add-comment").click(function (){
        var response = $("#modalTitle").attr("response");

        var from = user + " (<?= $userType ?>)";
        var what = $("#comment-input").val();
        from = encodeURIComponent(from);
        if(what == ""){
                showToast("You have to add some text first.");
                return;
        }
        what = encodeURIComponent(what);
        var when = new Date().getTime();

        var xhr = new XMLHttpRequest();
        xhr.open('POST', '/comment/');
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.onload = function() {
            try{
                var data = JSON.parse(xhr.responseText);
            } catch (e) {
                showToast(xhr.responseText);
                return;
            }
            if(data.result != 'success'){
                showToast(data.response);
            }else{
                addComment(decodeURIComponent(from), when, decodeURIComponent(what));
            }
        };

	<?php if($userType == "speaker"){ ?>
	        xhr.send(jQuery.param({'token': token, 'response': <?= $talk['response_id'] ?>, 'from': from, 'what': what , 'when': when, "public": 1}));
	<?php } else { ?>
		xhr.send(jQuery.param({'token': token, 'response': <?= $talk['response_id'] ?>, 'from': from, 'what': what , 'when': when}));
	<?php } ?>

        $("#comment-input").val("");
    });

    function addComment(from, when, what){
        var content = $("#comments-list").html();
        $("#comments-list").html("<div><h4>" + from + "</h4><p><i><small>" + new Date(when).toString() + "</small></i><br>" + what + "</p></div>" + content);
        var count = parseInt($("#comments-count").html());
	$("#comments-count").html(count+1);
    }

  <?php } ?>

 });
</script>
</body>
</html>  
