<div class="container-fluid container-cards-pf container-pf-nav-pf-vertical">
    <ol class="breadcrumb" role="navigation" aria-label="breadcrumbs">
        <li><a href="/"><span class="fa fa-home"></span></a></li>
    	<li><a href="/manager">PM Review</a></li>
        <li class="active"><span><?= $eventInfo['name'] . " (" . $eventInfo['year'] . ")" ?> - Sessions</span></li>
    </ol>
	<h1>List of proposals for <?= $eventInfo['name'] . " (" . $eventInfo['year'] . ")" ?></h1>
	<?php if(count($all) < 1) { ?>
	<div class="blank-slate-pf">
        <div class="blank-slate-pf-icon">
            <span class="pficon pficon-info"></span>
        </div>
        <h1>No responses are available.</h1>
        <p>You have to wait for them. Meanwhile you can help with promotion of this event.</p>

	<?php }else{ ?>
		<div class="modal fade" id="responseDetail" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
       						<button type="button" class="close" data-dismiss="modal" aria-hidden="true" aria-label="Close">
						        <span class="pficon pficon-close"></span>
					        </button>
		        			<h4 class="modal-title" id="modalTitle">Response</h4>
					</div> <!-- modal header -->
					<div class="modal-body">
						<div class="tab-content">
							<ul class="nav nav-tabs" role="tablist">
								<li role="presentation" id="tabActive" class="active"><a href="#modalBody" aria-controls="modalBody" role="tab" data-toggle="tab">Proposal</a></li>
							</ul>
						</div>
						<div class="tab-pane active" id="modalBody"></div>
					</div>
					 	<div class="modal-footer">
							<a id="prev" class="btn btn-default">Prev</a>
							<a id="skip" class="btn btn-default">Next</a>
						</div> <!-- modal footer -->
 					</div> <!-- modal content -->
				</div> <!-- modal dialog -->
			</div> <!-- modal -->
                	<table class="table table-striped table-bordered table-hover" id="table" style="background: white; height: auto !important">
        	    		<thead>
        	    			<tr>
						<th>#</th>
        	    				<th>Title</th>
        	    				<th>Type</th>
        	    				<th>Topic</th>
        	    				<?php if($admin || $current_program_manager){ ?><th>Speaker</th><?php } ?>
        	    				<th>Duration</th>
						<?php if($admin || $current_program_manager){ ?><th>Edit</th><th>Co-Speakers</th><th>Delete</th><?php } ?>
        	    			</tr>
        	    		</thead>
                		<tbody>
		        		<?php foreach($all as $r) { ?>	
		        			<tr style="cursor: pointer" response_id="<?= $r['id'] ?>">
							<td><?= $r['id'] ?></td>
							<td><?= $r['title'] ?></td>
							<td><?= $r['type'] ?></td>
							<td topic="<?= $r['id'] ?>"><?= $r['topic'] ?></td>
							<?php if($admin || $current_program_manager){ ?><td><?= $r['speaker'] ?></td><?php } ?>
							<td><?= $r['duration'] ?></td>
							<?php if($admin || $current_program_manager){ ?><td><a target="_blank" href="/proposal/<?= $r['form_id'] ?>/<?= $r['id'] ?>/-1" class="btn btn-default">Edit</a></td>
							<td><a target="_blank" href="/additional/<?= $r['form_id'] ?>/<?= $r['id'] ?>/-1" class="btn btn-default">Manage</a></td>
							<td><a class="btn btn-danger openModal" action="delete" href="#" data-toggle="tooltip" data-placement="top" session-id="/proposals/<?= $eventInfo['id'] ?>/delete/<?= $r['form_id'] . "/" . $r['id'] . "/" . $token ?>" title="Remove proposal">
								  Delete proposal
							  </a></td><?php } ?>
						</tr>
		        		<?php } ?>
	            		</tbody>
	        		</table>	      
				</div>
        </div>
	<?php } ?>
	<div class="modal fade" id="confirmations" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
   						<button type="button" class="close" data-dismiss="modal" aria-hidden="true" aria-label="Close">
					        <span class="pficon pficon-close"></span>
				        </button>
	        			<h4 class="modal-title" id="myModalLabel">Confirm session deletion</h4>
				</div> <!-- modal header -->
				<div class="modal-body">
					<p>Do you really want to delete this session? Keep in mind, that this action will <strong>remove all associated records</strong> (additional speakers, comments, confirmations and votes) and can't be undone.</p>
				</div>
			 	<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				        <a href="" id="modalDelete" class="btn btn-danger">Delete</a>
				</div> <!-- modal footer -->
			</div> <!-- modal content -->
		</div> <!-- modal dialog -->
	</div> <!-- modal -->  
    </div> <!-- tab content -->
</div> <!-- container -->
<script>
  $(document).ready(function() {
    // matchHeight the contents of each .card-pf and then the .card-pf itself
    $(".row-cards-pf > [class*='col'] > .card-pf .card-pf-title").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf > .card-pf-body").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf > .card-pf-footer").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf").matchHeight();
    
    var modalState = false;
    var database = {
		<?php if(count($all) > 0){
				 foreach($all as $val){ ?>
	    
		    "<?= $val['id'] ?>": {
			    <?php foreach($val as $key=>$v){
						if($key == "answers"){
							foreach($v as $answ){?>
							<?= json_encode(preg_replace('/\s+/', ' ', nl2br($answ['question'], false)))?>: <?= json_encode(preg_replace('/\s+/', ' ', nl2br($answ['text'], false))) ?>,
						<?php }
						      } else if($key != "form_id" && $key != "comments"){
							if(!$admin && !$current_program_manager && $key == "speaker"){ 
								continue;
							} else {?>
					    "<?= $key ?>": <?= json_encode(preg_replace('/\s+/', ' ', nl2br($v, false))) ?>,
				    <?php 	  } }
                }?>
		    },
	    <?php } } ?>
	};
    // Initialize the vertical navigation
    $().setupVerticalNavigation(true);
    
    $(".openModal").click(function (e){
        e.stopPropagation();
    	$("#modalDelete").attr("href", $(this).attr("session-id"));
    	$("#confirmations").modal('show');
    });

	$('#responseDetail').on('hidden.bs.modal', function (e) {
	  modalState = false;
	});

	function init(){
		$("tbody tr").off('click');
		$("tbody tr").on('click',function (){
			var response = $(this).attr("response_id");
			fillModal(response);
			modalState = true;
			$("#responseDetail").modal('show');
		});
	}

    function showToast(message){
        var toast = $(".toast-notifications-list-pf").html()+
                    '<div class="toast-pf alert alert-danger alert-dismissable">'+
                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">'+
                    '<span class="pficon pficon-close"></span>'+
                    '</button>'+
                    '<span class="pficon pficon-error-circle-o"></span>'+
                    message+
                    '</div>';
        $(".toast-notifications-list-pf").html(toast);        
    }

    function nextKey() {
        var response = $("#modalTitle").attr("response");
	    var key = $("#table tr[response_id=" + response +"]").next().attr("response_id");
	    if($.isNumeric(key)){
		    return key;
	    }
        return null;
    }

    function prevKey() {
        var response = $("#modalTitle").attr("response");
	    var key = $("#table tr[response_id=" + response +"]").prev().attr("response_id");
	    if($.isNumeric(key)){
		    return key;
	    }
        return null;
    }

    function fillModal(keys){
	    if(keys != null){
           	var record = database[keys];
		    $("#modalTitle").html(record.title);
    		var html = "";
	    	$.each(record, function (key, val){
	       		if(key != "topic_id" && key != "id" && key != "answer_id" && key != "title" && key != "vote"){
	    			html += "<p><label>" + key + "</label><br>"+ val+"</p>";
	    		}
	    	});
	    	$("#modalTitle").attr("response", keys);
		    $(".modal .active").removeClass("active");
		    $("#tabActive").addClass("active");
		    $("#modalBody").addClass("active");
	    	$("#modalBody").html(html);
        } else {
	        $("#responseDetail").modal('hide');
        }
    }
    $("#skip").click(function (){
        var nextKeys = nextKey();
	    fillModal(nextKeys);
    });

    $("#prev").click(function (){
	    var prevKeys = prevKey();
	    fillModal(prevKeys);
    });

    $(window).keydown(function (event){
	    if(modalState){
		    if(event.keyCode == 39){ // ArrowRight
			    var nextKeys = nextKey();
			    fillModal(nextKeys);
		    }

		    if(event.keyCode == 37){ // ArrowLeft
			    var prevKeys = prevKey();
			    fillModal(prevKeys);
		    }
	    }
    });

    init(); 
    $(".table").DataTable({paging:false});
  });
</script>
</body>
</html>  
