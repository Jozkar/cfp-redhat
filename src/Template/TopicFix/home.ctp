<div class="container-fluid container-cards-pf container-pf-nav-pf-vertical">
    <ol class="breadcrumb" role="navigation" aria-label="breadcrumbs">
        <li><a href="/"><span class="fa fa-home"></span></a></li>
	<li><a href="/manager">PM Review</a></li>
        <li class="active"><span><?= $eventInfo['name'] . " (" . $eventInfo['year'] . ")"?> - Topic assignment</span></li>
    </ol>
	<h1>Fix topics for proposals for <?= $eventInfo['name'] . " (" . $eventInfo['year'] . ")" ?></h1>
	<?php if(count($responses) < 1) { ?>
	<div class="blank-slate-pf">
        <div class="blank-slate-pf-icon">
            <span class="pficon pficon-info"></span>
        </div>
        <h1>No responses are available.</h1>
        <p>There aren't any proposals available for fixing. All of them have just one topic assigned or none of proposal is available for event in database.</p>

	<?php }else{ ?>
			<div class="modal fade" id="otherResponseDetail" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
        						<button type="button" class="close" data-dismiss="modal" aria-hidden="true" aria-label="Close">
							        <span class="pficon pficon-close"></span>
						        </button>
			        			<h4 class="modal-title">Response</h4>
						</div> <!-- modal header -->
						<div class="modal-body">
							<div class="h2 text-center" id="otherModalTitle"></div>
							<div class="tab-content">
								<ul class="nav nav-tabs" role="tablist">
									<li role="presentation" id="otherTabActive" class="active"><a href="#otherModalBody" aria-controls="otherModalBody" role="tab" data-toggle="tab">Proposal</a></li>
									<li role="presentation"><a href="#otherComments" aria-controls="otherComments" role="tab" data-toggle="tab">Comments <span class="badge" id="other-comments-count"></span></a></li>
								</ul>
								<div class="tab-pane active" id="otherModalBody"></div>
								<div class="tab-pane" id="otherComments">
									<div class="form-group">
										<label class="control-label">Add comment</label>
										<textarea id="other-comment-input" class="form-control"></textarea>
									</div>
									<div class="form-group">
										<a href="#" class="btn btn-primary add-other-comment" style="float: right" pub='0'>Add</a>
										<a href="#" class="btn btn-danger add-other-comment" pub='1'>Send comment to speaker</a>
									</div>
									<div id="other-comments-list"></div>
								</div>
							</div>
						</div>
					 	<div class="modal-footer">
							<div class="form-group">
								<?php foreach($topics as $t) { ?>
									<label class="checkbox-inline"><input name='topic[]' value='<?= $t['id'] ?>' type='checkbox'><?= $t['name'] ?></label>
								<?php } ?>
							</div>
							<div class="form-group">
								<a id="addTopic" class="btn btn-danger">Set to selected topics</a>	
							        <a id="next" class="btn btn-default">Next</a>
							</div>
						</div> <!-- modal footer -->
 					</div> <!-- modal content -->
				</div> <!-- modal dialog -->
			</div> <!-- modal -->
			<div class="alert alert-warning">
				<span class="pficon pficon-warning-triangle-o"></span>
				You can select only one topic to each proposal. Other topics, that had been assigned to the proposal, will be from proposal permanently erased and it's votes as well. Act responsibly.
			</div>
                	<table class="table table-striped table-bordered table-hover" id="table-others" style="background: white; height: auto !important">
        	    		<thead>
        	    			<tr>
						<th>#</th>
        	    				<th>Title</th>
        	    				<th>Type</th>
        	    				<th>Topic</th>
        	    				<th>Speaker</th>
        	    				<th>Duration</th>
        	    				<th>No. Comments</th>
						<th>External link</th>
						<th>Edit</th>
						<th>Addit Speakers</th>
        	    			</tr>
        	    		</thead>
                		<tbody>
		        		<?php foreach($responses as $r) { ?>	
		        			<tr style="cursor: pointer" from="nother" other_response_id="<?= $r['id'] ?>">
							<td><?= $r['id'] ?></td>
							<td><?= $r['title'] ?></td>
							<td><?= $r['type'] ?></td>
							<td other_topic="<?= $r['id'] ?>"><?= $r['topic'] ?></td>
							<td><?= $r['speaker'] ?></td>
							<td><?= $r['duration'] ?></td>
							<td><?= count($r['comments']) ?></td>
							<td><a href="/proposals/<?= $event . "/". $r['id'] ?>" target="_blank" class="btn btn-default">Open in new panel</a></td>
							<td><a target="_blank" href="/proposal/<?= $r['form_id'] ?>/<?= $r['id'] ?>/-1" class="btn btn-default">Edit</a></td>
							<td><a target="_blank" href="/additional/<?= $r['form_id'] ?>/<?= $r['id'] ?>/-1" class="btn btn-default">Manage</a></td>
						</tr>
		        		<?php } ?>
	            		</tbody>
	        		</table>	      
	<?php } ?>
</div> <!-- container -->
<script>
  $(document).ready(function() {
    // matchHeight the contents of each .card-pf and then the .card-pf itself
    $(".row-cards-pf > [class*='col'] > .card-pf .card-pf-title").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf > .card-pf-body").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf > .card-pf-footer").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf").matchHeight();
    
    var event_id = "<?= $event ?>", token = "<?= $token ?>", reviewer = "<?= $reviewer ?>", fullReviewer = "<?= $username ?>";
    var allTopics = { <?php foreach($topics as $t) { ?>
			<?= '"' . $t['id'] . '":"' . $t['name'] . '",'  ?>
				<?php } ?>
					};

    var database = {
		<?php if(count($responses) > 0){
				 foreach($responses as $val){ ?>
	    
		    "<?= $val['id'] ?>": {
			    <?php foreach($val as $key=>$v){
					if($key != "score" && $key != "answer_id" && $key != "reviews" && $key != "form_id") {
					   if($key == "comments"){ ?>
							"<?= $key ?>": [
							<?php foreach($v as $cmt){ ?>
										{"author": "<?= $cmt['author'] ?>",
										 "time": <?= $cmt['time'] ?>,
										 "public": <?= $cmt['public'] ?>,
										 "text": <?= json_encode(preg_replace('/\s+/', ' ', nl2br($cmt['comment'], false))) ?> },
							<?php } ?>
							],
						<?php } else if($key == "answers"){
							foreach($v as $answ){?>
							<?= json_encode(preg_replace('/\s+/', ' ', nl2br($answ['question'], false)))?>: <?= json_encode(preg_replace('/\s+/', ' ', nl2br($answ['text'], false))) ?>,
						<?php }
						      } else { ?>
					    "<?= $key ?>": <?= json_encode(preg_replace('/\s+/', ' ', nl2br($v, false))) ?>,
				    <?php 	  } 
					}
                }?>
		    },
	    <?php } } ?>
    };

    // Initialize the vertical navigation
    $().setupVerticalNavigation(true);

    function init(){
	$("tbody").off('click', 'tr');
	$("tbody").on('click', "tr",function (){
		var other_response = $(this).attr("other_response_id");
		fillOtherModal(other_response);
		$("#otherResponseDetail").modal('show');
	});
    }

    function showErrorToast(message){
        var toast = $(".toast-notifications-list-pf").html()+
                    '<div class="toast-pf alert alert-danger alert-dismissable">'+
                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">'+
                    '<span class="pficon pficon-close"></span>'+
                    '</button>'+
                    '<span class="pficon pficon-error-circle-o"></span>'+
                    message+
                    '</div>';
         $(".toast-notifications-list-pf").html(toast);        
    }

    function showSuccessToast(message){
        var toast = $(".toast-notifications-list-pf").html()+
                    '<div class="toast-pf alert alert-success alert-dismissable">'+
                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">'+
                    '<span class="pficon pficon-close"></span>'+
                    '</button>'+
                    '<span class="pficon pficon-ok"></span>'+
                    message+
                    '</div>';
         $(".toast-notifications-list-pf").html(toast);        
    }

    function nextOther() {
        var response = $("#otherModalTitle").attr("response");

	var key = $("#table-others tr[other_response_id=" + response +"]").next().attr("other_response_id");
        if($.isNumeric(key)){
                return key;
        }
        return null;
    }

    function sendTopics(response, toAdd) {
	    var xhr = new XMLHttpRequest();
	    xhr.open('POST', '/vote/settopic/');
	    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	    xhr.onload = function() {
	        try{
			var data = JSON.parse(xhr.responseText);
	        } catch (e) {
			$("#otherResponseDetail").modal('hide');
			showErrorToast(xhr.responseText);
	        }
	        if(data.result != 'success'){
	      		$("#otherResponseDetail").modal('hide');
	        	showErrorToast(data.reason);
	        } else {
			database[response].topic_id = toAdd;
			database[response].topic = allTopics[toAdd];
			$("td[other_topic=" + response + "]").html(database[response].topic);
		}
  	     };
  	     xhr.send(jQuery.param({'token': token, 'response': response, 'event': event_id, 'topic': toAdd}));
    }

    $("#next").click(function (){
        var nextKeys = nextOther();

        fillOtherModal(nextKeys);
    });

    $("#addTopic").click(function (){
        var nextKeys = nextOther();
        var response = $("#otherModalTitle").attr("response");
	var checkboxes = $("input[type=checkbox]:checked");
	
	if(checkboxes.length > 1) {
		showErrorToast("You have to specify just one topic.");
	} else {
		toAdd = $(checkboxes).attr("value");
		if(toAdd != undefined){
			if(fillOtherModal(nextKeys)){
				sendTopics(response, toAdd);
			}
		} else {
			showErrorToast("You didn't specified any topic to add.");
		}
	}
        
    });

 function fillOtherModal(nextKeys){
        if($("#other-comment-input").val() != ""){
            alert("You haven't submit your comment for this proposal. Please, submit your comment or remove text from comment input field.");
            return false;
        } else {
		if(nextKeys != null){
			$('input[type=checkbox]').removeAttr('checked');
			$('input[type=checkbox]').removeAttr('disabled');
			$('input[type=checkbox]').prop('checked', false);

	        	var record = database[nextKeys];
    			$("#otherModalTitle").html("<b>" + record.title + "</b>");
    			var html = "", comments = "";
		    	$.each(record, function (key, val){
		       		if(key != "topic_id" && key != "comments" && key != "title" && key != "vote"){
	    				html += "<p><label>" + key + "</label><br>"+ val+"</p>";
	    			}
				if(key == "topic_id") {
					var tid = val.split(",");
					$.each(tid, function( tkey, tval ){
						$("input[type=checkbox][value="+tval+"]").prop('checked', true);
						$("input[type=checkbox][value="+tval+"]").attr("checked", "checked");
					});
				}
	    		});
			$.each(record.comments, function (key, val){
				if(val.public != 1){
					comments += "<div class='comment alert alert-danger'><span class='pficon pficon-private' title='Private comment'></span><h4>" + 
						val.author +"</h4><p><i><small>"+new Date(val.time).toString()+"</small></i><br>"+val.text+"</p></div>";
				}else{
					comments += "<div class='comment'><h4>" + val.author +"</h4><p><i><small>"+new Date(val.time).toString()+"</small></i><br>"+val.text+"</p></div>";
				}
			});
		    	$("#otherModalTitle").attr("response", nextKeys);
			$(".modal .active").removeClass("active");
			$("#otherTabActive").addClass("active");
			$("#otherModalBody").addClass("active");
		    	$("#otherModalBody").html(html);
	    		$("#other-comments-list").html(comments);
			$("#other-comments-count").html(record.comments.length);
			$("#otherResponseDetail").animate({scrollTop: 0}, 100);
		
        } else {
        	$("#otherResponseDetail").modal('hide');
        }
	return true;
	}
    }

    function addOtherComment(from, when, what, record, pub){
	var content = $("#other-comments-list").html();
	var ncom = {"author": from, "time": when, "text": what, "public": parseInt(pub)};
	if(pub != "1"){
		$("#other-comments-list").html("<div class='comment alert alert-danger'><span class='pficon pficon-private' title='Private comment'></span><h4>" + from + "</h4><p><i><small>" + new Date(when).toString() + "</small></i><br>" + what + "</p></div>" + content);
	}else{
		$("#other-comments-list").html("<div class='comment'><h4>" + from + "</h4><p><i><small>" + new Date(when).toString() + "</small></i><br>" + what + "</p></div>" + content);
	}
	record.comments.unshift(ncom);
	$.each(database, function (key, val){
		if(val.response_id == record.id){
			val.comments.unshift(ncom);
		}
	});
	$("#other-comments-count").html(record.comments.length);
    }

    $(".add-other-comment").click(function (){
	var response = $("#otherModalTitle").attr("response");
	var publicly = $(this).attr("pub");
	var record = database[response];

	var from = fullReviewer + " (program manager)";
	var what = $("#other-comment-input").val();
	if(what == ""){
		showErrorToast("You have to add some text first.");
		return;
	}
	var when = new Date().getTime();

	from = encodeURIComponent(from);
	what = encodeURIComponent(what);

	var xhr = new XMLHttpRequest();
        xhr.open('POST', '/comment/');
       	xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.onload = function() {
       	    try{
               	var data = JSON.parse(xhr.responseText);
            } catch (e) {
       	        $("#otherResponseDetail").modal('hide');
               	showErrorToast(xhr.responseText);
                return;
       	    }
            if(data.result != 'success'){
       	        $("#otherResponseDetail").modal('hide');
               	showErrorToast(data.reason);
            }else{
		if(data.message != undefined){
			showSuccessToast(data.message);
		}
       	        addOtherComment(decodeURIComponent(from), when, decodeURIComponent(what), record, publicly);
            }
       	};

	if(publicly != "1"){
	        xhr.send(jQuery.param({'token': token, 'response': record['id'], 'from': from, 'what': what , 'when': when}));
	} else {
	        xhr.send(jQuery.param({'token': token, 'response': record['id'], 'from': from, 'what': what , 'when': when, 'public': 1}));
	}

	$("#other-comment-input").val("");
    });

    init(); 
    $(".table").DataTable({paging:false});
  });
</script>
</body>
</html>  
