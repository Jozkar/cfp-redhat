<div class="container-fluid container-cards-pf container-pf-nav-pf-vertical">
    <ol class="breadcrumb" role="navigation" aria-label="breadcrumbs">
        <li><a href="/"><span class="fa fa-home"></span></a></li>
        <li><a href="/admin">Administration</a></li>
        <li><a href="/forms/<?= $eventInfo['id'] ?>">Forms</a></li>
        <li class="active"><span><?= ucfirst($formType) ?></span></li>
    </ol>
	<h1><?= ucfirst($formType) . " form for " . $eventInfo['name'] . " (" . $eventInfo['year'] . ")" ?></h1>
	<a href="#" action="add" type="question" class="openModal btn btn-primary">Add question</a>
	<a href="#" action="add" type="description" class="openModal btn btn-success">Add title and description</a>
	<div class="modal fade" id="newQuestion" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true" aria-label="Close">
						<span class="pficon pficon-close"></span>
					</button>
					<h4 class="modal-title" id="modalTitle">Confirm form deletion</h4>
				</div> <!-- modal header -->
				<div class="modal-body">
					<p>There will be some content.</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					<button type="button" id="save" class="btn btn-primary">Save</a>
				</div> <!-- modal footer -->
			</div> <!-- modal content -->
		</div> <!-- modal dialog -->
	</div> <!-- modal -->
	<div id="questionForm" class="hidden">
		<div class="form-group">
			<label class="control-label" for="type">Type of question</label>
			<div>
				<label class="radio-inline">
					<input type="radio" name="type" class="questionType" value="text"> Short answer
				</label>
				<label class="radio-inline">
					<input type="radio" name="type" class="questionType" value="textarea"> Long answer
				</label>
				<label class="radio-inline">
					<input type="radio" name="type" class="questionType" value="hidden"> Hidden field
				</label>
				<label class="radio-inline">
					<input type="radio" name="type" class="questionType" value="topic"> Single/Multiple choices
				</label>
			</div>
		</div>
		<div class="hidden" id="forText">
			<div class="form-group">
				<label class="control-label" for="question">Question</label>
				<input type="text" name="question" class="form-control">
				<span class="help-block">Fill your question.</span>
			</div>
			<div class="form-group">
				<label class="control-label" for="helpblock">Help Text</label>
				<textarea maxlength="65530" class="form-control" name="helpblock"></textarea>
				<span class="help-block">Fill help text for question.</span>
			</div>
			<div class="form-group">
				<label class="control-label" for="default">Default Value</label>
				<input type="text" name="default" class="form-control">
				<span class="help-block">Default value for this field (Optional).</span>
			</div>
			<div class="form-group">
				<label class="control-label" for="size">Character lenght limit</label>
				<input type="number" name="size" min="0" value="0" class="form-control">
				<span class="help-block">Set maximum number of accepted characters (0 = without limit)</span>
			</div>
			<div class="form-group">
				<label class="checkbox-inline">
					<input type="checkbox" name="required"> Mark this question as Required
				</label>
			</div>
		<?php if ($formType == "session"){ ?>
			<div class="form-group">
				<label class="control-label" for="storeAs">Store as</label>
				<select class="form-control" name="storeAs">
					<option value="" disabled selected>Database column</option>
					<?php foreach($columns as $col) { ?>
						<option value="<?=$col['value']?>"><?=$col['column']?></option>
					<?php } ?>
				</select>
				<span class="help-block">Select column from database, where should be stored answer from this question.</span>
			</div>
		<?php } else { ?>
            <div class="form-group">
                <label class="checkbox-inline">
                    <input type="checkbox" name="as-title"> Take answer for this question as proposal title
                </label>
            </div>
            <div class="form-group">
                 <label class="checkbox-inline">
                     <input type="checkbox" name="as-description"> Take answer for this question as proposal description
                </label>
            </div>
        <?php } ?>
		</div>
		<div class="hidden" id="forTopic">
			<div class="form-group">
				<label class="control-label" for="question">Question</label>
				<input type="text" name="question" class="form-control">
				<span class="help-block">Fill your question.</span>
			</div>
			<div class="form-group">
				<label class="control-label" for="helpblock">HelpText</label>
				<textarea maxlength="65530" class="form-control" name="helpblock"></textarea>
				<span class="help-block">Fill help text for question.</span>
			</div>
			<div class="form-group">
				<label class="checkbox-inline">
					<input type="checkbox" name="multiple" value="checkbox"> Allow select multiple options
 				</label>
			</div>
			<div class="form-group">
                                <label class="checkbox-inline">
                                        <input type="checkbox" name="required"> Mark this question as Required
                                </label>
                        </div>
			<div class="form-group">
				<label class="checkbox-inline">
					<input type="checkbox" name="custom"> Set custom options
				</label>
				<?php if(strpos($formType, "accommodation") !== false){ ?>
				<span class="help-block">If selected, you can set your own options. Otherwise, hotel room selection will be generated under this question.</span>
				<?php } else { ?>
				<span class="help-block">If selected, you can set your own options and where to store it. Otherwise, event's topics selection will be generated under this question.</span>
				<?php } ?>
			</div>
			<div id="ownContainer" class="hidden">
				<div id="ownOptions">
					<div class="form-group">
						<label class="control-label" for="optionVal">Value</label>
						<input type="text" name="optionVal" class="form-control">
						<span class="help-block">Value, which is stored in database. If empty, Description is used.</span>
						<label class="control-label" for="option">Description</label>
						<input type="text" name="option" class="form-control">
						<span class="help-block">Text, which is shown in form. If empty, this option will be ignored.</span>
					</div>
				</div>
				<div class="form-group">
					<button class='btn btn-primary addOption'>Add line for new option</button>
				</div>
			<?php if ($formType == "session"){ ?>
				<div class="form-group">
					<label class="control-label" for="storeAs">Store as</label>
	                                <select class="form-control" name="storeAs">
        	                                <option value="" disabled selected>Database column</option>
                	                        <?php foreach($columns as $col) { ?>
                        	                        <option value="<?=$col['value']?>"><?=$col['column']?></option>
                                	        <?php } ?>
	                                </select>
        	                        <span class="help-block">Select column from database, where should be stored answer from this question. If empty, value will not be saved during form processing.</span>
                	        </div>
			<?php } else { ?>
    	    <div class="form-group">
                <label class="checkbox-inline">
                    <input type="checkbox" name="as-duration"> Take answer for this question as proposal duration (value of such has to be whole number and no multiple options are allowed).
                </label>
            </div>
                        <?php } ?>
			</div>
		</div>
				
		<div class="hidden" id="forHidden">
			<div class="form-group">
				<label class="control-label" for="question">Description</label>
				<input type="text" name="question" class="form-control">
				<span class="help-block">Fill description for this field.</span>
			</div>
						<div class="form-group">
				<label class="control-label" for="default">Value</label>
				<input type="text" name="default" class="form-control">
				<span class="help-block">Value for this field.</span>
			</div>
			<?php if ($formType == "session"){ ?>
			<div class="form-group">
				<label class="control-label" for="storeAs">Store as</label>
				<select class="form-control" name="storeAs">
					<option value="" disabled selected>Database column</option>
					<?php foreach($columns as $col) { ?>
						<option value="<?=$col['value']?>"><?=$col['column']?></option>
					<?php } ?>
				</select>
				<span class="help-block">Select column from database, where should be stored answer from this question.</span>
			</div>
		<?php } else { ?>
	    <div class="form-group">
                <label class="checkbox-inline">
                    <input type="checkbox" name="as-duration"> Take answer for this question as proposal duration (value of such has to be whole number)
                </label>
            </div>
                        <?php } ?>
		</div>
	</div>

	<div id="descriptionForm" class="hidden">
		<div class="form-group">
			<label class="control-label" for="title">Title</label>
			<input type="text" name="title" class="form-control" required>
			<span class="help-block">Fill title</span>
		</div>	
		<div class="form-group">
			<label class="control-label" for="description">Description</label>
			<textarea maxlength="65530" class="form-control" name="description" required></textarea>
			<span class="help-block">Fill description. HTML tags are supported.</span>
		</div>
	</div>

	<div id="noQuestion" class="blank-slate-pf<?php if(count($questions) >= 1) echo " hidden";?>">
        	<div class="blank-slate-pf-icon">
	            <span class="pficon pficon-info"></span>
        	</div>
	        <h1>No question or description has been added to this form.</h1>
        	<p>Click on '<i>Add question</i>' or '<i>Add title and description</i>' button to create one.</p>
	</div>
	<div id="questionContainer" class="list-group list-view-pf list-view-pf-view<?php if(count($questions) < 1) echo " hidden";?>">
		<?php foreach($questions as $q){ ?>
		<div class='list-group-item' id='question<?= $q['id'] ?>'>
			<span class="pf-grab ui-sortable-handle"></span>
			<div class='list-view-pf-actions'>
		                <button class='btn btn-default openModal' action='edit' type='<?= ($q['type'] != 'description'?'question':'title') ?>' key='<?= $q['id'] ?>'>Edit</button>
		                <button class='btn btn-danger delete' key='<?= $q['id'] ?>'>Delete</button>
		        </div>
			<div class='list-view-pf-main-info'>
				<div class='list-view-pf-body' style='display: block'>
	<?php
        switch($q['type']){
                case 'description':
                        if(isset($q['question']) && $q['question'] != ""){
                                echo "<h2>" . $q['question'] . "</h2>";
                        }
                        if(isset($q['help_text']) && $q['help_text'] != ""){
                                echo "<p>" . $q['help_text'] . "</p>";
                        }
                        echo "<i>Title/Description</i>";
                        break;
                case 'hidden':
                        echo "<h2>" . $q['question'] . "</h2>";
                        echo "<br><i>Hidden field";
			if($formType == "session"){
				echo " stored in " . $q['take_as'];
			}
			echo "</i>";
			if($q['default_value'] != null){
				echo ", value set to " . $q['default_value'] . ".";
			}
                        break;
                case 'text':
                        echo "<h2>" . $q['question'];
			if($q['required']){
				echo " <span class='required-pf'>*</span>(Required)";
			}
			echo "</h2>";
                        if(isset($q['help_text']) && $q['help_text'] != ""){
                                echo "<small>" . $q['help_text'] . "</small>";
                        }
                        echo "<br><i>Short answer";
			if($formType == "session"){
				echo " stored in " . $q['take_as'];
			}
			echo "</i>";
			if($q['default_value'] != null){
				echo ", default value set to " . $q['default_value'];
			}		
			if($q['size'] != null && $q['size'] > 0){
				echo ", character limit set to " . $q['size'] . " characters";
			}
			echo ".";
                        break;
                case 'textarea':
                        echo "<h2>" . $q['question'];
			if($q['required']){
                                echo " <span class='required-pf'>*</span>(Required)";
                        }
			echo "</h2>";
                        if(isset($q['help_text']) && $q['help_text'] != ""){
                                echo "<small>" . $q['help_text'] . "</small>";
                        }
                        echo "<br><i>Long answer";
			if($formType == "session"){
				echo " stored in " . $q['take_as'];
			}
			echo "</i>";
			if($q['default_value'] != null){
				echo ", value set to " . $q['default_value'];
			}

			if($q['size'] != null && $q['size'] > 0){
				echo ", character limit set to " . $q['size'] . " characters";
			}
			echo ".";
                        break;
                case 'slider':
                        echo "<h2>" . $q['question'];
			if($q['required']){
                                echo " <span class='required-pf'>*</span>(Required)";
                        }
			echo "</h2>";
                        if(isset($q['help_text']) && $q['help_text'] != ""){
                                echo "<small>" . $q['help_text'] . "</small>";
                        }
//                        echo "<p>From: " + value.from + " To: " + value.to + " Step: " + value.step + " Units: " + value.unit + "</p>";
                        echo "<br><i>Slider";
			if($formType == "session"){
				echo " stored in " . $q['take_as'];
			}
			echo "</i>";
                        break;
                case 'radio':
                case 'checkbox':
                        echo "<h2>" . $q['question'];
			if($q['required']){
                                echo " <span class='required-pf'>*</span>(Required)";
                        }
			echo "</h2>";
                        if(isset($q['help_text']) && $q['help_text'] != ""){
                                echo "<small>" . $q['help_text'] . "</small>";
                        }
			if($q['type'] == "radio"){
                                echo "<br><i>Radio button selection";
				if($formType == "session"){
					echo " stored in " . $q['take_as'];
				}
                        }else{
				echo "<br><i>Checkbox selection";
				if($formType == "session"){
					echo " stored in " . $q['take_as'];
				}
                        }
			echo "</i>";
                        break;
        }
	?>	    </div>
	        </div>
	    </div>
	<?php } ?>
	</div>
    </div> <!-- tab content -->
</div> <!-- container -->
<script>
  $(document).ready(function() {
    // matchHeight the contents of each .card-pf and then the .card-pf itself
    $(".row-cards-pf > [class*='col'] > .card-pf .card-pf-title").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf > .card-pf-body").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf > .card-pf-footer").matchHeight();
    $(".row-cards-pf > [class*='col'] > .card-pf").matchHeight();
    $( "#questionContainer" ).sortable({
	handle: ".pf-grab",
	stop: function (event, ui) {
		var order = [];
		$.each($("#questionContainer .list-group-item"), function(i,question){
			order.push(parseInt(question.getAttribute('id').replace("question","")));
		});
		updateOrder(order);
	      }
    });
    
    var formId = "<?= $formId ?>", token = "<?= $token ?>";
    var database = { 
	<?php $max = 0; $used = array();
		foreach($questions as $val) { 
			if($max < $val['id']) {$max = $val['id'];}
		?>
		"<?= $val['id'] ?>" : {
			"id": <?= $val['id'] ?>,
			"question": <?= json_encode($val['question']) ?>,
			"type": <?= json_encode($val['type']) ?>,
			"required": <?= $val['required'] ?>,
		<?php if($val['type'] == 'radio' || $val['type'] == "checkbox"){ 
			$opt = ""; $opVal = "";
			foreach($options as $o){
				if($o['question_id'] == $val['id']){
					$opt .= "'" . str_replace("'", "\'", $o['label']) . "',";
					$opVal .= "'" . str_replace("'", "\'", $o['value']) . "',";
				}
			}
		?>
			"options": [<?= $opt ?>],
			"values": [<?= $opVal ?>],
		<?php } ?>
			"helpText": <?= json_encode($val['help_text']) ?>,
			"defVal": <?= json_encode($val['default_value']) ?>,
		<?php if($formType == "session"){ ?>
			"storage": <?= json_encode($val['take_as']) ?>,
		<?php } else { ?>
            "as-title": <?= $val['as_title'] ?>,
            "as-duration": <?= $val['as_duration'] ?>,
            "as-description": <?= $val['as_description'] ?>,
        <?php } ?>
			"order": <?= $val['question_order'] ?>,
			"size": <?= (isset($val['size'])?$val['size']:0) ?>
		},
	<?php   if($formType == "session"){
			$used[$val['take_as']] = true;
		}
		} ?>

    }, newKey = <?= $max+1 ?>;
    <?php if($formType == "session"){ ?>
    var options = {<?php foreach($columns as $col) { ?>
		'<?= $col['value'] ?>': {'value': "<?=$col['value']?>", 'description':"<?=$col['column']?>", 'used':<?= (isset($used[$col['value']])?"true":"false") ?>},
	<?php } ?>};
    <?php } ?>


    // Initialize the vertical navigation
    $().setupVerticalNavigation(true);

    function initRadio(type){
	if(type != undefined){
		if(type == "text" || type == "textarea"){
			$(".modal-body .questionType[value=" + type +"]").prop("checked", true);
			$(".modal-body  #forHidden").addClass("hidden");
			$(".modal-body  #forTopic").addClass("hidden");
			$(".modal-body  #forText").removeClass("hidden");
		}
		if(type == "radio" || type == "checkbox"){
			$(".modal-body .questionType[value=topic]").prop("checked", true);
			$(".modal-body  #forText").addClass("hidden");
			$(".modal-body  #forHidden").addClass("hidden");
			$(".modal-body  #forTopic").removeClass("hidden");
		}
		if(type == "hidden"){
			$(".modal-body .questionType[value=hidden]").prop("checked", true);
			$(".modal-body  #forText").addClass("hidden");
			$(".modal-body  #forTopic").addClass("hidden");
			$(".modal-body  #forHidden").removeClass("hidden");
		}
	}
	$(".questionType").change(function(){
		var type = $(this).attr('value');
		if(type == "hidden"){
			$(".modal-body  #forText").addClass("hidden");
			$(".modal-body  #forTopic").addClass("hidden");
			$(".modal-body  #forHidden").removeClass("hidden");
		} else {
			if(type == "topic"){
				$(".modal-body  #forText").addClass("hidden");
				$(".modal-body  #forHidden").addClass("hidden");
				$(".modal-body  #forTopic").removeClass("hidden");
			} else {
				$(".modal-body  #forHidden").addClass("hidden");
				$(".modal-body  #forTopic").addClass("hidden");
				$(".modal-body  #forText").removeClass("hidden");
			}
		}
	});
	$(".modal-body #forTopic input[name=custom]").change(function(){
		if($(this).is(":checked")){
			$(".modal-body  #forTopic #ownContainer").removeClass("hidden");
		} else {
			$(".modal-body  #forTopic #ownContainer").addClass("hidden");
		}
	});

	$(".modal-body #forTopic .addOption").click(appendOption);
	
    }

    function appendOption(value, description){
	if(value != undefined && description != undefined){
		$(".modal-body #forTopic #ownOptions").prepend('<label class="control-label" for="optionVal">Value</label><input type="text" name="optionVal" class="form-control" value="'+ value +'">' +
			'<span class="help-block">Value, which is stored in database. If empty, Description is used.</span><label class="control-label" for="option">Description</label>' +
			'<input type="text" name="option" class="form-control" value="'+ description +'"><span class="help-block">Text, which is shown in form. If empty, this option will be ignored.</span>');
	} else {
		$(".modal-body #forTopic #ownOptions").append('<label class="control-label" for="optionVal">Value</label><input type="text" name="optionVal" class="form-control">' +
			'<span class="help-block">Value, which is stored in database. If empty, Description is used.</span><label class="control-label" for="option">Description</label>' +
			'<input type="text" name="option" class="form-control"><span class="help-block">Text, which is shown in form. If empty, this option will be ignored.</span>');
	}
    }

    function initStoreTo(storage){
	$(".modal-body select").html("");
	var html = "";
	var selected = false;
	$.each(options, function(key, val){
		if(val.used == false){
			html += "<option value='" + val.value + "'";
			if(storage != undefined && storage == val.value){
				html += " selected";
				selected = true;
			}
			html += ">" + val.description + "</option>";
		}
	});
	if(html != ""){
		if(selected){
			html = "<option value='' disabled>Database column</option>" + html;
		} else {
			html = "<option value='' disabled selected>Database column</option>" + html;
		}
	}
	$(".modal-body select").html(html);
	$(".modal-body select").selectpicker();
    }

    function init(){
	$(".openModal").off("click");
	$(".openModal").on("click", function(){
		var action = $(this).attr("action");
		var type = $(this).attr("type");

		if(action == "add"){
			$("#modalTitle").html("Add " + type);
			if(type == "question"){
				$(".modal-body").html($("#questionForm").html());
                var editor = new Jodit($(".modal-body #forText textarea")[0], {
                             "disablePlugins": "symbols,stat,media,imageproperties,imageprocessor,fullsize,draganddropelement,draganddrop,pastestorage,autofocus,addnewline",
                             "buttonsXS": "bold,strikethrough,underline,italic,|,ul,ol,outdent,indent,|,fontsize,link,|,undo,redo,cut,copy,paste,|",
                             "buttonsMD": "bold,strikethrough,underline,italic,|,ul,ol,outdent,indent,|,fontsize,link,|,undo,redo,cut,copy,paste,|",
                             "buttonsSM": "bold,strikethrough,underline,italic,|,ul,ol,outdent,indent,|,fontsize,link,|,undo,redo,cut,copy,paste,|",
                             "buttons": "bold,strikethrough,underline,italic,|,ul,ol,outdent,indent,|,fontsize,link,|,undo,redo,cut,copy,paste,|",
                });
                var editor2 = new Jodit($(".modal-body #forTopic textarea")[0], {
                             "disablePlugins": "symbols,stat,media,imageproperties,imageprocessor,fullsize,draganddropelement,draganddrop,pastestorage,autofocus,addnewline",
                             "buttonsXS": "bold,strikethrough,underline,italic,|,ul,ol,outdent,indent,|,fontsize,link,|,undo,redo,cut,copy,paste,|",
                             "buttonsMD": "bold,strikethrough,underline,italic,|,ul,ol,outdent,indent,|,fontsize,link,|,undo,redo,cut,copy,paste,|",
                             "buttonsSM": "bold,strikethrough,underline,italic,|,ul,ol,outdent,indent,|,fontsize,link,|,undo,redo,cut,copy,paste,|",
                             "buttons": "bold,strikethrough,underline,italic,|,ul,ol,outdent,indent,|,fontsize,link,|,undo,redo,cut,copy,paste,|",
                });
				initRadio();
				<?php if($formType == "session"){ ?>
				initStoreTo();
				<?php } ?>
			} else {
				$(".modal-body").html($("#descriptionForm").html());
                var editor = new Jodit($(".modal-body textarea")[0], {
                             "disablePlugins": "symbols,stat,media,imageproperties,imageprocessor,fullsize,draganddropelement,draganddrop,pastestorage,autofocus,addnewline",
                             "buttonsXS": "bold,strikethrough,underline,italic,|,ul,ol,outdent,indent,|,fontsize,link,|,undo,redo,cut,copy,paste,|",
                             "buttonsMD": "bold,strikethrough,underline,italic,|,ul,ol,outdent,indent,|,fontsize,link,|,undo,redo,cut,copy,paste,|",
                             "buttonsSM": "bold,strikethrough,underline,italic,|,ul,ol,outdent,indent,|,fontsize,link,|,undo,redo,cut,copy,paste,|",
                             "buttons": "bold,strikethrough,underline,italic,|,ul,ol,outdent,indent,|,fontsize,link,|,undo,redo,cut,copy,paste,|",
                });
			}
			$("#newQuestion").attr('key', -1);
		} else {
			var key = $(this).attr('key');
			$("#newQuestion").attr('key', key);
			$("#modalTitle").html("Edit " + type);
			if(type == "question"){
				$(".modal-body").html($("#questionForm").html());
				initRadio(database[key]['type']);
				$(".modal-body input[name=question]").val(database[key]['question']);
				$(".modal-body input[name=default]").val(database[key]['defVal']);
				$(".modal-body textarea[name=helpblock]").html(database[key]['helpText']);
				if(database[key]['required'] == 1){
					$(".modal-body input[name=required]").prop("checked", true);
				}
				<?php if($formType == "session"){ ?>
				if(database[key]['storage'] in options){
					options[database[key]['storage']]['used'] = false;
				}
				<?php } else { ?>
                if(database[key]['as-title'] == 1){
                    $(".modal-body input[name=as-title]").prop("checked", true);
                }
                if(database[key]['as-duration'] == 1){
                    $(".modal-body input[name=as-duration]").prop("checked", true);
                }
                if(database[key]['as-description'] == 1){
                    $(".modal-body input[name=as-description]").prop("checked", true);
                }
                <?php } ?>
				if(database[key]['type'] == "text" || database[key]['type'] == "textarea" || database[key]['type'] == "hidden"){
					$(".modal-body input[name=size]").val(database[key]['size']);
				} else {
					if(database[key]['type'] == "checkbox"){
						$(".modal-body input[name=multiple]").prop("checked", true);
					}
					var length = database[key]['options'].length;
					if(length > 0){
						$(".modal-body input[name=custom]").prop("checked", true);
						$(".modal-body  #forTopic #ownContainer").removeClass("hidden");
						for(var i = length-1; i >= 0; i--){
							appendOption(database[key]['values'][i], database[key]['options'][i]);
						}
						 if(database[key]['as-duration'] == 1){
                                                    $(".modal-body input[name=as-duration]").prop("checked", true);
                                                }
					}

				}
                var editor = new Jodit($(".modal-body #forText textarea")[0], {
                             "disablePlugins": "symbols,stat,media,imageproperties,imageprocessor,fullsize,draganddropelement,draganddrop,pastestorage,autofocus,addnewline",
                             "buttonsXS": "bold,strikethrough,underline,italic,|,ul,ol,outdent,indent,|,fontsize,link,|,undo,redo,cut,copy,paste,|",
                             "buttonsMD": "bold,strikethrough,underline,italic,|,ul,ol,outdent,indent,|,fontsize,link,|,undo,redo,cut,copy,paste,|",
                             "buttonsSM": "bold,strikethrough,underline,italic,|,ul,ol,outdent,indent,|,fontsize,link,|,undo,redo,cut,copy,paste,|",
                             "buttons": "bold,strikethrough,underline,italic,|,ul,ol,outdent,indent,|,fontsize,link,|,undo,redo,cut,copy,paste,|",
                });
                var editor2 = new Jodit($(".modal-body #forTopic textarea")[0], {
                             "disablePlugins": "symbols,stat,media,imageproperties,imageprocessor,fullsize,draganddropelement,draganddrop,pastestorage,autofocus,addnewline",
                             "buttonsXS": "bold,strikethrough,underline,italic,|,ul,ol,outdent,indent,|,fontsize,link,|,undo,redo,cut,copy,paste,|",
                             "buttonsMD": "bold,strikethrough,underline,italic,|,ul,ol,outdent,indent,|,fontsize,link,|,undo,redo,cut,copy,paste,|",
                             "buttonsSM": "bold,strikethrough,underline,italic,|,ul,ol,outdent,indent,|,fontsize,link,|,undo,redo,cut,copy,paste,|",
                             "buttons": "bold,strikethrough,underline,italic,|,ul,ol,outdent,indent,|,fontsize,link,|,undo,redo,cut,copy,paste,|",
                 });
				<?php if($formType == "session"){ ?>
				initStoreTo(database[key]['storage']);
				<?php } ?>
			} else {
				$(".modal-body").html($("#descriptionForm").html());
				$(".modal-body input[name=title]").val(database[key]['question']);
				$(".modal-body textarea[name=description]").html(database[key]['helpText']);
                var editor = new Jodit($(".modal-body textarea")[0], {
                             "disablePlugins": "symbols,stat,media,imageproperties,imageprocessor,fullsize,draganddropelement,draganddrop,pastestorage,autofocus,addnewline",
                             "buttonsXS": "bold,strikethrough,underline,italic,|,ul,ol,outdent,indent,|,fontsize,link,|,undo,redo,cut,copy,paste,|",
                             "buttonsMD": "bold,strikethrough,underline,italic,|,ul,ol,outdent,indent,|,fontsize,link,|,undo,redo,cut,copy,paste,|",
                             "buttonsSM": "bold,strikethrough,underline,italic,|,ul,ol,outdent,indent,|,fontsize,link,|,undo,redo,cut,copy,paste,|",
                             "buttons": "bold,strikethrough,underline,italic,|,ul,ol,outdent,indent,|,fontsize,link,|,undo,redo,cut,copy,paste,|",
                });
			}
		}

		$("#newQuestion").modal('show');
		$("#newQuestion").attr('type', type);
		$("#newQuestion").attr('action', action);
		$("#save").off("click");
		$("#save").on("click", checkSave);
	});
	$(".delete").off("click");
	$(".delete").on("click", deleteData);
	<?php if($formType == "session") { ?>
	$("#newQuestion button[data-dismiss=modal]").off("click");
	$("#newQuestion button[data-dismiss=modal]").on("click", restoreOptions);
	<?php } ?>

    }

    function deleteData(){
	var key = $(this).attr('key');

	sendUpdate({"id": key, "type": database[key]['type']}, "delete");
    }

    function checkSave(){
	var radio = $(".questionType:checked").val();
	var type = $("#newQuestion").attr("type");
	var id = $("#newQuestion").attr("key");
	var action = (id > 0?"update":"add");

	if(type == "question"){
		if(radio == undefined){
			showToast("No question type selected.");
		} else {
			var question, helpText, storeAs = "",size,required, defVal, asTitle = 0, asDuration = 0, asDescription = 0;

			switch(radio){
				case "text": 
				case "textarea":
					question = $.trim($(".modal-body #forText input[name=question]").val());
					defVal = $.trim($(".modal-body #forText input[name=default]").val());
					helpText = $.trim($(".modal-body #forText textarea[name=helpblock]").val());
					required = ($(".modal-body #forText input[name=required]").is(":checked")?1:0);
					<?php if($formType == "session"){ ?>
					storeAs = $.trim($(".modal-body #forText select[name=storeAs] option:selected").val());
					<?php } else { ?>
                    asTitle = ($(".modal-body #forText input[name=as-title]").is(":checked")?1:0);
                    asDescription = ($(".modal-body #forText input[name=as-description]").is(":checked")?1:0);
                    <?php } ?>
					size = $.trim($(".modal-body #forText input[name=size]").val());

					if(!$.isNumeric(size)){
						size = 0;
					}
					<?php if($formType == "session"){ ?>
					if(question == "" || (storeAs == "" && radio != "topic")){
						showToast("You have to fill <b>Question</b> field and select one of <b>Store as</b> options.");
					}else{
					<?php } else { ?>
					if(question == ""){
                                                showToast("You have to fill <b>Question</b> field.");
                                        }else{
					<?php } ?>
						if(storeAs == ""){
							storeAs = "self";
						}
                                                storeData({'question': question, 'helpText': helpText, 'storage': storeAs, 'type': radio, 'size': size, 'required': required, 'id': id, 'asTitle': asTitle,
                                                 'asDuration': asDuration, 'asDescription': asDescription, 'defValue': defVal}, action);
                                        }
					break;
				case "hidden":
					question = $.trim($(".modal-body #forHidden input[name=question]").val());
					defVal = $.trim($(".modal-body #forHidden input[name=default]").val());
					<?php if($formType == "session"){ ?>
					storeAs = $.trim($(".modal-body #forHidden select[name=storeAs] option:selected").val());
					<?php } else { ?>
					asDuration = ($(".modal-body #forHidden input[name=as-duration]").is(":checked")?1:0);
					<?php } ?>
					<?php if($formType == "session"){ ?>
					if(question == "" || (storeAs == "" && radio != "topic")){
						showToast("You have to fill <b>Question</b> field and select one of <b>Store as</b> options.");
					}else{
					<?php } else { ?>
					if(question == ""){
                                                showToast("You have to fill <b>Description</b> field.");
                                        }else{
					<?php } ?>
						if(storeAs == ""){
							storeAs = "self";
						}
                                                storeData({'question': question, 'helpText': "", 'storage': storeAs, 'type': radio, 'size': 0, 'required': 0, 'id': id, 'asTitle': 0, 'asDuration': asDuration,
                                                 'defValue': defVal}, action);
                                        }
					break;
				case "slider":
					var from = $.trim($(".modal-body #forSlider input[name=from]").val()),
					    to = $.trim($(".modal-body #forSlider input[name=to]").val()),
					    step = $.trim($(".modal-body #forSlider input[name=step]").val()),
					    unit = $.trim($(".modal-body #forSlider input[name=unit]").val());
					question = $.trim($(".modal-body #forSlider input[name=question]").val());
					helpText = $.trim($(".modal-body #forSlider textarea[name=helpblock]").val());
					required = ($(".modal-body #forSlider input[name=required]").is(":checked")?1:0);
					<?php if($formType == "session"){ ?>
					storeAs = $.trim($(".modal-body #forSlider select[name=storeAs] option:selected").val());

					if(question == "" || storeAs == "" || from == "" || to == "" || step == "" || unit == ""){
						showToast("You have to fill <b>Question, From, To, Step and Unit</b> fields and select one of <b>Store as</b> options.");
					<?php } else { ?>
					if(question == "" || from == "" || to == "" || step == "" || unit == ""){
                                                showToast("You have to fill <b>Question, From, To, Step and Unit</b> fields.");
					<?php } ?>
                                        }else{
						if(storeAs == ""){
							storeAs = "self";
						}
                                                storeData({'question': question, 'helpText': helpText, 'storage': storeAs, 'type': 'slider',
                                                        'from': from, 'to': to, 'step': step, 'unit': unit, 'required': required, 'id': id}, action);
                                        }
					break;
				default: 
					question = $.trim($(".modal-body #forTopic input[name=question]").val());
					helpText = $.trim($(".modal-body #forTopic textarea[name=helpblock]").val());
					required = ($(".modal-body #forTopic input[name=required]").is(":checked")?1:0);
					var multiple = $.trim($(".modal-body #forTopic input[name=multiple]:checked").val()),
					    customOptions = [], customValues = [];
					if($(".modal-body #forTopic input[name=custom]").is(":checked")){
						var cVals = $(".modal-body #ownOptions input[name=optionVal]");
						$(".modal-body #ownOptions input[name=option]").each(function (index){ 
							if($.trim($(this).val()) != ""){
								customOptions.push($.trim($(this).val()));
								if($.trim($(cVals[index]).val()) != ""){
									customValues.push($.trim($(cVals[index]).val()));
								} else {
									customValues.push($.trim($(this).val()));
								}
							}
						 });
						<?php if($formType == "session"){ ?>
						storeAs = $.trim($(".modal-body #forTopic select[name=storeAs] option:selected").val());
						<?php } else { ?>
						asDuration = ($(".modal-body #forTopic input[name=as-duration]").is(":checked")?1:0);
						<?php } ?>
						if(storeAs == ""){
							storeAs = "self";
						}
						if(customOptions.length == 0){
							showToast("You have to fill at least one option");
							break;
						}	
					}else{
					<?php if(strpos($formType, "accommodation") !== false) { ?>
						storeAs = "accommodation";
					<?php } else { ?>
						storeAs = "topics";
					<?php } ?>
						customOptions = [];
						customValues = [];
					}
					if(question == ""){
						showToast("You have to fill <b>Question</b> field.");
					} else {
						storeData({'question': question, 'helpText': helpText, 'type': (multiple != ""?multiple:'radio'), 'required': required, 'id': id, 
								'storage': storeAs, 'options': customOptions, 'values': customValues, 'asDuration': asDuration},action);
					}
					break;
			}
		}
	} else {
		var title = $.trim($(".modal-body input[name=title]").val()),
                    description = $.trim($(".modal-body textarea[name=description]").val());

		if(title == "" && description == ""){
			showToast("You have to fill <b>Title</b> or <b>Description</b> field.");
		} else {
			storeData({'title': title,'description': description, 'type': 'description', 'id': id}, action);
		}
		
	}
    }

    function showToast(message){
        var toast = $(".toast-notifications-list-pf").html()+
                    '<div class="toast-pf alert alert-danger alert-dismissable">'+
                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">'+
                    '<span class="pficon pficon-close"></span>'+
                    '</button>'+
                    '<span class="pficon pficon-error-circle-o"></span>'+
                    message+
                    '</div>';
         $(".toast-notifications-list-pf").html(toast);        
    }

    function storeData(value, action){
	if(value.id == -1){
		value.id = newKey;
	}
	if("storage" in value && value.storage == "self"){
		value.storage = "self" + value.id;
	}

	sendUpdate(value, action);
    }

    function getHtmlRow(value){
	var html = "";
	switch(value.type){
                case 'description':
                        if(value.question != ''){
                                html += "<h2>" + value.question + "</h2>";
                        }
                        if(value.helpText != ''){
                                html += "<p>" + value.helpText + "</p>";
                        }
                        html += "<i>Title/Description</i>";
                        break;
                case 'hidden':
                        html += "<h2>" + value.question + "</h2>";
                        html += "<br><i>Hidden field";
			<?php if($formType == "session"){ ?>
				html += " stored in " + value.storage;
			<?php } ?>
			html += "</i>";
			if(value.defValue != ""){
				html += ", value set to " + value.defValue;
			}
                        break;        
                case 'text':
                        html += "<h2>" + value.question;
			if(value.required == 1){
				html += " <span class='required-pf'>*</span>(Required)";
			}
			html += "</h2>";
                        if(value.helpText != ""){
                                html += "<small>" + value.helpText + "</small>";
                        }
			<?php if($formType == "session"){ ?>
                        html += "<br><i>Short answer stored in " + value.storage + "</i>";
			<?php } else { ?>
			html += "<br><i>Short answer</i>";
			<?php } ?>
			if(value.defValue != ""){
				html += ", default value set to " + value.defValue;
			}
                        if(value.size != 0){
                                html += ", character limit set to " + value.size + " characters.";
                        }
                        break;
                case 'textarea':
                        html += "<h2>" + value.question;
                        if(value.required == 1){
                                html += " <span class='required-pf'>*</span>(Required)";
                        }
                        html += "</h2>";
                        if(value.helpText != ""){
                                html += "<small>" + value.helpText + "</small>";
                        }
			<?php if($formType == "session"){ ?>
                        html += "<br><i>Long answer stored in " + value.storage + "</i>";
			<?php } else { ?>
			html += "<br><i>Long answer</i>";
			<?php } ?>
			if(value.defValue != ""){
				html += ", default value set to " + value.defValue;
			}
                        if(value.size != 0){
                                html += ", character limit set to " + value.size + " characters.";
                        }
                        break;
                case 'slider':
                        html += "<h2>" + value.question;
                        if(value.required == 1){
                                html += " <span class='required-pf'>*</span>(Required)";
                        }
                        html += "</h2>";
                        if(value.helpText != ""){
                                html += "<small>" + value.helpText + "</small>";
                        }
                        html += "<p>From: " + value.from + " To: " + value.to + " Step: " + value.step + " Units: " + value.unit + "</p>";
			<?php if($formType == "session"){ ?>
                        html += "<br><i>Slider stored in " + value.storage + "</i>";
			<?php } else { ?>
			html += "<br><i>Slider</i>";
			<?php } ?>
                        break;
                case 'radio':
                case 'checkbox':
                        html += "<h2>" + value.question;
                        if(value.required == 1){
                                html += " <span class='required-pf'>*</span>(Required)";
                        }
                        html += "</h2>";
                        if(value.helpText != ""){
                                html += "<small>" + value.helpText + "</small>";
                        }
			<?php if($formType == "session"){ ?>
			if(value.type == "radio"){
				html += "<br><i>Radio button selection stored in " + value.type + "</i>";
			}else{
	                        html += "<br><i>Checkbox selection stored in " + value.type + "</i>";
			}
			<?php } else { ?>
			if(value.type == "radio"){
                                html += "<br><i>Radio button selection</i>";
                        }else{
                                html += "<br><i>Checkbox selection</i>";
                        }
			<?php } ?>
                        break;
        }
	return html;
    }

    function updateRow(value){
	var html = getHtmlRow(value);
	$("#question"+value.id+" .list-view-pf-body").html(html);
	$("#newQuestion").modal('hide');
    }

    function createRow(value){
	html = "<div class='list-group-item' id='question" + value.id + "'>"+
		"<span class='pf-grab ui-sortable-handle'></span>" +
		"<div class='list-view-pf-actions'>"+
		"<button class='btn btn-default openModal' action='edit' type='" + (value.type != 'description'?'question':'title') + "' key='" + value.id + "'>Edit</button>"+
		"<button class='btn btn-danger delete' key='" + value.id +"'>Delete</button>"+ 
		"</div><div class='list-view-pf-main-info'><div class='list-view-pf-body' style='display: block'>";
	html += getHtmlRow(value);
	html += "</div></div>";

	$("#questionContainer").append(html);
	$("#questionContainer").removeClass("hidden");
	$("#noQuestion").addClass("hidden");
	$("#newQuestion").modal('hide');
	init();		
    }

    function updateOrder(order){
	sendUpdate({'order': order}, "order");
    }

    function sendUpdate(updateData, action) {
	var beforeUpdate = updateData;
	updateData['token'] = token;
	updateData['formId'] = formId;
	updateData['action'] = action;
        var xhr = new XMLHttpRequest();
        xhr.open('POST', '/formupdate/');
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.onload = function() {
            try{
                var data = JSON.parse(xhr.responseText);
            } catch (e) {
                showToast(xhr.responseText);
                return;
            }
            if(data.result != 'success'){
                showToast(data.reason);
            }else{
		if(data.action == "add"){
			if(beforeUpdate['type'] == 'description'){
				beforeUpdate = changeMapping(beforeUpdate);
			}
			beforeUpdate['as-title'] = beforeUpdate['asTitle'];
			beforeUpdate['as-description'] = beforeUpdate['asDescription'];
			beforeUpdate['as-duration'] = beforeUpdate['asDuration'];
			database[beforeUpdate['id']]=beforeUpdate;
			newKey++;
			<?php if($formType == "session") { ?>
			if(beforeUpdate['type'] != 'description' && !beforeUpdate['storage'].includes("self") && beforeUpdate['storage'] != "topics"){
				options[beforeUpdate['storage']]['used'] = true;
			}
			<?php } ?>
		        createRow(beforeUpdate);
		}else if(data.action == "delete"){
			<?php if($formType == "session") { ?>
			if(database[data.id]['type'] != 'description' && !database[data.id]['storage'].includes("self") && database[data.id]['storage'] != "topics"){
				options[database[data.id]['storage']]['used'] = false;
			}
			<?php } ?>
			delete database[data.id];
		        $("#question" + data.id).remove();
		        if(Object.keys(database).length == 0){
		                $("#questionContainer").addClass("hidden");
		                $("#noQuestion").removeClass("hidden");
		        }
		}else if(data.action == "update"){
			if(beforeUpdate['type'] == 'description'){
				beforeUpdate = changeMapping(beforeUpdate);
			}
			database[beforeUpdate['id']]=beforeUpdate;
			beforeUpdate['as-title'] = beforeUpdate['asTitle'];
			beforeUpdate['as-description'] = beforeUpdate['asDescription'];
			beforeUpdate['as-duration'] = beforeUpdate['asDuration'];
			<?php if($formType == "session") { ?>
			if(beforeUpdate['type'] != 'description' && !beforeUpdate['storage'].includes("self") && beforeUpdate['storage'] != "topics"){
				options[beforeUpdate['storage']]['used'] = true;
			}
			<?php } ?>
			updateRow(beforeUpdate);
		}else if(data.action == "order"){

		}else{
			showToast("Unsupported operation. Please, reload this page.");
		}
            }
        };
	console.log(updateData);
        xhr.send(jQuery.param(updateData));
    }

    function changeMapping(value){
	return { "id": value.id, "question": value.title, "helpText": value.description, "type": value.type }
    }

    function restoreOptions(){
	var key = $("#newQuestion").attr("key");
	if(key > 0){
		if($("#newQuestion").attr("type") == "question"){
			var storage = database[key]['storage'];
			if(storage in options && storage != 'self'){
				options[storage]['used'] = true;
			}
		}
	}
    }

    init(); 
  });
</script>
</body>
</html>  
