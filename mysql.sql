-- MySQL dump 10.13  Distrib 5.7.21, for Linux (x86_64)
--
-- Host: localhost    Database: cfp
-- ------------------------------------------------------
-- Server version       5.7.21

DROP DATABASE IF EXISTS `cfp`;

CREATE DATABASE `cfp` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;

USE `cfp`;

DROP TABLE IF EXISTS `accepts`;
CREATE TABLE `accepts` (
  `event_id` int(11) DEFAULT NULL,
  `vote` int(11) DEFAULT NULL,
  `manager_id` varchar(50) DEFAULT NULL,
  `response_to_topic_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

DROP TABLE IF EXISTS `comments`;
CREATE TABLE `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `author` varchar(200) DEFAULT NULL,
  `comment` text DEFAULT NULL,
  `response_id` int(11) DEFAULT NULL,
  `time` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

DROP TABLE IF EXISTS `events`;
CREATE TABLE `events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `logo` varchar(1024) CHARACTER SET utf8 DEFAULT NULL,
  `cfp_open` date DEFAULT NULL,
  `cfp_close` date DEFAULT NULL,
  `year` int(4) DEFAULT NULL,
  `url` varchar(1024) CHARACTER SET utf8 DEFAULT NULL,
  `redhat_event` tinyint(1) DEFAULT 0,
  `for_redhat` tinyint(1) DEFAULT 0,
  `blind_review` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

LOCK TABLES `events` WRITE;
INSERT INTO `events` VALUES (1,'DevConf','/webroot/img/logos/devconf.png','2018-10-14','2018-10-20',2019,'https://devconf.info/cz','','\0');
UNLOCK TABLES;

DROP TABLE IF EXISTS `managers`;
CREATE TABLE `managers` (
  `user_id` varchar(50) COLLATE utf8_general_ci DEFAULT NULL,
  `event_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

DROP TABLE IF EXISTS `responses`;
CREATE TABLE `responses` (
  `title` varchar(50) COLLATE utf8_general_ci DEFAULT NULL,
  `type` varchar(30) COLLATE utf8_general_ci DEFAULT NULL,
  `duration` varchar(10) COLLATE utf8_general_ci DEFAULT NULL,
  `difficulty` varchar(100) COLLATE utf8_general_ci DEFAULT NULL,
  `abstract` text COLLATE utf8_general_ci,
  `summary` varchar(100) COLLATE utf8_general_ci DEFAULT NULL,
  `notes` varchar(1024) COLLATE utf8_general_ci DEFAULT NULL,
  `timestamp` datetime DEFAULT NULL,
  `event_id` int(11) DEFAULT NULL,
  `user_id` varchar(100) COLLATE utf8_general_ci DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `spreadsheet_id` varchar(100) COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

DROP TABLE IF EXISTS `responses_to_topics`;
CREATE TABLE `responses_to_topics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `response_id` int(11) DEFAULT NULL,
  `topic_id` int(11) DEFAULT NULL,
  `event_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

DROP TABLE IF EXISTS `reviewers`;
CREATE TABLE `reviewers` (
  `user_id` varchar(50) COLLATE utf8_general_ci DEFAULT NULL,
  `event_id` int(11) DEFAULT NULL,
  `topics` varchar(1024) COLLATE utf8_general_ci DEFAULT NULL,
  `capitan` tinyint(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

DROP TABLE IF EXISTS `topics`;
CREATE TABLE `topics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;


LOCK TABLES `topics` WRITE;
INSERT INTO `topics` VALUES (1,'I don\'t know / None of these'),(2,'Academic / Research'),(3,'Agile, DevOps & CI/CD'),(4,'Blockchain'),(5,'Cloud and Containers'),(6,'Community'),(7,'Immutable OS'),(8,'Debug / Tracing'),(9,'Desktop'),(10,'Developer Tools'),(11,'Documentation'),(12,'Fedora'),(13,'Frontend / UI / UX'),(14,'IoT'),(15,'Kernel'),(16,'Microservices'),(17,'Middleware'),(18,'ML / AI / Big Data'),(19,'Networking'),(20,'Platform / OS'),(21,'Quality / Testing'),(22,'Security / IdM'),(23,'Storage / Ceph / Gluster'),(24,'Virtualization');
UNLOCK TABLES;

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` varchar(50) CHARACTER SET utf8 NOT NULL,
  `first_name` varchar(512) CHARACTER SET utf8 DEFAULT NULL,
  `last_name` varchar(512) CHARACTER SET utf8 DEFAULT NULL,
  `email` varchar(512) CHARACTER SET utf8 DEFAULT NULL,
  `bio` text CHARACTER SET utf8,
  `organization` varchar(512) CHARACTER SET utf8 DEFAULT NULL,
  `country` varchar(2) CHARACTER SET utf8 DEFAULT NULL,
  `avatar` varchar(1024) COLLATE utf8_general_ci DEFAULT '/webroot/img/avatars/noperson.png',
  `admin` tinyint(1) DEFAULT 0,
  `reviewer` tinyint(1) DEFAULT 0,
  `program_manager` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

DROP TABLE IF EXISTS `votes`;
CREATE TABLE `votes` (
  `event_id` int(11) DEFAULT NULL,
  `vote` int(11) DEFAULT NULL,
  `reviewer_id` varchar(50) COLLATE utf8_general_ci DEFAULT NULL,
  `response_to_topic_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

CREATE USER 'web'@'%';
GRANT ALL ON cfp.* TO 'web'@'%';



-- Dump completed on 2018-10-24 11:53:28
