<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

use Cake\Core\Plugin;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;

/**
 * The default class to use for all routes
 *
 * The following route classes are supplied with CakePHP and are appropriate
 * to set as the default:
 *
 * - Route
 * - InflectedRoute
 * - DashedRoute
 *
 * If no call is made to `Router::defaultRouteClass()`, the class used is
 * `Route` (`Cake\Routing\Route\Route`)
 *
 * Note that `Route` does not do any inflections on URLs which will result in
 * inconsistently cased URLs when used with `:plugin`, `:controller` and
 * `:action` markers.
 *
 */
Router::defaultRouteClass(DashedRoute::class);

Router::scope('/', function (RouteBuilder $routes) {
    /**
     * Here, we are connecting '/' (base path) to a controller called 'Pages',
     * its action called 'display', and we pass a param to select the view file
     * to use (in this case, src/Template/Pages/home.ctp)...
     */
    $routes->connect('/', ['controller' => 'HomePage', 'action' => 'display', 'home']);
    /**
     * ...and connect the rest of 'Pages' controller's URLs.
     */

    $routes->connect('/auth/google/*', ['controller' => 'Auth', 'action' => 'google']);
    $routes->connect('/auth/github/*', ['controller' => 'Auth', 'action' => 'github']);
    $routes->connect('/auth/fas/*', ['controller' => 'Auth', 'action' => 'fas']);
    $routes->connect('/logout/*', ['controller' => 'Auth', 'action' => 'logout']);
    $routes->connect('/login/*', ['controller' => 'Auth', 'action' => 'login']);

    $routes->connect('/profile/*', ['controller' => 'Profile', 'action' => 'show']);
    $routes->connect('/admin/*', ['controller' => 'Admin', 'action' => 'show']);		
    $routes->connect('/event/*', ['controller' => 'Event', 'action' => 'show']);		
    $routes->connect('/administrators/*', ['controller' => 'Administrators', 'action' => 'show']);		
    $routes->connect('/managers/*', ['controller' => 'Managers', 'action' => 'show']);
    $routes->connect('/manager/*', ['controller' => 'Manager', 'action' => 'show']);
    $routes->connect('/manage/*', ['controller' => 'Manage', 'action' => 'show']);
    $routes->connect('/managebooking/*', ['controller' => 'ManageBooking', 'action' => 'show']);
    $routes->connect('/topicfix/*', ['controller' => 'TopicFix', 'action' => 'show']);
    $routes->connect('/reviewers/*', ['controller' => 'Reviewers', 'action' => 'show']);
    $routes->connect('/speakers/*', ['controller' => 'Speakers', 'action' => 'show']); 
    $routes->connect('/capreviewers/*', ['controller' => 'CaptainReviewers', 'action' => 'show']);
    $routes->connect('/reviewstate/*', ['controller' => 'ReviewState', 'action' => 'show']);
    $routes->connect('/reviewer/*', ['controller' => 'Reviewer', 'action' => 'show']);
    $routes->connect('/capreview/*', ['controller' => 'CaptainReview', 'action' => 'show']);
    $routes->connect('/review/*', ['controller' => 'Review', 'action' => 'show']);
    $routes->connect('/vote/*', ['controller' => 'Vote', 'action' => 'add']);
    $routes->connect('/topicupdate/*', ['controller' => 'TopicUpdate', 'action' => 'add']);
    $routes->connect('/slots/*', ['controller' => 'Slots', 'action' => 'show']);
    $routes->connect('/comment/*', ['controller' => 'Comment', 'action' => 'add']);
    $routes->connect('/accept/*', ['controller' => 'Accept', 'action' => 'add']);
    $routes->connect('/emailpi/*', ['controller' => 'EmailApi', 'action' => 'add']);

    $routes->connect("/myproposals/*", ['controller' => 'MyProposals', 'action'=>'show']);
    $routes->connect("/proposals/*", ['controller' => 'Proposals', 'action'=>'show']);
    $routes->connect("/answers/*", ['controller' => 'Answers', 'action'=>'show']);
    $routes->connect("/answer/*", ['controller' => 'Answer', 'action'=>'show']);
    $routes->connect("/proposal/*", ['controller' => 'Proposal', 'action'=>'show']);
    $routes->connect("/form/*", ['controller' => 'Form', 'action'=>'show']);
    $routes->connect("/forms/*", ['controller' => 'Forms', 'action'=>'show']);
    $routes->connect("/formupdate/*", ['controller' => 'FormUpdate', 'action' => 'manage']);
    $routes->connect("/emails/*", ['controller' => 'Emails', 'action'=>'show']);
    $routes->connect("/faq/*", ['controller' => 'Faq', 'action'=>'show']);
    $routes->connect("/smtp/*", ['controller' => 'Smtp', 'action' => 'show']);
    $routes->connect("/topics/*", ['controller' => 'Topics', 'action' => 'show']);
    $routes->connect("/signins/*", ['controller' => 'Signin', 'action' => 'show']);
    $routes->connect("/domains/*", ['controller' => 'Domain', 'action' => 'show']);
    $routes->connect("/submit/*", ['controller' => 'Submit', 'action' => 'show']);
    $routes->connect("/additional/*", ['controller' => 'Additional', 'action' => 'show']);
    $routes->connect("/confirm/*", ['controller' => 'Proposal', 'action' => 'confirmation']);
    $routes->connect("/acceptproposals/*", ['controller' => 'AcceptProposals', 'action' => 'show']);
    $routes->connect("/acceptedproposals/*", ['controller' => 'AcceptProposals', 'action' => 'confirmed']);
    $routes->connect("/confirmproposals/*", ['controller' => 'ConfirmProposals', 'action' => 'show']);
    $routes->connect("/codes/*", ['controller' => 'Codes', 'action' => 'show']);
    $routes->connect("/collect/*", ['controller' => 'Collect', 'action' => 'show']);
    $routes->connect("/hotel/*", ['controller' => 'Hotel', 'action' => 'show']);
    $routes->connect("/hotels/*", ['controller' => 'Hotels', 'action' => 'show']);
	$routes->connect("/hotelroom/*", ['controller' => 'HotelRoom', 'action' => 'show']);
    $routes->connect("/hotelrooms/*", ['controller' => 'HotelRooms', 'action' => 'show']);
    $routes->connect("/booking/*", ['controller' => 'HotelBooking', 'action' => 'show']);
    $routes->connect("/sendreservation/*", ['controller' => 'SendReservation', 'action' => 'show']);

    $routes->connect("/rooms/*", ['controller' => 'Rooms', 'action' => 'show']);
    $routes->connect("/room/*", ['controller' => 'Room', 'action' => 'show']);
    $routes->connect("/scheduler/*", ['controller' => 'Scheduler', 'action' => 'show']);
    $routes->connect("/schedule/*", ['controller' => 'Schedule', 'action' => 'show']); 

   /**
     * Connect catchall routes for all controllers.
     *
     * Using the argument `DashedRoute`, the `fallbacks` method is a shortcut for
     *    `$routes->connect('/:controller', ['action' => 'index'], ['routeClass' => 'DashedRoute']);`
     *    `$routes->connect('/:controller/:action/*', [], ['routeClass' => 'DashedRoute']);`
     *
     * Any route class can be used with this method, such as:
     * - DashedRoute
     * - InflectedRoute
     * - Route
     * - Or your own route class
     *
     * You can remove these routes once you've connected the
     * routes you want in your application.
     */
    $routes->fallbacks(DashedRoute::class);
});

/**
 * Load all plugin routes. See the Plugin documentation on
 * how to customize the loading of plugin routes.
 */
Plugin::routes();
